package rx

/* An abstraction over the opengl framebuffer object (FBO). */

import (
	"fmt"

	"github.com/go-gl/gl/v2.1/gl"
)

type Framebuffer struct {
	width     int
	height    int
	modeDepth bool     // is a depth frame buffer
	onlyColor bool     // always false
	tex       *Texture // gl texture

	// gl
	fbo       uint32
	rbo_depth uint32 // depth render buffer object
}

func NewFramebuffer(width, height int, modeDepth bool) *Framebuffer {
	f := &Framebuffer{
		tex:       nil,
		width:     width,
		height:    height,
		modeDepth: modeDepth,
		onlyColor: false,
	}
	f.create()
	return f
}

func (f *Framebuffer) create() {
	f.tex = CreateTexture(f.width, f.height, f.modeDepth)
	glcheck()

	// tune the texture
	if f.modeDepth {
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	} else {
		// need for fxaa
		//gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	}

	// create FBO
	gl.GenFramebuffers(1, &f.fbo)
	//rt.fbo.Bind()
	gl.BindFramebuffer(gl.FRAMEBUFFER, f.fbo)
	glcheck()

	if f.onlyColor {
		gl.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0,
			gl.TEXTURE_2D, f.tex.Tex(), 0)
		glcheck()
	} else {
		// depth buffer
		gl.GenRenderbuffers(1, &f.rbo_depth)
		//rbo_depth.Bind()
		gl.BindRenderbuffer(gl.RENDERBUFFER, f.rbo_depth)
		glcheck()
		gl.RenderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT24,
			int32(f.width), int32(f.height))
		glcheck()
		gl.FramebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, f.rbo_depth)
		glcheck()

		// attach the texture
		if !f.modeDepth {
			gl.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0,
				gl.TEXTURE_2D, f.tex.Tex(), 0)
		} else {
			gl.DrawBuffer(gl.NONE)
			gl.ReadBuffer(gl.NONE)
			gl.FramebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT,
				gl.TEXTURE_2D, f.tex.Tex(), 0)
		}
	}
	glcheck()

	// check status
	status := gl.CheckFramebufferStatus(gl.FRAMEBUFFER)
	if status != gl.FRAMEBUFFER_COMPLETE {
		panic("Bad FBO, status: " + fmt.Sprintf("%d", status))
	}
	glcheck()

	// unbind
	//rt.fbo.Unbind()
	//rbo_depth.Unbind()
	gl.BindFramebuffer(gl.FRAMEBUFFER, 0)
	glcheck()
}

func (f *Framebuffer) Bind() {
	gl.BindFramebuffer(gl.FRAMEBUFFER, f.fbo)
}

func (f *Framebuffer) Unbind() {
	gl.BindFramebuffer(gl.FRAMEBUFFER, 0)
}
