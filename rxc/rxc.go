package main

/*
#include <stdlib.h>
#include <stdbool.h>
*/
import (
	"C"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"
)

var rxi *rx.Rx
var win *rx.Window
var app *rx.App

//export rx_init
func rx_init(win_w, win_h C.int) {
	rxi = rx.NewRx() 
	rxi.Init(int(win_w), int(win_h))
	
	app = rxi.App
	win = app.Win()
}

//export rx_step
func rx_step() C.uint {
	r := app.Step()
	if r != true {
		return 0
	}
	return 1
}

//export rx_app_getdt
func rx_app_getdt() C.double {
	return C.double(app.GetDt())
}

//export rx_testfw_create_default_scene
func rx_testfw_create_default_scene() {
	rx.TestFwCreateDefaultScene()
}

//export rx_sceneloader_loadspawn
func rx_sceneloader_loadspawn(path *C.char) {
	sl := rx.NewSceneLoader()
	sl.Load(C.GoString(path))
	sl.Spawn()
}

//export rx_scene_get_node
func rx_scene_get_node(name *C.char) C.int {
	return C.int(rxi.Scene().GetNode(C.GoString(name)).Id())
}

//export rx_node_set_rot
func rx_node_set_rot(node_id C.int, x, y, z C.double) {
	node := rxi.Scene().GetNodeById(int(node_id))
	if node == nil {
		panic("node not found")
	}
	node.SetRot(Vec3{float64(x), float64(y), float64(z)})
}

func main() {}