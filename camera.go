package rx

import (
  . "bitbucket.org/coaljoe/rx/math"
  //"bitbucket.org/coaljoe/rx/transform"
)

// CameraNode.
type Camera struct {
	node                     *Node
	fov, aspect, znear, zfar float64
	zoom                     float64
	ortho                    bool
	orthoPlanes              []float64
	//target                   Vec3
	rt *RenderTarget
	// Culling AABB, currently only 2d
	cullAABB AABB
}

func (c *Camera) Fov() float64   { return c.fov }
func (c *Camera) Zfar() float64  { return c.zfar }
func (c *Camera) Znear() float64 { return c.znear }
func (c *Camera) Zoom() float64  { return c.zoom }

func NewCamera(node *Node) *Camera {
	return &Camera{
		node:        node,
		fov:         45,
		aspect:      1.0,
		znear:       0.1,
		zfar:        1000,
		zoom:        1.0,
		ortho:       false,
		orthoPlanes: make([]float64, 4),
		//target:      Vec3Zero,
		rt: nil,
		//cullAABB: AABB{Vec3{-1000, -1000, -1000}, Vec3{1000.0, 1000, 1000}},
		cullAABB: NewAABB(),
		//cullAABB: AABB{Vec3{-10, -10, -10}, Vec3{10, 10, 10}},
		//cullAABB: AABB{Vec3{-20, -20, -20}, Vec3{20, 20, 20}},
		//cullAABB: AABB{Vec3{-30, -30, -30}, Vec3{30, 30, 30}},
		//cullAABB: AABB{Vec3{-100, -100, -100}, Vec3{100, 100, 100}},
		//cullAABB: AABB{Vec3{-170, -150, -150}, Vec3{170, 150, 150}},
		//cullAABB: AABB{Vec3{-100, -100, -100}, Vec3{300, 300, 300}},
	}
}

func (c *Camera) SetupView(fov, aspect, znear, zfar float64, ortho bool) {
	c.fov = fov
	c.aspect = aspect
	c.znear = znear
	c.zfar = zfar
	c.ortho = ortho

	// reset scale
	//c.SetZoom(1)
	c.build()
}

func (c *Camera) build() {
	// extra checks
	if Debug {
		if !(c.Zfar() > c.Znear()) {
			panic("projection is invalid")
		}
	}
	if c.ortho {
		ortho_scale := c.fov
		aspect_ratio := c.aspect
		xaspect := aspect_ratio
		yaspect := 1.0
		xaspect = xaspect * ortho_scale / (aspect_ratio * 2)
		yaspect = yaspect * ortho_scale / (aspect_ratio * 2)
		c.orthoPlanes = []float64{-xaspect * c.zoom, xaspect * c.zoom,
			-yaspect * c.zoom, yaspect * c.zoom}
	}
}

func (c *Camera) SetZoom(newzoom float64) {
	if newzoom <= 0 {
		println("camera: err, zoom cannot be <= 0")
		return
	}
	c.zoom = newzoom
	c.build()
}

func (c *Camera) AdjZoom(v float64) {
	c.SetZoom(c.Zoom() + v)
}

/*
// Deprecated.
func (c *Camera) Target() Vec3 {
	return c.target
}

// Deprecated.
func (c *Camera) SetTarget(v Vec3) {
	c.target = v
}
*/

func (c *Camera) AddRenderTarget(name string, width, height int, depth bool) {
	c.rt = NewRenderTarget(name, width, height, depth)
}

func (c *Camera) HasRenderTarget() bool {
	return c.HasRT()
}

func (c *Camera) HasRT() bool {
	return c.rt != nil
}

// Is active camera. remove?
func (c *Camera) IsDefault() bool {
	return c == rxi.Camera.Camera
}

func (c *Camera) IsOrtho() bool {
	return c.ortho
}

func (c *Camera) SetZnear(v float64) {
	c.znear = v
	c.build()
}

func (c *Camera) SetZfar(v float64) {
	c.zfar = v
	c.build()
}

func (c *Camera) SetFov(v float64) {
	c.fov = v
	c.build()
}

// Get camera matrix.
func (c *Camera) GetViewMatrix() Mat4 {
	// create inverse matrix (view matrix) (works fine)
	// TODO: add cache?
	viewM := c.node.Mat().Invert()
	return viewM
}

// Get camera projection matrix.
func (c *Camera) GetProjectionMatrix() Mat4 {
	// same as glOrtho; from nim glm
	// TODO: add cache?
	left := c.orthoPlanes[0]
	right := c.orthoPlanes[1]
	bottom := c.orthoPlanes[2]
	top := c.orthoPlanes[3]
	zNear, zFar := c.znear, c.zfar
	/*
		m := Mat4Identity
		m[0][0] = 2.0 / (right - left)
		m[1][1] = 2.0 / (top - bottom)
		m[2][2] = -2.0 / (zFar - zNear)
		m[3][0] = -(right + left) / (right - left)
		m[3][1] = -(top + bottom) / (top - bottom)
		m[3][2] = -(zFar + zNear) / (zFar - zNear)
	*/
	// Use rx/math ortho version
	m := Ortho(left, right, bottom, top, zNear, zFar)
	return m
}

var _ = `

// Cast a ray from camera position to camera direction.
func (c *Camera) CreateRay__() Vec3 {
	// From this:
	// stackoverflow.com/questions/29997209/opengl-c-mouse-ray-picking-glmunproject
	// away3d.com/wikicontent/tutorials/picking/overview/images/figure_01.jpg

	// These positions must be in range [-1, 1], not [0, width] and [0, height]
	// NDC coords?
	mx := (float64(_InputSys.Mouse.X)/float64(rxi.Renderer().Width()) - 0.5) * 2.0
	//my := (float64(_InputSys.Mouse.Y)/float64(rxi.Renderer().Height()) - 0.5) * 2.0
	my := 1.0 - (float64(_InputSys.Mouse.Y)*2.0)/float64(rxi.Renderer().Height())

	p(_InputSys.Mouse.X, _InputSys.Mouse.Y)
	p(mx, my)

	proj := c.GetProjectionMatrix()
	//view := Mat4.LookAt(Vec3Zero, c.Direction(), c.UpVector())
	view := c.GetViewMatrix()

	//glm::mat4 invVP = glm::inverse(proj * view);
	//invVP := c.GetViewMatrix().Invert()
	//invVP := c.GetViewMatrix()
	invVP := proj.Mul(view).Invert()
	screenPos := Vec4{mx, -my, 1.0, 1.0}
	worldPos := invVP.MulVec4(screenPos).ToVec3()

	dir := worldPos.Norm()
	p("dir:", dir)

	return dir
}

// Cast a ray from camera position to camera direction.
func (c *Camera) CreateRay_() Vec3 {
	// From this:
	// stackoverflow.com/questions/29997209/opengl-c-mouse-ray-picking-glmunproject
	// away3d.com/wikicontent/tutorials/picking/overview/images/figure_01.jpg

	// These positions must be in range [-1, 1], not [0, width] and [0, height]
	// NDC coords?
	mx := (float64(_InputSys.Mouse.X)/float64(rxi.Renderer().Width()) - 0.5) * 2.0
	my := (float64(_InputSys.Mouse.Y)/float64(rxi.Renderer().Height()) - 0.5) * 2.0

	p(mx, my)

	rayStart_ndc := Vec4{mx, my, -1.0, 1.0}
	rayEnd_ndc := Vec4{mx, my, 0.0, 1.0}

	projM := c.GetProjectionMatrix()
	viewM := c.GetViewMatrix()

	M := projM.Mul(viewM).Invert()

	rayStart_world := M.MulVec4(rayStart_ndc)
	rayEnd_world := M.MulVec4(rayEnd_ndc)
	dir_world := rayEnd_world.Sub(rayStart_world).ToVec3().Norm()
	p("dir_world:", dir_world)

	return dir_world
}

// Cast a ray from camera position to camera direction.
func (c *Camera) CreateRay___() Vec3 {
	// From this:
	// stackoverflow.com/questions/29997209/opengl-c-mouse-ray-picking-glmunproject
	// away3d.com/wikicontent/tutorials/picking/overview/images/figure_01.jpg

	// These positions must be in range [-1, 1], not [0, width] and [0, height]
	// NDC coords?
	mx := 2.0*float64(_InputSys.Mouse.X)/float64(rxi.Renderer().Width()) - 1.0
	//my := (float64(_InputSys.Mouse.Y)/float64(rxi.Renderer().Height()) - 0.5) * 2.0
	my := 1.0 - 2.0*float64(_InputSys.Mouse.Y)/float64(rxi.Renderer().Height())

	p(_InputSys.Mouse.X, _InputSys.Mouse.Y)
	p(mx, my)

	proj := c.GetProjectionMatrix()
	//view := Mat4.LookAt(Vec3Zero, c.Direction(), c.UpVector())
	view := c.GetViewMatrix()

	//glm::mat4 invVP = glm::inverse(proj * view);
	//invVP := c.GetViewMatrix().Invert()
	//invVP := c.GetViewMatrix()
	invVP := (proj.Mul(view)).Invert()
	//screenPos := Vec4{mx, -my, 1.0, 1.0}
	//worldPos := invVP.MulVec4(screenPos).ToVec3()
	near_point := invVP.MulVec4(Vec4{mx, my, 0, 1}).ToVec3()
	camera_pos := c.node.WorldPos()
	dir := near_point.Sub(camera_pos)

	p("dir no norm:", dir)
	//dir = dir.Norm()
	p("dir:", dir)

	return dir
}
`

// threejs version
func (c *Camera) CreateRay_() Ray {

	/*
		this.ray.origin.set( coords.x, coords.y, ( camera.near + camera.far ) / ( camera.near - camera.far ) ).unproject( camera ); // set origin in plane of camera
		this.ray.direction.set( 0, 0, - 1 ).transformDirection( camera.matrixWorld );
	*/
	// NDC coords
	x := (2.0*float64(_InputSys.Mouse.X))/float64(rxi.Renderer().Width()) - 1.0
	y := 1.0 - (2.0*float64(_InputSys.Mouse.Y))/float64(rxi.Renderer().Height())
	z := 1.0

	deviceCoords := Vec3{x, y, z}
	_ = deviceCoords
	p("deviceCoords:", deviceCoords)

	var r Ray
	ori := Vec3{deviceCoords.X(), deviceCoords.Y(), (c.Znear() + c.Zfar()) / (c.Znear() - c.Zfar())}
	// unproject
	projM := c.GetProjectionMatrix()
	worldM := c.node.WorldMat()
	ori4 := ori.ToVec4()
	ori4 = ori4.MulMat4(projM.Invert().Mul(worldM))
	r.Origin = ori4.ToVec3()

	dir := Vec3{0, 0, -1}
	dir4 := dir.ToVec4()
	dir4 = dir4.MulMat4(worldM).Norm()
	r.Direction = dir4.ToVec3()

	p("r:", r)

	return r
}

// Cast a ray from camera position to camera direction.
// raylib version
func (c *Camera) CreateRay() Ray {
	// From this:
	// stackoverflow.com/questions/29997209/opengl-c-mouse-ray-picking-glmunproject
	// away3d.com/wikicontent/tutorials/picking/overview/images/figure_01.jpg

	if !c.IsOrtho() {
		panic("only ortho cameras supported")
	}

	_ = `
	// These positions must be in range [-1, 1], not [0, width] and [0, height]
	// NDC coords?
	mx := 2.0*float64(_InputSys.Mouse.X)/float64(rxi.Renderer().Width()) - 1.0
	//my := (float64(_InputSys.Mouse.Y)/float64(rxi.Renderer().Height()) - 0.5) * 2.0
	my := 1.0 - 2.0*float64(_InputSys.Mouse.Y)/float64(rxi.Renderer().Height())

	p(_InputSys.Mouse.X, _InputSys.Mouse.Y)
	p(mx, my)

	rayOrigin := Vec3{mx, my, c.Znear() - c.Zfar()}
	_ = rayOrigin

	proj := c.GetProjectionMatrix()
	//view := Mat4.LookAt(Vec3Zero, c.Direction(), c.UpVector())
	view := c.GetViewMatrix()

	//glm::mat4 invVP = glm::inverse(proj * view);
	//invVP := c.GetViewMatrix().Invert()
	//invVP := c.GetViewMatrix()
	invVP := (proj.Mul(view)).Invert()
	//screenPos := Vec4{mx, -my, 1.0, 1.0}
	//worldPos := invVP.MulVec4(screenPos).ToVec3()
	near_point := invVP.MulVec4(Vec4{mx, my, 0, 1}).ToVec3()
	/*
	/*
	/*
	camera_pos := c.node.WorldPos()
	dir := near_point.Sub(camera_pos)

	p("dir no norm:", dir)
	//dir = dir.Norm()
	p("dir:", dir)
	`
	// From here:
	// https://github.com/urho3d/Urho3D/blob/ee054a1507cb3518c57d4ebc43cfd6dc93de9a27/
	// Source/Urho3D/Graphics/Camera.cpp

	p("mouse coords:", _InputSys.Mouse.X, _InputSys.Mouse.Y)

	proj := c.GetProjectionMatrix()
	view := c.GetViewMatrix()
	viewProjInverse := (proj.Mul(view)).Invert()

	//x := float64(_InputSys.Mouse.X)
	//y := float64(_InputSys.Mouse.Y)

	// NDC coords
	x := (2.0*float64(_InputSys.Mouse.X))/float64(rxi.Renderer().Width()) - 1.0
	y := 1.0 - (2.0*float64(_InputSys.Mouse.Y))/float64(rxi.Renderer().Height())
	z := 1.0

	deviceCoords := Vec3{x, y, z}
	_ = deviceCoords
	p("deviceCoords:", deviceCoords)

	//x = 2.0*x - 1.0
	//y = 1.0 - 2.0*y
	//near := Vec3{x, y, 0.0}.ToVec4()
	//far := Vec3{x, y, 1.0}.ToVec4()
	var _ = `
	// XXX seems work better with these settings, but the origin is shifted by z=1
	near := Vec3{x, y, -1.0}
	far := Vec3{x, y, 0.0}
	`
	//near := Vec3{x, y, 0.0}
	//far := Vec3{x, y, 1.0}
	near := Vec3{x, y, 0.0}
	far := Vec3{x, y, 1.0}
	_ = near
	_ = far
	_ = viewProjInverse

	vpW, vpH := rxi.Renderer().Size()
	//vpH = -vpH

	if true {
		aspect := float64(vpW) / float64(vpH)
		top := c.Fov() / 2.0
		right := top * aspect

		// Calculate projection matrix from orthographic
		proj = Ortho(-right, right, -top, top, 0.01, 1000.0)
		p("-> proj:")
		p(proj)
	}
	p("-> view:")
	p(view)

	//mxf := float64(_InputSys.Mouse.X)
	//myf := float64(_InputSys.Mouse.Y)
	mxf := float64(_InputSys.Mouse.SX) * float64(rxi.Renderer().Width())
	myf := float64(_InputSys.Mouse.SY) * float64(rxi.Renderer().Height())
	//nearPoint := UnProject(Vec3{deviceCoords.X(), deviceCoords.Y(), 0}, view, proj, 0, 0, vpW, vpH)
	//farPoint := UnProject(Vec3{deviceCoords.X(), deviceCoords.Y(), 1}, view, proj, 0, 0, vpW, vpH)
	nearPoint := UnProject(Vec3{mxf, myf, 0}, view, proj, 0, 0, vpW, vpH)
	farPoint := UnProject(Vec3{mxf, myf, 1}, view, proj, 0, 0, vpW, vpH)
	p("nearPoint:", nearPoint)
	p("farPoint:", farPoint)

	ret := Ray{}
	//ret.Origin = (viewProjInverse.MulVec4(near)).ToVec3()
	//ret.Origin = viewProjInverse.MulVec3(near)
	//ret.Origin = UnProject(Vec3{deviceCoords.X(), deviceCoords.Y(), -1.0}, view, proj, 0, 0, vpW, vpH)
	ret.Origin = UnProject(Vec3{mxf, myf, -1.0}, view, proj, 0, 0, vpW, vpH)

	//ret.Direction = (viewProjInverse.MulVec4(far)).ToVec3().Sub(ret.Origin)
	//ret.Origin.SetZ(c.node.WorldPos().Z())
	//ret.Direction = (viewProjInverse.MulVec4(far)).ToVec3().Sub(ret.Origin).Norm()
	//ret.Direction = (viewProjInverse.MulVec3(far)).Sub(ret.Origin).Norm()
	ret.Direction = farPoint.Sub(nearPoint).Norm()

	var _ = `
	// XXX move the origin by z=1 locally (optional, but fixes the wrong origin bug)
	t := transform.NewTransform()
	t.MoveByVec(Vec3{0, 0, 1})
	ret.Origin = t.Mat().MulVec3(ret.Origin)
	//ret.Origin = (t.Mat()).MulVec4(ret.Origin.ToVec4()).ToVec3()
	`

	//p("ret:", ret)
	//pp(2)

	return ret
}

// Cast a ray with a specific lenght.
func (c *Camera) CreateRayWithLenght(rayLenght float64) Ray {
	r := c.CreateRay()
	rayEnd := r.Origin.Add(r.Direction.MulScalar(rayLenght))
	return NewRay(r.Origin, rayEnd)
}

// XXX Not tested.
// Get camera's up-vector of unit lenght.
func (c *Camera) _UpVector() Vec3 {
	t := c.node.Transform.Clone()
	t.MoveByVec(Vec3Up)
	return t.WorldPos()
}

func (c *Camera) SetLocalCullAABB(x AABB) {
	c.cullAABB = x
}

func (c *Camera) GetLocalCullAABB() AABB {
	return c.cullAABB
}

// Get Culling AABB transformed to world space.
func (c *Camera) GetCullAABB() AABB {
	// XXX a copy of mesh.GetAABB
	AABBlocal := c.cullAABB

	// ***** 3d version (doesn't work) ******
	{
		// Calculate world AABB
		//Wmin := c.node.WorldMat().MulVec3(AABBlocal.min)
		//Wmax := c.node.WorldMat().MulVec3(AABBlocal.max)
		//AABBworld := AABB{Wmin, Wmax}
	}

	// ***** 2d version *****

	//Wmin := AABBlocal.min.Add(c.node.Pos())
	//Wmax := AABBlocal.max.Add(c.node.Pos())
	Wmin := c.node.Pos().Add(AABBlocal.min)
	Wmax := c.node.Pos().Add(AABBlocal.max)

	//AABBworld := AABB{Wmin, Wmax}
	AABBworld := AABB{Vec3{Wmin[0], Wmin[1], AABBlocal.min[2]},
		Vec3{Wmax[0], Wmax[1], AABBlocal.max[2]}}

	//p("AABBlocal:", AABBlocal, "AABBWorld:", AABBworld)

	_ = AABBworld
	return AABBworld
	//return AABBlocal
}
