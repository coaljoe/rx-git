package rx

import (
	. "bitbucket.org/coaljoe/rx/math"
	"github.com/qmuntal/gltf"
	//"strings"
	"bitbucket.org/coaljoe/rx/transform"
	"encoding/binary"
	"io/ioutil"
	"math"
	"os"
	"path/filepath"
)

type GltfLoader struct {
	// Transform Y-Up data (standard) to Z-Up
	transformVerticesToZUp bool
	doc                    *gltf.Document
	path                   string
}

func NewGltfLoader() *GltfLoader {
	gl := &GltfLoader{
		transformVerticesToZUp: true,
	}
	return gl
}

func (gl *GltfLoader) Load(path string) bool {
	Log.Inf("%F path:", path)

	doc, err := gltf.Open(path)
	if err != nil {
		panic(err)
	}

	err = doc.Validate()
	if err != nil {
		panic(err)
	}

	p(doc.Asset)
	//pp(2)

	gl.doc = doc
	gl.path = path
	return true
}

func (gl *GltfLoader) MakeMeshNodes(nodeNameFilter string, noCache bool) []*Node {
	Log.Dbg("num nodes:", len(gl.doc.Nodes))

	ret := make([]*Node, 0)

	for _, n := range gl.doc.Nodes {
		p("node:", n)

		// filter nodes by name
		if nodeNameFilter != "" {
			if n.Name != nodeNameFilter {
				Log.Dbg("skip ", n.Name)
				continue
			}
		}
		// XXX FIXME TODO
		/*
			isEmpty := false
			if len(n.Instance_geometry.Url) < 1 {
				isEmpty = true
			}
		*/

		var scale, rot_deg, pos Vec3
		if n.MatrixOrDefault() != gltf.DefaultMatrix {
			//pp(n.Matrix, n.MatrixOrDefault())
			matrix := gl.parseMatrix(n.Matrix)
			p("matrix:", matrix)

			scale = matrix.ExtractScale()
			rot_deg = matrix.ExtractRotation()
			pos = matrix.ExtractTranslation()
		} else {
			// No matrix or default matrix specified in .gltf file?
			scale = Vec3(n.ScaleOrDefault())
			qrot := n.RotationOrDefault()
			roll, pitch, yaw := gl.QuatToEulerAngles(qrot[3], qrot[0], qrot[1], qrot[2])
			//p(roll, pitch, yaw)
			roll_deg, pitch_deg, yaw_deg := Degrees(roll), Degrees(pitch), Degrees(yaw)
			//p(roll_deg, pitch_deg, yaw_deg)
			rot_deg = Vec3{roll_deg, pitch_deg, yaw_deg}
			pos = Vec3(n.TranslationOrDefault())
		}

		//pp(scale, rot_deg, pos)

		isEmpty := false

		mn := NewMeshNode()
		mn.name = n.Name

		mn.SetPos(pos)
		/*
			if isEmpty {
				mn.Mesh.isEmpty = true
			}
		*/

		mn.SetRot(rot_deg)
		mn.SetScale(scale)

		// cache ids
		var cacheId, cachePath string
		//meshName := strings.Replace(n.Instance_geometry.Url, "#", "", 1) // XXX FIXME
		if n.Mesh == nil {
			continue
		}
		//pp(n.Mesh)
		meshId := int(*n.Mesh)
		meshName := gl.doc.Meshes[meshId].Name
		p("meshId:", meshId)
		p("meshName:", meshName)
		if !isEmpty {
			cacheId = gl.path + "#" + meshName
			cachePath = gl.path
		} else {
			cacheId = "_empty_" + n.Name // warning: must be unique
			cachePath = ""
		}

		if m, ok := mn.Mesh.TryLoadFromCache(cacheId); ok && !noCache {
			mn.Mesh = m
		} else {
			// set mesh data
			var mb *MeshBuffer
			if !isEmpty {
				mb = gl.makeMeshBufferForMesh(meshName)
			} else {
				//mb = cl.MakeBoxMeshBuffer()
				pp("not supported")
			}
			mn.Mesh.LoadFromMeshBuffer(mb, cacheId, cachePath)
		}

		var mat *Material
		if !isEmpty {
			mesh := &gl.doc.Meshes[meshId]
			mat = gl.makeMaterialForMesh(mesh)
		} else {
			//mat = gl.makeEmptyMaterial()
			pp("not implemented")
		}
		mn.Mesh.SetMaterial(mat)

		// set mesh name
		mn.Mesh.SetMeshName(n.Name)

		ret = append(ret, mn)
	}

	//pp(2)
	return ret
}

func (gl *GltfLoader) makeMeshBufferForMesh(meshName string) *MeshBuffer {
	var mesh *gltf.Mesh
	for _, m := range gl.doc.Meshes {
		if m.Name == meshName {
			mesh = &m
			break
		}
	}

	if mesh == nil {
		pp("error: no mesh with name:", meshName)
	}

	mb := NewMeshBuffer()

	//pdump(mesh)

	for _, p := range mesh.Primitives {
		if len(p.Targets) > 0 {
			pp("error: targets are not supported")
		}
		//pp(p.Targets)

		// Mesh attributes

		indicesIdx := int(*p.Indices)
		positionsIdx := -1
		normalsIdx := -1
		texcoordsIdx := -1
		if idx, ok := p.Attributes["POSITION"]; ok {
			positionsIdx = int(idx)
		}
		if idx, ok := p.Attributes["NORMAL"]; ok {
			normalsIdx = int(idx)
		}
		if idx, ok := p.Attributes["TEXCOORD_0"]; ok {
			texcoordsIdx = int(idx)
		}

		_ = indicesIdx
		_ = positionsIdx
		_ = normalsIdx
		//pp(indicesIdx, positionsIdx, normalsIdx)

		if positionsIdx == -1 {
			pp("error: no positions in mesh")
		}

		if normalsIdx == -1 {
			pp("error: no normals in mesh")
		}

		// Mesh accessors

		// Indices
		iAcc := gl.doc.Accessors[indicesIdx]
		iBufView := gl.doc.BufferViews[int(*iAcc.BufferView)]
		dump(gl.doc.Buffers)
		dump(iBufView)
		dump(iAcc)

		// commented-out, not always presented
		/*
			if iBufView.Target != gltf.ElementArrayBuffer { // 34936
				pp("error: bad bufferview target:", iBufView.Target)
			}
		*/

		if iAcc.ComponentType != gltf.UnsignedShort {
			pp("error: only componentType=5123 (UNSIGNED_SHORT) indices are supported now (FIXME);",
				"componentType:", iAcc.ComponentType)
		}

		bufData := gl.doc.Buffers[0].Data
		//indices := gl.readBinArrayUint16(bufData, int(iBufView.ByteOffset), int(iAcc.Count))
		//indices := gl.readBinArrayUint16(bufData, int(iBufView.ByteOffset), int(iBufView.ByteLength))
		indices := gl.readBinArrayUint16(bufData,
			int(iAcc.ByteOffset)+int(iBufView.ByteOffset), int(iAcc.Count)) // SCALAR
		__p("indices:", indices)

		// Positions
		pAcc := gl.doc.Accessors[positionsIdx]
		pBufView := gl.doc.BufferViews[int(*pAcc.BufferView)]

		//pdump(pBufView)

		positions := gl.readBinArrayFloat32(bufData,
			int(pAcc.ByteOffset)+int(pBufView.ByteOffset), int(pAcc.Count)*3) // VEC3
		__p("positions:", positions)

		// Normals
		nAcc := gl.doc.Accessors[normalsIdx]
		nBufView := gl.doc.BufferViews[int(*nAcc.BufferView)]

		//pdump(pBufView)

		normals := gl.readBinArrayFloat32(bufData,
			int(nAcc.ByteOffset)+int(nBufView.ByteOffset), int(nAcc.Count)*3) // VEC3
		__p("normals:", normals)

		//pp(iBufView)

		// Texcoords

		texcoords := make([]float32, 0)
		if texcoordsIdx != -1 {
			tAcc := gl.doc.Accessors[texcoordsIdx]
			tBufView := gl.doc.BufferViews[int(*tAcc.BufferView)]

			texcoords = gl.readBinArrayFloat32(bufData,
				int(tAcc.ByteOffset)+int(tBufView.ByteOffset), int(tAcc.Count)*2) // VEC2
			__p("texcoords:", texcoords)
		}

		// Finally

		if gl.transformVerticesToZUp {
			tr := transform.NewTransform()
			tr.SetRot(Vec3{90, 0, 0})
			mat := tr.WorldMat()
			for i := 0; i < len(positions); i += 3 {
				v := Vec3{float64(positions[i]),
					float64(positions[i+1]),
					float64(positions[i+2])}
				newV := mat.MulVec3(v)
				positions[i] = float32(newV[0])
				positions[i+1] = float32(newV[1])
				positions[i+2] = float32(newV[2])
			}

			for i := 0; i < len(normals); i += 3 {
				v := Vec3{float64(normals[i]),
					float64(normals[i+1]),
					float64(normals[i+2])}
				newV := mat.MulVec3(v)
				normals[i] = float32(newV[0])
				normals[i+1] = float32(newV[1])
				normals[i+2] = float32(newV[2])
			}
		}

		indicesConv := make([]uint32, len(indices))
		// []uint16 -> []uint32
		for i, v := range indices {
			indicesConv[i] = uint32(v)
		}
		/*
			if iAcc.ComponentType == 5123 {
				for i, v := range indices {
					indicesConv[i] = uint32(v)
				}
			} else {
				copy(indicesConv, indices)
			}
		*/

		mb.Add(positions, normals, texcoords, indicesConv)
		break // XXX FIXME
	}

	// Add the mesh to the buffer.
	//mb.Add(positions, normals, texcoords, indices)
	//mb.Add(positions, normals, texcoords, []uint32{})

	return mb
}

func (gl *GltfLoader) makeMaterialForMesh(m *gltf.Mesh) *Material {

	matId := int(*m.Primitives[0].Material) // XXX FIXME
	matRec := gl.doc.Materials[matId]
	matName := matRec.Name

	// get materials data
	var emission, ambient, diffuse, specular Vec3
	var shininess float64
	//var diffuse_tex_path string

	rgbaToVec4 := func(v *gltf.RGBA) Vec4 {
		return Vec4{v.R, v.G, v.B, v.A}
	}

	// See also:
	// https://computergraphics.stackexchange.com/questions/1515/
	// what-is-the-accepted-method-of-converting-shininess-to-roughness-and-vice-versa

	diffuse = rgbaToVec4(matRec.PBRMetallicRoughness.BaseColorFactor).ToVec3()
	ambient = Vec3Zero
	emission = Vec3Zero
	specular = Vec3One // XXX fixme? pbr doesn't support colored phong / phong values

	// create material
	var mat *Material
	//mat = NewMaterial(Conf.res_path + "/materials/static.json")
	mat = NewMaterial(Conf.res_path + Conf.DefaultMaterialPath)
	mat.Name = matName
	mat.Emission = emission
	mat.Ambient = ambient
	mat.Diffuse = diffuse
	mat.Specular = specular
	mat.Hardness = shininess

	//Log.Dbg("HARDNESS", float32(shininess))

	if ti := matRec.PBRMetallicRoughness.BaseColorTexture; ti != nil {
		idx := int(ti.Index)
		texSourceIdx := int(*gl.doc.Textures[idx].Source)
		image := gl.doc.Images[texSourceIdx]
		imageUri := image.URI

		if imageUri != "" {

			path := filepath.Dir(gl.path) + "/" + imageUri
			//pp(path)
			Log.Dbg("load texture from path:", path)
			mat.SetTexture(path)

		} else {

			bufViewIdx := int(*image.BufferView)
			bufView := gl.doc.BufferViews[bufViewIdx]
			offset := int(bufView.ByteOffset)
			length := int(bufView.ByteLength)
			data := gl.doc.Buffers[0].Data[offset : offset+length] // XXX FIXME support non-0 buffers?

			mimeType := image.MimeType
			ext := ""
			if mimeType == "image/png" {
				ext = ".png"
			} else if mimeType == "image/jpeg" {
				ext = ".jpg"
			} else {
				pp("unknown mimetype:", mimeType)
			}

			// XXX FIXME will caching work with it?
			// generate random tmp filename?
			path := Conf.tmp_dir + "/" + "rx_gltfloader_tmp_image" + ext
			err := ioutil.WriteFile(path, data, 0644)
			if err != nil {
				panic(err)
			}
			Log.Dbg("load texture from path:", path)
			mat.SetTexture(path)

			os.Remove(path)
		}
	}

	return mat
}

func (gl *GltfLoader) parseMatrix(ms [16]float64) Mat4 {
	/*
	   x, y = 0.5, colunm major

	   1 0 0 0.5 -- stored as row-major?
	   0 1 0 0.5
	   0 0 1 0
	   0 0 0 1

	   flat:
	   1 0 0 0.5 0 1 0 0.5 0 0 1 0 0 0 0 1
	*/

	//ms := parseFloatArray(s)
	m := Mat4Identity

	//Log.Dbg(m.String())

	// fill matrix
	for i := 0; i < 4; i++ {
		for j := 0; j < 4; j++ {
			m[i][j] = float64(ms[i+j*4]) // row- -> col- major trans. not checked
			//m[j][i] = float64(ms[i+j*4]) // row- -> col- major trans. not checked
			//m.Set(i, j, float64(ms[i+j*4]))
		}
	}
	//Log.Dbg("s:\n" + s)
	p("ms:", ms)
	Log.Dbg("m:\n" + m.String())
	Log.Dbg("data:\n%v\n", m.Data())
	//pp("derp")

	return m
}

// componentType 5123 - UNSIGNED_SHORT (2 bytes)
func (gl *GltfLoader) readBinArrayUint16(data []uint8, byteOffset, count int) []uint16 {
	//Log.Dbg("%F data:", data, "offset:", offset, "num:", num)
	Log.Dbg("%F byteOffset:", byteOffset, "count:", count)
	ret := make([]uint16, 0)

	byteSize := 2 // Size in bytes
	//offset := byteOffset / byteSize
	offset := byteOffset

	for i := 0; i < count; i++ {
		j := offset + (i * byteSize)
		k := offset + ((i + 1) * byteSize)
		p("j:", j, "k:", k)
		binV := data[j:k]
		v := binary.LittleEndian.Uint16(binV)
		ret = append(ret, v)
	}

	return ret
}

// componentType 5126 - FLOAT (4 bytes)
func (gl *GltfLoader) readBinArrayFloat32(data []uint8, byteOffset, count int) []float32 {
	//Log.Dbg("%F data:", data, "offset:", offset, "num:", num)
	Log.Dbg("%F byteOffset:", byteOffset, "count:", count)
	ret := make([]float32, 0)

	byteSize := 4 // Size in bytes
	offset := byteOffset

	for i := 0; i < count; i++ {
		j := offset + (i * byteSize)
		k := offset + ((i + 1) * byteSize)
		binV := data[j:k]
		bits := binary.LittleEndian.Uint32(binV)
		v := math.Float32frombits(bits)
		ret = append(ret, v)
	}

	return ret
}

func (gl *GltfLoader) QuatToEulerAngles(qw, qx, qy, qz float64) (roll, pitch, yaw float64) {
	// roll (x-axis rotation)
	sinr_cosp := +2.0 * (qw*qx + qy*qz)
	cosr_cosp := +1.0 - 2.0*(qx*qx+qy*qy)
	roll = math.Atan2(sinr_cosp, cosr_cosp)

	// pitch (y-axis rotation)
	sinp := +2.0 * (qw*qy - qz*qx)
	if math.Abs(sinp) >= 1 {
		pitch = math.Copysign(math.Pi/2, sinp) // use 90 degrees if out of range
	} else {
		pitch = math.Asin(sinp)
	}

	// yaw (z-axis rotation)
	siny_cosp := +2.0 * (qw*qz + qx*qy)
	cosy_cosp := +1.0 - 2.0*(qy*qy+qz*qz)
	yaw = math.Atan2(siny_cosp, cosy_cosp)

	return roll, pitch, yaw
}
