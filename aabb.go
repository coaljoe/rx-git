package rx

import (
	. "bitbucket.org/coaljoe/rx/math"
	"fmt"
	"math"
)

type AABB struct {
	min, max Vec3 // local space
	//scale    float64 // scale aabb by factor
}

func (x AABB) Min() Vec3 { return x.min }
func (x AABB) Max() Vec3 { return x.max }

func NewAABB() AABB {
	//return AABB{}
	return AABB{ // XXX not tested
		Vec3{math.Inf(+1), math.Inf(+1), math.Inf(+1)},
		Vec3{math.Inf(-1), math.Inf(-1), math.Inf(-1)}}
}

func NewAABBFromPoints(min, max Vec3) AABB {
	return AABB{min, max}
}

func (x *AABB) calculateFromMeshBuffer(mb *MeshBuffer) {
	vs := mb.GetVertices()
	x.min = vs[0]
	x.max = vs[0]
	for _, v := range vs {
		/*
			if v.Greater(x.max) {
				p("greater:", v, x.max)
				x.max = v
			} else if v.Less(x.min) {
				p("less:", v, x.min)
				x.min = v
			} else {
				p("unknown", v)
			}
		*/
		if v[0] < x.min[0] {
			x.min[0] = v[0]
		}
		if v[1] < x.min[1] {
			x.min[1] = v[1]
		}
		if v[2] < x.min[2] {
			x.min[2] = v[2]
		}

		if v[0] > x.max[0] {
			x.max[0] = v[0]
		}
		if v[1] > x.max[1] {
			x.max[1] = v[1]
		}
		if v[2] > x.max[2] {
			x.max[2] = v[2]
		}

	}
	//p(vs)
	//pp(x)
}

func (x *AABB) Scale(v float64) {
	pp("not implemented")
}

func (x *AABB) Center() Vec3 {
	return x.min.Add(x.max).MulScalar(0.5)
}

// ContainsBox returns whether the bounding volume fully contains the volume
// defined by the given bounding box
func (a *AABB) ContainsBox(ib AABB) bool {
	return a.ContainsPoint(ib.min) && a.ContainsPoint(ib.max)
}

// ContainsPoint returns whether the bounding volume contains the given point
func (a *AABB) ContainsPoint(ip Vec3) bool {
	gt := ip[0] >= a.min[0] && ip[1] >= a.min[1] && ip[2] >= a.min[2]
	lt := ip[0] <= a.max[0] && ip[1] <= a.max[1] && ip[2] <= a.max[2]
	return gt && lt
}

func (a *AABB) Empty() bool {
	return (a.max[0] < a.min[0]) || (a.max[1] < a.min[1]) || (a.max[2] < a.min[2])
}

func (x AABB) String() string {
	return fmt.Sprintf("AABB<min: %s, max: %s>", x.min, x.max)
}
