#include <iostream>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/constants.hpp> // glm::pi
#include <glm/gtc/quaternion.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/quaternion.hpp>

using namespace std;
using namespace glm;

const vec3 Vec3ZUp = vec3(0, 0, 1);
const vec3 Vec3YUp = vec3(0, 1, 0);

// move (pos, rot) by vector v
vec3 move_by_vec(vec3 pos, vec3 rot, vec3 v, vec3 upV) {

	mat4 tm = translate(pos);

	if (upV == vec3(0., 0., 1.)) {
	// if z-up
		cout << "debug: using z-up version of move_by_vec" << endl;
		cout << to_string(rot) << endl;
		vec3 tmp = rot;
		// swap yz
		rot.y = tmp.z;
		rot.z = tmp.y;
		cout << to_string(rot) << endl;
	}
	mat4 rm = mat4();
	rm = rotate(rm, radians(rot.x), vec3(1.0, 0.0, 0.0));
	rm = rotate(rm, radians(rot.y), vec3(0.0, 1.0, 0.0));
	rm = rotate(rm, radians(rot.z), vec3(0.0, 0.0, 1.0));
	
	// model matrix
	mat4 m = tm * rm;
	
	mat4 inv_ = inverse(m);
	//mat4 inv_ = affineInverse(p);
	//mat3 inv3_ = toMat3(inv_);
	vec4 vec_rot = vec4(v, 1.0) * inv_;
	//vec3 vec_rot2 = v * inv3_;
	vec3 newP = pos + vec3(vec_rot);
	
	return newP;
}

// move by vector v
vec3 move_by_vec2(mat4 m, vec3 pos, vec3 v) {
	
	// extract rotation matrix
	mat3 rm = mat3(m);

	vec3 trans_local = v;

	vec3 trans_world = rm * trans_local;
	
	vec3 newP = pos + trans_world;
	
	return newP;
}

int main()
{
	vec3 obj = vec3(0.0, 0.0, 0.0);
	vec3 upV = vec3(0.0, 0.0, 1.0);
	int vpW = 960;
	int vpH = 540;

    mat4 modelM = mat4(1.0);

	/*
	mat4 rm = mat4(1.0);
	//mat4 tm = translate(vec3(0, 0, -4));
	mat4 tm = translate(vec3(2, 3, 4));
	mat4 sm = mat4(1.0);
	*/

	mat4 rm = mat4(); // identity matrix
	// ZYX order (yaw, pitch, roll); "XYZ" in blender.
	rm = rotate(rm, radians(45.0f), vec3(0.0, 0.0, 1.0));
	rm = rotate(rm, radians(45.0f), vec3(0.0, 1.0, 0.0));
	rm = rotate(rm, radians(45.0f), vec3(1.0, 0.0, 0.0));

	//mat4 tm = mat4();
	mat4 tm = translate(vec3(2., 3., 4.));
	mat4 sm = mat4();

	mat4 modelMatrix = tm * rm * sm;
	//mat4 modelMatrix = sm * rm * tm;
    mat4 m = modelMatrix;
    //mat4 m = inverse(modelMatrix);
	
	cout << "model matrix:" << endl;
	cout << to_string(m) << endl << endl;

	// quaternion rm test
	
	vec3 EulerAngles = radians(vec3(45, 45, 45)); // radians
	quat q1 = quat(EulerAngles);
	
	rm = mat4();
	//rm = mat4_cast(q1);
	rm = toMat4(q1);
	m = tm * rm * sm;
	
	cout << "model matrix from quat:" << endl;
	cout << to_string(m) << endl << endl;
	
	cout << "quat:" << endl;
	cout << to_string(q1) << endl << endl;
	
	// move by vec
	/*
	vec3 p_pos = vec3(0., 0., 0.);
	mat4 p = translate(vec3(1.0, 0.0, 0.0));
	p = rotate(p, radians(45.), vec3(0.0, 0.0, 1.0));
	vec4 v = vec4(1.0, 0.0, 0., 0.);
	*/
	/*
	vec3 p_pos = vec3(-0.70711, -0.70711, 0);
	mat4 p = translate(vec3(0.0, 0.0, 0.0));
	
	p = rotate(p, radians(45.), vec3(1.0, 0.0, 0.0));
	p = rotate(p, radians(45.), vec3(0.0, 0.0, 1.0));
	*/
	vec3 p_pos = vec3(0.0);
	mat4 p = translate(vec3(0.0, 0.0, 0.0));
	
	p = rotate(p, radians(2.0f), vec3(1.0, 0.0, 0.0));
	p = rotate(p, radians(3.0f), vec3(0.0, 1.0, 0.0));
	p = rotate(p, radians(4.0f), vec3(0.0, 0.0, 1.0));
	vec4 v = vec4(0.0, 1.0, 0., 1.);
	mat4 inv_ = inverse(p);
	//mat4 inv_ = affineInverse(p);
	//mat3 inv3_ = toMat3(inv_);
	vec4 vec_rot = v * inv_;
	//vec3 vec_rot2 = v * inv3_;
	vec3 newP = p_pos + vec3(vec_rot);

	cout << "move by vec:" << endl;
	cout << to_string(inv_) << endl << endl;
	cout << to_string(vec_rot) << endl << endl;
	cout << to_string(newP) << endl << endl;
	
	cout << "move by vec func:" << endl;
	//vec3 d = move_by_vec(vec3(0.), vec3(2., 3., 4.), vec3(0., 1., 0.), Vec3ZUp);
	//vec3 d = move_by_vec(vec3(0.), vec3(2., 3., 4.), vec3(0., 0., 1.), Vec3YUp);
	p = translate(vec3(0.0));
	/*
	p = rotate(p, radians(45.0f), vec3(1.0, 0.0, 0.0));
	p = rotate(p, radians(0.0f), vec3(0.0, 1.0, 0.0));
	p = rotate(p, radians(0.0f), vec3(0.0, 0.0, 1.0));
	*/
	p = rotate(p, radians(4.0f), vec3(0.0, 0.0, 1.0));
	p = rotate(p, radians(3.0f), vec3(0.0, 1.0, 0.0));
	p = rotate(p, radians(2.0f), vec3(1.0, 0.0, 0.0));


	vec3 d = move_by_vec2(p, vec3(0., 0., 0.), vec3(0., 1., 0.));
	cout << to_string(d) << endl << endl;

	// mul mat
	vec4 vv = vec4(2.,3.,4.,5.);
	mat4 mm = mat4(1.0);
	mm = translate(vec3(1.0, 2.0, 3.0));
	mm = rotate(mm, radians(45.0f), vec3(1.0, 0.0, 0.0));
	mm = rotate(mm, radians(45.0f), vec3(0.0, 1.0, 0.0));
	mm = rotate(mm, radians(45.0f), vec3(0.0, 0.0, 1.0));
	vec4 rr = vv * mm;

	cout << "mm: " << to_string(mm) << endl << endl;
	cout << "rr: " << to_string(rr) << endl << endl;

	quat q;
	vec3 scale_;
	vec3 translation;
	vec3 skew;
	vec4 persp;
	decompose(m, scale_, q, translation, skew, persp);


	cout << to_string(q) << endl << endl;
	cout << to_string(toMat3(q)) << endl << endl;
	cout << to_string(m) << endl;
	

	// TRS (transposed output to match rx)
	cout << endl << "---" << endl;
	cout << "TRS (transposed):" << endl;
	tm = translate(vec3(2.0, 3.0, 4.0));
	rm = mat4(1.0);
	// XYZ -> wrong?
	/*
	rm = rotate(rm, radians(45.0f), vec3(1.0, 0.0, 0.0));
	rm = rotate(rm, radians(45.0f), vec3(0.0, 1.0, 0.0));
	rm = rotate(rm, radians(45.0f), vec3(0.0, 0.0, 1.0));
	*/
	// ZYX -> correct?
	rm = rotate(rm, radians(45.0f), vec3(0.0, 0.0, 1.0));
	rm = rotate(rm, radians(45.0f), vec3(0.0, 1.0, 0.0));
	rm = rotate(rm, radians(45.0f), vec3(1.0, 0.0, 0.0));
	//sm = scale(vec3(1.0, 1.0, 1.0));
	sm = scale(vec3(1.0, 2.0, 3.0));

	cout << "tm: " << to_string(transpose(tm)) << endl;
	cout << "rm: " << to_string(transpose(rm)) << endl;
	cout << "sm: " << to_string(transpose(sm)) << endl;
	cout << endl;

	// Default t * r * s (reversed?)
	mat4 res1 = tm * rm * sm;
	cout << "res1: " << to_string(transpose(res1)) << endl;

	// Emulate t * r * s
	mat4 tmp = tm * rm;
	tmp = tmp * sm;
	cout << "res2: " << to_string(transpose(tmp)) << endl;

	// Emulate t * r * s
	tmp = sm * rm;
	tmp = tmp * tm;
	cout << "res3: " << to_string(transpose(tmp)) << endl;

	return 0;
}
