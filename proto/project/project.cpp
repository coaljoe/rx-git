#include <iostream>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/constants.hpp> // glm::pi
#include <glm/gtc/quaternion.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/matrix_decompose.hpp>

using namespace std;
using namespace glm;

/*
glm::mat4 camera(float Translate, glm::vec2 const & Rotate)
{
    glm::mat4 Projection = glm::perspective(glm::pi<float>() * 0.25f, 4.0f / 3.0f, 0.1f, 100.f);
    glm::mat4 View = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -Translate));
    View = glm::rotate(View, Rotate.y, glm::vec3(-1.0f, 0.0f, 0.0f));
    View = glm::rotate(View, Rotate.x, glm::vec3(0.0f, 1.0f, 0.0f));
    glm::mat4 Model = glm::scale(glm::mat4(1.0f), glm::vec3(0.5f));
    return Projection * View * Model;
}
*/

int main()
{
	float Translate = 5.0;
	vec2 Rotate = vec2(1.0, 0.0);
	//vec3 obj = vec3(1, 2, 3);
	//vec3 obj = vec3(-3, 1, 1);
	//vec3 obj = vec3(-1, 0, 0);
	vec3 obj = vec3(0, 0, 0);
	vec3 upV = vec3(0, 0, 1.0);
	int vpW = 960;
	int vpH = 540;

    //mat4 Projection = perspective(pi<float>() * 0.25f, 4.0f / 3.0f, 0.1f, 100.f);
    //mat4 Projection = ortho(0, vpW, 0, vpH, 0.1, 1.0);
    //mat4 Projection = ortho(0, vpW, 0, vpH);
    mat4 Projection = ortho(-1, 1, -1, 1);
    //mat4 Projection = ortho(0, 1, 0, 1);
    //mat4 View = lookAt(vec3(1, 1, 1), vec3(0, 0, 0), upV);
    mat4 View = lookAt(vec3(10, -10, 20), vec3(0, 0, 0), upV);
    //mat4 View = lookAt(vec3(1, 1, 1), vec3(0, 0, 0), vec3(0, 1, 0));
    //View = rotate(View, Rotate.y, vec3(-1.0f, 0.0f, 0.0f));
    //View = rotate(View, Rotate.x, vec3(0.0f, 1.0f, 0.0f));
    mat4 Model = mat4(1);
	vec4 vp = vec4(0, 0, vpW, vpH);
	//vec3 win = project(obj, View*Model, Projection, vp);
	vec3 win = project(obj, View, Projection, vp);
    mat4 m = Projection * View * Model;

	quat q;
	vec3 scale;
	vec3 translation;
	vec3 skew;
	vec4 persp;
	decompose(View, scale, q, translation, skew, persp);


	cout << to_string(q) << endl << endl;
	cout << to_string(toMat3(q)) << endl << endl;
	cout << to_string(View) << endl;
	cout << to_string(m) << endl;
	cout << to_string(win) << endl;

	return 0;
}
