// Functionality for debugging.
package rx

import (
	. "bitbucket.org/coaljoe/rx/math"
	//"github.com/go-gl/gl/v2.1/gl"
)

func DebugDrawCube(pos Vec3, size float64, color Vec3) {
	/*
		//r := rxi.Renderer()
		//gl.Disable(gl.TEXTURE_2D)
		DrawBegin()
		DrawSetAtV(pos)
		DrawSetColorV(color)
		DrawCube(size)
		DrawResetColor()
		DrawReset()
		DrawEnd()
		//gl.Enable(gl.TEXTURE_2D)
	*/

	drawer.SetAtV(pos)
	drawer.SetColorV(color)
	drawer.DrawCube(size)
	drawer.ResetColor()
	drawer.Reset()
}

func DebugDrawLine(from, to Vec3, width float64, color Vec3) {
	panic("not ported")
	//gl.Disable(gl.TEXTURE_2D)
	DrawBegin()
	//DrawSetAtV(pos)
	DrawSetColorV(color)
	DrawSetLineWidth(width)
	DrawLine(from[0], from[1], from[2], to[0], to[1], to[2])
	DrawResetColor()
	DrawReset()
	DrawEnd()
	//gl.Enable(gl.TEXTURE_2D)
}

func DebugDrawPlane(pos Vec3, size float64, color Vec3) {
	panic("not ported")
	//r := rxi.Renderer()
	//gl.Disable(gl.TEXTURE_2D)
	DrawBegin()
	DrawSetAtV(pos)
	DrawSetColorV(color)
	DrawPlane(size)
	DrawResetColor()
	DrawReset()
	DrawEnd()
	//gl.Enable(gl.TEXTURE_2D)
}

func DebugDrawText(pos Vec3, size float64, color Vec3) {
	panic("not implemented")
	r := rxi.Renderer()

	r.Set2DMode()

	r.Unset2DMode()

}
