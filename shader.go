package rx

import (
	"fmt"
	"hash/fnv"
	"io/ioutil"
	"os"
	. "path"
	"path/filepath"
	. "strconv"
	"strings"

	. "bitbucket.org/coaljoe/rx/math"

	"github.com/go-gl/gl/v2.1/gl"
)

type Shader struct {
	//version int
	//vs_path, fs_path string
	vs_macro, fs_macro   string
	vs_source, fs_source string
	_uniform_locs_cache  map[string]int32
	cacheId, cachePath   string
	shaderHash           uint32
	// gl
	_shader uint32
}

func NewShader(vs_path, fs_path string) *Shader {
	cachePath := vs_path + ";" + fs_path

	//version := 130
	version := 120
	s := &Shader{
		vs_macro:            fmt.Sprintf("#version %d\n", version),
		fs_macro:            fmt.Sprintf("#version %d\n", version),
		_uniform_locs_cache: make(map[string]int32),
		cachePath:           cachePath,
	}
	s.load(Conf.res_path+"/"+vs_path, Conf.res_path+"/"+fs_path)
	return s
}

func (s *Shader) load(vs_path, fs_path string) {

	Log.Inf("loading shaders " + vs_path + " and " + fs_path + "...")

	var add_includes func(dat []byte, path string) string
	add_includes = func(dat []byte, path string) string {
		r := ""
		lines := strings.Split(string(dat), "\n")
		for _, l := range lines {
			s := strings.Split(l, " ")
			if s[0] == "#include" {
				filepath := strings.TrimSpace(s[1]) // removes \r on windows
				filepath = strings.Replace(filepath, "\"", "", -1)
				fp := Dir(path) + "/" + filepath
				dat_, err := ioutil.ReadFile(fp)
				//Log.Inf("adding shader include from " + fp + "...")
				if err != nil {
					p("file not found", fp)
					panic(err)
				}
				//check(err)
				txt := fmt.Sprintf("/*#### %s ####*/\n", filepath)
				txt += add_includes(dat_, fp)
				//txt += add_includes(path)
				txt += fmt.Sprintf("/*#### end of %s ####*/\n", filepath)
				//println("derp " + filepath)
				//println(txt)
				r += txt + "\n"
				//Log.Inf("added shader include from " + conf.res_path + filepath)
				Log.Inf("added shader include from " + fp)
			} else {
				r += l + "\n"
			}
		}
		return r
	}

	vsDat, err := ioutil.ReadFile(vs_path)
	check(err)

	fsDat, err := ioutil.ReadFile(fs_path)
	check(err)

	s.vs_source = add_includes(vsDat, vs_path)
	s.fs_source = add_includes(fsDat, fs_path)

	s.compile()
}

func hash(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}

func (s *Shader) Hash() uint32 {
	return hash(s.vs_macro + s.fs_macro + s.fs_source + s.vs_source)
}

func (s *Shader) compile() {
	// rename to build?
	cacheId := "Shader_" + Itoa(int(s.Hash()))
	s.cacheId = cacheId

	// return from cache
	rs := rxi.ResourceSys
	if r := rs.Get(cacheId, ResourceTypeShader); r != nil {
		println("reusing shader, cacheId: " + cacheId)
		cs := r.data.(*Shader)
		//return cs
		s._compile()
		s = cs
	} else {
		s._compile()
	}

	// resource reusing
	r := NewResource()
	r.rtype = ResourceTypeShader
	r.id = s.cacheId
	r.path = s.cachePath
	r.data = s
	rxi.ResourceSys.AddResource(r)
}

func (s *Shader) _compile() {
	Log.Dbg("_compile shader, id:", s.cacheId)

	/*
	  vs := gl.CreateShader(gl.VERTEX_SHADER)
	  vs.Source(s.vs_macro + s.vs_source)
	  vs.Compile()
	  if vs.Get(gl.COMPILE_STATUS) != gl.TRUE {
	    //println(s.vs_macro)
	    //println(s.vs_source)
	    println("source: \n", vs.GetSource())
	    panic("vertex shader error: " + vs.GetInfoLog())
	  }

	  fs := gl.CreateShader(gl.FRAGMENT_SHADER)
	  fs.Source(s.fs_macro + s.fs_source)
	  fs.Compile()
	  if fs.Get(gl.COMPILE_STATUS) != gl.TRUE {
	    panic("fragment shader error: " + fs.GetInfoLog())
	  }
	*/

	vsStr := s.vs_macro + s.vs_source + "\x00"
	fsStr := s.fs_macro + s.fs_source + "\x00"

	prg, err := newProgram(vsStr, fsStr)
	if err != nil {
		s.saveShaderSource()
		Log.Err("shader linker error")
		println("")
		panic(err)
	}

	s._shader = prg
	glcheck()

	// for rendering
	s.shaderHash = s.Hash()
}

func (s *Shader) saveShaderSource() {
	// save shader source
	exts := []string{"vert", "frag"}
	for _, ext := range exts {
		source := ""
		switch ext {
		case "vert":
			source = s.vs_macro + s.vs_source
		case "frag":
			source = s.fs_macro + s.fs_source
		default:
			panic("unknown ext: " + ext)
		}
		fout := filepath.Join(os.TempDir(), "dump."+ext)

		f, err := os.Create(fout)
		defer f.Close()
		if err != nil {
			println("can't save shader to " + fout)
			panic(err)
		}
		f.WriteString(source)
		Log.Inf("shader saved to " + fout)
	}
}

// Can be with or without extra -1 checking.
func (s *Shader) getUniformLocationCheck(name string, check bool) int32 {
	// return from cache
	if loc, ok := s._uniform_locs_cache[name]; ok {
		return loc
	}

	// TODO: -1's are not erros but an optimizations
	// https://www.opengl.org/wiki/GLSL_:_common_mistakes
	glcheck()
	loc := gl.GetUniformLocation(s._shader, gl.Str(name+"\x00"))
	// fixme: doesn't fail? returns -1 instead
	if err := gl.GetError(); err != gl.NO_ERROR {
		fmt.Println("err:", err)
		panic("getUniformLocation failed, name: " + name)
	}
	if loc == -1 && check {
		//Log.Err("getUniformLocation returned -1, name: " + name)
		//dump(s)
		//s.saveShaderSource()
		//panic("getUniformLocation returned -1, name: " + name)
	}

	// add to cache
	s._uniform_locs_cache[name] = loc
	return loc
}

// Default with no -1 checking.
func (s *Shader) GetUniformLocation(name string) int32 {
	return s.getUniformLocationCheck(name, true)
}

// Check if a uniform was defined.
func (s *Shader) HasUniform(name string) bool {
	return s.getUniformLocationCheck(name, false) != -1
}

func (s *Shader) GetAttribLocation(name string) int32 {
	//p("XXX s:", s)
	//p("XXX s:", s != nil)
	loc := gl.GetAttribLocation(s._shader, gl.Str(name+"\x00"))
	if loc == -1 {
		println("err: cound not get attribute location; name: ", name)
	}
	if err := gl.GetError(); err != gl.NO_ERROR {
		fmt.Println("err:", err)
		panic("getAttribLocation failed, name: " + name)
	}
	return loc
}

func (s *Shader) PassUniform3f(name string, x, y, z float32) {
	//s._shader.BindAttribLocation(index, name)
	loc := s.GetUniformLocation(name)
	gl.Uniform3f(loc, x, y, z)
	if err := gl.GetError(); err != gl.NO_ERROR {
		fmt.Println("err:", err)
		panic("PassUniform3f failed, name: " + name)
	}
}

func (s *Shader) PassUniform2f(name string, x, y float32) {
	loc := s.GetUniformLocation(name)
	gl.Uniform2f(loc, x, y)
	if err := gl.GetError(); err != gl.NO_ERROR {
		fmt.Println("err:", err)
		panic("PassUniform2f failed, name: " + name)
	}
}

// fixme rename to *V
func (s *Shader) PassUniform3f_(name string, v Vec3) {
	s.PassUniform3f(name, float32(v.X()), float32(v.Y()), float32(v.Z()))
}

func (s *Shader) PassUniform4f(name string, x, y, z, w float64) {
	//s._shader.BindAttribLocation(index, name)
	loc := s.GetUniformLocation(name)
	gl.Uniform4f(loc, float32(x), float32(y), float32(z), float32(w))
	if err := gl.GetError(); err != gl.NO_ERROR {
		fmt.Println("err:", err)
		panic("PassUniform4f failed, name: " + name)
	}
}

// fixme rename to *V
func (s *Shader) PassUniform4f_(name string, v Vec4) {
	s.PassUniform4f(name, v.X(), v.Y(), v.Z(), v.W())
}

func (s *Shader) PassUniform1f(name string, v float64) {
	loc := s.GetUniformLocation(name)
	gl.Uniform1f(loc, float32(v))
	if gl.GetError() != gl.NO_ERROR {
		panic("PassUniform1f failed, name: " + name)
	}
}

func (s *Shader) PassUniform1i(name string, v int) {
	loc := s.GetUniformLocation(name)
	gl.Uniform1i(loc, int32(v))
	if gl.GetError() != gl.NO_ERROR {
		panic("PassUniform1i failed, name: " + name)
	}
}

func (s *Shader) PassUniformMatrix4f(name string, v Mat4) {
	loc := s.GetUniformLocation(name)
	df := v.Dataf()
	gl.UniformMatrix4fv(loc, 1, false, &df[0])
	if gl.GetError() != gl.NO_ERROR {
		panic("PassUniformMatrix4f failed, name: " + name)
	}
}

/*
func (s *Shader) PassUniformMatrix3f(name string, v Mat3) {
	loc := s.GetUniformLocation(name)
	df := v.Dataf()
	gl.UniformMatrix3fv(loc, 1, false, &df[0])
	if gl.GetError() != gl.NO_ERROR {
		panic("PassUniformMatrix3f failed, name: " + name)
	}
}
*/

func (s *Shader) passDefaultUniforms() {
	if s.HasUniform("buffersize") { // fixme: pass always?
		r := rxi.Renderer()
		sx, sy := r.width, r.height
		s.PassUniform2f("buffersize", float32(sx), float32(sy))
	}
	glcheck()
}

func (s *Shader) Bind() {
	gl.UseProgram(s._shader)
	//s._shader.Use()
	s.passDefaultUniforms()
}

func (s *Shader) Unbind() {
	gl.UseProgram(0)
	//s._shader.Unuse()
}

func (s *Shader) Free() {
	dbg("Shader.Free")
	gl.DeleteProgram(s._shader)
	//s._shader.Delete()
}
