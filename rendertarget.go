package rx

type RenderTarget struct {
	name   string
	depth  bool
	width  int
	height int
	fb     *Framebuffer
}

func (rt *RenderTarget) Name() string { return rt.name }
func (rt *RenderTarget) Tex() uint32  { return rt.fb.tex.Tex() }

func NewRenderTarget(name string, width, height int, depth bool) *RenderTarget {
	rt := &RenderTarget{
		name:   name,
		width:  width,
		height: height,
		depth:  depth,
		fb:     NewFramebuffer(width, height, depth),
	}
	//rt.create()
	glcheck()

	// register element
	_RenderTargetSys.Add(rt)
	return rt
}

func (rt *RenderTarget) Bind() {
	_RenderTargetSys.lastUsedRt = rt // Save last used Rt
	rt.fb.Bind()
}

func (rt *RenderTarget) Unbind() {
	rt.fb.Unbind()
}
