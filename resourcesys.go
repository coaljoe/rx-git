package rx

import "fmt"

type rmkey struct {
	id    string
	rtype ResourceType
}
type resmap map[rmkey]*Resource

//type resmap map[struct{string; ResourceType}]*Resource
//type resmap map[string]*Resource

type ResourceSys struct {
	res resmap
}

func NewResourceSys() *ResourceSys {
	t := &ResourceSys{
		res: make(resmap),
	}
	return t
}

func (rs *ResourceSys) AddResource(r *Resource) {
	if rs.HasResource(r) {
		fmt.Println("ResourceSys: resource already loaded, not adding; type:", r.rtype)
		return
	}
	rs.res[rmkey{r.id, r.rtype}] = r
}

/*
//Deprecated
func (rs *ResourceSys) AddResourceFromFile(path string) *Resource {
	r := NewResource()
	r.LoadFromFile(path)
	rs.AddResource(r)
	return r
}
*/

/*
func (rs *ResourceSys) AddResourceFromType(rtype ResourceType) *Resource {
	r := NewResource()
	r.rtype =
	rs.AddResource(r)
	return r
}
*/

func (rs *ResourceSys) RemoveResource(r *Resource) {
	delete(rs.res, rmkey{r.id, r.rtype})
}

func (rs *ResourceSys) HasResource(r *Resource) bool {
	return rs.res[rmkey{r.id, r.rtype}] != nil
}

func (rs *ResourceSys) GetResource(id string, rtype ResourceType) *Resource {
	return rs.res[rmkey{id, rtype}]
}

func (rs *ResourceSys) Get(id string, rtype ResourceType) *Resource {
	return rs.GetResource(id, rtype)
}

func (rs *ResourceSys) GetByPath(path string, rtype ResourceType) *Resource {
	for _, r := range rs.res {
		if r.rtype == rtype && r.path == path {
			return r
		}
	}
	return nil
}

func (rs *ResourceSys) PrintResources() {
	Log.Inf("PrintResources begin")
	for _, r := range rs.res {
		fmt.Println(fmt.Sprintf("- (%s)\n  id: %s\n  path: %s", r.rtype, r.id, r.path))
	}
	Log.Inf("PrintResources end")
	// doto: add sort by id
}

func (rs *ResourceSys) FreeResources() {
	Log.Inf("FreeResources")
	for _, r := range rs.res {
		r.Free()
		rs.RemoveResource(r)
	}
}
