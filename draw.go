package rx

import (
	. "bitbucket.org/coaljoe/rx/math"

	"bitbucket.org/coaljoe/rx/text"
	"bitbucket.org/coaljoe/rx/transform"
	"github.com/go-gl/gl/v2.1/gl"
)

//var _ = `
var _draw_fillMat *Material
var _draw_geom_buf uint32
var _draw_geom_buf_init bool
var _draw_init bool

func init_draw() {
	if !_draw_init {
		if _draw_geom_buf_init == false {
			gl.GenBuffers(1, &_draw_geom_buf)
			_draw_geom_buf_init = true
		}
		if _draw_fillMat == nil {
			_draw_fillMat = NewMaterial(Conf.res_path + "/materials/fill.json")
		}
		_draw_init = true
	}
}

//`

var drawer *Drawer = newDrawer()

type Drawer struct {
	fillMat     *Material
	geomBuf     uint32
	color       Vec3
	transform   *transform.Transform
	sizeGLfloat int
	initialized bool
}

func newDrawer() *Drawer {
	d := &Drawer{
		sizeGLfloat: 4,
	}
	d.Reset()
	return d
}

func (d *Drawer) init() {
	if d.initialized {
		pp("can't init drawer more than once")
	}
	gl.GenBuffers(1, &d.geomBuf)
	d.fillMat = NewMaterial(Conf.res_path + "/materials/fill.json")
	d.initialized = true
}

func (d *Drawer) addTransform() {
	if d.transform == nil {
		d.transform = transform.NewTransform()
	}
}

func (d *Drawer) GetMVPMatrix() Mat4 {
	r := rxi.renderer
	if d.transform == nil {
		return r.matMVPIdent
	} else {
		m := d.transform.FinalTransform()
		mvp := r.CalculateMVPModel(m)
		return mvp
	}
}

func (d *Drawer) Reset() {
	//d.transform = transform.NewTransform()
	d.transform = nil
	d.color = Vec3One
}

func (d *Drawer) ResetColor() {
	d.SetColorV(ColorWhite)
}

func (d *Drawer) SetAt(x, y, z float64) {
	d.addTransform()
	d.transform.SetPos(Vec3{x, y, z})
}

func (d *Drawer) SetAtV(v Vec3) {
	d.SetAt(v[0], v[1], v[2])
}

func (d *Drawer) SetColor(r, g, b float64) {
	d.color = Vec3{r, g, b}
}

func (d *Drawer) SetColorV(v Vec3) {
	d.SetColor(v[0], v[1], v[2])
}

// TODO: add normals and texcoords
func (d *Drawer) DrawBox(dx, dy, dz float64) {
	rxi.renderer.setMaterial(d.fillMat)

	// XXX warning using half size for 1-unit cuboid
	// centered at 0,0,0
	dx_ := float32(dx / 2)
	dy_ := float32(dy / 2)
	dz_ := float32(dz / 2)

	points := []float32{
		-1 * dx_, -1 * dy_, 1 * dz_,
		1 * dx_, -1 * dy_, 1 * dz_,
		1 * dx_, 1 * dy_, 1 * dz_,
		-1 * dx_, 1 * dy_, 1 * dz_,

		-1 * dx_, -1 * dy_, -1 * dz_,
		-1 * dx_, 1 * dy_, -1 * dz_,
		1 * dx_, 1 * dy_, -1 * dz_,
		1 * dx_, -1 * dy_, -1 * dz_,

		-1 * dx_, 1 * dy_, -1 * dz_,
		-1 * dx_, 1 * dy_, 1 * dz_,
		1 * dx_, 1 * dy_, 1 * dz_,
		1 * dx_, 1 * dy_, -1 * dz_,

		-1 * dx_, -1 * dy_, -1 * dz_,
		1 * dx_, -1 * dy_, -1 * dz_,
		1 * dx_, -1 * dy_, 1 * dz_,
		-1 * dx_, -1 * dy_, 1 * dz_,

		1 * dx_, -1 * dy_, -1 * dz_,
		1 * dx_, 1 * dy_, -1 * dz_,
		1 * dx_, 1 * dy_, 1 * dz_,
		1 * dx_, -1 * dy_, 1 * dz_,

		-1 * dx_, -1 * dy_, -1 * dz_,
		-1 * dx_, -1 * dy_, 1 * dz_,
		-1 * dx_, 1 * dy_, 1 * dz_,
		-1 * dx_, 1 * dy_, -1 * dz_,
	}

	gl.BindBuffer(gl.ARRAY_BUFFER, d.geomBuf)
	//gl.BufferData(gl.ARRAY_BUFFER, len(points)*sizeGLfloat, gl.Ptr(points), gl.STATIC_DRAW)
	gl.BufferData(gl.ARRAY_BUFFER, len(points)*d.sizeGLfloat, gl.Ptr(points), gl.DYNAMIC_DRAW)

	sh := rxi.renderer.curMaterial.shader
	attrVPosition := sh.GetAttribLocation("v_position")
	if attrVPosition == -1 {
		pp("v_position is -1")
	}
	gl.EnableVertexAttribArray(uint32(attrVPosition))

	matMVP := sh.GetUniformLocation("mvp")
	if matMVP == -1 {
		pp("mvp is -1")
	}

	//sh.PassUniformMatrix4f("mvp", rxi.renderer.matMVPIdent)
	mvp := d.GetMVPMatrix()
	sh.PassUniformMatrix4f("mvp", mvp)
	//color := Vec4{1.0, 1.0, 1.0, 1.0}
	//color := Vec4{1.0, 0, 0, 1.0}
	color := d.color.ToVec4()
	sh.PassUniform4f_("fill_color", color)

	//sh.saveShaderSource()
	//pp(attrVPosition)
	//glcheck()

	gl.VertexAttribPointer(
		uint32(attrVPosition),
		3,        // Number of elements per vertex
		gl.FLOAT, // Element type
		false,
		0,
		nil)

	gl.DrawArrays(gl.QUADS, int32(0), int32(4*6))
	gl.DisableVertexAttribArray(uint32(attrVPosition))

	glcheck()

	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, 0)

	//rxi.renderer.unsetMaterial(rxi.renderer.defaultMaterial)
	rxi.renderer.unsetMaterial(d.fillMat)

	/*
		gl.Normal3f(0, 0, 1)
		gl.TexCoord2f(0, 0)
		gl.TexCoord2f(1, 0)
		gl.TexCoord2f(1, 1)
		gl.TexCoord2f(0, 1)

		gl.Normal3f(0, 0, -1)
		gl.TexCoord2f(1, 0)
		gl.TexCoord2f(1, 1)
		gl.TexCoord2f(0, 1)
		gl.TexCoord2f(0, 0)

		gl.Normal3f(0, 1, 0)
		gl.TexCoord2f(0, 1)
		gl.TexCoord2f(0, 0)
		gl.TexCoord2f(1, 0)
		gl.TexCoord2f(1, 1)

		gl.Normal3f(0, -1, 0)
		gl.TexCoord2f(1, 1)
		gl.TexCoord2f(0, 1)
		gl.TexCoord2f(0, 0)
		gl.TexCoord2f(1, 0)

		gl.Normal3f(1, 0, 0)
		gl.TexCoord2f(1, 0)
		gl.TexCoord2f(1, 1)
		gl.TexCoord2f(0, 1)
		gl.TexCoord2f(0, 0)

		gl.Normal3f(-1, 0, 0)
		gl.TexCoord2f(0, 0)
		gl.TexCoord2f(1, 0)
		gl.TexCoord2f(1, 1)
		gl.TexCoord2f(0, 1)
	*/

	var _ = `
	// XXX warning using half size for 1-unit cuboid
	// centered at 0,0,0
	dx_ := float32(dx / 2)
	dy_ := float32(dy / 2)
	dz_ := float32(dz / 2)

	gl.Begin(gl.QUADS)

	gl.Normal3f(0, 0, 1)
	gl.TexCoord2f(0, 0)
	gl.Vertex3f(-1*dx_, -1*dy_, 1*dz_)
	gl.TexCoord2f(1, 0)
	gl.Vertex3f(1*dx_, -1*dy_, 1*dz_)
	gl.TexCoord2f(1, 1)
	gl.Vertex3f(1*dx_, 1*dy_, 1*dz_)
	gl.TexCoord2f(0, 1)
	gl.Vertex3f(-1*dx_, 1*dy_, 1*dz_)

	gl.Normal3f(0, 0, -1)
	gl.TexCoord2f(1, 0)
	gl.Vertex3f(-1*dx_, -1*dy_, -1*dz_)
	gl.TexCoord2f(1, 1)
	gl.Vertex3f(-1*dx_, 1*dy_, -1*dz_)
	gl.TexCoord2f(0, 1)
	gl.Vertex3f(1*dx_, 1*dy_, -1*dz_)
	gl.TexCoord2f(0, 0)
	gl.Vertex3f(1*dx_, -1*dy_, -1*dz_)

	gl.Normal3f(0, 1, 0)
	gl.TexCoord2f(0, 1)
	gl.Vertex3f(-1*dx_, 1*dy_, -1*dz_)
	gl.TexCoord2f(0, 0)
	gl.Vertex3f(-1*dx_, 1*dy_, 1*dz_)
	gl.TexCoord2f(1, 0)
	gl.Vertex3f(1*dx_, 1*dy_, 1*dz_)
	gl.TexCoord2f(1, 1)
	gl.Vertex3f(1*dx_, 1*dy_, -1*dz_)

	gl.Normal3f(0, -1, 0)
	gl.TexCoord2f(1, 1)
	gl.Vertex3f(-1*dx_, -1*dy_, -1*dz_)
	gl.TexCoord2f(0, 1)
	gl.Vertex3f(1*dx_, -1*dy_, -1*dz_)
	gl.TexCoord2f(0, 0)
	gl.Vertex3f(1*dx_, -1*dy_, 1*dz_)
	gl.TexCoord2f(1, 0)
	gl.Vertex3f(-1*dx_, -1*dy_, 1*dz_)

	gl.Normal3f(1, 0, 0)
	gl.TexCoord2f(1, 0)
	gl.Vertex3f(1*dx_, -1*dy_, -1*dz_)
	gl.TexCoord2f(1, 1)
	gl.Vertex3f(1*dx_, 1*dy_, -1*dz_)
	gl.TexCoord2f(0, 1)
	gl.Vertex3f(1*dx_, 1*dy_, 1*dz_)
	gl.TexCoord2f(0, 0)
	gl.Vertex3f(1*dx_, -1*dy_, 1*dz_)

	gl.Normal3f(-1, 0, 0)
	gl.TexCoord2f(0, 0)
	gl.Vertex3f(-1*dx_, -1*dy_, -1*dz_)
	gl.TexCoord2f(1, 0)
	gl.Vertex3f(-1*dx_, -1*dy_, 1*dz_)
	gl.TexCoord2f(1, 1)
	gl.Vertex3f(-1*dx_, 1*dy_, 1*dz_)
	gl.TexCoord2f(0, 1)
	gl.Vertex3f(-1*dx_, 1*dy_, -1*dz_)

	gl.End()
	`
}

func (d *Drawer) DrawCube(scale float64) {
	//d.SetScale(scale, scale, scale)
	//DrawBox(1, 1, 1)
	d.DrawBox(scale, scale, scale)
}

func draw_grid(size float64, cell_size float64) {
	gl.LineWidth(1.0)
	n, s := size, cell_size
	color := Vec3One
	var i float64
	for i = 0.0; i < n; i += s {
		if i == 0 {
			color = Vec3{.3, .6, .3}
		} else {
			color = Vec3{.25, .25, .25}
		}
		//gl.Vertex3f(i, 0, 0)
		//gl.Vertex3f(i, n, 0)
		draw_line_color(i, 0, 0, i, n, 0, color.X(), color.Y(), color.Z())
		if i == 0 {
			color = Vec3{.6, .3, .3}
		} else {
			color = Vec3{.25, .25, .25}
		}
		//gl.Vertex3f(0, i, 0)
		//gl.Vertex3f(n, i, 0)
		draw_line_color(0, i, 0, n, i, 0, color.X(), color.Y(), color.Z())
	}

	var _ = `
	//gl.PushAttrib(gl.CURRENT_BIT)
	gl.LineWidth(1.0)
	n, s := float32(size), float32(cell_size)
	gl.Begin(gl.LINES)
	var i float32
	for i = 0.0; i < n; i += s {
		if i == 0 {
			gl.Color3f(.3, .6, .3)
		} else {
			gl.Color3f(.25, .25, .25)
		}
		gl.Vertex3f(i, 0, 0)
		gl.Vertex3f(i, n, 0)
		if i == 0 {
			gl.Color3f(.6, .3, .3)
		} else {
			gl.Color3f(.25, .25, .25)
		}
		gl.Vertex3f(0, i, 0)
		gl.Vertex3f(n, i, 0)
	}
	gl.End()
	//gl.PopAttrib()
	`
}

func draw_line_color(x1, y1, z1, x2, y2, z2, cr, cb, cg float64) {
	//pp(rxi.renderer.defaultMaterial)

	//rxi.renderer.setMaterial(rxi.renderer.defaultMaterial)
	rxi.renderer.setMaterial(_draw_fillMat)

	sizeGLfloat := 4
	/*
		points := []float32{
			0.0, 0.0, 0.0,
			//0.0, 1.0, 0.0,
			100.0, 100.0, 0.0,
		}
	*/
	points := []float32{
		float32(x1), float32(y1), float32(z1),
		float32(x2), float32(y2), float32(z2),
	}

	//var buf uint32
	//p("Z", _draw_line_buf)
	gl.BindBuffer(gl.ARRAY_BUFFER, _draw_geom_buf)
	//gl.BufferData(gl.ARRAY_BUFFER, len(points)*sizeGLfloat, gl.Ptr(points), gl.STATIC_DRAW)
	gl.BufferData(gl.ARRAY_BUFFER, len(points)*sizeGLfloat, gl.Ptr(points), gl.DYNAMIC_DRAW)

	sh := rxi.renderer.curMaterial.shader
	attrVPosition := sh.GetAttribLocation("v_position")
	if attrVPosition == -1 {
		pp("v_position is -1")
	}
	gl.EnableVertexAttribArray(uint32(attrVPosition))

	matMVP := sh.GetUniformLocation("mvp")
	if matMVP == -1 {
		pp("mvp is -1")
	}

	sh.PassUniformMatrix4f("mvp", rxi.renderer.matMVPIdent)
	color := Vec4{cr, cg, cb, 1.0}
	//color := Vec4{1.0, 0, 0, 1.0}
	sh.PassUniform4f_("fill_color", color)

	//sh.saveShaderSource()
	//pp(attrVPosition)
	//glcheck()

	gl.VertexAttribPointer(
		uint32(attrVPosition),
		3,        // Number of elements per vertex
		gl.FLOAT, // Element type
		false,
		0,
		nil)

	gl.DrawArrays(gl.LINES, int32(0), int32(len(points)/3))
	gl.DisableVertexAttribArray(uint32(attrVPosition))

	glcheck()

	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, 0)

	//rxi.renderer.unsetMaterial(rxi.renderer.defaultMaterial)
	rxi.renderer.unsetMaterial(_draw_fillMat)
}

func draw_line(x1, y1, z1, x2, y2, z2 float64) {
	draw_line_color(x1, y1, z1, x2, y2, z2, 1, 1, 1)
}

func draw_axes() {
	//gl.PushAttrib(gl.CURRENT_BIT)
	//gl.Color3f(1, 0, 0)
	draw_line_color(0, 0, 0, 1, 0, 0, 1, 0, 0)
	//gl.Color3f(0, 1, 0)
	draw_line_color(0, 0, 0, 0, 1, 0, 0, 1, 0)
	//gl.Color3f(0, 0, 1)
	draw_line_color(0, 0, 0, 0, 0, 1, 0, 0, 1)
	//gl.PopAttrib()
}

func DrawAxes() {
	draw_axes()
}

func DrawLine(x1, y1, z1, x2, y2, z2 float64) {
	draw_line(x1, y1, z1, x2, y2, z2)
}

func DrawLineV(a, b Vec3) {
	DrawLine(a[0], a[1], a[2], b[0], b[1], b[2])
}

func DrawDot(x, y, z float64) {
	gl.Begin(gl.POINTS)
	gl.Vertex3f(float32(x), float32(y), float32(z))
	gl.End()
}

// Draw 2D Rect (unfilled)
func DrawRect(x, y, w, h float64) {
	/*
		w := x2-x1
		h := y2-y1
		DrawLine(x1, y1, 0, x1, w, 0)
		DrawLine(x2, y1, 0, x2, -h, 0)
		DrawLine(x2, y2, 0, x1, y2, 0)
		DrawLine(x2, y2, 0, x1, y1, 0)
	*/
	// https://en.wikibooks.org/wiki/
	// OpenGL_Programming/Modern_OpenGL_Tutorial_2D
	//gl.PushMatrix()
	//gl.MatrixMode(gl.MODELVIEW)
	//gl.LoadIdentity()
	//gl.Translatef(0.375, 0.375, 0)
	DrawLine(x, y, 0, x+w, y, 0)     // bottom
	DrawLine(x+w, y, 0, x+w, y+h, 0) // right
	DrawLine(x, y+h, 0, x+w, y+h, 0) // top
	DrawLine(x, y, 0, x, y+h, 0)     // left
	//gl.PopMatrix()
	/*
		x1, y1 := float32(x), float32(y)
		x2, y2 := float32(w), float32(h)
		gl.PolygonMode(gl.FRONT_AND_BACK, gl.LINE)
		gl.Rectf(x1, y1, x2, y2)
		gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
	*/
	/*
		// Version with fixed 'missing pixel' effect,
		// seems not perfect but better.
		// factor-language.blogspot.com/2008/11/some-recent-ui-rendering-fixes.html
		pw := rxi.Renderer().GetPw()
		ph := rxi.Renderer().GetPh()
		o1x, o1y := 0.5*pw, 0.5*ph               // top-left offset
		o2x, o2y := -0.3*pw, -0.3*ph             // bottom-right offset
		DrawLine(x, y, 0, x+w+o2x, y+o2y, 0)     // bottom
		DrawLine(x+w+o2x, y+o2y, 0, x+w, y+h, 0) // right
		DrawLine(x+o1x, y+h+o1y, 0, x+w, y+h, 0) // top
		DrawLine(x+o1x, y+o1y, 0, x, y+h, 0)     // left
	*/
}

func DrawRectV(v Vec4) {
	DrawRect(v[0], v[1], v[2], v[3])
}

/*
func DrawCube(scale float64) {

	//gl.BindTexture(gl.TEXTURE_2D, texture)

	//gl.Color4f(1, 1, 1, 1)

	gl.PushMatrix()
	gl.Scalef(float32(scale), float32(scale), float32(scale))

	DrawBox(1, 1, 1)

	gl.PopMatrix()
}
*/

func DrawPlane(scale float64) {

	//gl.BindTexture(gl.TEXTURE_2D, texture)

	//gl.Color4f(1, 1, 1, 1)

	gl.PushMatrix()
	gl.Scalef(float32(scale), float32(scale), float32(scale))

	dx_ := float32(1.0 / 2)
	dy_ := float32(1.0 / 2)
	//dz_ := float32(1.0 / 2)

	gl.Begin(gl.QUADS)

	//gl.Normal3f(0, 0, 1)
	//gl.TexCoord2f(0, 0)
	gl.Vertex3f(-1*dx_, -1*dy_, 0)
	//gl.TexCoord2f(1, 0)
	gl.Vertex3f(1*dx_, -1*dy_, 0)
	//gl.TexCoord2f(1, 1)
	gl.Vertex3f(1*dx_, 1*dy_, 0)
	//gl.TexCoord2f(0, 1)
	gl.Vertex3f(-1*dx_, 1*dy_, 0)

	gl.End()

	gl.PopMatrix()
}

func DrawQuad(x, y, w, h float64) {
	gl.Begin(gl.QUADS)
	//gl.TexCoord2f(0, 0)
	gl.Vertex2f(float32(x), float32(y))
	//gl.TexCoord2f(1, 0)
	gl.Vertex2f(float32(x+w), float32(y))
	//gl.TexCoord2f(1, 1)
	gl.Vertex2f(float32(x+w), float32(y+h))
	//gl.TexCoord2f(0, 1)
	gl.Vertex2f(float32(x), float32(y+h))
	gl.End()
}

func DrawQuadV(v Vec4) {
	DrawQuad(v[0], v[1], v[2], v[3])
}

func DrawBegin() {
	gl.PushMatrix()
}

func DrawEnd() {
	gl.PopMatrix()
	gl.LineWidth(1.0)
}

func DrawSetAt(x, y, z float64) {
	gl.Translatef(float32(x), float32(y), float32(z))
}

func DrawSetAtV(v Vec3) {
	DrawSetAt(v[0], v[1], v[2])
}

func DrawSetLineWidth(v float64) {
	gl.LineWidth(float32(v))
}

func DrawReset() {
	gl.LoadIdentity()
}

func DrawSetColor(r, g, b float64) {
	gl.Color3f(float32(r), float32(g), float32(b))
}

func DrawSetColorV(v Vec3) {
	DrawSetColor(v[0], v[1], v[2])
}

// fixme: use with get color3f?
func DrawResetColor() {
	DrawSetColorV(ColorWhite)
}

// FS Quad
func BuildFsQuad() (vao, vbo, ibo uint32) {
	// original code from 05fbo_fxaa.cpp

	// sizeof(GLFloat)
	sizeGLfloat := 4
	sizeGLuint := 4

	// vao and vbo handle
	//var vao, vbo, ibo uint32

	// generate and bind the vao
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	// generate and bind the vertex buffer object
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)

	// data for a fullscreen quad (this time with texture coords)
	var post_effect_vertexData = []float32{
		//  X    Y    Z          U    V
		1.0, 1.0, 0.0, 1.0, 1.0, // vertex 0
		-1.0, 1.0, 0.0, 0.0, 1.0, // vertex 1
		1.0, -1.0, 0.0, 1.0, 0.0, // vertex 2
		-1.0, -1.0, 0.0, 0.0, 0.0, // vertex 3
	} // 4 vertices with 5 components (floats) each

	// fill with data
	gl.BufferData(gl.ARRAY_BUFFER, sizeGLfloat*4*5, gl.Ptr(post_effect_vertexData), gl.STATIC_DRAW)

	// set up generic attrib pointers
	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, int32(5*sizeGLfloat), gl.PtrOffset(0*sizeGLfloat))

	gl.EnableVertexAttribArray(1)
	gl.VertexAttribPointer(1, 2, gl.FLOAT, false, int32(5*sizeGLfloat), gl.PtrOffset(3*sizeGLfloat))

	// generate and bind the index buffer object
	gl.GenBuffers(1, &ibo)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo)

	var post_effect_indexData = []uint32{
		0, 1, 2, // first triangle
		2, 1, 3, // second triangle
	}

	// fill with data
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, sizeGLuint*2*3, gl.Ptr(post_effect_indexData), gl.STATIC_DRAW)

	// "unbind" vao
	gl.BindVertexArray(0)

	return
}

func DrawFsQuad(vao uint32) {
	// bind the vao
	gl.BindVertexArray(vao)

	// draw
	gl.DrawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, gl.Ptr(nil))

	// unbind
	gl.BindVertexArray(0)
}

func DestroyFsQuad(vao, vbo, ibo uint32) {
	gl.DeleteVertexArrays(1, &vao)
	gl.DeleteBuffers(1, &ibo)
	gl.DeleteBuffers(1, &vbo)
}

func DrawText(text_ string, x, y float64, color Vec4) {
	//p("DrawText:", text_, x, y, color)
	if text_ == "" {
		return
	}
	font := text.DefaultFont

	// Set font properties
	//font.SetAttributes(&l.style.FontAttributes)
	font.SetAttributes(&text.DefaultFontAttributes)
	font.SetColor(color)

	// Create an image with the text
	textImage := font.DrawText(text_)

	// Create texture if it doesn't exist yet
	//if text.DefaultFontTexture == nil {
	//tex = texture.NewTexture2DFromRGBA(textImage)
	//tex.SetMagFilter(gls.NEAREST)
	//tex.SetMinFilter(gls.NEAREST)
	//tex.SetFromRGBA(textImage)

	b := textImage.Bounds()
	imgWidth := b.Max.X
	imgHeight := b.Max.Y
	tex := NewEmptyTexture(imgWidth, imgHeight, TextureFormatRGBA)
	tex.SetOpts(TextureOptions{
		MinFilter: TextureFilterNearest,
		MagFilter: TextureFilterNearest})
	//tex.SetFromRGBA(textImage)
	/*
		imData := &ImageData{
			Data: textImage.Pix,
			W:    ix, H: iy,
		}
	*/
	tex.SetTextureData(textImage.Pix)
	if false {
		tex.Save("/tmp/1_" + text_ + ".png")
	}

	//tex2 := NewTexture("/tmp/2.png")
	//tex2 := NewTexture("/tmp/1a.png")
	//tex2 := NewTexture("/tmp/1.png")

	gl.Enable(gl.BLEND)
	//gl.Disable(gl.DEPTH_TEST)
	tex.Bind()
	//tex2.Bind()
	//DrawBox(4, 4, 4)
	//rxi.renderer.renderQuad(.0, .0, 0.5, 0.5, -1)
	//rxi.renderer.renderQuad(.0, .0, 0.6, 0.6, 0)
	//rxi.renderer.RenderQuadPx(.0, .0, 500, 500, 1)
	sx := rxi.Renderer().GetPw() * float64(imgWidth)
	sy := rxi.Renderer().GetPh() * float64(imgHeight)
	//rxi.renderer.RenderQuad(x, y, sx, sy, 1)
	rxi.renderer.RenderQuadFlipY(x, y, sx, sy, 1)
	tex.Unbind()
	//tex2.Unbind()
	gl.Disable(gl.BLEND)
	//gl.Enable(gl.DEPTH_TEST)

	tex.Free()
}
