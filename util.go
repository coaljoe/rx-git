package rx

import (
	//	"errors"
	"bitbucket.org/coaljoe/lib/xlog"
	"bytes"
	"fmt"
	"log"
	"os"
	"regexp"
	"runtime/debug"

	js "github.com/bitly/go-simplejson"
	"github.com/davecgh/go-spew/spew"
)

const (
	Debug = true
)

var __p = p

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func trace(s string) {
	fmt.Println("\nTrace:\n" + s)
	debug.PrintStack()
	panic(s)
}

func dbg(v ...interface{}) {
	if Conf.log_level >= xlog.LogInfo {
		fmt.Println(v...)
	}
}

// exported btoi
func Xbtoi(b bool) int { return btoi(b) }

func btoi(b bool) int {
	if b {
		return 1
	}
	return 0
}

func getJsonFromFile(fp string) *js.Json {
	r, err := os.Open(fp)
	if err != nil {
		wd, _ := os.Getwd()
		println("wd:", wd)
		println("cant read file: " + fp)
		log.Fatal(err)
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(r)
	s := buf.String()

	reg, err := regexp.Compile("//.*?\n")
	if err != nil {
		log.Fatal(err)
	}
	safe := reg.ReplaceAllString(s, "\n")
	//Println(safe)

	//d, err := js.NewFromReader(r)
	js, err := js.NewJson([]byte(safe))
	if err != nil {
		fmt.Println(safe)
		println("cant read json from file: " + fp)
		log.Fatal(err)
	}

	return js
}

/*
func newTexture(file string) (uint32, error) {
  imgFile, err := os.Open(file)
  if err != nil {
    return 0, err
  }
  img, _, err := image.Decode(imgFile)
  if err != nil {
    return 0, err
  }

  rgba := image.NewRGBA(img.Bounds())
  if rgba.Stride != rgba.Rect.Size().X*4 {
    return 0, fmt.Errorf("unsupported stride")
  }
  draw.Draw(rgba, rgba.Bounds(), img, image.Point{0, 0}, draw.Src)

  var texture uint32
  gl.GenTextures(1, &texture)
  gl.ActiveTexture(gl.TEXTURE0)
  gl.BindTexture(gl.TEXTURE_2D, texture)
  gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
  gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
  gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
  gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
  gl.TexImage2D(
    gl.TEXTURE_2D,
    0,
    gl.RGBA,
    int32(rgba.Rect.Size().X),
    int32(rgba.Rect.Size().Y),
    0,
    gl.RGBA,
    gl.UNSIGNED_BYTE,
    gl.Ptr(rgba.Pix))

  return texture, nil
}
*/

func p(args ...interface{}) {
	fmt.Println(args...)
}

func pp(args ...interface{}) {
	fmt.Println(args...)
	panic("pp")
}

func dump(s interface{}) {
	spew.Dump(s)
}

func pdump(s interface{}) {
	dump(s)
	panic("pdump")
}
