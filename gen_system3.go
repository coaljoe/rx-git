//go:generate genny -in=$GOFILE -out=gen-$GOFILE gen "Typ=RenderTarget"
package rx

import "github.com/cheekybits/genny/generic"

type Typ generic.Type

// TypSys is a system of Typ.
type TypSys struct {
	items []*Typ
}

func NewTypSys() *TypSys {
	return &TypSys{items: make([]*Typ, 0)}
}
func (s *TypSys) Push(item *Typ) {
	s.items = append(s.items, item)
}
func (s *TypSys) Pop() *Typ {
	item := s.items[0]
	s.items = s.items[1:]
	return item
}
