package rx

import (
	. "bitbucket.org/coaljoe/rx/math"

	"github.com/go-gl/glfw/v3.1/glfw"
)

type EventHandler interface {
	OnKeyPress(key glfw.Key)
	OnKeyRelease(key glfw.Key)
	OnMouseMove(mx, my int)
	OnMousePress(btn glfw.MouseButton)
	OnMouseRelease(btn glfw.MouseButton)
}

type InputSys struct {
	Mouse         *Mouse
	Keyboard      *Keyboard
	win           *Window
	eventHandlers []EventHandler
}

func NewInputSys(app *App) *InputSys {
	is := &InputSys{
		Mouse:    NewMouse(app.Win()),
		Keyboard: NewKeyboard(app.Win()),
		win:      app.Win(),
	}
	is.AddEventHandler(is)
	return is
}

func (is *InputSys) on_key(win *glfw.Window, key glfw.Key, scancode int,
	action glfw.Action, mods glfw.ModifierKey) {
	for _, h := range is.eventHandlers {
		if action == glfw.Press {
			h.OnKeyPress(key)
		} else if action == glfw.Release {
			h.OnKeyRelease(key)
		}
	}
}

func (is *InputSys) on_mouse_move(win *glfw.Window, xpos, ypos float64) {
	for _, h := range is.eventHandlers {
		h.OnMouseMove(int(xpos), int(ypos))
	}
}

func (is *InputSys) on_mouse_button(win *glfw.Window, btn glfw.MouseButton,
	action glfw.Action, mods glfw.ModifierKey) {
	for _, h := range is.eventHandlers {
		if action == glfw.Press {
			h.OnMousePress(btn)
		} else if action == glfw.Release {
			h.OnMouseRelease(btn)
		}
	}
}

func (is *InputSys) OnKeyPress(key glfw.Key) {
	is.Keyboard.KeyPressCB(key)
}

func (is *InputSys) OnKeyRelease(key glfw.Key) {
	is.Keyboard.KeyReleaseCB(key)
}

func (is *InputSys) OnMouseMove(x, y int) {
	is.Mouse.MouseMoveCB(x, y)
}

func (is *InputSys) OnMousePress(btn glfw.MouseButton) {
	is.Mouse.MousePressCB(btn)
}

func (is *InputSys) OnMouseRelease(btn glfw.MouseButton) {
	is.Mouse.MouseReleaseCB(btn)
}

func (is *InputSys) AddEventHandler(eh EventHandler) {
	// fixme: check doubles
	is.eventHandlers = append(is.eventHandlers, eh)
}

// ***** Keyboard *****

type Keyboard struct {
	win *Window
}

func NewKeyboard(win *Window) *Keyboard {
	k := &Keyboard{
		win: win,
	}
	return k
}

func (k *Keyboard) KeyPressCB(key glfw.Key) {
	Pub(Ev_key_press, key)
}

func (k *Keyboard) KeyReleaseCB(key glfw.Key) {
	Pub(Ev_key_release, key)
}

func (k *Keyboard) IsKeyPressed(key glfw.Key) bool {
	// fixme?
	return k.win.IsKeyPressed(key)
}

// ***** Mouse *****

// Data for ev_mouse_move.
type EvMouseMoveData struct {
	X, Y           int
	PrevX, PrevY   int
	SX, SY         float64
	PrevSX, PrevSY float64
}

type Mouse struct {
	win *Window
	// Position in pixels (viewport/window position, y not inverted)
	X, Y         int
	PrevX, PrevY int
	// OpenGL screen position in units (y inverted)
	SX, SY         float64
	PrevSX, PrevSY float64
}

func NewMouse(win *Window) *Mouse {
	sw, sh := win.Size()
	m := &Mouse{
		win: win,
		// initial values - screen center
		X: sw / 2,
		Y: sh / 2,
	}
	/*
		Sub(Ev_Mouse_move, t.onMouseMove)
		Sub(Ev_Mouse_up, t.onMouseUp)
	*/
	return m
}

func (m *Mouse) MouseMoveCB(x, y int) {
	// Save previous coords
	m.PrevX = m.X
	m.PrevY = m.Y
	m.PrevSX = m.SX
	m.PrevSY = m.SY
	// Make abs coords instead of wrapped ones
	sw, sh := m.win.Size()
	m.X = int(Clamp(float64(x), 0.0, float64(sw)))
	m.Y = int(Clamp(float64(y), 0.0, float64(sh)))
	// Update position in units
	sxi, syi := rxi.Renderer().Size()
	m.SX = float64(m.X) / float64(sxi)
	m.SY = 1.0 - (float64(m.Y) / float64(syi)) // y inverted
	//Pub(Ev_mouse_move, struct{ x, y int }{m.X, m.Y})
	data := EvMouseMoveData{m.X, m.Y, m.PrevX, m.PrevY,
		m.SX, m.SY, m.PrevSX, m.PrevSY}
	Pub(Ev_mouse_move, data)
}

func (m *Mouse) MousePressCB(btn glfw.MouseButton) {
	Pub(Ev_mouse_press, btn)
}

func (m *Mouse) MouseReleaseCB(btn glfw.MouseButton) {
	Pub(Ev_mouse_release, btn)
}

func (m *Mouse) IsButtonPressed(btn glfw.MouseButton) bool {
	return m.win.IsMouseButtonPressed(btn)
}
