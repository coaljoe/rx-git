package math

// from: https://github.com/go-gl/mathgl/blob/9e3ea3e806141087fbb83a8a8e7038bf73eff120/mgl64/project.go
/*
func Project(obj Vec3, modelview, projection Mat4, initialX, initialY, width, height int) (win Vec3) {
	obj4 := obj.Vec4(1)

	vpp := projection.Mul4(modelview).Mul4x1(obj4)
	vpp = vpp.Mul(1 / vpp.W())
	win[0] = float64(initialX) + (float64(width)*(vpp[0]+1))/2
	win[1] = float64(initialY) + (float64(height)*(vpp[1]+1))/2
	win[2] = (vpp[2] + 1) / 2

	return win
}
*/

// Transform a set of coordinates from object space (in obj) to window coordinates (with depth)
//
// Window coordinates are continuous, not discrete (well, as continuous as an IEEE Floating Point can be), so you won't get exact pixel locations
// without rounding or similar
func Project(obj Vec3, modelview, projection Mat4, initialX, initialY, width, height int) (win Vec3) {
	// extend to Vec4 homogenious coords
	obj4 := obj.ToVec4()
	obj4[3] = 1

	vpp := projection.Mul(modelview).MulVec4(obj4)
	vpp = vpp.MulScalar(1 / vpp.W())
	// win x
	win[0] = float64(initialX) + (float64(width)*(vpp[0]+1))/2
	// win y
	win[1] = float64(initialY) + (float64(height)*(vpp[1]+1))/2
	// win z
	win[2] = (vpp[2] + 1) / 2

	return win
}

var _ = `

// Transform a set of window coordinates to object space. If your MVP (projection.Mul(modelview) matrix is not invertible, this will return an error
//
// Note that the projection may not be perfect if you use strict pixel locations rather than the exact values given by Projectf.
// (It's still unlikely to be perfect due to precision errors, but it will be closer)
func UnProject(win Vec3, modelview, projection Mat4, initialX, initialY, width, height int) (obj Vec3, err error) {
	inv := projection.Mul4(modelview).Inv()
	var blank Mat4
	if inv == blank {
		return Vec3{}, errors.New("Could not find matrix inverse (projection times modelview is probably non-singular)")
	}

	obj4 := inv.Mul4x1(Vec4{
		(2 * (win[0] - float64(initialX)) / float64(width)) - 1,
		(2 * (win[1] - float64(initialY)) / float64(height)) - 1,
		2*win[2] - 1,
		1.0,
	})
	obj = obj4.Vec3()

	//if obj4[3] > MinValue {}
	obj[0] /= obj4[3]
	obj[1] /= obj4[3]
	obj[2] /= obj4[3]

	return obj, nil
}
`

// Untested. (Upd: test was added and passed)
// XXX upd: note win coords are not NDC (should be checked?)
// Transform a set of window coordinates to object space. If your MVP (projection.Mul(modelview) matrix is not invertible, this will return an error
func UnProject(win Vec3, modelview, projection Mat4, initialX, initialY, width, height int) (obj Vec3) {
	inv := projection.Mul(modelview).Invert()

	// This code is ignored
	_ = `
	var blank Mat4
	if inv == blank {
		return Vec3{}, errors.New("Could not find matrix inverse (projection times modelview is probably non-singular)")
	}
	`

	obj4 := inv.MulVec4(Vec4{
		(2 * (win[0] - float64(initialX)) / float64(width)) - 1,
		(2 * (win[1] - float64(initialY)) / float64(height)) - 1,
		2*win[2] - 1,
		1.0,
	})
	obj = obj4.ToVec3()

	//if obj4[3] > MinValue {}
	obj[0] /= obj4[3]
	obj[1] /= obj4[3]
	obj[2] /= obj4[3]

	return obj
}
