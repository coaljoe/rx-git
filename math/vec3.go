package math

// based on linmath.h: https://github.com/datenwolf/linmath.h

import (
	"fmt"
	"math"
)

var (
	Vec3Zero  = Vec3{0, 0, 0}
	Vec3One   = Vec3{1, 1, 1}
	Vec3XUnit = Vec3{1, 0, 0}
	Vec3YUnit = Vec3{0, 1, 0}
	Vec3ZUnit = Vec3{0, 0, 1}
	// z-up, iso rpy
	Vec3Up      = Vec3{0, 0, 1}
	Vec3Forward = Vec3{1, 0, 0}
	Vec3Left    = Vec3{0, 1, 0}
	Vec3Right   = Vec3{0, -1, 0}
)

type Vec3 [3]float64

func (v Vec3) X() float64      { return v[0] }
func (v *Vec3) SetX(x float64) { v[0] = x }
func (v Vec3) Y() float64      { return v[1] }
func (v *Vec3) SetY(x float64) { v[1] = x }
func (v Vec3) Z() float64      { return v[2] }
func (v *Vec3) SetZ(x float64) { v[2] = x }

func (a Vec3) Add(b Vec3) Vec3 {
	return Vec3{a[0] + b[0], a[1] + b[1], a[2] + b[2]}
}

func (v Vec3) AddScalar(n float64) Vec3 {
	return Vec3{v[0] + n, v[1] + n, v[2] + n}
}

func (a Vec3) Sub(b Vec3) Vec3 {
	return Vec3{a[0] - b[0], a[1] - b[1], a[2] - b[2]}
}

func (v Vec3) SubScalar(n float64) Vec3 {
	return Vec3{v[0] - n, v[1] - n, v[2] - n}
}

func (v Vec3) Scale(s float64) Vec3 {
	return Vec3{v[0] * s, v[1] * s, v[2] * s}
}

// Dot product.
func (a Vec3) MulInner(b Vec3) float64 {
	return a[0]*b[0] + a[1]*b[1] + a[2]*b[2]
}

func (a Vec3) Mul(b Vec3) Vec3 {
	return Vec3{a[0] * b[0], a[1] * b[1], a[2] * b[2]}
}

func (v Vec3) MulScalar(n float64) Vec3 {
	return v.Scale(n)
}

func (v Vec3) Len() float64 {
	return math.Sqrt(v.MulInner(v))
}

func (v Vec3) Norm() Vec3 {
	k := 1.0 / v.Len()
	return v.Scale(k)
}

func (a Vec3) MulCross(b Vec3) Vec3 {
	return Vec3{
		a[1]*b[2] - a[2]*b[1],
		a[2]*b[0] - a[0]*b[2],
		a[0]*b[1] - a[1]*b[0],
	}
}

func (a Vec3) Reflect(b Vec3) Vec3 {
	// warn: slower than native vec3
	// warn: not tested
	a1 := a.ToVec4()
	b1 := b.ToVec4()
	return a1.Reflect(b1).ToVec3()
}

func (v Vec3) Degrees() Vec3 {
	return Vec3{Degrees(v[0]), Degrees(v[1]), Degrees(v[2])}
}

func (v Vec3) Radians() Vec3 {
	return Vec3{Radians(v[0]), Radians(v[1]), Radians(v[2])}
}

func (a Vec3) Equal(b Vec3) bool {
	return Equal(a[0], b[0]) && Equal(a[1], b[1]) && Equal(a[2], b[2])
}

func (a Vec3) ApproxEqual(b Vec3, e float64) bool {
	return ApproxEqual(a[0], b[0], e) && ApproxEqual(a[1], b[1], e) && ApproxEqual(a[2], b[2], e)
}

// Component-wise GreaterThan
func (a Vec3) Greater(b Vec3) bool {
	return a[0] > b[0] && a[1] > b[1] && a[2] > b[2]
}

// Component-wise LessThan
func (a Vec3) Less(b Vec3) bool {
	return a[0] < b[0] && b[1] < a[1] && b[2] < a[2]
}

func (v Vec3) IsNaN() bool {
	return math.IsNaN(v[0]) || math.IsNaN(v[1]) || math.IsNaN(v[2])
}

func (v Vec3) ToVec4() Vec4 {
	return Vec4{v[0], v[1], v[2], 1.0}
}

func (v Vec3) ToVec2() Vec2 {
	return Vec2{v[0], v[1]}
}

// Modify vector in-place.
func (v *Vec3) SetInPlace(x, y, z float64) {
	*v = Vec3{x, y, z}
}

func (v Vec3) Set(x, y, z float64) Vec3 {
	return Vec3{x, y, z}
}

// Unpack vector.
func (v Vec3) Elem() [3]float64 {
	return [3]float64{v[0], v[1], v[2]}
}

func (v Vec3) String() string {
	return fmt.Sprintf("Vec3(X=%.5f, Y=%.5f, Z=%.5f)", v[0], v[1], v[2])
}
