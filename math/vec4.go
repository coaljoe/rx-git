package math

// based on linmath.h: https://github.com/datenwolf/linmath.h

import (
	"fmt"
	"math"
)

var (
	Vec4Zero = Vec4{0, 0, 0, 0}
	Vec4One  = Vec4{1, 1, 1, 1}
)

type Vec4 [4]float64

func (v Vec4) X() float64      { return v[0] }
func (v *Vec4) SetX(x float64) { v[0] = x }
func (v Vec4) Y() float64      { return v[1] }
func (v *Vec4) SetY(x float64) { v[1] = x }
func (v Vec4) Z() float64      { return v[2] }
func (v *Vec4) SetZ(x float64) { v[2] = x }
func (v Vec4) W() float64      { return v[3] }
func (v *Vec4) SetW(x float64) { v[3] = x }

func (a Vec4) Add(b Vec4) Vec4 {
	return Vec4{a[0] + b[0], a[1] + b[1], a[2] + b[2], a[3] + b[3]}
}

func (v Vec4) AddScalar(n float64) Vec4 {
	return Vec4{v[0] + n, v[1] + n, v[2] + n, v[3] + n}
}

func (a Vec4) Sub(b Vec4) Vec4 {
	return Vec4{a[0] - b[0], a[1] - b[1], a[2] - b[2], a[3] - b[3]}
}

func (v Vec4) SubScalar(n float64) Vec4 {
	return Vec4{v[0] - n, v[1] - n, v[2] - n, v[3] - n}
}

func (v Vec4) Scale(s float64) Vec4 {
	return Vec4{v[0] * s, v[1] * s, v[2] * s, v[3] * s}
}

func (a Vec4) MulInner(b Vec4) float64 {
	return a[0]*b[0] + a[1]*b[1] + a[2]*b[2] + a[3]*b[3]
}

func (a Vec4) Mul(b Vec4) Vec4 {
	return Vec4{a[0] * b[0], a[1] * b[1], a[2] * b[2], a[3] * b[3]}
}

func (v Vec4) MulScalar(n float64) Vec4 {
	return v.Scale(n)
}

func (v Vec4) Len() float64 {
	return math.Sqrt(v.MulInner(v))
}

func (v Vec4) Norm() Vec4 {
	k := 1.0 / v.Len()
	return v.Scale(k)
}

func (a Vec4) Min(b Vec4) Vec4 {
	var r Vec4
	if a[0] < b[0] {
		r[0] = a[0]
	} else {
		r[0] = b[0]
	}
	if a[1] < b[1] {
		r[1] = a[1]
	} else {
		r[1] = b[1]
	}
	if a[2] < b[2] {
		r[2] = a[2]
	} else {
		r[2] = b[2]
	}
	if a[3] < b[3] {
		r[3] = a[3]
	} else {
		r[3] = b[3]
	}
	return r
}

func (a Vec4) Max(b Vec4) Vec4 {
	var r Vec4
	if a[0] > b[0] {
		r[0] = a[0]
	} else {
		r[0] = b[0]
	}
	if a[1] > b[1] {
		r[1] = a[1]
	} else {
		r[1] = b[1]
	}
	if a[2] > b[2] {
		r[2] = a[2]
	} else {
		r[2] = b[2]
	}
	if a[3] > b[3] {
		r[3] = a[3]
	} else {
		r[3] = b[3]
	}
	return r
}

// Multiply vector by matrix.
func (v Vec4) MulMat4(m Mat4) Vec4 {
	return Vec4{
		v.X()*m[0][0] + v.Y()*m[0][1] + v.Z()*m[0][2] + v.W()*m[0][3],
		v.X()*m[1][0] + v.Y()*m[1][1] + v.Z()*m[1][2] + v.W()*m[1][3],
		v.X()*m[2][0] + v.Y()*m[2][1] + v.Z()*m[2][2] + v.W()*m[2][3],
		v.X()*m[3][0] + v.Y()*m[3][1] + v.Z()*m[3][2] + v.W()*m[3][3]}
}

func (v Vec4) Reflect(n Vec4) Vec4 {
	var p = 2.0 * v.MulInner(n)
	return Vec4{
		v[0] - p*n[0],
		v[1] - p*n[1],
		v[2] - p*n[2],
		v[3] - p*n[3]}
}

func (a Vec4) Equal(b Vec4) bool {
	return Equal(a[0], b[0]) && Equal(a[1], b[1]) && Equal(a[2], b[2]) && Equal(a[3], b[3])
}

func (a Vec4) ApproxEqual(b Vec4, e float64) bool {
	return ApproxEqual(a[0], b[0], e) && ApproxEqual(a[1], b[1], e) &&
		ApproxEqual(a[2], b[2], e) && ApproxEqual(a[3], b[3], e)
}

func (a Vec4) Greater(b Vec4) bool {
	return a[0] > b[0] && a[1] > b[1] && a[2] > b[2] && a[3] > b[3]
}

func (a Vec4) Less(b Vec4) bool {
	return a[0] < b[0] && a[1] < b[1] && a[2] < b[2] && a[3] < b[3]
}

func (v Vec4) IsNaN() bool {
	return math.IsNaN(v[0]) || math.IsNaN(v[1]) || math.IsNaN(v[2]) || math.IsNaN(v[3])
}

func (v Vec4) ToVec3() Vec3 {
	return Vec3{v[0], v[1], v[2]}
}

func (v Vec4) ToVec2() Vec2 {
	return Vec2{v[0], v[1]}
}

// Unpack vector.
func (v Vec4) Elem() [4]float64 {
	return [4]float64{v[0], v[1], v[2], v[3]}
}

func (v Vec4) String() string {
	return fmt.Sprintf("Vec4(X=%.5f, Y=%.5f, Z=%.5f, W=%.5f)", v[0], v[1], v[2], v[3])
}
