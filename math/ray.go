package math

import "fmt"

type Ray struct {
	Origin    Vec3
	Direction Vec3
}

func NewRay(origin, direction Vec3) Ray {
	return Ray{origin, direction}
}

func (r Ray) Set(origin, direction Vec3) Ray {
	return Ray{origin, direction}
}

// Not tested.
func (r Ray) At(t float64) Vec3 {
	return r.Direction.MulScalar(t).Add(r.Origin)
}

func (r Ray) String() string {
	return fmt.Sprintf("Ray(Origin: %s, Direction: %s)", r.Origin, r.Direction)
}
