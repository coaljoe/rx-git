// TODO: Move to geom package?
package math

// True if point is inside the rectange.
func InRect(x, y, rx, ry, rw, rh float64) bool {
	if x >= rx && x <= rx+rw &&
		y >= ry && y <= ry+rh {
		return true
	} else {
		return false
	}
}
