package math

import (
	"fmt"
	"testing"
)

func TestMat(t *testing.T) {
	a := Mat4Identity
	b := Mat4{
		{1., 0., 0., 0.},
		{0., 1., 0., 0.},
		{0., 0., 1., 0.},
		{0., 0., 0., 1.}}

	if !a.Equal(b) {
		t.Fail()
	}
}

func TestMat1(t *testing.T) {
	a := Mat4Identity
	b := Mat4{
		{1., 0., 0., 0.},
		{0., 1., 0., 0.},
		{0., 0., 1., 0.},
		{0., 0., 0., 1.}}

	if !a.Equal(b) {
		t.Fail()
	}

	b = b.Translate(2, 2, 0)

	fmt.Println("a:", a)
	fmt.Println("b:", b)
}

func TestMat2(t *testing.T) {
	a := Mat4Identity
	b := Mat4{ // column-major
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{2, 3, 4, 1}}

	a = a.Translate(2, 3, 4)

	fmt.Println("a:", a, a.Data())
	fmt.Println("b:", b, b.Data())

	if !a.Equal(b) {
		t.Fail()
	}
}

func TestMat3(t *testing.T) {
	a := Mat4Identity
	//a.SetTranslation(1, 2, 3)
	a.SetTranslation(1, 0, 0)

	fmt.Println(a)
	fmt.Println(a.Data())
}

func TestRotateX(t *testing.T) {
	a := Mat4Identity
	b := Mat4{
		{1, 0, 0, 0},
		{0, 0.707107, 0.707107, 0},
		{0, -0.707107, 0.707107, 0},
		{0, 0, 0, 1}}

	/*	glm output:
		mat4x4((1.000000, 0.000000, 0.000000, 0.000000),
			   (0.000000, 0.707107, 0.707107, 0.000000),
			   (0.000000, -0.707107, 0.707107, 0.000000),
			   (0.000000, 0.000000, 0.000000, 1.000000))
	*/

	a = a.RotateX(Radians(45))

	fmt.Println("a:", a, a.Data())
	fmt.Println("b:", b, b.Data())

	if !a.ApproxEqual(b, 1.0E-4) {
		t.Fail()
	}
}

func TestRotateY(t *testing.T) {
	a := Mat4Identity
	b := Mat4{
		{0.707107, 0, -0.707107, 0},
		{0, 1, 0, 0},
		{0.707107, 0, 0.707107, 0},
		{0, 0, 0, 1}}

	/*	glm output:
		mat4x4((0.707107, 0.000000, -0.707107, 0.000000),
			   (0.000000, 1.000000, 0.000000, 0.000000),
			   (0.707107, 0.000000, 0.707107, 0.000000),
			   (0.000000, 0.000000, 0.000000, 1.000000))
	*/

	a = a.RotateY(Radians(45))

	fmt.Println("a:", a, a.Data())
	fmt.Println("b:", b, b.Data())

	if !a.ApproxEqual(b, 1.0E-4) {
		t.Fail()
	}
}

func TestRotateZ(t *testing.T) {
	a := Mat4Identity
	b := Mat4{
		{0.707107, 0.707107, 0, 0},
		{-0.707107, 0.707107, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1}}

	/*	glm output:
		mat4x4((0.707107, 0.707107, 0.000000, 0.000000),
		       (-0.707107, 0.707107, 0.000000, 0.000000),
			   (0.000000, 0.000000, 1.000000, 0.000000),
			   (0.000000, 0.000000, 0.000000, 1.000000))
	*/

	a = a.RotateZ(Radians(45))

	fmt.Println("a:", a, a.Data())
	fmt.Println("b:", b, b.Data())

	if !a.ApproxEqual(b, 1.0E-4) {
		t.Fail()
	}
}

func TestRotateXYZ(t *testing.T) {
	a := Mat4Identity
	b := Mat4{
		{0.500000, 0.853553, 0.146447, 0},
		{-0.500000, 0.146447, 0.853553, 0},
		{0.707107, -0.500000, 0.500000, 0},
		{0, 0, 0, 1}}

	/*	glm output:
		mat4x4((0.500000, 0.853553, 0.146447, 0.000000),
			   (-0.500000, 0.146447, 0.853553, 0.000000),
			   (0.707107, -0.500000, 0.500000, 0.000000),
			   (0.000000, 0.000000, 0.000000, 1.000000))

	*/

	/*
		a = a.RotateX(Radians(45))
		a = a.RotateY(Radians(45))
		a = a.RotateZ(Radians(45))
	*/
	a = a.RotateX(Radians(45)).RotateY(Radians(45)).RotateZ(Radians(45))

	fmt.Println("a:", a, a.Data())
	fmt.Println("b:", b, b.Data())

	if !a.ApproxEqual(b, 1.0E-4) {
		t.Fail()
	}
}

func TestInv(t *testing.T) {
	a := Mat4{
		// pre inv
		{0.500000, 0.853553, 0.146447, 0},
		{-0.500000, 0.146447, 0.853553, 0},
		{0.707107, -0.500000, 0.500000, 0},
		{2, 3, 4, 1}}
	b := Mat4{
		// after inv
		{0.500000, -0.500000, 0.707107, -0.000000},
		{0.853553, 0.146447, -0.500000, 0.000000},
		{0.146447, 0.853553, 0.500000, -0.000000},
		{-4.146447, -2.853554, -1.914214, 1.000000}}

	/*	glm output:
		rot: 45, 45, 45; trans: 2, 3, 4; inv; ->

		mat4x4((0.500000, -0.500000, 0.707107, -0.000000),
			   (0.853553, 0.146447, -0.500000, 0.000000),
			   (0.146447, 0.853553, 0.500000, -0.000000),
			   (-4.146447, -2.853554, -1.914214, 1.000000))


	*/

	a = a.Invert()

	fmt.Println("a:", a, a.Data())
	fmt.Println("b:", b, b.Data())

	if !a.ApproxEqual(b, 1.0E-4) {
		t.Fail()
	}
}

/*
func TestMatRot(t *testing.T) {
	//a := Mat4Identity.
	//angle := Radians(45)
	angle := Radians(90)
	axis := Vec3{1, 0, 0}
	a := HomogRotate3D(angle, axis)
	b := Mat4{
		1., 0., 0., 0.,
		0., 0., 1., 0.,
		0., -1., 0., 0.,
		0., 0., 0., 1.}

	fmt.Println("a:")
	fmt.Println(a)
	fmt.Println("b:")
	fmt.Println(b)
	fmt.Println("ApproxEqual: ", a.ApproxEqual(b))
	fmt.Println(a.Rotation())
	fmt.Println(a.Rotation().Degrees()) // fixme
	if !a.ApproxEqual(b) {
		t.Fail()
	}
}
*/

func TestUpper3(t *testing.T) {
	a := Mat4{}.Fill(1)
	b := Mat4{
		{1, 1, 1, 0},
		{1, 1, 1, 0},
		{1, 1, 1, 0},
		{0, 0, 0, 1}}

	a = a.Upper3()

	if !a.Equal(b) {
		t.Fail()
	}
}
