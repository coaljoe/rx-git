package math

import (
	"fmt"
	"testing"
)

/*
func TestProject(t *testing.T) {
	t.Parallel()

	obj := Vec3{1002, 960, 0}
	modelview := Mat4{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 203, 1, 0, 1}
	projection := Mat4{0.0013020833721384406, 0, 0, 0, -0, -0.0020833334419876337, -0, -0, -0, -0, -1, -0, -1, 1, 0, 1}
	initialX, initialY, width, height := 0, 0, 1536, 960
	win := Project(obj, modelview, projection, initialX, initialY, width, height)
	answer := Vec3{1205.0000359117985, -1.0000501200556755, 0.5} // From glu.Project()

	if !win.ApproxEqualThreshold(answer, 1e-4) {
		t.Errorf("Project does something weird, differs from expected by of %v", win.Sub(answer).Len())
	}

	objr, err := UnProject(win, modelview, projection, initialX, initialY, width, height)
	if err != nil {
		t.Errorf("UnProject returned error: %v", err)
	}
	if !objr.ApproxEqualThreshold(obj, 1e-4) {
		t.Errorf("UnProject(%v) != %v (got %v)", win, obj, objr)
	}
}
*/
func TestProject(t *testing.T) {
	obj := Vec3{1002, 960, 0}
	//obj := Vec3{1002, 0, 960}
	modelview := Mat4{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {203, 1, 0, 1}}
	projection := Mat4{
		{0.0013020833721384406, 0, 0, 0},
		{-0, -0.0020833334419876337, -0, -0},
		{-0, -0, -1, -0},
		{-1, 1, 0, 1}}
	initialX, initialY, width, height := 0, 0, 1536, 960
	win := Project(obj, modelview, projection, initialX, initialY, width, height)
	answer := Vec3{1205.0000359117985, -1.0000501200556755, 0.5} // From glu.Project()

	fmt.Println(win)
	fmt.Println(answer)

	//if !win.ApproxEqualThreshold(answer, 1e-4)
	if !win.Equal(answer) {
		t.Errorf("Project does something weird, differs from expected by of %v", win.Sub(answer).Len())
	}

	/*
		objr, err := UnProject(win, modelview, projection, initialX, initialY, width, height)
		if err != nil {
			t.Errorf("UnProject returned error: %v", err)
		}
		if !objr.ApproxEqualThreshold(obj, 1e-4) {
			t.Errorf("UnProject(%v) != %v (got %v)", win, obj, objr)
		}
	*/
}

func TestUnProject(t *testing.T) {
	vpW := 960
	vpH := 540
	win := Vec3{float64(vpW / 2), float64(vpH / 2), 0}

	// View?
	modelview := Mat4{
		{0.707107, -0.577350, 0.408248, 0.000000},
		{0.707107, 0.577350, -0.408248, 0.000000},
		{-0.000000, 0.577350, 0.816497, 0.000000},
		{-0.000000, -0.000000, -24.494896, 1.000000}}

	projection := Mat4{
		{1.0, 0, 0, 0},
		{0.0, 1.0, 0, 0},
		{0.0, 0.0, -1.0, 0},
		{0.0, 0, 0, 1.0}}
	initialX, initialY, width, height := 0, 0, vpW, vpH
	obj := UnProject(win, modelview, projection, initialX, initialY, width, height)
	answer := Vec3{10.408248, -10.408248, 20.816496}

	fmt.Println("obj:", obj)
	fmt.Println("answer:", answer)

	if !obj.ApproxEqual(answer, 1e-4) {
		//if !obj.Equal(answer) {
		t.Errorf("UnProject does something weird, differs from expected by of %v", obj.Sub(answer).Len())
	}

	/*
		objr, err := UnProject(win, modelview, projection, initialX, initialY, width, height)
		if err != nil {
			t.Errorf("UnProject returned error: %v", err)
		}
		if !objr.ApproxEqualThreshold(obj, 1e-4) {
			t.Errorf("UnProject(%v) != %v (got %v)", win, obj, objr)
		}
	*/
}
