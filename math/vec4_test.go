package math

// based on azul3d

import "testing"

func TestVec4Add(t *testing.T) {
	a := Vec4{1, 3, 3, 3}
	b := Vec4{1, 3, 3, 3}
	if !a.Add(b).Equal(Vec4{2, 6, 6, 6}) {
		t.Fail()
	}

	if !a.AddScalar(2).Equal(Vec4{3, 5, 5, 5}) {
		t.Fail()
	}
}

func TestVec4Sub(t *testing.T) {
	a := Vec4{1, 3, 3, 3}
	b := Vec4{1, 3, 3, 3}
	if !a.Sub(b).Equal(Vec4{0, 0, 0, 0}) {
		t.Fail()
	}

	if !a.SubScalar(2).Equal(Vec4{-1, 1, 1, 1}) {
		t.Fail()
	}
}

func TestVec4Mul(t *testing.T) {
	a := Vec4{1, 3, 3, 3}
	b := Vec4{1, 3, 3, 3}
	if !a.Mul(b).Equal(Vec4{1, 9, 9, 9}) {
		t.Fail()
	}

	if !a.MulScalar(2).Equal(Vec4{2, 6, 6, 6}) {
		t.Fail()
	}
}

func TestVec4MulMat4(t *testing.T) {
	a := Vec4{2, 3, 4, 5}
	b := Mat4Identity
	ar := Radians(45)
	b = b.RotateX(ar).RotateY(ar).RotateZ(ar)
	//fmt.Println(b)

	r := a.MulMat4(b)
	//fmt.Println(r)

	if !r.ApproxEqual(Vec4{4.146446, 2.853553, 1.914214, 5.000000}, 1.0E-4) {
		t.Fail()
	}
}

/*
func TestVec4Div(t *testing.T) {
	a := Vec4{1, 3, 3, 3}
	b := Vec4{1, 3, 3, 3}
	if !a.Div(b).Equal(Vec4{1, 1, 1, 1}) {
		t.Fail()
	}

	if !a.DivScalar(2).Equal(Vec4{0.5, 3.0 / 2.0, 3.0 / 2.0, 3.0 / 2.0}) {
		t.Fail()
	}
}
*/
