package math

// based on azul3d/lmath

import "math"

// The default epsilon value used for floating point comparisons.
const EPSILON = 1.0E-8 // 0.00000001

// ApproxEqual tells if the two floating point values x and y are considered
// equal within the specified absolute==relative tolerence value.
//
// The method of comparison used is that described at:
//  http://realtimecollisiondetection.net/blog/?p=89
func ApproxEqual(x, y, absTol float64) bool {
	if x == y || (math.Abs(x-y) <= absTol*math.Max(1.0, math.Max(math.Abs(x), math.Abs(y)))) {
		return true
	}
	return false
}

// Equal tells if the two floating point values a and b are considered equal
// within the default EPSILON comparison value. It is short-handed for:
//  ApproxEqual(a, b, EPSILON)
func Equal(a, b float64) bool {
	return ApproxEqual(a, b, EPSILON)
}

// Clamp returns the value v clamped to the range of [min, max].
func Clamp(v, min, max float64) float64 {
	return math.Max(math.Min(v, max), min)
}

// Radians converts from degrees to radians.
func Radians(degrees float64) float64 {
	return math.Pi * degrees / 180.0
}

// Degrees converts from radians to degrees.
func Degrees(radians float64) float64 {
	return radians * (180.0 / math.Pi)
}

// Rounded returns the value rounded to the nearest whole number.
//func Round(v float64) float64 {
//	if v < 0 {
//		return math.Ceil(v - 0.5)
//	}
//	return math.Floor(v + 0.5)
//}

// Lerp performs a linear interpolation between a and b. The t parameter is a
// number in the range 0.0-1.0. Some examples:
//
//  Lerp(0, 10, 0) == 0
//  Lerp(0, 10, 0.5) == 5
//  Lerp(0, 10, 1) == 10
//
// The interpolation method is precise, so it is guaranteed that:
//  Lerp(a, b, 1) == a
func Lerp(a, b, t float64) float64 {
	return (1-t)*a + t*b
}

func Lerp2(a, b Vec2, t float64) Vec2 {
	return Vec2{Lerp(a.X(), b.X(), t),
		Lerp(a.Y(), b.Y(), t)}
}

func Lerp3(a, b Vec3, t float64) Vec3 {
	return Vec3{Lerp(a.X(), b.X(), t),
		Lerp(a.Y(), b.Y(), t),
		Lerp(a.Z(), b.Z(), t)}
}
