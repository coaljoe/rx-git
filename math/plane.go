// An infinite 3d plane.
package math

import (
	"fmt"
	"math"
)

type Plane struct {
	Normal   Vec3    // Plane's normal
	Distance float64 // Distance from the origin
}

func NewPlane(normal Vec3, distance float64) Plane {
	return Plane{Normal: normal, Distance: distance}
}

// Test Ray-Plane intersection.
// Returns (true, intersection_point) if hit success, (false, Vec3Zero) if fail.
func (p Plane) IntersectsRay(r Ray) (bool, Vec3) {
	// From:
	// https://github.com/godotengine/godot/blob/master/core/math/plane.cpp#L105
	ro := r.Origin
	rd := r.Direction

	segment := rd
	den := p.Normal.MulInner(segment)
	if math.Abs(den) <= EPSILON {
		return false, Vec3Zero
	}

	dist := (p.Normal.MulInner(ro) - p.Distance) / den
	if dist > EPSILON {
		return false, Vec3Zero
	}

	dist = -dist
	p_intersection := ro.Add((segment.MulScalar(dist)))
	return true, p_intersection
}

func (p Plane) String() string {
	return fmt.Sprintf("Plane(Normal: %s, Distance: %.5f)", p.Normal, p.Distance)
}
