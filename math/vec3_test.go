package math

// based on azul3d/lmath

import (
	"testing"
)

func TestVec3Add(t *testing.T) {
	a := Vec3{1, 3, 3}
	b := Vec3{1, 3, 3}
	if !a.Add(b).Equal(Vec3{2, 6, 6}) {
		t.Fail()
	}

	if !a.AddScalar(2).Equal(Vec3{3, 5, 5}) {
		t.Fail()
	}
}

func TestVec3Sub(t *testing.T) {
	a := Vec3{1, 3, 3}
	b := Vec3{1, 3, 3}
	if !a.Sub(b).Equal(Vec3{0, 0, 0}) {
		t.Fail()
	}

	if !a.SubScalar(2).Equal(Vec3{-1, 1, 1}) {
		t.Fail()
	}
}

func TestVec3Mul(t *testing.T) {
	a := Vec3{1, 3, 3}
	b := Vec3{1, 3, 3}
	if !a.Mul(b).Equal(Vec3{1, 9, 9}) {
		t.Fail()
	}

	if !a.MulScalar(2).Equal(Vec3{2, 6, 6}) {
		t.Fail()
	}
}

func TestVec3Equal(t *testing.T) {
	if !(Vec3{1, 1, 1}.Equal(Vec3{1, 1, 1})) {
		t.Fail()
	}

	if (Vec3{1, 1, .99}.Equal(Vec3{1, 1, 1})) {
		t.Fail()
	}

	if !(Vec3{0.1234, 0, 0}.ApproxEqual(Vec3{0.1235, 0, 0}, 0.001)) {
		t.Fail()
	}

	if (Vec3{0.1234, 0, 0}.ApproxEqual(Vec3{0.1235, 0, 0}, 0.0001)) {
		t.Fail()
	}
}

func TestVec3Less(t *testing.T) {
	if (Vec3{0.5, 0.5, 0.5}).Less(Vec3{-0.5, -0.5, -0.5}) != false {
		t.Fail()
	}

	if !(Vec3{0.5, 0.5, 0.5}).Greater(Vec3{-0.5, -0.5, -0.5}) {
		t.Fail()
	}

	// Uncomment to fail. Fixme: use AnyLessThan?
	//
	// >>> -0.5 < 0.5 and -0.5 < 0.5 and -0.5 < -0.5
	// False
	// >>> -0.5 < 0.5 or -0.5 < 0.5 or -0.5 < -0.5
	// True
	//
	// Question: Should it fail or not?
	/*
		if !(Vec3{0.5, 0.5, -0.5}).Less(Vec3{-0.5, -0.5, -0.5}) {
			t.Fail()
		}
	*/
}

func TestVec3Set(t *testing.T) {
	v1 := Vec3{0.5, 0.5, 0.5}
	v1.SetInPlace(1, 1, 1)
	if v1 != (Vec3{1, 1, 1}) {
		t.Fail()
	}
	if v1.Set(1, v1.X()/2, 3) != (Vec3{1, 0.5, 3}) {
		t.Fail()
	}
}

/*
func TestVec3Div(t *testing.T) {
	a := Vec3{1, 3, 3}
	b := Vec3{1, 3, 3}
	if !a.Div(b).Equal(Vec3{1, 1, 1}) {
		t.Fail()
	}

	if !a.DivScalar(2).Equal(Vec3{0.5, 3.0 / 2.0, 3.0 / 2.0}) {
		t.Fail()
	}
}
*/
func BenchmarkVec3Equals(b *testing.B) {
	x := Vec3{1, 3, 3}
	y := Vec3{1.33, 3.33, 3.33}
	for n := 0; n < b.N; n++ {
		x.Equal(y)
	}
}
