package math

import (
	"testing"
)

func TestVec2Add(t *testing.T) {
	a := Vec2{1, 3}
	b := Vec2{1, 3}
	if !a.Add(b).Equal(Vec2{2, 6}) {
		t.Fail()
	}

	if !a.AddScalar(2).Equal(Vec2{3, 5}) {
		t.Fail()
	}
}

func TestVec2Sub(t *testing.T) {
	a := Vec2{1, 3}
	b := Vec2{1, 3}
	if !a.Sub(b).Equal(Vec2{0, 0}) {
		t.Fail()
	}

	if !a.SubScalar(2).Equal(Vec2{-1, 1}) {
		t.Fail()
	}
}

func TestVec2Mul(t *testing.T) {
	a := Vec2{1, 3}
	b := Vec2{1, 3}
	if !a.Mul(b).Equal(Vec2{1, 9}) {
		t.Fail()
	}

	if !a.MulScalar(2).Equal(Vec2{2, 6}) {
		t.Fail()
	}
}

func TestVec2Equal(t *testing.T) {
	if !(Vec2{1, 1}.Equal(Vec2{1, 1})) {
		t.Fail()
	}

	if (Vec2{1, .99}.Equal(Vec2{1, 1})) {
		t.Fail()
	}

	if !(Vec2{0.1234, 0}.ApproxEqual(Vec2{0.1235, 0}, 0.001)) {
		t.Fail()
	}

	if (Vec2{0.1234, 0}.ApproxEqual(Vec2{0.1235, 0}, 0.0001)) {
		t.Fail()
	}
}
