package math

import (
	"fmt"
	"testing"
)

func TestPlane(t *testing.T) {
	var hit bool
	var point Vec3

	p := NewPlane(Vec3{0, 0, 1}, 0) // Z-up facing normal
	r := NewRay(Vec3{0, 0, 1}, Vec3{0, 0, -1})

	// should hit at 0,0,0
	hit, point = p.IntersectsRay(r)
	fmt.Println("hit:", hit, "point:", point)
	if !hit || (point != Vec3{0, 0, 0}) {
		t.Fail()
	}

	p = NewPlane(Vec3{0, 0, 1}, 1)
	//r = NewRay(Vec3{0, 0, 2}, Vec3{0, 0, 0}) // fail?
	//r = NewRay(Vec3{0, 0, 2}, Vec3{0, 0, -1})
	r = NewRay(Vec3{0, 0, 100}, Vec3{0, 0, -100})
	r = NewRay(Vec3{100, 100, 100}, Vec3{0, 0, -100}) // should fail

	// should hit at 0, 0, 1
	hit, point = p.IntersectsRay(r)
	fmt.Println("hit:", hit, "point:", point)
	if !hit || (point != Vec3{0, 0, 1}) {
		t.Fail()
	}
}
