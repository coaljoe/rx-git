package math

// Create ortho matrix. Same as glm::ortho.
func Ortho(left, right, bottom, top, znear, zfar float64) Mat4 {
	m := Mat4Identity
	m[0][0] = 2.0 / (right - left)
	m[1][1] = 2.0 / (top - bottom)
	m[2][2] = -2.0 / (zfar - znear)
	m[3][0] = -(right + left) / (right - left)
	m[3][1] = -(top + bottom) / (top - bottom)
	m[3][2] = -(zfar + znear) / (zfar - znear)
	m[3][3] = 1.0
	return m
}

// XXX not implement
func LookAt() {
	panic("not implemented")
}
