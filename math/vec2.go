package math

// based on linmath.h: https://github.com/datenwolf/linmath.h

import (
	"fmt"
	"math"
)

var (
	Vec2Zero  = Vec2{0, 0}
	Vec2One   = Vec2{1, 1}
	Vec2XUnit = Vec2{1, 0}
	Vec2YUnit = Vec2{0, 1}
)

type Vec2 [2]float64

func (v Vec2) X() float64      { return v[0] }
func (v *Vec2) SetX(x float64) { v[0] = x }
func (v Vec2) Y() float64      { return v[1] }
func (v *Vec2) SetY(x float64) { v[1] = x }

func (a Vec2) Add(b Vec2) Vec2 {
	return Vec2{a[0] + b[0], a[1] + b[1]}
}

func (v Vec2) AddScalar(n float64) Vec2 {
	return Vec2{v[0] + n, v[1] + n}
}

func (a Vec2) Sub(b Vec2) Vec2 {
	return Vec2{a[0] - b[0], a[1] - b[1]}
}

func (v Vec2) SubScalar(n float64) Vec2 {
	return Vec2{v[0] - n, v[1] - n}
}

func (v Vec2) Scale(s float64) Vec2 {
	return Vec2{v[0] * s, v[1] * s}
}

func (a Vec2) MulInner(b Vec2) float64 {
	return a[0]*b[0] + a[1]*b[1]
}

func (a Vec2) Mul(b Vec2) Vec2 {
	return Vec2{a[0] * b[0], a[1] * b[1]}
}

func (v Vec2) MulScalar(n float64) Vec2 {
	return v.Scale(n)
}

func (v Vec2) Len() float64 {
	return math.Sqrt(v.MulInner(v))
}

func (v Vec2) Norm() Vec2 {
	k := 1.0 / v.Len()
	return v.Scale(k)
}

func (a Vec2) Reflect(b Vec2) Vec2 {
	// warn: slower than native Vec2
	// warn: not tested
	a1 := a.ToVec4()
	b1 := b.ToVec4()
	return a1.Reflect(b1).ToVec2()
}

func (v Vec2) Degrees() Vec2 {
	return Vec2{Degrees(v[0]), Degrees(v[1])}
}

func (v Vec2) Radians() Vec2 {
	return Vec2{Radians(v[0]), Radians(v[1])}
}

func (a Vec2) Equal(b Vec2) bool {
	return Equal(a[0], b[0]) && Equal(a[1], b[1])
}

func (a Vec2) ApproxEqual(b Vec2, e float64) bool {
	return ApproxEqual(a[0], b[0], e) && ApproxEqual(a[1], b[1], e)
}

func (a Vec2) Greater(b Vec2) bool {
	return a[0] > b[0] && a[1] > b[1]
}

func (a Vec2) Less(b Vec2) bool {
	return a[0] < b[0] && a[1] < b[1]
}

func (v Vec2) IsNaN() bool {
	return math.IsNaN(v[0]) || math.IsNaN(v[1])
}

func (v Vec2) ToVec3() Vec3 {
	return Vec3{v[0], v[1], 0}
}

func (v Vec2) ToVec4() Vec4 {
	return Vec4{v[0], v[1], 0, 1}
}

// Unpack vector.
func (v Vec2) Elem() [2]float64 {
	return [2]float64{v[0], v[1]}
}

func (v Vec2) String() string {
	return fmt.Sprintf("Vec2(X=%.5f, Y=%.5f)", v[0], v[1])
}
