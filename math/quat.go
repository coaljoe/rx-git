package math

import (
	"fmt"
	"math"
)

var (
	QuatIdentity = Quat{0, 0, 0, 1}
)

type Quat [4]float64

func (q Quat) X() float64      { return q[0] }
func (q *Quat) SetX(x float64) { q[0] = x }
func (q Quat) Y() float64      { return q[1] }
func (q *Quat) SetY(x float64) { q[1] = x }
func (q Quat) Z() float64      { return q[2] }
func (q *Quat) SetZ(x float64) { q[2] = x }
func (q Quat) W() float64      { return q[3] }
func (q *Quat) SetW(x float64) { q[3] = x }

func (a Quat) Add(b Quat) Quat {
	return Quat{a[0] + b[0], a[1] + b[1], a[2] + b[2], a[3] + b[3]}
}

func (a Quat) Sub(b Quat) Quat {
	return Quat{a[0] - b[0], a[1] - b[1], a[2] - b[2], a[3] - b[3]}
}

func (q Quat) Scale(s float64) Quat {
	return Quat{q[0] * s, q[1] * s, q[2] * s, q[3] * s}
}

func (a Quat) Mul(b Quat) Quat {
	var w, r Vec3
	a1, b1 := a.ToVec3(), b.ToVec3()

	r = a1.MulCross(b1)
	w = a1.Scale(b[3])
	r = r.Add(w)
	w = b1.Scale(b[3])
	r = r.Add(w)

	// convert r to rr
	rr := Quat{r[0], r[1], r[2], 0}
	rr[3] = a[3]*b[3] - b1.MulInner(a1)

	return rr
}

func (a Quat) Rotate(angle float64, axis Vec3) Quat {
	var r Quat
	v := axis.Scale(math.Sin(angle / 2))
	for i := 0; i < 3; i++ {
		r[i] = v[i]
	}
	r[3] = math.Cos(angle / 2)
	return r
}

func Mat4FromQuat(q Quat) Mat4 {
	var M Mat4

	a := q[3]
	b := q[0]
	c := q[1]
	d := q[2]
	a2 := a * a
	b2 := b * b
	c2 := c * c
	d2 := d * d

	M[0][0] = a2 + b2 - c2 - d2
	M[0][1] = 2. * (b*c + a*d)
	M[0][2] = 2. * (b*d - a*c)
	M[0][3] = 0.

	M[1][0] = 2 * (b*c - a*d)
	M[1][1] = a2 - b2 + c2 - d2
	M[1][2] = 2. * (c*d + a*b)
	M[1][3] = 0.

	M[2][0] = 2. * (b*d + a*c)
	M[2][1] = 2. * (c*d - a*b)
	M[2][2] = a2 - b2 - c2 + d2
	M[2][3] = 0.

	M[3][0] = 0.0
	M[3][1] = 0.0
	M[3][2] = 0.0
	M[3][3] = 1.0

	return M
}

func QuatFromMat4(a Mat4) Quat {
	// Y-up version
	/*
			a = Mat4{
				{0.707107, 0.707107, 0.000000, 1.000000},
		 		{-0.707107,  0.707107, 0.000000, 1.000000},
		 		{0.000000,  0.000000, 1.000000, 0.000000},
		 		{0.000000,  0.000000, 0.000000, 1.000000}}

			// column major
			a = Mat4{
				{0.707107, 0.707107, 0.000000, 0.000000},
		 		{-0.707107,  0.707107, 0.000000, 0.000000},
		 		{0.000000,  0.000000, 1.000000, 0.000000},
		 		{0.000000,  0.000000, 0.000000, 0.000000}}

			// row major
			a = Mat4{
				{0.707107, -0.707107, 0.000000, 0.000000},
		 		{0.707107,  0.707107, 0.000000, 0.000000},
		 		{0.000000,  0.000000, 1.000000, 0.000000},
		 		{0.000000,  0.000000, 0.000000, 0.000000}}
	*/
	a = a.Transpose()

	//fmt.Println("a:")
	//fmt.Println(a)
	//panic(2)
	var w, x, y, z float64
	trace := a[0][0] + a[1][1] + a[2][2] // I removed + 1.0f; see discussion with Ethan
	if trace > 0 {                       // I changed M_EPSILON to 0
		s := 0.5 / math.Sqrt(trace+1.0)
		w = 0.25 / s
		x = (a[2][1] - a[1][2]) * s
		y = (a[0][2] - a[2][0]) * s
		z = (a[1][0] - a[0][1]) * s
	} else {
		if a[0][0] > a[1][1] && a[0][0] > a[2][2] {
			s := 2.0 * math.Sqrt(1.0+a[0][0]-a[1][1]-a[2][2])
			w = (a[2][1] - a[1][2]) / s
			x = 0.25 * s
			y = (a[0][1] + a[1][0]) / s
			z = (a[0][2] + a[2][0]) / s
		} else if a[1][1] > a[2][2] {
			s := 2.0 * math.Sqrt(1.0+a[1][1]-a[0][0]-a[2][2])
			w = (a[0][2] - a[2][0]) / s
			x = (a[0][1] + a[1][0]) / s
			y = 0.25 * s
			z = (a[1][2] + a[2][1]) / s
		} else {
			s := 2.0 * math.Sqrt(1.0+a[2][2]-a[0][0]-a[1][1])
			w = (a[1][0] - a[0][1]) / s
			x = (a[0][2] + a[2][0]) / s
			y = (a[1][2] + a[2][1]) / s
			z = 0.25 * s
		}
	}
	// swap y,z
	q := Quat{x, y, z, w}
	//fmt.Println("q:", q)
	return q
}

func QuatFromMat4_(M Mat4) Quat {
	var q Quat
	r := 0.0

	// fix matrix
	//M[0][3] = 0
	//M[1][3] = 0
	//M[2][3] = 0
	M.SetTranslation(0, 0, 0)
	//fmt.Println(M)
	//panic(2)

	perm := [5]int{0, 1, 2, 0, 1}
	//p := &perm
	p := perm[0:]

	for i := 0; i < 3; i++ {
		m := M[i][i]
		if m < r {
			continue
		}
		m = r
		//p = &perm[i]
		p = perm[i:]
	}

	r = math.Sqrt(1. + M[p[0]][p[0]] - M[p[1]][p[1]] - M[p[2]][p[2]])

	if r < 1e-6 {
		q[0] = 1.
		q[1] = 0.
		q[2] = 0.
		q[3] = 0.
		return q
	}

	q[0] = r / 2.
	q[1] = (M[p[0]][p[1]] - M[p[1]][p[0]]) / (2. * r)
	q[2] = (M[p[2]][p[0]] - M[p[0]][p[2]]) / (2. * r)
	q[3] = (M[p[2]][p[1]] - M[p[1]][p[2]]) / (2. * r)
	return q
}

func (q Quat) EulerAngles() Vec3 {
	// Derivation from http://www.geometrictools.com/Documentation/EulerAngles.pdf
	// Order of rotations: Z first, then X, then Y
	check := 2.0 * (-q.Y()*q.Z() + q.W()*q.X())
	M_RADTODEG := 1.0 / (math.Pi / 180.0)
	var r Vec3

	if check < -0.995 {
		r = Vec3{
			-90.0,
			0.0,
			-math.Atan2(2.0*(q.X()*q.Z()-q.W()*q.Y()),
				1.0-2.0*(q.Y()*q.Y()+q.Z()*q.Z())) * M_RADTODEG}
	} else if check > 0.995 {
		r = Vec3{
			90.0,
			0.0,
			math.Atan2(2.0*(q.X()*q.Z()-q.W()*q.Y()),
				1.0-2.0*(q.Y()*q.Y()+q.Z()*q.Z())) * M_RADTODEG}
	} else {
		r = Vec3{
			math.Asin(check) * M_RADTODEG,
			math.Atan2(2.0*(q.X()*q.Z()+q.W()*q.Y()), 1.0-2.0*(q.X()*q.X()+q.Y()*q.Y())) * M_RADTODEG,
			math.Atan2(2.0*(q.X()*q.Y()+q.W()*q.Z()), 1.0-2.0*(q.X()*q.X()+q.Z()*q.Z())) * M_RADTODEG}
	}

	/*
		m := Mat4{{1, 0, 0, 0},
			{0, 0, 1, 0},
			{0, 1, 0, 0},
			{0, 0, 0, 1}}

		return m.MulVec4(r.ToVec4()).ToVec3()
	*/
	return r
}

// return EulerAngles in degrees
func (q Quat) EulerAngles_() Vec3 {
	// method from
	// http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/
	// Z-up version(?)
	sqw := q.W() * q.W()
	sqx := q.X() * q.X()
	sqy := q.Y() * q.Y()
	sqz := q.Z() * q.Z()
	unit := sqx + sqy + sqz + sqw // if normalised is one, otherwise is correction factor
	test := q.X()*q.Y() + q.Z()*q.W()

	if test > 0.499*unit { // singularity at north pole
		return Vec3{
			2 * math.Atan2(q.X(), q.W()), // yaw
			math.Pi / 2,                  // pitch
			0}.                           // roll
			Degrees()
	}
	if test < -0.499*unit { // singularity at south pole
		return Vec3{
			-2 * math.Atan2(q.X(), q.W()), // yaw
			math.Pi / 2,                   // pitch
			0}.                            // roll
			Degrees()
	}
	return Vec3{
		math.Atan2(2*q.Y()*q.W()-2*q.X()*q.Z(), sqx-sqy-sqz+sqw),   // yaw
		math.Asin(2 * test / unit),                                 // pitch
		math.Atan2(2*q.X()*q.W()-2*q.Y()*q.Z(), -sqx+sqy-sqz+sqw)}. // roll
		Degrees()
}

/*
// Vec4.Len
func (q Quat) Len() float64 {
	return math.Sqrt(q.MulInner(q))
}


// Vec4.Norm
func (q Quat) Norm_() Quat {
	k := 1.0 / q.Len()
	return q.Scale(k)
}
*/

// Vec4.Norm
func (q Quat) Norm() Quat {
	v := Vec4(q)
	return Quat(v.Norm())
}

func (q Quat) ToVec3() Vec3 {
	return Vec3{q[0], q[1], q[2]}
}

func (q Quat) String() string {
	return fmt.Sprintf("Quat(X=%f, Y=%f, Z=%f, W=%f)", q[0], q[1], q[2], q[3])
}
