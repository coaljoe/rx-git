package math

// based on linmath.h: https://github.com/datenwolf/linmath.h

import (
	"bytes"
	"fmt"
	"math"
	"text/tabwriter"
)

var (
	Mat4Identity = Mat4{
		{1, 0, 0, 0}, // col 1
		{0, 1, 0, 0}, // col 2
		{0, 0, 1, 0}, // col 3
		{0, 0, 0, 1}} // col 4
	Mat4Zero = Mat4{}
)

// hw column-major order:
// {1 0 0 0, 0 1 0 0, 0 0 1 0, Tx Ty Tz 1}
/*
         c0  c1  c2  c3
        | 0   4   8  12 |
mat = 	| 1   5   9  13 |
        | 2   6  10  14 |
        | 3   7  11  15 |

        | 00  10  20  30 |
mat = 	| 01  11  21  31 |
col-    | 02  12  22  32 |
major   | 03  13  23  33 |

flat: {00 01 02 03, 10 11 12 13, 20 21 22 23, 30 31 32 33}

ij notation:
  1..x
  i,j = y,x

xy notation:
  0..x
  normal

mXX notation:
https://github.com/cristicbz/rust-doom/blob/master/src/math/mat.rs
*/
/*
opengl matrices:
  translate by x=1:
  column-major (right)
  1,0,0,0,
  0,1,0,0,
  0,0,1,0,
  1,0,0,1
  flat: 1,0,0,0,0,1,0,0,0,0,1,0,1,0,0,1
  row-major (wrong)
  1,0,0,1,
  0,1,0,0,
  0,0,1,0,
  0,0,0,1
  flat: 1,0,0,1,0,1,0,0,0,0,1,0,0,0,0,1
  -- flat c-arrays for glMultMatrix and such
*/

// col-major, stored in row-major(flat c-array)
type Mat4 [4]Vec4

func (m Mat4) Row(i int) Vec4 {
	return Vec4{m[0][i], m[1][i], m[2][i], m[3][i]}
}

func (m Mat4) Col(i int) Vec4 {
	return m[i]
}

// Return cleared upper 3x3 matrix.
// Note: the matrix will look like indentity matrix with extra 1 at bottom right corner.
func (m Mat4) Upper3() Mat4 {
	m[3] = Vec4{0, 0, 0, 1}
	m[0][3] = 0
	m[1][3] = 0
	m[2][3] = 0
	return m
}

func (a Mat4) Add(b Mat4) Mat4 {
	return Mat4{a[0].Add(b[0]), a[1].Add(b[1]), a[2].Add(b[2]), a[3].Add(b[3])}
}

func (a Mat4) Sub(b Mat4) Mat4 {
	return Mat4{a[0].Sub(b[0]), a[1].Sub(b[1]), a[2].Sub(b[2]), a[3].Sub(b[3])}
}

func (a Mat4) Scale(k float64) Mat4 {
	return Mat4{a[0].Scale(k), a[1].Scale(k), a[2].Scale(k), a[3].Scale(k)}
}

func (a Mat4) Mul(b Mat4) Mat4 {
	var temp Mat4
	//var k, r, c int
	for c := 0; c < 4; c++ {
		for r := 0; r < 4; r++ {
			temp[c][r] = 0.0
			for k := 0; k < 4; k++ {
				temp[c][r] += a[k][r] * b[c][k]
			}
		}
	}
	return temp
}

func (m Mat4) MulVec4(v Vec4) Vec4 {
	var r Vec4
	//var i, j int
	for j := 0; j < 4; j++ {
		//r[j] = 0.0
		for i := 0; i < 4; i++ {
			r[j] += m[i][j] * v[i]
		}
	}
	return r
}

// Multiply upper 3x3 matrix.
func (a Mat4) MulUpper3(b Mat4) Mat4 {
	temp := Mat4Identity
	for c := 0; c < 3; c++ {
		for r := 0; r < 3; r++ {
			temp[c][r] = 0.0
			for k := 0; k < 3; k++ {
				temp[c][r] += a[k][r] * b[c][k]
			}
		}
	}
	return temp
}

// Multiply upper 3x3 matrix by Vec3.
func (m Mat4) MulVec3(v Vec3) Vec3 {
	var r Vec3
	xv := v.ToVec4()
	for j := 0; j < 3; j++ {
		//r[j] = 0.0
		for i := 0; i < 4; i++ {
			r[j] += m[i][j] * xv[i]
		}
	}

	//fmt.Println("r:", r)
	//r2 := m.MulVec4(v.ToVec4()).ToVec3()
	//fmt.Println("r2:", r2)
	return r
}

// Translate matrix.
func (m Mat4) Translate(x, y, z float64) Mat4 {
	t := Mat4Identity // fixme?
	t[3][0] = x
	t[3][1] = y
	t[3][2] = z
	return t
}

// Update translation of the matrix.
func (m *Mat4) SetTranslation(x, y, z float64) {
	m[3][0] = x
	m[3][1] = y
	m[3][2] = z
}

func (m Mat4) Translate_(x, y, z float64) Mat4 {
	t := Mat4Identity
	t[0][3] = x
	t[1][3] = y
	t[2][3] = z
	return t
}

// fixme: make pure?
// warn: not checked
func (m *Mat4) TranslateInPlace(x, y, z float64) {
	t := Vec4{x, y, z, 0}
	for i := 0; i < 4; i++ {
		r := m.Row(i)
		m[3][i] += r.MulInner(t)
	}
}

// fixme?
func (m Mat4) FromVec3MulOuter(a, b Vec3) Mat4 {
	var M Mat4
	for i := 0; i < 4; i++ {
		for j := 0; j < 4; j++ {
			if i < 3 && j < 3 {
				M[i][j] = a[i] * b[j]
			} else {
				M[i][j] = 0.0
			}
		}
	}
	return M
}

// Rotate around axis x, y, z by angle
func (m Mat4) Rotate(x, y, z, angle float64) Mat4 {
	c, s := math.Sincos(angle)
	u := Vec3{x, y, z}
	var R Mat4

	if u.Len() > 1e-4 {
		u := u.Norm()
		var T Mat4
		T = T.FromVec3MulOuter(u, u)

		S := Mat4{
			{0, u[2], -u[1], 0},
			{-u[2], 0, u[0], 0},
			{u[1], -u[0], 0, 0},
			{0, 0, 0, 0}}
		S = S.Scale(s)

		C := Mat4Identity
		C = C.Sub(T)

		C = C.Scale(c)

		T = T.Add(C)
		T = T.Add(S)

		T[3][3] = 1.
		R = m.Mul(T)
	} else {
		R = m
	}
	return R
}

// Takes radians.
func (m Mat4) RotateX(angle float64) Mat4 {
	s, c := math.Sincos(angle)

	r := Mat4{
		{1, 0, 0, 0},
		{0, c, s, 0},
		{0, -s, c, 0},
		{0, 0, 0, 1}}

	return m.Mul(r) // should be in-place?
}

// Takes radians.
func (m Mat4) RotateY(angle float64) Mat4 {
	s, c := math.Sincos(angle)
	/*
		// wrong
		r := Mat4{
			{c, 0, s, 0},
			{0, 1, 0, 0},
			{-s, 0, c, 0},
			{0, 0, 0, 1}}
	*/
	r := Mat4{
		{c, 0, -s, 0},
		{0, 1, 0, 0},
		{s, 0, c, 0},
		{0, 0, 0, 1}}

	return m.Mul(r) // should be in-place?
}

// Takes radians.
func (m Mat4) RotateZ(angle float64) Mat4 {
	s, c := math.Sincos(angle)

	r := Mat4{
		{c, s, 0, 0},
		{-s, c, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1}}

	return m.Mul(r) // should be in-place?
}

func (m Mat4) NewEulerRotation(yaw, pitch, roll float64) Mat4 {
	ca, sa := math.Sincos(yaw)   // swap?
	cb, sb := math.Sincos(pitch) // swap?
	cc, sc := math.Sincos(roll)

	return Mat4{
		{cb * cc, -cb * sc, sb, 0.0},
		{sa*sb*cc + ca*sc, -sa*sb*sc + ca*cc, -sa * cb, 0.0},
		{-ca*sb*cc + sa*sc, ca*sb*sc + sa*cc, ca * cb, 0.0},
		{0.0, 0.0, 0.0, 1.0}}
}

func (m Mat4) Invert() Mat4 {
	var s [6]float64
	var c [6]float64
	var T Mat4
	M := m.Dup()

	s[0] = M[0][0]*M[1][1] - M[1][0]*M[0][1]
	s[1] = M[0][0]*M[1][2] - M[1][0]*M[0][2]
	s[2] = M[0][0]*M[1][3] - M[1][0]*M[0][3]
	s[3] = M[0][1]*M[1][2] - M[1][1]*M[0][2]
	s[4] = M[0][1]*M[1][3] - M[1][1]*M[0][3]
	s[5] = M[0][2]*M[1][3] - M[1][2]*M[0][3]

	c[0] = M[2][0]*M[3][1] - M[3][0]*M[2][1]
	c[1] = M[2][0]*M[3][2] - M[3][0]*M[2][2]
	c[2] = M[2][0]*M[3][3] - M[3][0]*M[2][3]
	c[3] = M[2][1]*M[3][2] - M[3][1]*M[2][2]
	c[4] = M[2][1]*M[3][3] - M[3][1]*M[2][3]
	c[5] = M[2][2]*M[3][3] - M[3][2]*M[2][3]

	/* Assumes it is invertible */
	idet := 1.0 / (s[0]*c[5] - s[1]*c[4] + s[2]*c[3] + s[3]*c[2] - s[4]*c[1] + s[5]*c[0])

	T[0][0] = (M[1][1]*c[5] - M[1][2]*c[4] + M[1][3]*c[3]) * idet
	T[0][1] = (-M[0][1]*c[5] + M[0][2]*c[4] - M[0][3]*c[3]) * idet
	T[0][2] = (M[3][1]*s[5] - M[3][2]*s[4] + M[3][3]*s[3]) * idet
	T[0][3] = (-M[2][1]*s[5] + M[2][2]*s[4] - M[2][3]*s[3]) * idet

	T[1][0] = (-M[1][0]*c[5] + M[1][2]*c[2] - M[1][3]*c[1]) * idet
	T[1][1] = (M[0][0]*c[5] - M[0][2]*c[2] + M[0][3]*c[1]) * idet
	T[1][2] = (-M[3][0]*s[5] + M[3][2]*s[2] - M[3][3]*s[1]) * idet
	T[1][3] = (M[2][0]*s[5] - M[2][2]*s[2] + M[2][3]*s[1]) * idet

	T[2][0] = (M[1][0]*c[4] - M[1][1]*c[2] + M[1][3]*c[0]) * idet
	T[2][1] = (-M[0][0]*c[4] + M[0][1]*c[2] - M[0][3]*c[0]) * idet
	T[2][2] = (M[3][0]*s[4] - M[3][1]*s[2] + M[3][3]*s[0]) * idet
	T[2][3] = (-M[2][0]*s[4] + M[2][1]*s[2] - M[2][3]*s[0]) * idet

	T[3][0] = (-M[1][0]*c[3] + M[1][1]*c[1] - M[1][2]*c[0]) * idet
	T[3][1] = (M[0][0]*c[3] - M[0][1]*c[1] + M[0][2]*c[0]) * idet
	T[3][2] = (-M[3][0]*s[3] + M[3][1]*s[1] - M[3][2]*s[0]) * idet
	T[3][3] = (M[2][0]*s[3] - M[2][1]*s[1] + M[2][2]*s[0]) * idet

	return T
}

func (m Mat4) LookAt(p Vec3) Mat4 {
	var up = Vec3ZUnit
	var center = p
	var eye = m.Translation()
	/* Adapted from Android's OpenGL Matrix.java.                        */
	/* See the OpenGL GLUT documentation for gluLookAt for a description */
	/* of the algorithm. We implement it in a straightforward way:       */

	/* TODO: The negation of of can be spared by swapping the order of
	 *       operands in the following cross products in the right way. */
	var f Vec3
	f = center.Sub(eye).Norm()

	var s Vec3
	s = f.MulCross(up).Norm()

	var t Vec3
	t = s.MulCross(f)

	m[0][0] = s[0]
	m[0][1] = t[0]
	m[0][2] = -f[0]
	m[0][3] = 0.

	m[1][0] = s[1]
	m[1][1] = t[1]
	m[1][2] = -f[1]
	m[1][3] = 0.

	m[2][0] = s[2]
	m[2][1] = t[2]
	m[2][2] = -f[2]
	m[2][3] = 0.

	m[3][0] = 0.
	m[3][1] = 0.
	m[3][2] = 0.
	m[3][3] = 1.

	m.TranslateInPlace(-eye[0], -eye[1], -eye[2])
	return m
}

func (a Mat4) Equal(b Mat4) bool {
	for i := 0; i < 4; i++ {
		if !a.Row(i).Equal(b.Row(i)) {
			return false
		}
	}
	return true
}

func (a Mat4) ApproxEqual(b Mat4, e float64) bool {
	for i := 0; i < 4; i++ {
		if !a.Row(i).ApproxEqual(b.Row(i), e) {
			return false
		}
	}
	return true
}

// fixme: rename?
func (m Mat4) Translation() Vec3 {
	return Vec3{
		m[3][0],
		m[3][1],
		m[3][2]}
	/*
		return Vec3{
			m[0][3],
			m[1][3],
			m[2][3]}
	*/
}

func (m Mat4) ExtractTranslation() Vec3 {
	//println("derp:")
	//println(m.String())
	//fmt.Println(m.Translation())
	//panic(2)
	return m.Translation()
}

func (m Mat4) ExtractScale() Vec3 {
	return Vec3One
}

func (m Mat4) ExtractRotation() Vec3 {
	//return Vec3Zero
	q := QuatFromMat4(m)
	angles := q.EulerAngles()
	//fmt.Println(m)
	//fmt.Println(m.Data())
	//fmt.Println(q)
	//fmt.Println(angles)
	//panic(2)
	return angles
}

// Transpose matrix.
func (m Mat4) Transpose() Mat4 {
	var N Mat4
	for j := 0; j < 4; j++ {
		for i := 0; i < 4; i++ {
			N[i][j] = m[j][i]
		}
	}
	return N
}

// Fill matrix with a value.
func (m Mat4) Fill(v float64) Mat4 {
	var r Mat4
	for j := 0; j < 4; j++ {
		for i := 0; i < 4; i++ {
			r[i][j] = v
		}
	}
	return r
}

// Duplicate matrix.
func (m Mat4) Dup() Mat4 {
	return m
}

// Return flat matrix data.
func (m Mat4) Data() [16]float64 {
	return [16]float64{
		m[0][0], m[0][1], m[0][2], m[0][3], // col 1
		m[1][0], m[1][1], m[1][2], m[1][3], // col 2
		m[2][0], m[2][1], m[2][2], m[2][3], // col 3
		m[3][0], m[3][1], m[3][2], m[3][3]} // col 4
}

func (m Mat4) Dataf() [16]float32 {
	return [16]float32{
		float32(m[0][0]), float32(m[0][1]), float32(m[0][2]), float32(m[0][3]), // col 1
		float32(m[1][0]), float32(m[1][1]), float32(m[1][2]), float32(m[1][3]), // col 2
		float32(m[2][0]), float32(m[2][1]), float32(m[2][2]), float32(m[2][3]), // col 3
		float32(m[3][0]), float32(m[3][1]), float32(m[3][2]), float32(m[3][3])} // col 4
}

// Pretty-print matrix. Use .Data() for flat c-array print.
func (m Mat4) String() string {
	// Column-major output, not flat.
	//    [[1, 0, 0, Tx]]
	//    [[0, 1, 0, Ty]]
	//    [[0, 0, 1, Tz]]
	//    [[0, 0, 0, 1 ]]
	buf := new(bytes.Buffer)
	w := tabwriter.NewWriter(buf, 4, 4, 1, ' ', tabwriter.AlignRight)
	for i := 0; i < 4; i++ {
		for _, val := range m.Row(i) {
			fmt.Fprintf(w, "%f\t", val)
		}
		fmt.Fprintln(w, "")
	}
	w.Flush()

	return buf.String()
}

func (m Mat4) String_() string {
	s := ""
	for i := 0; i < 4; i++ {
		for j := 0; j < 4; j++ {
			s += fmt.Sprintf("%f\t", m[i][j])
		}
		s += "\n"
	}
	return s
}
