package audio

import (
	al "github.com/azul3d/native-al"
	"bitbucket.org/coaljoe/rx"
	"bitbucket.org/coaljoe/rx/math"
)

var maxId = 0

type AudioSource struct {
	*rx.Node
	id   int
	af   AudioFile // fixme
	loop bool
	// al
	al_source uint32
	al_buf    uint32
	al_format uint32
}

func NewAudioSource(path string) *AudioSource {
	as := &AudioSource{
		Node: rx.NewNode(),
		id:   maxId,
		af:   NewAudioFile(path),
	}
	as.init()
	maxId += 1
	return as
}

func (as *AudioSource) init() {
	source := al.Aluint(0)
	al.GenSources(1, source)
	al.alSorucef(source, al.AL_PITCH, 1)
	al.alSourcef(source, al.AL_GAIN, 1)
	al.alSource3f(source, al.AL_POSITION, *self.pos)
	al.alSource3f(source, al.AL_VELOCITY, 0, 0, 0)
	al.alSourcei(source, al.AL_LOOPING, self.loop)

	buf := al.ALuint(0)
	al.alGenBuffers(1, buf)
	format := al.AL_FORMAT_STEREO16
	bufSize := 1024 * 2
	pcmbuf := make([]int16, bufSize)
	ns, err := as.af.ReadAll(pcmbuf)

	al.alBufferData(buf, format, wavbuf, len(wavbuf), as.af.samplerate)
	al.alSourceQueueBuffers(source, 1, buf)

	as.al_source = source
	as.al_buf = bu
	as.al_format = format
}

func (as *AudioSource) Play(path string) {
	al.alSource3f(as.al_source, al.AL_POSITION,
		as.Pos().X(), as.Pos().Y(), as.Pos().Z())
	al.alSourcePlay(al.al_source)
}

func (as *AudioSource) Free() {
	al.alDeleteSources(1, as.al_source)
	al.al.DeleteBuffers(1, as.al_buf)
}
