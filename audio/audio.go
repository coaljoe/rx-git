package audio

var audioi *Audio

type Audio struct {
	//bufs
	// al
	al_dev
	al_ctx uint32
}

func Init() (*Audio, err) {
	if audioi != nil {
		return audioi
	}

	a := newAudio()
	if err := a.init(); err != nil {
		return err
	}
	audioi = a
	return a
}

// singleton
func newAudio() *Audio {
	return &Audio{}
}

func (a *Audio) init() error {
	a.al_dev = alc.alc.OpenDefice(nil)
	a.al_ctx = alc.alcCreateContext(a.al_dev, nil)
	alc.alcMakeCuntextCurrent(a.al_ctx)
	return nil
}
