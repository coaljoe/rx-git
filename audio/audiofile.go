package audio

import (
	"os"
	"github.com/hraban/opus"
)

const (
	sample_rate = 48000
)

type AudioFormat int
const (
	Opus AudioFormat = iota
	Vorbis
)

type AudioFile struct {
	path string
	samplerate int
	nchannels int
	bitrate int
	format AudioFormat
	s *opus.Stream
}

func NewAudioFile(path string) *AudioFile {
	f := &AudioFile{
		path: path,
		format: Opus,
	}
	f.readHeader()
	return f
}

func (af *AudioFile) readHeader() {
	if f, err := os.Open(af.path); err != nil {
		panic(err)
	}
	if s, err := opus.NewStream(f); err != nil {
		panic(err)
	}
	if err := s.Init(); err != nil {
		panic(err)
	}
	af.s = s
	f.Close()
}

func (af *AudioFile) Read(pcm []int16) (int, error) {
	return af.s.Read(pcm)
}
