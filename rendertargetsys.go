package rx

import "github.com/go-gl/glfw/v3.1/glfw"

type RenderTargetSys struct {
	*System
	lastUsedRt *RenderTarget
	debug      bool

	// debug
	debugViews []*RenderTargetDebugView
	lastX      float64
	lastY      float64
}

func NewRenderTargetSys() *RenderTargetSys {
	s := &RenderTargetSys{
		System:     NewSystem("RenderTargetSys", "RenderTarget system"),
		debugViews: make([]*RenderTargetDebugView, 0),
		debug:      false,
		lastX:      0.8,
		lastY:      0.8,
	}
	_InputSys.AddEventHandler(s)
	return s
}

func (rts *RenderTargetSys) Add(rt *RenderTarget) {
	rts.AddElem(rt)

	// create debug view
	dv := NewRenderTargetDebugView(rt)

	// adjust position
	dv.c.x = rts.lastX
	dv.c.y = rts.lastY
	rts.lastY -= dv.c.h
	if rts.lastY < 0.0 {
		rts.lastY = 0.8
		rts.lastX -= dv.c.w
	}
	dv.SetVisible(false)
	rts.debugViews = append(rts.debugViews, dv)
}

func (rts *RenderTargetSys) OnKeyPress(key glfw.Key) {
	if key == glfw.KeyF8 {
		rts.ToggleDebug()
	}
}

func (rts *RenderTargetSys) OnKeyRelease(key glfw.Key)           {}
func (rts *RenderTargetSys) OnMouseMove(x, y int)                {}
func (rts *RenderTargetSys) OnMousePress(btn glfw.MouseButton)   {}
func (rts *RenderTargetSys) OnMouseRelease(btn glfw.MouseButton) {}

func (rts *RenderTargetSys) ToggleDebug() {
	prev_debug := rts.debug
	rts.debug = !rts.debug
	Log.Inf("RenderTargetSys.ToggleDebug:", rts.debug)

	// update debug views
	for _, dv := range rts.debugViews {
		dv.SetVisible(rts.debug)

		if rts.debug && !prev_debug {
			if dv.rt.depth {
				// skip depth rts because cannot save their textures
				Log.Inf("Skipping depth RT; name: ", dv.rt.name)
				continue
			}
			path := Conf.tmp_dir + "/rx_debug_rt__" + dv.rt.name + ".png"
			Log.Inf("Saving RT image to file: ", path)
			dv.c.tex.Save(path)
		}
	}

	if rts.debug {
		rts.ListElems()
	}
}

func (rts *RenderTargetSys) Update(dt float64) {

}
