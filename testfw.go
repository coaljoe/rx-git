package rx

import (
	. "bitbucket.org/coaljoe/rx/math"
	"fmt"
	"os"
)

var win *Window
var app *App

// fixme: better return *Rx?
func TestFwInit() *App {
	println("TestFw init begin")

	SetDefaultConf()
	//ConfSetResPath("/home/j/dev/go/src/rx/res/") // fixme
	//ConfSetResPath("c:/Users/k/k/dev/go/src/rx/res/") // fixme
	wd, err := os.Getwd()
	if err != nil {
		fmt.Println("Cant get CWD")
		panic(err)
	}
	_ = wd
	//ConfSetResPath(wd + "/res/")
	ConfSetResPath("res")

	// Enable debug draw for test fw by default
	Conf.debug_draw = true

	/*
		win = NewWindow()
		//win.Init(640, 480)
		//win.Init(1024, 576)
		win.Init(960, 540) // fullhd/2
		//win.Init(512, 512)
		app = NewApp(win)
		app.Init()
		rxi := app.Rxi
	*/
	_rxi := NewRx()
	//_rxi.Init(960, 540)
	_rxi.Init(512, 512)

	app = rxi.App
	win = app.Win()

	Log.Inf("TestFw init done")
	return app
}

func TestFwCreateDefaultScene() *Scene {
	Log.Inf("TestFw create default scene")
	sce := rxi.Scene() // fixme

	/* add camera */

	//app.rxi.camera.transform(10, 10, 10,  -35.264, 45, 0)
	cn := rxi.Camera // CameraNode
	cn.Camera.SetZoom(6)
	//cam.Pos = rx.NewVector3(10, 20, 10)
	//cam.Rot = rx.NewVector3(35.264, 45, 0)
	//cam.Pos = rx.NewVector3(0, 50, 0)
	//cam.Rot = rx.NewVector3(90, 0, 0)
	//cam.SetPos(Vec3{-5, 5, -5})
	cn.SetPos(Vec3{5, 5, 5})
	//cam.Rot = rx.NewVector3(90+35.264, 45, 0)
	//cam.Rot = rx.NewVector3(35.264, 45, 0)

	//cam.SetTarget(Vec3{0, 0, 0})

	println(cn.Pos().String())
	println(cn.Rot().String())

	/* add light */

	l1 := sce.CreateLightNode("light1")
	println("l1 pos:", l1.Pos().String())

	return sce
}

func TestFwDeinit() {
	Log.Inf("TestFw deinit")
}
