package main

import (
	. "fmt"
	//. "ltest"
	. "math"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"
)

func main() {
	Println("main()")
	//rx.Init()

	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()
	rxi := rx.Rxi()

	sl := rx.NewSceneLoader()
	_ = sl

	// set up renderer

	rx.Xglcheck()
	r := rxi.Renderer()
	rp := rx.NewFxaaRenderPass()
	rp.Init(r)
	r.AddRenderPass(rp)
	r.SetDefaultRenderPass(false)
	rx.Xglcheck()

	/*
	  sl.Load("src/rx/res/test/test_plane/test_plane.dae")
	  sl.Spawn()

	  sl.Load("src/rx/res/test/test_box/test_box.dae")
	  sl.Spawn()

	  n := rxi.Scene().GetNode("Cube")
	  n.SetPos(Vec3{2, 0, 0})

	*/
	//sl.Load("src/rx/res/test/test_tricone/test_tricone.dae")
	//sl.Spawn()
	/*
	  sl.Load("src/rx/res/test/test_box/test_box_textured.dae")
	  sl.Spawn()

	  n := rxi.Scene().GetNode("Cube")
	  n.SetPos(Vec3{2, 0, 0})
	*/

	/*
	  nodes := sl.Load("src/rx/res/test/test_box/test_box_textured.dae")
	  sl.Spawn()
	  for _, node := range nodes {
	    node.SetPos(Vec3{1, 0, 3})
	  }
	*/

	/*
	  sl.Load("src/rx/res/test/test_plane/test_plane_textured.dae")
	  sl.Spawn()
	*/
	/*
	  sl.Load("src/rx/res/test/test_plane/test_triangle_textured.dae")
	  sl.Spawn()
	*/

	/*
	   sl.Load("src/rx/res/test/test_box/test_box_textured_numbers.dae")
	   sl.Spawn()
	*/

	// load tank //

	/*

	  sl.Load("src/rx/res/test/tank_renew2/tank-renew2.dae")
	  sl.Spawn()
	  //rxi.Scene().GetNode("body").SetPos(Vec3{0, -5, 0})
	  //rxi.Scene().GetNode("turret").SetPos(Vec3{0, -5, 0})
	  rxi.Scene().GetNode("body").SetPos(Vec3{5, 0, 0})
	  rxi.Scene().GetNode("turret").SetPos(Vec3{5, 0, 0})
	  rxi.Scene().GetNode("turret").SetRot(Vec3{0, 45, 0})

	*/
	/*
	   sl.Load("src/rx/res/test/tank_renew2/tank-renew2.dae")
	   sl.Spawn()
	   //rxi.Scene().GetNode("body").SetPos(Vec3{0, 0, 5})
	   //rxi.Scene().GetNode("turret").SetPos(Vec3{0, 0, 5})
	   //rxi.Scene().GetNode("turret").SetRot(Vec3{0, 0, 0})

	   body := sce.GetNode("body")
	   turret := sce.GetNode("turret")
	   turret.SetParent(body.Transform)

	   body.SetPos(Vec3{0, 0, 5})
	   Println("body local pos", body.Pos())
	   Println("turret local pos", turret.Pos())
	   Println("body world pos", body.WorldPos())
	   Println("turret world pos", turret.WorldPos())
	*/
	/*
	   sl.Load("res/models/primitives/box/box.dae")
	   //sl.Load("res/models/primitives/triangle/triangle.dae")
	   sl.Spawn()
	*/

	//sl.Load("/home/j/dev/go/src/rx/res/test/units/ground/heavytank/reds/model/model.dae")
	sl.Load("res/test/units/ground/heavytank/reds/model/model.dae")
	sl.Spawn()
	n := sce.GetNode("body")
	//n.LookAt2d(Vec3{10, 0, 10})
	//n.LookAt2d(Vec3{0, 0, 10})
	//n.SetPos(Vec3{0, 0, 10})
	Println(n.Rot())
	//panic(2)
	//sce.GetNode("body").SetRot(Vec3{45, -45, 45})
	//rxi.Scene().GetNode("body").SetPos(Vec3{0, -5, 0})
	//rxi.Scene().GetNode("turret").SetPos(Vec3{0, -5, 0})
	//Println("mg pos:", rxi.Scene().GetNode("mg").Pos())

	// clone

	var n1 *rx.MeshNode
	n1 = sce.GetMeshNode("body")
	n2 := n1.Clone()
	n2.SetPos(Vec3{5, 0, 5})
	rxi.Scene().AddMeshNode(n2)

	if n1.Name() == n2.Name() {
		panic("bad clone")
	}

	Println(n1.Transform)
	Println(n2.Transform)
	Println(n1.Transform.Pos())
	Println(n2.Transform.Pos())
	n1.Transform.SetPos(Vec3{1, 1, 1})
	Println(n1.Transform.Pos())
	Println(n2.Transform.Pos())
	Println(n1.Name())
	Println(n2.Name())
	//panic(1)

	//rxi.Scene().RemoveMeshNode(n2)

	/*
	  rxi.Scene().GetNode("body").SetPos(Vec3{5, 0, 0})
	  rxi.Scene().GetNode("turret").SetPos(Vec3{5, 0, 0})
	  rxi.Scene().GetNode("turret").SetRot(Vec3{0, 45, 0})
	  rxi.Scene().GetNode("mg").SetRot(Vec3{0, 45, 0})
	  rxi.Scene().GetNode("mg").SetPos(Vec3{5, 0, 0})
	  //rxi.Scene().GetNode("mg").SetPos(Vec3{5, 3, 0})
	*/

	/* add camera */

	//app.rxi.camera.transform(10, 10, 10,  -35.264, 45, 0)
	cam := rxi.Camera
	cam.SetZoom(6)
	//cam.Pos = rx.NewVector3(10, 20, 10)
	//cam.Rot = rx.NewVector3(35.264, 45, 0)
	//cam.Pos = rx.NewVector3(0, 50, 0)
	//cam.Rot = rx.NewVector3(90, 0, 0)
	//cam.SetPos(Vec3{-5, 5, -5})
	cam.SetPos(Vec3{5, 5, 5})
	//cam.Rot = rx.NewVector3(90+35.264, 45, 0)
	//cam.Rot = rx.NewVector3(35.264, 45, 0)

	//cam.SetTarget(Vec3{0, 0, 0})
	//cam.LookAt(Vec3{0, 0, 0})

	println(cam.Pos().String())
	println(cam.Rot().String())

	/* add light */

	l1 := sce.AddLightNode("light1")
	println("l1 pos:", l1.Pos().String())

	v := 0.0
	vInc := 10.0
	rotate := true
	for app.Step() {
		//println("step")
		dt := app.GetDt()

		if rotate {
			v = Mod(v+vInc*dt, 360)
			sce.GetNode("turret").SetRot(Vec3{0, 0, v})
		}
	}

	println("exiting...")
}
