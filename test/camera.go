package main

import (
	"fmt"
	. "math"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"
)

func main() {
	fmt.Println("main()")
	//rx.Init()

	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()
	rxi := rx.Rxi()
	rxi.Renderer()

	sl := rx.NewSceneLoader()
	_ = sl

	//sl.Load("res/models/primitives/triangle/triangle.dae")
	sl.Load("res/models/primitives/box/box.dae")
	sl.Spawn()

	cam := rxi.Camera
	//rot := Vec3{35.264, 45, 0}
	//pos := Vec3{10, 10, 20}

	// 45 degrees isometric cam
	rot := Vec3{35.264, 0, 45}
	pos := Vec3{10, -10, 20}

	cam.SetRot(rot)
	cam.SetPos(pos)

	v := 0.0
	vInc := 20.0
	rotate := true
	for app.Step() {
		//println("step")
		dt := app.GetDt()

		if rotate {
			v = Mod(v+vInc*dt, 360)
			//sce.GetNode("turret").SetRot(Vec3{0, 0, v})
			sce.GetNodeByName("box").SetRot(Vec3{v / 2, v / 3, v / 4})
			//sce.GetNode("triangle").SetRot(Vec3{v, 0, 0})
		}
	}

	println("exiting...")
}
