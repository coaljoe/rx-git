package main

import (
	"fmt"
	. "math"
	"bitbucket.org/coaljoe/rx"

	. "bitbucket.org/coaljoe/rx/math"
)

func main() {
	fmt.Println("main()")

	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()

	sl := rx.NewSceneLoader()
	_ = sl

	sl.Load("res/models/primitives/triangle/triangle.dae")
	//sl.Load("res/models/primitives/box/box.dae")
	sl.Spawn()

	tri := sce.GetNodeByName("triangle")
	tri_mn := sce.GetNodeContent("triangle").(*rx.Node)

	//tri_mn := tri.Content().(*rx.MeshNode)
	fmt.Println(tri_mn.Mesh.Material().Diffuse)
	tri_mn.Mesh.Material().Diffuse = Vec3{1, 0, 0}
	fmt.Println(tri_mn.Mesh.Material().Diffuse)

	v := 0.0
	vInc := 50.0
	rotate := true
	for app.Step() {
		//println("step")
		dt := app.GetDt()

		if rotate {
			v = Mod(v+vInc*dt, 360)
			tri.SetRot(Vec3{v, 0, 0})
		}
	}

	rx.TestFwDeinit()

	println("exiting...")
}
