package main

import (
	"fmt"
	. "math"
	"bitbucket.org/coaljoe/rx"

	. "bitbucket.org/coaljoe/rx/math"
)

func main() {
	fmt.Println("main()")

	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()

	sl := rx.NewSceneLoader()
	_ = sl

	//sl.Load("res/models/primitives/triangle/triangle.dae")
	sl.Load("res/models/primitives/box/box.dae")
	sl.Spawn()

	v := 0.0
	vInc := 50.0
	rotate := true
	for app.Step() {
		//println("step")
		dt := app.GetDt()

		//rx.DrawText("TestString", Vec4{1, 1, 1, 1})
		//rx.DrawText("AA", Vec4{0, 0, 1, 0})
		//rx.DrawText("AA", Vec4{0, 0, 1, 1})
		rx.DrawText("Aa", Vec4{0, 0, 1, 1})

		if rotate {
			v = Mod(v+vInc*dt, 360)
			//sce.GetNode("turret").SetRot(Vec3{0, 0, v})
			sce.GetNodeByName("box").SetRot(Vec3{0, 0, v})
			//sce.GetNode("triangle").SetRot(Vec3{v, 0, 0})
		}
	}

	rx.TestFwDeinit()

	println("exiting...")
}
