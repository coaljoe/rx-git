package main

import (
	"fmt"
	//. "ltest"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"
	. "math"
)

func main() {
	fmt.Println("main()")
	//rx.Init()

	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()
	_ = sce

	sl := rx.NewSceneLoader()
	_ = sl

	//nodes := sl.Load("res/test/test_hipoly/model.dae")
	//nodes := sl.Load("res/test/test_hipoly_tex/hipoly_tex.dae")
	//nodes := sl.Load("res/test/sphere/sphere.dae")
	nodes := sl.Load("res/test/sphere/sphere.gltf")
	sl.Spawn()
	_ = nodes

	node := nodes[0]
	//node.SetRotX(-90)

	node.SetScale(Vec3{5, 5, 5})

	//mat := rx.NewMaterial("res/materials/model.json")
	//node.Mesh.SetMaterial(mat)

	//node.Mesh.Material().SetTexture("res/test/sphere/solid_blue.png")

	//app.rxi.camera.transform(10, 10, 10,  -35.264, 45, 0)
	cam := rx.Rxi().Camera
	cam.Camera.SetZoom(6)
	cam.SetPos(Vec3{5, 5, 5})

	//cam.SetTarget(Vec3{0, 0, 0})
	//cam.LookAt(Vec3{0, 0, 0})

	println(cam.Pos().String())
	println(cam.Rot().String())

	var _ = `
	/* add light */

	l1 := sce.CreateLightNode("light1")
	l1.SetPos(Vec3{10, 10, 10})
	println("l1 pos:", l1.Pos().String())

	fmt.Println(rx.Rxi().Scene().Lights())
	panic(2)
	`

	l0Rotate := true
	l0Angle := 0.0
	l0RotSpeed := 50.0
	l0RotRadius := 10.0
	//l0Node := sce.Lights()[0] // fixme
	l0Node := sce.GetDefaultLight()

	v := 0.0
	vInc := 50.0
	rotate := false
	for app.Step() {
		//println("step")
		dt := app.GetDt()

		if rotate {
			v = Mod(v+vInc*dt, 360)
			//sce.GetNode("turret").SetRot(Vec3{0, v, 0})
			node.SetRot(Vec3{0, v, 0})
		}

		if l0Rotate {
			a := Radians(l0Angle)
			zPos := l0Node.PosZ()
			newPos := Vec3{l0RotRadius * Cos(a),
				l0RotRadius * Sin(a), zPos}
			l0Node.SetPos(newPos)

			l0Angle += l0RotSpeed * dt
		}
	}

	println("exiting...")
}
