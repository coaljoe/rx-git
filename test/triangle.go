package main

import (
	"bitbucket.org/coaljoe/rx"
	"fmt"
	. "math"

	. "bitbucket.org/coaljoe/rx/math"
)

func main() {
	fmt.Println("main()")

	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()

	sl := rx.NewSceneLoader()
	_ = sl

	//sl.Load("res/test/test_gltf_triangle/glTF-Embedded/Triangle.gltf")
	//sl.Load("res/models/primitives/triangle/triangle.gltf")
	//sl.Load("res/models/primitives/triangle/triangle.dae")
	//sl.Load("res/models/primitives/box/box.dae")
	//sl.Load("tmp/test_uv/test_uv.dae")
	sl.Load("tmp/test_uv/q5d.gltf")
	sl.Spawn()

	node := sl.Nodes()[0]
	node.SetPos(Vec3{4, 4, 0})

	v := 0.0
	vInc := 50.0
	rotate := false
	for app.Step() {
		//println("step")
		dt := app.GetDt()

		if rotate {
			v = Mod(v+vInc*dt, 360)
			//sce.GetNode("turret").SetRot(Vec3{0, 0, v})
			//sce.GetNodeByName("box").SetRot(Vec3{0, 0, v})
			sce.GetNodeByName("triangle").SetRot(Vec3{v, 0, 0})
		}
	}

	rx.TestFwDeinit()

	println("exiting...")
}
