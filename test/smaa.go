package main

import (
	. "fmt"
	. "math"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"
)

func main() {
	Println("main()")
	//rx.Init()

	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()
	rxi := rx.Rxi()

	sl := rx.NewSceneLoader()
	_ = sl

	// set up renderer

	rx.Xglcheck()
	r := rxi.Renderer()
	rp := rx.NewSmaaRenderPass()
	rp.Init(r)
	r.AddRenderPass(rp)
	r.SetDefaultRenderPass(false)
	rx.Xglcheck()
	//sl.Load("res/models/primitives/triangle/triangle.dae")
	sl.Load("res/models/primitives/box/box.dae")
	sl.Spawn()

	v := 0.0
	vInc := 20.0
	rotate := true
	for app.Step() {
		//println("step")
		dt := app.GetDt()

		if rotate {
			v = Mod(v+vInc*dt, 360)
			//sce.GetNode("turret").SetRot(Vec3{0, 0, v})
			sce.GetNode("box").SetRot(Vec3{v / 2, v / 3, v / 4})
			//sce.GetNode("triangle").SetRot(Vec3{v, 0, 0})
		}
	}

	println("exiting...")
}
