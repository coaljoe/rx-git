package main

import (
	"bitbucket.org/coaljoe/rx"
	"fmt"
	"github.com/go-gl/gl/v2.1/gl"
	. "math"

	. "bitbucket.org/coaljoe/rx/math"
)

func main() {
	fmt.Println("main()")

	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()
	rxi := rx.Rxi()
	_ = rxi

	sl := rx.NewSceneLoader()
	_ = sl

	//sl.Load("res/models/primitives/triangle/triangle.dae")
	sl.Load("res/models/primitives/box/box.dae")
	sl.Spawn()

	// box#2
	sl.Load("res/models/primitives/box/box.dae")
	sl.Spawn()

	cn := rxi.Camera
	if false {
		//cam.SetPos(Vec3{5, 5, 5})
		//cam.LookAt(Vec3Zero)
		cn.SetPos(Vec3{5, 10, 21.5})
		cn.SetRot(Vec3{12, 0, 11.8})
		cn.Camera.SetZoom(11.5 * 2)
	}
	//cn.Camera.SetFov(10)
	//cn.Camera.SetZoom(6)
	//cn.Camera.SetFov(20)
	cn.Camera.SetFov(40)
	cn.Camera.SetZoom(1)
	cn.Camera.SetZnear(0.01)
	cn.Camera.SetZfar(100)

	// Iso cam
	cn.SetPos(Vec3{12.24745, -12.24745, 10.0})
	// Game iso 1:2
	cn.SetRot(Vec3{60, 0, 45})

	boxNode := sce.GetNodeByName("box")
	boxNode.SetPos(Vec3{10, 10, 0})

	v := 0.0
	vInc := 50.0
	rotate := false
	for app.Step() {
		//println("step")
		dt := app.GetDt()

		if rotate {
			v = Mod(v+vInc*dt, 360)
			//sce.GetNode("turret").SetRot(Vec3{0, 0, v})
			//sce.GetNode("box").SetRot(Vec3{0, 0, v})
			sce.GetNodeByName("box").SetRot(Vec3{0, 0, v})
			//sce.GetNode("triangle").SetRot(Vec3{v, 0, 0})
		}

		c := rxi.Camera
		c.SetPos(Vec3{0, 0, 5})
		fmt.Println(c.Camera.Fov())
		fmt.Println(c.Camera.Zoom())
		//panic(2)
		println(c)
		//rayLenght := 10.0
		rayLenght := 1000.0
		//rayLenght := 100.0
		rayDir := c.Camera.CreateRay()
		ray := rayDir
		//rayDir_norm := rayDir.Norm()
		//rayStart := c.WorldPos()
		rayStart := ray.Origin
		//rayStart := Vec3{0, 0, 0}
		rayEnd := ray.Origin.Add(ray.Direction.MulScalar(rayLenght))
		//rayEnd := rayStart.Add(rayDir.MulScalar(rayLenght))
		//rayEnd := rayDir.Add(rayStart)
		//rayEnd := Vec3Zero
		//rayEnd_z := rayStart.Add(rayDir_norm.MulScalar(rayLenght))
		//rayEnd[2] = rayEnd_z[2]
		//rayEnd := c.Pos().Add(rayDir.MulScalar(rayLenght))
		//rayEnd := rayDir.MulScalar(100)
		//rayEnd.SetZ(0)
		//rayEnd := rayStart.Mul(rayDir.MulScalar(rayLenght))
		//fmt.Println(c.Pos())
		//fmt.Println(rayDir)
		fmt.Println(rayLenght, rayDir)
		fmt.Println("start:", rayStart, "\nend:  ", rayEnd)
		fmt.Println("cpos:", c.WorldPos())
		fmt.Println("crot:", c.Rot())

		// Place the object at ray's end
		//sce.GetNode("box").SetPos(rayEnd)
		//sce.GetNode("box").MoveByVec(Vec3{0, 0, -5})
		//sce.GetNode("box").SetPos(ray.Origin)
		//sce.GetNode("box").MoveByVec(Vec3{0, 0, -10})
		//sce.GetNode("box").SetPos(Vec3{4, 46, 0})
		//fmt.Println("box pos:", sce.GetNode("box").Pos())
		fmt.Println("box pos:", sce.GetNodeByName("box").Pos())

		//selRay := NewRay(rayStart, rayEnd)
		selRay := NewRay(ray.Origin, ray.Direction)
		// Do scene rayquery
		rc := rx.NewRaycaster()
		rc.TestRayScene(selRay)
		hitInfo := rc.TestRayGround(selRay)

		if hitInfo.Hit {
			sce.GetNodeByName("box#2").SetPos(hitInfo.Position)
		}

		rxi.Renderer().SetPostRenderCB(func(r *rx.Renderer) {
			println("test")
			//panic(2)
			rx.DrawAxes()
			//rx.DrawLine(0, 0, 0, 100, 100, 100)
			a := selRay.Origin
			b := selRay.Direction
			fmt.Println(a)
			fmt.Println(b)
			//panic(2)
			/*
				t := rx.NewTransform()
				t.SetPos(a)
				//t.LookAt(b)
				fmt.Println(t.Pos())
				panic(2)
				t.MoveByVec(Vec3{0, 0, -5})
				a = t.Pos()
				fmt.Println(a)
				panic(2)
			*/
			//rx.DrawLine(a[0], a[1], a[2], b[0]*100, b[1]*100, b[2]*100)
			rx.DrawLine(a[0], a[1], a[2], b[0], b[1], b[2])
			gl.PushMatrix()
			gl.Translated(b[0], b[1], b[2])
			//gl.Translated(a[0], a[1], a[2])
			rx.DrawCube(1)
			rx.DrawAxes()
			gl.PopMatrix()
			r.Set2DMode()
			rx.DrawCube(100)
			rx.DrawCube(10)
			rx.DrawLine(0, 0, 0, 1, 1, 1)
			rx.DrawAxes()
			r.Unset2DMode()
		})
		_ = `
		rxi.Renderer().SetPostRenderCB(func(r *rx.Renderer) {
			println("test")
			//panic(2)
			rx.DrawAxes()
			//rx.DrawLine(0, 0, 0, 100, 100, 100)
			a := selRay.Origin
			b := selRay.Direction
			t := rx.NewTransform()
			t.SetPos(a)
			t.LookAt(b)
			t.MoveByVec(Vec3{0, 0, -5})
			a = t.Pos()
			//rx.DrawLine(a[0], a[1], a[2], b[0]*100, b[1]*100, b[2]*100)
			rx.DrawLine(a[0], a[1], a[2], b[0], b[1], b[2])
			gl.PushMatrix()
			//gl.Translated(b[0], b[1], b[2])
			gl.Translated(a[0], a[1], a[2])
			rx.DrawCube(1)
			rx.DrawAxes()
			gl.PopMatrix()
			r.Set2DMode()
			rx.DrawCube(100)
			rx.DrawCube(10)
			rx.DrawLine(0, 0, 0, 1, 1, 1)
			rx.DrawAxes()
			r.Unset2DMode()
		})
		`
		/*

				if !app.Step() {
					break
				}
			rx.DrawCube(100)
		*/
	}

	rx.TestFwDeinit()

	println("exiting...")
}
