package main

import (
	"fmt"
	. "fmt"
	. "math"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"

	"github.com/go-gl/gl/v2.1/gl"
)

// **** Fx RenderPass ****

type FxRenderPass struct {
	*rx.RenderPass

	fsShader *rx.Shader // fullscreen shader

	// options
	effectNumber int
}

func NewFxRenderPass() *FxRenderPass {
	rp := &FxRenderPass{
		RenderPass:   rx.NewRenderPass("FxRenderPass"),
		effectNumber: 0,
	}
	return rp
}

func (rp *FxRenderPass) Init(r *rx.Renderer) {
	rp.fsShader = rx.NewShader(
		"shaders/camera_fsquad.vert",
		"shaders/rp_fx.frag")
	rx.Xglcheck()
}

func (rp *FxRenderPass) Render(r *rx.Renderer) {

	// Pass 1.
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	// render scene to rendertarget's fbo
	rp.FboRt().Bind()

	// clear tmp_rt texture
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	r.RenderAll()

	//rp.FboRt().Unbind()
	// XXX: not unbinging FBO here, render pass 2 in this FBO/RT

	// Pass 2.
	// render the rendertarget texture with fullscreen shader
	gl.Disable(gl.DEPTH_TEST)
	rp.fsShader.Bind()

	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, rp.Tex())
	rp.fsShader.PassUniform1i("fboTex", 0)
	rp.fsShader.PassUniform1i("effect_number", rp.effectNumber)
	//rp.fsShader.PassUniform2i("buffersize", 960, 540)

	r.RenderFsQuad()

	gl.Enable(gl.DEPTH_TEST)
	rp.fsShader.Unbind()
	gl.BindTexture(gl.TEXTURE_2D, 0)

	// unbind the fbo
	rp.FboRt().Unbind()
}

func main() {
	Println("main()")
	//rx.Init()

	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()
	rxi := rx.Rxi()

	sl := rx.NewSceneLoader()
	_ = sl

	// set up renderer

	rx.Xglcheck()
	r := rxi.Renderer()
	r.SetDefaultRenderPass(false)

	// rp1
	rp := NewFxRenderPass()
	//rp.effectNumber = 4 // blur
	rp.effectNumber = 1 // nightvision
	rp.Init(r)
	r.AddRenderPass(rp)
	// rp2
	rp2 := rx.NewFxaaRenderPass()
	rp2.SetPrevRenderPass(rp.RenderPass)
	rp2.Init(r)
	r.AddRenderPass(rp2)

	rx.Xglcheck()
	//sl.Load("res/models/primitives/triangle/triangle.dae")
	sl.Load("res/models/primitives/box/box.dae")
	sl.Spawn()

	rxi.RenderTargetSys.ListElems()
	fmt.Println(rxi.RenderTargetSys.Elems())
	//panic(2)

	v := 0.0
	vInc := 20.0
	rotate := true
	for app.Step() {
		//println("step")
		dt := app.GetDt()

		if rotate {
			v = Mod(v+vInc*dt, 360)
			//sce.GetNode("turret").SetRot(Vec3{0, 0, v})
			sce.GetNode("box").SetRot(Vec3{v / 2, v / 3, v / 4})
			//sce.GetNode("triangle").SetRot(Vec3{v, 0, 0})
		}
	}

	println("exiting...")
}
