package main

import (
	"fmt"
	. "math"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"
)

var (
	app *rx.App
	sce *rx.Scene
	sl  *rx.SceneLoader
)

func testBox() (func(), func(float64)) {
	var node *rx.Node

	//sl.Load("res/models/primitives/triangle/triangle.dae")
	sl.Load("res/models/primitives/box/box.dae")
	sl.Spawn()

	node = sce.GetNodeByName("box")
	fmt.Println(node.Mat())
	node.Translate(Vec3{2, 2, 0})
	fmt.Println(node.Mat())
	//panic(2)

	stop := func() {
		return
	}

	v := 0.0
	vInc := 50.0
	//rotate := false
	rotate := true
	update := func(dt float64) {
		if rotate {
			v = Mod(v+vInc*dt, 360)
			//node.SetRot(Vec3{0, 0, v})
			node.SetRot(Vec3{v, v, v})
			fmt.Println(v)
		}
	}

	return stop, update
}

func testTriangle() (func(), func(float64)) {
	var node *rx.Node

	sl.Load("res/models/primitives/triangle/triangle.dae")
	sl.Spawn()

	node = sce.GetNodeByName("triangle")
	fmt.Println(node.Mat())
	//panic(2)

	stop := func() {
		return
	}

	v := 0.0
	vInc := 50.0
	rotate := true
	update := func(dt float64) {
		if rotate {
			v = Mod(v+vInc*dt, 360)
			node.SetRot(Vec3{1, 1, 1})
			fmt.Println(v)
		}
	}

	return stop, update
}

func testParent() (func(), func(float64)) {
	var node, node2 *rx.Node

	sl.Load("res/test/test_parent_transform/test_parent_transform.dae")
	sl.Spawn()

	node = sce.GetNodeByName("box_p")
	node2 = sce.GetNodeByName("box_c")
	fmt.Println(node.Mat())

	//fmt.Printf("%#v", node)
	//panic(2)

	//fmt.Printf("xxxxxxxxxxxxxxxx %#v\n", node2.Parent())
	//panic(2)
	//if node2.Parent() != node.Transform {
	if node2.Transform.Parent() != node.Transform {
		//if node2.Parent() != nil {
		//if true {
		fmt.Println("bad parenting:", node, node2.Parent())
		panic(2)
	} else {
		fmt.Println("good parenting:", "node:", node, "node2.Parent:", node2.Parent())
		fmt.Println("node2.Transform.parent:", node2.Transform.Parent())
		//panic(2)
	}
	println("node mat:")
	fmt.Println(node.Mat())
	fmt.Println(node.Mat().Data())
	println("node2 mat:")
	fmt.Println(node2.Mat())
	fmt.Println(node2.Mat().Data())
	//panic(2)

	stop := func() {
		return
	}

	v := 0.0
	vInc := 50.0
	rotate := false
	update := func(dt float64) {
		if rotate {
			v = Mod(v+vInc*dt, 360)
			node.SetRot(Vec3{v, v, v})
			//fmt.Println(v)
			//fmt.Println(node.Pos(), node2.Pos(), node2.WorldPos())
			//fmt.Println(node.Rot(), node2.Rot())
		}
	}

	return stop, update
}

func main() {
	fmt.Println("main()")

	app = rx.TestFwInit()
	sce = rx.TestFwCreateDefaultScene()
	sl = rx.NewSceneLoader()

	//stop, update := testBox()
	stop, update := testTriangle()
	//stop, update := testParent()
	var _ = stop

	for app.Step() {
		//println("step")
		dt := app.GetDt()
		update(dt)
	}

	rx.TestFwDeinit()

	println("exiting...")
}
