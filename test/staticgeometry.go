package main

import (
	//"bitbucket.org/coaljoe/rx"
	"../../rx"
	"fmt"
	. "math"

	. "bitbucket.org/coaljoe/rx/math"
)

func main() {
	fmt.Println("main()")

	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()

	sl := rx.NewSceneLoader()
	_ = sl

	//sl.Load("res/models/primitives/triangle/triangle.dae")
	nodes := sl.Load("res/models/primitives/box/box.dae")
	sl.Spawn()

	meshNode := nodes[0]

	sg := rx.NewStaticGeometryNode()
	fmt.Println("XXX sg:", sg)
	fmt.Printf("sg -> %p\n", sg)
	fmt.Printf("sg.Mesh -> %p\n", sg.Mesh)
	fmt.Printf("sg.Mesh.StaticGeometry -> %p\n", sg.Mesh.StaticGeometry)
	sg.Mesh.StaticGeometry.AddMesh(meshNode.Mesh)
	tr := rx.NewTransform()
	tr.SetPos(Vec3{2, 2, 0})
	sg.Mesh.StaticGeometry.AddMeshAt(meshNode.Mesh, tr.WorldMat())
	sg.Mesh.StaticGeometry.Build()
	sg.Mesh.SetMaterial(meshNode.Mesh.Material())
	sce.Add(sg)

	meshNode.SetPos(Vec3{-2, -2, 0})

	v := 0.0
	vInc := 50.0
	rotate := true
	for app.Step() {
		//println("step")
		dt := app.GetDt()

		if rotate {
			v = Mod(v+vInc*dt, 360)
			//sce.GetNode("turret").SetRot(Vec3{0, 0, v})
			sce.GetNodeByName("box").SetRot(Vec3{0, 0, v})
			//sce.GetNode("triangle").SetRot(Vec3{v, 0, 0})
		}
	}

	rx.TestFwDeinit()

	println("exiting...")
}
