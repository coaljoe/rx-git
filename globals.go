package rx

import (
	"bitbucket.org/coaljoe/lib/ider"
	. "bitbucket.org/coaljoe/rx/math"
)

// Vars
var (
	glfwInitialized = false
)

// RenderLayer
type RenderLayer int

const (
	RenderLayer_Default RenderLayer = iota
	RenderLayer_Debug
)

// NodeType
type NodeType int

const (
	NodeType_Unknown = iota
	NodeType_Empty
	NodeType_Mesh
	NodeType_Camera
	NodeType_Light
	//NodeType_StaticGeometry
	NodeType_Custom
)

// Ider
var _ider = ider.NewIder()

// Color
var (
	ColorBlack   = Vec3{0, 0, 0}
	ColorWhite   = Vec3{1, 1, 1}
	ColorRed     = Vec3{1, 0, 0}
	ColorGreen   = Vec3{0, 1, 0}
	ColorBlue    = Vec3{0, 0, 1}
	ColorYellow  = Vec3{1, 1, 0}
	ColorMagenta = Vec3{1, 0, 1}
	ColorCyan    = Vec3{0, 1, 1}
)
