module bitbucket.org/coaljoe/rx

require (
	bitbucket.org/coaljoe/lib v0.0.0-20190306224124-ae61fdea8bb6
	bitbucket.org/coaljoe/lib/sr v0.0.0-20190220232856-c944304543fd
	github.com/azul3d/native-al v0.0.0-20140921145107-918d7a95ab2a
	github.com/bitly/go-simplejson v0.5.0
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/cheekybits/genny v1.0.0
	github.com/davecgh/go-spew v1.1.1
	github.com/disintegration/imaging v1.6.0
	github.com/emirpasic/gods v1.12.0
	github.com/go-gl/gl v0.0.0-20181026044259-55b76b7df9d2
	github.com/go-gl/glfw v0.0.0-20181213070059-819e8ce5125f
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/hraban/opus v0.0.0-20180426093920-0f2e0b4fc6cd
	github.com/kr/pretty v0.1.0 // indirect
	github.com/qmuntal/gltf v0.9.1
	golang.org/x/image v0.0.0-20180708004352-c73c2afc3b81
)

go 1.13
