package rx

// cardnode
type Card struct {
	*Node
	x, y, w, h, z float64
	tex           *Texture
}

func (c *Card) Width() float64  { return c.w }
func (c *Card) Height() float64 { return c.h }
func (c *Card) PosX() float64   { return c.x }
func (c *Card) PosY() float64   { return c.y }

func NewCard() *Card {
	c := &Card{
		Node: NewNode(NodeType_Custom),
		x:    0, y: 0,
		w: 1, h: 1,
		z: 0,
	}
	c.SetContent(c)
	c.build()
	return c
}

func (c *Card) build() {

}

func (c *Card) Spawn() {
	rxi.Scene().AddRenderable(c)
}

func (c *Card) Destroy() {
	rxi.Scene().RemoveRenderable(c)
	//c.tex.Free() bug?
}

func (c *Card) SetDim(x, y, w, h float64) {
	c.x = x
	c.y = y
	c.w = w
	c.h = h
}

func (c *Card) SetZ(z float64) {
	c.z = z
}

func (c *Card) SetTexture(tex *Texture) {
	c.tex = tex
}

func (c *Card) Render(r *Renderer) {
	//p("XXX1", &c, c.x, c.y, c.w, c.h)
	if !c.visible {
		return
	}

	if c.tex != nil {
		c.tex.Bind()
	}

	r.renderQuad(c.x, c.y, c.w, c.h, c.z, false)
	//r.RenderQuadFlipY(c.x, c.y, c.w, c.h, c.z)

	if c.tex != nil {
		c.tex.Unbind()
	}
}

func (c *Card) Update(dt float64) {

}
