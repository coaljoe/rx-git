package rx

import (
	"bitbucket.org/coaljoe/lib/xlog"
	"os"
)

var (
	Conf conf
)

type conf struct {
	max_anisotropy      int
	res_path            string
	res_path_rel        string
	debug_draw          bool
	log_level           xlog.LogLevel
	tmp_dir             string
	EnableCulling       bool   // RW
	DefaultMaterialPath string // RW fixme?: only relative
	DisableExitOnEscape bool
	DisableExitOnQ      bool
}

func SetDefaultConf() {
	Conf.max_anisotropy = 8
	//conf.res_path = "src/rx/res/"
	Conf.res_path = "NOTSET" // no trailing slash
	Conf.debug_draw = false
	Conf.log_level = xlog.LogDebug
	//Conf.tmp_dir = "/tmp"
	Conf.tmp_dir = os.TempDir()
	Conf.EnableCulling = false
	//Conf.DefaultMaterialPath = Conf.res_path + "/materials/static.json"
	Conf.DefaultMaterialPath = "/materials/static.json"
	Conf.DisableExitOnEscape = false
	Conf.DisableExitOnQ = false
}

//func (c *conf) SetResPath(s string) {
//	c.res_path = s
//}

func ConfSetResPath(s string) {
	Conf.res_path = s
}

func ConfGetResPath() string {
	return Conf.res_path
}

func ConfSetDebugDraw(v bool) {
	Conf.debug_draw = v
}
