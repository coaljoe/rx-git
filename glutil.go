// GL utils.
package rx

import (
	"fmt"
	"github.com/disintegration/imaging"
	"image"
	"image/color"
	"image/png"
	"math/rand"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/go-gl/gl/v2.1/gl"
	"github.com/go-gl/glfw/v3.1/glfw"
)

func init() {
	// FIXME: duplicate?
	//pp("derp2")
	//pp("GOMAXPROCS:", runtime.GOMAXPROCS(0))

	// from https://github.com/go-gl/examples/blob/master/glfw31-gl21-cube/cube.go

	// GLFW event handling must run on the main OS thread
	runtime.LockOSThread()
}

// Fixme: move to rx.util?
// Get desktop resolution.
// Useful for fullscreen mode settings.
func GetDesktopResolution() (int, int) {
	sx, sy := 0, 0

	// Temporary init glfw
	if !glfwInitialized {
		err := glfw.Init()
		if err != nil {
			println("can't temporary init glfw")
			panic(err)
		}
	}

	var m *glfw.Monitor
	m = glfw.GetPrimaryMonitor()
	mode := m.GetVideoMode()
	sx = mode.Width
	sy = mode.Height

	// Deinit the temporary glfw
	if !glfwInitialized {
		glfw.Terminate()
	}

	return sx, sy
}

// Save the screenshot.
// XXX use doSaveScreenShot for actual saving code.
func SaveScreenshot(path string) bool {
	app := Rxi().App
	app.ssqTake = true
	app.ssqName = path
	return true
}

// Do save screenshot.
func doSaveScreenshot(path string) bool {

	if !glfwInitialized {
		println("ERR: glfw is not initialized")
		return false
	}

	w := Rxi().App.Win().Width()
	h := Rxi().App.Win().Height()

	im := image.NewNRGBA(image.Rect(0, 0, w, h))
	//im.Pix = image_data

	//image_data := make([]uint8, w*h*4)

	//makeContextCurrent()

	//gl.PixelStorei(gl.PACK_ALIGNMENT, 1)
	//gl.ReadPixels(0, 0, int32(w), int32(h), gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(image_data))
	gl.ReadPixels(0, 0, int32(w), int32(h), gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(&im.Pix[0]))
	glcheck()

	// Flip texture
	im = imaging.FlipV(im)

	// Fill image with random noise.
	// For tesing.
	if false {
		for y := 0; y < w; y++ {
			for x := 0; x < w; x++ {
				v := uint8(rand.Intn(255))
				c := color.RGBA{v, v, v, 255}
				im.Set(x, y, c)
			}
		}
	}

	of, err := os.Create(path)
	defer of.Close()
	if err != nil {
		Log.Err("can't save image to path; path: ", path)
		panic(err)
	}

	enc := &png.Encoder{CompressionLevel: 9}
	//enc := &png.Encoder{CompressionLevel: -1}
	enc.Encode(of, im)

	return true
}

// Exported glcheck.
func Xglcheck() {
	glcheck()
}

func makeContextCurrent() {
	// FIXME: redundant?
	Rxi().App.Win().GlfwWin().MakeContextCurrent()
}

func checkGLExt(name string) bool {
	makeContextCurrent()
	//pp("derp")
	return glfw.ExtensionSupported(name)
}

func newProgram(vertexShaderSource, fragmentShaderSource string) (uint32, error) {
	//makeContextCurrent()

	vertexShader := compileShader(vertexShaderSource, gl.VERTEX_SHADER)
	fragmentShader := compileShader(fragmentShaderSource, gl.FRAGMENT_SHADER)

	program := gl.CreateProgram()

	gl.AttachShader(program, vertexShader)
	gl.AttachShader(program, fragmentShader)
	gl.LinkProgram(program)

	var status int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(program, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(program, logLength, nil, gl.Str(log))

		//return 0, errors.New(fmt.Sprintf("failed to link program: %v", log))
		return 0, fmt.Errorf(fmt.Sprintf("failed to link program: %v", log))
	}

	gl.DeleteShader(vertexShader)
	gl.DeleteShader(fragmentShader)

	return program, nil
}

func compileShader(source string, shaderType uint32) uint32 {
	//makeContextCurrent()

	shader := gl.CreateShader(shaderType)

	//csource := gl.Str(source)
	//gl.ShaderSource(shader, 1, &csource, nil)

	csources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		// save shader source
		var ext string
		switch shaderType {
		case gl.VERTEX_SHADER:
			ext = "vert"
		case gl.FRAGMENT_SHADER:
			ext = "frag"
		default:
			panic("unknown shaderType")
		}
		fout := filepath.Join(os.TempDir(), "crash."+ext)

		f, err := os.Create(fout)
		defer f.Close()
		if err != nil {
			println("can't save shader to " + fout)
			panic(err)
		}
		f.WriteString(source)
		Log.Inf("shader saved to " + fout)

		switch ext {
		case "vert":
			Log.Err("vs compile error")
		case "frag":
			Log.Err("fs compile error")
		}
		panic(fmt.Errorf("failed to compile (log:\n%v)", log))
		//return 0, fmt.Errorf("failed to compile (source:\n%v) (log:\n%v)", source, log)
	}

	return shader
}

func glcheck() {
	//return
	if !Debug {
		return
	}

	if err := gl.GetError(); err != gl.NO_ERROR {
		switch err {
		case gl.INVALID_ENUM:
			trace("OpenGL Error: GL_INVALID_ENUM")
		case gl.INVALID_VALUE:
			trace("OpenGL Error: GL_INVALID_VALUE")
		case gl.INVALID_OPERATION:
			trace("OpenGL Error: GL_INVALID_OPERATION")
		case gl.STACK_OVERFLOW:
			trace("OpenGL Error: GL_STACK_OVERFLOW")
		case gl.STACK_UNDERFLOW:
			trace("OpenGL Error: GL_STACK_UNDERFLOW")
		case gl.OUT_OF_MEMORY:
			trace("OpenGL Error: GL_OUT_OF_MEMORY")
		}
	}

	// gl.NO_ERROR
}
