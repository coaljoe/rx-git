package rx

import (
  "bitbucket.org/coaljoe/lib/sr"
)

func (n *Node) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		n.id, n.Transform, n.name,
		n.nodetype, n.renderlayer, n.visible,
		n.Mesh, n.Light, n.Camera,
		n.parent, n.children, n.culling,
		//n.content,
		n.tags,
	), nil
}

func (n *Node) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&n.id, &n.Transform, &n.name,
		&n.nodetype, &n.renderlayer, &n.visible,
		&n.Mesh, &n.Light, &n.Camera,
		&n.parent, &n.children, &n.culling,
		//&n.content,
		&n.tags,
	)

	return nil
}
