package rx

import (
	"bitbucket.org/coaljoe/lib/sr"
	. "bitbucket.org/coaljoe/rx/math"
	"fmt"
	"reflect"
	"testing"
)

func TestSerializeTransform(_t *testing.T) {
	t := NewTransform()
	t.SetPos(Vec3{100, 100, 100})

	b, _ := t.GobEncode()
	t.SetPos(Vec3Zero)
	t.GobDecode(b)

	v := Vec3{100, 100, 100}
	if t.Pos() != v {
		_t.Fail()
		panic(2)
	}

	println("derp")
	ta, tb := NewTransform(), NewTransform()
	//fmt.Printf("ta: %#v\n\n", ta)
	//fmt.Printf("tb: %#v\n\n", tb)
	if !reflect.DeepEqual(ta, tb) {
		_t.Fail()
		panic(3)
	}
	println("/derp")

	t2 := NewTransform()
	t2.SetPos(Vec3{100, 100, 100})
	t3 := &Transform{}
	//t3 := NewTransform()
	//t3.SetPos(Vec3{100, 100, 100})
	_ = t2

	b = sr.SerializeValue(t2)
	sr.DeserializeValue(b, t3)
	fmt.Printf("t2: %#v\n\n", t2)
	fmt.Printf("t3: %#v\n\n", t3)

	if !reflect.DeepEqual(t2, t3) {
		_t.Fail()
		panic(4)
	}

}

func TestSerializeNode(t *testing.T) {
	var n1, n2 *Node

	if false {
		n1 = NewNode(NodeType_Mesh)
		//n2 := NewNode(NodeType_Mesh)
		n2 = n1.clone()

		fmt.Printf("n1: %#v\n\n", n1)
		fmt.Printf("n2: %#v\n\n", n2)
		if !reflect.DeepEqual(n1, n2) {
			t.Fail()
			panic(1)
		}
	}

	n1 = NewNode(NodeType_Mesh)
	//n2 = NewNode(NodeType_Mesh)
	n2 = &Node{}
	n2.setDefaults()

	b := sr.SerializeValue(n1)
	sr.DeserializeValue(b, n2)
	fmt.Printf("n1: %#v\n\n", n1)
	fmt.Printf("n2: %#v\n\n", n2)
	//fmt.Printf("n1: %#v\n\n", n1.Transform)
	//fmt.Printf("n2: %#v\n\n", n2.Transform)

	if !reflect.DeepEqual(n1, n2) {
		t.Log("Not equal")
		t.Fail()
		//panic(2)
	}

	nx := NewNode(NodeType_Mesh)
	if nx.id != n1.id+1 {
		t.Logf("Wrong next id; %d != %d", nx.id, n1.id+1)
		t.Fail()
	}

}
