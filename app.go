package rx

import (
	_ "fmt"

	"math"
	"runtime"
	"time"

	. "bitbucket.org/coaljoe/rx/math"

	"github.com/go-gl/gl/v2.1/gl"
	"github.com/go-gl/glfw/v3.1/glfw"
)

func init() {
	// from https://github.com/go-gl/examples/blob/master/glfw31-gl21-cube/cube.go

	// GLFW event handling must run on the main OS thread
	runtime.LockOSThread()
}

// ***** Window *****

type Window struct {
	width, height int
	fullscreen    bool
	lockCursor    bool
	title         string
	glfwWin       *glfw.Window
	glfwWinSlave  *glfw.Window
}

func (w *Window) Width() int                 { return w.width }
func (w *Window) Height() int                { return w.height }
func (w *Window) GlfwWin() *glfw.Window      { return w.glfwWin }
func (w *Window) GlfwWinSlave() *glfw.Window { return w.glfwWinSlave }
func (w *Window) Size() (int, int)           { return w.width, w.height }
func (w *Window) Title() string              { return w.title }

func NewWindow() *Window {
	println("window.new")
	return &Window{
		lockCursor: false,
		fullscreen: false,
		title:      "rx",
	}
}

func (w *Window) Init(width, height int, fullscreen bool) bool {
	println("window.init")

	// check args
	if width < 1 || height < 1 {
		panic("bad width and height")
	}

	w.width = width
	w.height = height
	w.fullscreen = fullscreen

	/* Init GLFW */

	err := glfw.Init()
	if err != nil {
		println("Can't init glfw!")
		panic(err)
	}
	// Set global var
	glfwInitialized = true
	//defer glfw.Terminate()

	glfw.WindowHint(glfw.ContextVersionMajor, 2)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)

	//glfwWindowHint(GLFW_WINDOW_NO_RESIZE, true)
	//glfwWindowHint(glfw.FSAA_SAMPLES, 8)
	//glfw.WindowHint(glfw.Samples, 0)
	//glfw.WindowHint(glfw.Samples, 2)
	//glfw.WindowHint(glfw.Samples, 8)
	glfw.WindowHint(glfw.Samples, glfw.DontCare) // use os settings
	glfw.WindowHint(glfw.Resizable, gl.FALSE)
	glfw.WindowHint(glfw.Focused, gl.TRUE)

	if Debug {
		//GLFW_OPENGL_DEBUG_CONTEXT
		glfw.WindowHint(glfw.OpenGLDebugContext, gl.TRUE)
	}

	var monitor *glfw.Monitor
	if w.fullscreen {
		monitor = glfw.GetPrimaryMonitor()
	}

	glfw.WindowHint(glfw.RedBits, 8)
	glfw.WindowHint(glfw.GreenBits, 8)
	glfw.WindowHint(glfw.BlueBits, 8)
	glfw.WindowHint(glfw.AlphaBits, 0)
	glfw.WindowHint(glfw.DepthBits, 24)
	glfw.WindowHint(glfw.StencilBits, 0)

	window, err := glfw.CreateWindow(w.width, w.height, w.title, monitor, nil)
	if err != nil {
		panic(err)
	}
	w.glfwWin = window

	// Slave window.

	glfw.WindowHint(glfw.ContextVersionMajor, 2)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)

	glfw.WindowHint(glfw.Visible, gl.FALSE)
	sWindow, err := glfw.CreateWindow(1, 1, "rx_slave", nil, w.glfwWin)
	if err != nil {
		panic(err)
	}
	w.glfwWinSlave = sWindow
	//fmt.Println(window)
	//fmt.Println(sWindow)
	//pp(sWindow)

	// glfw context.
	window.MakeContextCurrent()

	// Input callbacks.
	//glfw.SetMouseButtonCallback(w.glfwWin, w.on_mouse_button)
	//glfw.SetScrollCallback(w.glfwWin, w.on_mouse_scroll)
	//glfw.SetCursorPosCallback(w.glfwWin, w.on_mouse_pos)
	window.SetKeyCallback(_InputSys.on_key)
	window.SetCursorPosCallback(_InputSys.on_mouse_move)
	window.SetMouseButtonCallback(_InputSys.on_mouse_button)
	window.SetFocusCallback(w.on_focus)
	window.SetCursorEnterCallback(w.on_cursor_enter)

	glfw.SwapInterval(1) // vsync
	//glfw.SetWindowPos(w.glfwWin, 100, 100)
	//glfw.Disable(glfw.MOUSE_CURSOR)

	/* Init GL */

	// have to call this here or some OpenGL calls like CreateProgram will cause segfault
	if err := gl.Init(); err != nil {
		println("GL init failed. (Can't init GLEW(?))")
		panic(err)
	}
	// important?
	//gl.Enable(gl.MULTISAMPLE)     // xxx: not tested / don't needed?
	//gl.Enable(gl.MULTISAMPLE_ARB) // xxx: not tested / don't needed?
	gl.Viewport(0, 0, int32(w.width), int32(w.height))
	//gl.ClearColor(0.0, 0.0, 0.0, 1.0)
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	gl.MatrixMode(gl.MODELVIEW)

	// lock cursor
	//w.updateCursorLock()
	/*
		  XXX not working: X11 async bug
		  // center cursor
		  w.glfwWin.SetCursorPos(float64(w.width/2), float64(w.height/2))
		  fmt.Println(w.glfwWin.GetCursorPos())
			panic(2)
	*/
	return true
}

func (w *Window) IsKeyPressed(key glfw.Key) bool {
	return w.glfwWin.GetKey(key) == glfw.Press
}

func (w *Window) IsMouseButtonPressed(btn glfw.MouseButton) bool {
	return w.glfwWin.GetMouseButton(btn) == glfw.Press
}

func (w *Window) on_focus(win *glfw.Window, focused bool) {
	if focused {
		println("FOCUSED")
	} else {
		println("NOT FOCUSED")
	}
}

func (w *Window) on_cursor_enter(win *glfw.Window, entered bool) {
	if entered {
		println("ENTERED")
	} else {
		println("NOT ENTERED")
	}
}

func (w *Window) updateCursorLock() {
	if w.lockCursor {
		w.glfwWin.SetInputMode(glfw.CursorMode, glfw.CursorDisabled)
	} else {
		w.glfwWin.SetInputMode(glfw.CursorMode, glfw.CursorNormal)
	}
}

func (w *Window) SetTitle(s string) {
	w.title = s
	w.glfwWin.SetTitle(s)
	//glfw.SetWindowTitle(w.glfwWin, s)
}

func (w *Window) HasExit() bool {
	return w.glfwWin.ShouldClose()
}

// Handle input.
func (w *Window) updateInput() {
	glfw.PollEvents()
}

func (w *Window) Update(dt float64) {
	//if !w.HasExit() {
	w.glfwWin.SwapBuffers()
	//glfw.PollEvents()
	//}
}

// ***** App *****

type App struct {
	win *Window
	dt  float64
	//dtms int64
	lastTime     time.Time
	frame        uint64        // Frame number
	frameCounter time.Duration // Used to count fps
	nframes      uint64        // Used to count fps
	fps          int
	cam_a        float64 // fixme
	cam_rot_adj  float64 // fixme
	// Screenshot queue
	ssqTake bool
	ssqName string
}

func (a *App) Win() *Window                        { return a.win }
func (a *App) Frame() uint64                       { return a.frame }
func (a *App) Fps() int                            { return a.fps }
func (a *App) OnMouseMove(x, y int)                {}
func (a *App) OnMousePress(btn glfw.MouseButton)   {}
func (a *App) OnMouseRelease(btn glfw.MouseButton) {}

func NewApp() *App {
	println("app.new")
	return &App{
		win:   NewWindow(),
		cam_a: 0,
	}
}

func (a *App) OnKeyPress(key glfw.Key) {
	println("App.OnKeyPress", key)

	cam := rxi.Camera.Camera

	switch key {
	// close on q
	case glfw.KeyQ:
		if !Conf.DisableExitOnQ {
			a.win.glfwWin.SetShouldClose(true)
		}
	case glfw.KeyEscape:
		if !Conf.DisableExitOnEscape {
			a.win.glfwWin.SetShouldClose(true)
		}
	case glfw.Key2, glfw.KeyKPSubtract:
		if cam.Zoom() >= 1.0 {
			cam.AdjZoom(+1.0)
		} else {
			cam.AdjZoom(+0.5)
		}
	case glfw.Key1, glfw.KeyKPAdd:
		if cam.Zoom() <= 1.0 {
			cam.AdjZoom(-0.5)
		} else {
			cam.AdjZoom(-1.0)
		}
	case glfw.KeyF11, glfw.KeyF10: // f11 is conflicting with the vm
		Conf.debug_draw = !Conf.debug_draw
		Log.Inf("[App] Toggle debug draw:", Conf.debug_draw)
	case glfw.KeyF12:
		a.win.lockCursor = !a.win.lockCursor
		a.win.updateCursorLock()
		Log.Inf("[App] Toggle cursor lock:", a.win.lockCursor)
	}

	/*
		var adj, adjamt float64
		adjamt = 90
		if key == glfw.KeyZ {
			adj = +adjamt
		} else if key == glfw.KeyX {
			adj = -adjamt
		} else {
			return
		}
		println("adj", adj)
		a.cam_rot_adj = adj
	*/
}

func (a *App) OnKeyRelease(key glfw.Key) {
	println("App.OnKeyRelease", key)
	switch key {
	//case glfw.KeyZ, glfw.KeyX:
	//	a.cam_rot_adj = 0
	default:
		return
	}
}

func (a *App) SetShouldClose(v bool) {
	a.win.glfwWin.SetShouldClose(v)
}

// Fixme?
func (a *App) init(width, height int, fullscreen bool) {
	a.win.Init(width, height, fullscreen)
	_InputSys.AddEventHandler(a)
}

func (a *App) Init(width, height int) bool {
	println("app.init")

	a.init(width, height, false)
	return true
}

func (a *App) InitFullscreen(width, height int) bool {
	println("app.initFullscreen")

	a.init(width, height, true)
	return true
}

func (a *App) Step() bool {
	// http://gameprogrammingpatterns.com/game-loop.html

	thisTime := time.Now()
	if a.frame == 0 {
		a.lastTime = thisTime
	}
	passedTime := thisTime.Sub(a.lastTime) // Same as dt but in time format
	//a.dt = float64(thisTime.Sub(a.lastTime))/float64(time.Second)
	//a.dt = thisTime.Sub(a.lastTime).Seconds()
	a.dt = passedTime.Seconds()
	//a.dtms = thisTime.Sub(a.lastTime) / time.Millisecond
	//a.dt = float64(a.dtms / 1000)

	a.frameCounter += passedTime
	if a.frameCounter >= time.Second {
		//println(a.fps)
		a.fps = int(a.nframes)
		a.nframes = 0
		a.frameCounter -= time.Second
	}

	// Handle input
	a.win.updateInput()

	if a.win.HasExit() {
		a.Quit()
		return false
	}

	// Update app
	a.Update(a.dt)

	// Do OpenGL stuff
	rxi.Render()

	// Update window
	a.win.Update(a.dt)
	//doSaveScreenshot(fmt.Sprintf("frame_%d.png", a.frame))

	// Finally
	a.lastTime = thisTime
	a.frame++
	a.nframes++

	// Handle screenshot queue
	if a.ssqTake {
		doSaveScreenshot(a.ssqName)
		a.ssqName = ""
		a.ssqTake = false
	}

	// Do post window flip stuff
	rxi.Renderer().PostWindowFlip()

	// XXX doesn't work here becaues of previous call (PostWindowFlip)
	//doSaveScreenshot(fmt.Sprintf("frame_%d.png", a.frame))
	return true
}

func (a *App) GetDt() float64 {
	return a.dt
}

func (a *App) GetFrameRate() int {
	return a.Fps()
}

/*
func (a *App) GetDtMs() int64 {
  return a.dtms
}
*/

func (a *App) Quit() {
	println("App.Quit")
	rxi.ResourceSys.FreeResources()
	glfw.Terminate()
}

func (a *App) updateCamera(dt float64) {
	if a.cam_rot_adj == 0 {
		return
	}
	//a.Rxi.Camera.Pos.X += 0.1
	//r := 5.0
	r := 10.0 // fixme
	nv := math.Mod(a.cam_a+a.cam_rot_adj*dt, 360)
	a.cam_a = nv
	//fmt.Println("a.cam_a", a.cam_a)
	aa := a.cam_a * (math.Pi / 180)
	// AdjPos()
	pos := rxi.Camera.Pos()
	//tpos := a.Rxi.Camera.Target()
	tpos := Vec3Zero
	pos.SetX(r*math.Sin(aa) + tpos.X())
	pos.SetZ(r*math.Cos(aa) + tpos.Z())
	//println("pos", pos.String())
	rxi.Camera.SetPos(pos)
}

func (a *App) Update(dt float64) {
	//println("app.update")

	if a.win.HasExit() {
		a.Quit()
		return
	}

	// render
	a.updateCamera(dt)
	rxi.Update(dt)

	/*
		// hacky X11 bug fix
		// http://www.glfw.org/docs/latest/group__input.html#ga04b03af936d906ca123c8f4ee08b39e7
		if a.frame == 0 {
			w := a.Win()
			tx, ty := float64(w.width/2), float64(w.height/2)
			w.glfwWin.SetCursorPos(tx, ty)
			px, py := w.glfwWin.GetCursorPos()
			if px != tx || py != ty {
				println("X11 bug fix failed")
				fmt.Println(w.glfwWin.GetCursorPos())
				panic(2)
			} else {
				println("DONE!")
				fmt.Println(w.glfwWin.GetCursorPos())
				w.updateCursorLock()
				//panic(2)
			}
		}
	*/
}
