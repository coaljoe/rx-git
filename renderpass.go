package rx

import (
	//. "fmt"
	_ "math"

	"github.com/go-gl/gl/v2.1/gl"
	//. "bitbucket.org/coaljoe/rx/math"
)

type RenderPassI interface {
	Name() string
	Render(r *Renderer)
}

type RenderPass struct {
	name string
	//inTex  *Texture // fboTex from previous renderpass
	//outTex *Texture // fboTex
	//inTex  uint32 // fboTex from previous renderpass
	outTex uint32 // fboTex
	prevRp *RenderPass
	fboRt  *RenderTarget // RP's default render target
}

func NewRenderPass(name string) *RenderPass {
	r := rxi.Renderer()
	rp := &RenderPass{
		name:   name,
		fboRt:  NewRenderTarget(name+"_rt", r.width, r.height, false),
		prevRp: nil,
	}
	/*
		// init
		if rp.prevRp != nil {
			rp.inTex = rp.prevRp.InTex()
		}
	*/
	return rp
}

func (rp *RenderPass) Name() string         { return rp.name }
func (rp *RenderPass) FboRt() *RenderTarget { return rp.fboRt }

// Get previous RP's texture.
func (rp *RenderPass) InTex() uint32 {
	if rp.prevRp == nil {
		Log.Err("no prev render pass")
		pp("err")
	}
	return rp.prevRp.Tex()
}

// Shortcut to fbo tex, same as OutTex.
func (rp *RenderPass) Tex() uint32 {
	return rp.fboRt.Tex()
}

// Set or unset previous render pass (nil to unset)
func (rp *RenderPass) SetPrevRenderPass(prevRp *RenderPass) {
	rp.prevRp = prevRp
}

func (rp *RenderPass) HasPrevRenderPass() bool {
	return rp.prevRp != nil
}

// **** RP Collection ****

// **** SmaaRp ****

type SmaaRenderPass struct {
	*RenderPass
	edgeShader        *Shader
	blendWeightShader *Shader
	neighborShader    *Shader
	//cameraPass2 *Shader
	edgeRt *RenderTarget
	tmpRt  *RenderTarget
	fboTex *Texture // pre-loaded fboTex

	// options
	use_smaa bool
}

func NewSmaaRenderPass() *SmaaRenderPass {
	rp := &SmaaRenderPass{
		RenderPass: NewRenderPass("SmaaRenderPass"),
		use_smaa:   true,
	}
	return rp
}

func (rp *SmaaRenderPass) Init(r *Renderer) {
	rp.edgeShader = NewShader("shaders/lib/smaa/smaaEdge.vert", "shaders/lib/smaa/smaaEdge.frag")
	glcheck()

	rp.blendWeightShader = NewShader("shaders/lib/smaa/smaaBlendWeight.vert", "shaders/lib/smaa/smaaBlendWeight.frag")
	glcheck()

	rp.neighborShader = NewShader("shaders/lib/smaa/smaaNeighbor.vert", "shaders/lib/smaa/smaaNeighbor.frag")
	glcheck()

	rp.edgeRt = NewRenderTarget("edgeRt", r.width, r.height, false)
	glcheck()

	rp.tmpRt = NewRenderTarget("tmpRt", r.width, r.height, false)
	glcheck()
}

func (rp *SmaaRenderPass) Render(r *Renderer) {

	//XXX fixme
	//r.RenderAll()
	//return

	//cam := r.curCam

	//if cam.HasRT() && cam.IsActive() { // fixme

	rp.edgeRt.Bind()

	// clear tmp_rt texture
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	r.RenderAll()

	rp.edgeRt.Unbind()
}

// **** FXAA RenderPass ****

type FxaaRenderPass struct {
	*RenderPass

	fsShader *Shader // fullscreen shader
	tmpRt    *RenderTarget

	preFboTex *Texture // pre-loaded fboTex

	// options
	useFxaa bool
}

func NewFxaaRenderPass() *FxaaRenderPass {
	rp := &FxaaRenderPass{
		RenderPass: NewRenderPass("FxaaRenderPass"),
		useFxaa:    true,
	}
	return rp
}

func (rp *FxaaRenderPass) Init(r *Renderer) {
	rp.fsShader = NewShader(
		"shaders/camera_fsquad.vert",
		"shaders/camera_fsquad.frag")
	glcheck()

	rp.tmpRt = NewRenderTarget("tmpRt", r.width, r.height, false)
	glcheck()

	rp.preFboTex = NewTextureOpt("tmp/font_960x540_rgb.png",
		TextureOptions{Flip: false, Nomipmaps: true})
}

func (rp *FxaaRenderPass) Render(r *Renderer) {

	//XXX fixme
	//r.RenderAll()
	//return

	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	//cam := r.curCam

	//if cam.HasRT() && cam.IsActive() { // fixme

	// XXX: not tested
	if !rp.HasPrevRenderPass() {

		// render scene to rendertarget
		rp.tmpRt.Bind()

		// clear tmp_rt texture
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		r.RenderAll()

		rp.tmpRt.Unbind()

		gl.Disable(gl.DEPTH_TEST)
	}

	// Pass1.
	// render the rendertarget texture with fullscreen shader
	rp.fsShader.Bind()

	gl.ActiveTexture(gl.TEXTURE0)
	//cam.rt.tex.Bind(gl.TEXTURE_2D)
	//gl.BindTexture(gl.TEXTURE_2D, cam.rt.tex)
	//gl.BindTexture(gl.TEXTURE_2D, rp.tmpRt.tex)
	if rp.HasPrevRenderPass() {
		// bind texture from previous RP
		gl.BindTexture(gl.TEXTURE_2D, rp.InTex())
	} else {
		// bind tmpRt texture
		gl.BindTexture(gl.TEXTURE_2D, rp.tmpRt.Tex())
	}
	//gl.BindTexture(gl.TEXTURE_2D, rp.preFboTex.tex)
	rp.fsShader.PassUniform1i("fboTex", 0)
	rp.fsShader.PassUniform1i("use_fxaa", btoi(rp.useFxaa))

	r.RenderFsQuad()

	rp.fsShader.Unbind()
	//cam.rt.tex.Unbind(gl.TEXTURE_2D)
	gl.BindTexture(gl.TEXTURE_2D, 0)

	gl.Enable(gl.DEPTH_TEST)

	//rp.tmpRt.Unbind()

	/*
	   #glBindFramebuffer(GL_READ_FRAMEBUFFER, cam.rt.fbo_id)
	   #glBindTexture(GL_TEXTURE_2D, cam.rt.tex_id);
	   #glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, cam.rt.width, cam.rt.height)
	   #glBindFramebuffer(GL_READ_FRAMEBUFFER, 0)
	*/
}

// **** TestRenderPass ****

type TestRenderPass struct {
	*RenderPass
	// fixme?
	cameraShader,
	cameraPass2 *Shader
	tmpRt *RenderTarget

	// options
	use_fxaa,
	use_sharpen_pass bool
	effect_number int
}

func NewTestRenderPass() (t *TestRenderPass) {
	t = &TestRenderPass{
		RenderPass:       NewRenderPass("NewTestRenderPass"),
		use_fxaa:         true,
		use_sharpen_pass: false,
		//effect_number:    0, //1, //5,
		effect_number: 0, //5, //5,
	}
	return
}

func (rp *TestRenderPass) Init(r *Renderer) {
	// fixme?
	rp.cameraShader = NewShader("shaders/camera_fsquad.vert",
		"shaders/camera_fsquad.frag")
	glcheck()
	rp.cameraPass2 = NewShader("shaders/camera_fsquad.vert", "shaders/camera_pass2.frag")
	glcheck()
	rp.tmpRt = NewRenderTarget("tmpRt", r.width, r.height, false)
	glcheck()
}

func (rp *TestRenderPass) Render(r *Renderer) {

	//XXX fixme
	//r.RenderAll()
	//return

	//cam := r.curCam

	//if cam.HasRT() && cam.IsActive() { // fixme

	rp.tmpRt.Bind()

	// clear tmp_rt texture
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	r.RenderAll() // fixme: check file rev.history

	rp.tmpRt.Unbind()

	// Pass1.
	// render the cam.rt texture with fsquad shader
	rp.cameraShader.Bind()

	gl.ActiveTexture(gl.TEXTURE0)
	//cam.rt.tex.Bind(gl.TEXTURE_2D)
	//gl.BindTexture(gl.TEXTURE_2D, cam.rt.tex)
	gl.BindTexture(gl.TEXTURE_2D, rp.tmpRt.Tex())
	rp.cameraShader.PassUniform1i("fboTex", 0)
	rp.cameraShader.PassUniform1i("use_fxaa", btoi(rp.use_fxaa))

	r.RenderFsQuad()

	rp.cameraShader.Unbind()
	//cam.rt.tex.Unbind(gl.TEXTURE_2D)
	gl.BindTexture(gl.TEXTURE_2D, 0)

	//rp.tmpRt.Unbind()

	/*
	   #glBindFramebuffer(GL_READ_FRAMEBUFFER, cam.rt.fbo_id)
	   #glBindTexture(GL_TEXTURE_2D, cam.rt.tex_id);
	   #glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, cam.rt.width, cam.rt.height)
	   #glBindFramebuffer(GL_READ_FRAMEBUFFER, 0)
	*/
	/*

		// Pass2.
		//
		// render to screen
		gl.Enable(gl.TEXTURE_2D)
		rp.cameraPass2.Bind()

		gl.ActiveTexture(gl.TEXTURE0)
		//glBindTexture(GL_TEXTURE_2D, r.tmp_rt.tex_id)
		//rp.tmpRt.tex.Bind(gl.TEXTURE_2D)
		gl.BindTexture(gl.TEXTURE_2D, rp.tmpRt.tex)
		//#glBindTexture(GL_TEXTURE_2D, cam.rt.tex_id)
		rp.cameraPass2.PassUniform1i("fboTex", 0)
		rp.cameraPass2.PassUniform1i("use_sharpen_pass", btoi(rp.use_sharpen_pass))
		rp.cameraPass2.PassUniform1i("effect_number", rp.effect_number)

		r.RenderFsQuad()

		//glUseProgram(0)
		rp.cameraPass2.Unbind()
		//glBindTexture(GL_TEXTURE_2D, 0)
		//rp.tmpRt.tex.Unbind(gl.TEXTURE_2D)
		gl.BindTexture(gl.TEXTURE_2D, 0)
	*/
}

// **** DepthRenderPass ****

type DepthRenderPass struct {
	*RenderPass
	// fixme?
	cameraShader *Shader
	depthRt      *RenderTarget
}

func NewDepthRenderPass() (t *DepthRenderPass) {
	t = &DepthRenderPass{
		RenderPass: NewRenderPass("NewDepthRenderPass"),
	}
	return
}

func (rp *DepthRenderPass) Init(r *Renderer) {
	// fixme?
	rp.cameraShader = NewShader("shaders/camera_fsquad_depth.vert", "shaders/camera_fsquad_depth.frag")
	rp.depthRt = NewRenderTarget("depthRt", r.width, r.height, true)
}

func (rp *DepthRenderPass) Render(r *Renderer) {

	//cam := r.curCam

	// Render scene to depthRt.
	//
	rp.depthRt.Bind()

	// clear tmp_rt texture
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	// render scene
	//r.renderScene()
	//r.renderRenderables()
	//r.renderDebug()
	r.RenderAll()

	rp.depthRt.Unbind()

	// Render the cam.rt texture with fsquad shader.
	//
	rp.cameraShader.Bind()
	rp.cameraShader.PassUniform1i("depthTex", 0)

	gl.ActiveTexture(gl.TEXTURE0)
	//rp.depthRt.tex.Bind(gl.TEXTURE_2D)
	gl.BindTexture(gl.TEXTURE_2D, rp.depthRt.Tex())

	r.RenderFsQuad()

	rp.cameraShader.Unbind()
	//rp.depthRt.tex.Unbind(gl.TEXTURE_2D)
	gl.BindTexture(gl.TEXTURE_2D, 0)
}

// **** DepthCombRenderPass ****

type DepthCombRenderPass struct {
	*RenderPass
	BgDepthTex *Texture
	BgTex      *Texture
	// fixme?
	combShader *Shader
	fboRt      *RenderTarget
	depthRt    *RenderTarget
}

func NewDepthCombRenderPass() (rp *DepthCombRenderPass) {
	rp = &DepthCombRenderPass{
		RenderPass: NewRenderPass("NewDepthCombRenderPass"),
	}
	return
}

func (rp *DepthCombRenderPass) Init(r *Renderer) {
	// fixme?
	rp.combShader = NewShader("shaders/camera_fsquad_depth_combine.vert",
		"shaders/camera_fsquad_depth_combine.frag")
	rp.depthRt = NewRenderTarget("depthRt", r.width, r.height, true)
	rp.fboRt = NewRenderTarget("fboRt", r.width, r.height, false)
}

func (rp *DepthCombRenderPass) Render(r *Renderer) {

	//cam := r.curCam

	// Render dynamic objects to fbo
	//
	rp.fboRt.Bind()

	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	r.RenderAll()

	//rp.depthRt.Unbind()
	rp.fboRt.Unbind()

	// Render scene to depthRt.
	//
	rp.depthRt.Bind()

	// clear tmp_rt texture
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	// render scene
	//r.renderScene()
	//r.renderRenderables()
	//r.renderDebug()
	r.RenderAll()

	rp.depthRt.Unbind()

	// Render the cam.rt texture with fsquad shader.
	//
	rp.combShader.Bind()
	rp.combShader.PassUniform1i("fboTex", 0)
	rp.combShader.PassUniform1i("depthTex", 1)
	rp.combShader.PassUniform1i("bgTex", 2)
	rp.combShader.PassUniform1i("bgDepthTex", 3)

	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, rp.fboRt.Tex())
	gl.ActiveTexture(gl.TEXTURE1)
	gl.BindTexture(gl.TEXTURE_2D, rp.depthRt.Tex())
	gl.ActiveTexture(gl.TEXTURE2)
	gl.BindTexture(gl.TEXTURE_2D, rp.BgTex.Tex())
	gl.ActiveTexture(gl.TEXTURE3)
	gl.BindTexture(gl.TEXTURE_2D, rp.BgDepthTex.Tex())

	r.RenderFsQuad()

	rp.combShader.Unbind()
	gl.BindTexture(gl.TEXTURE_2D, 0)

	// end
}

var _ = `

// **** TDepthCombRenderPass ****

type TDepthCombRenderPass struct {
  *RenderPass
  BgDepthTex *Texture
  BgTex *Texture
  // fixme?
  combShader *Shader
  fboRt *RenderTarget
  depthRt *RenderTarget
  bgTexRt *RenderTarget
  bgDepthTexRt *RenderTarget
}

func NewTDepthCombRenderPass() (rp *TDepthCombRenderPass) {
  rp = &TDepthCombRenderPass{
    RenderPass: NewRenderPass("NewTDepthCombRenderPass"),
  }
  return
}

func (rp *TDepthCombRenderPass) Init(r *Renderer) {
  // fixme?
  rp.combShader = NewShader(conf.res_path + "shaders/camera_fsquad_depth_combine.vert",
                            conf.res_path + "shaders/camera_fsquad_depth_combine.frag")
  rp.depthRt = NewRenderTarget("depthRt", r.width, r.height, true)
  rp.fboRt = NewRenderTarget("fboRt", r.width, r.height, false)
  rp.bgTexRt = NewRenderTarget("bgTexRt", r.width, r.height, false)
  rp.bgDepthTexRt = NewRenderTarget("bgDepthTexRt", r.width, r.height, false)
}

func (rp *TDepthCombRenderPass) Render(r *Renderer) {

  cam := r.curCam

  //pp(rxi.Scene().renderables)

  if cam.IsActive() {

    // Render dynamic objects to fbo
    //
    rp.fboRt.Bind()

    gl.Clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT)

    r.RenderAll()

    //rp.depthRt.Unbind()
    rp.fboRt.Unbind()


    // Render bdTexRt
    //
    rp.bgTexRt.Bind()

    gl.Clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT)

    // tiledbackground renderable
    //tbr := rxi.Scene().renderables[0]
    //tbr.Render()
    r.renderRenderables()

    rp.bgTexRt.Unbind()


    // Render scene to depthRt.
    //
    rp.depthRt.Bind()

    // clear tmp_rt texture
    gl.Clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT)

    // render scene
    //r.renderScene()
    //r.renderRenderables()
    //r.renderDebug()
    r.RenderAll()

    rp.depthRt.Unbind()
    

    // Render the cam.rt texture with fsquad shader.
    //
    gl.Enable(gl.TEXTURE_2D)
    rp.combShader.Bind()
    rp.combShader.PassUniform1i("fboTex", 0)
    rp.combShader.PassUniform1i("depthTex", 1)
    rp.combShader.PassUniform1i("bgTex", 2)
    rp.combShader.PassUniform1i("bgDepthTex", 3)

    gl.ActiveTexture(gl.TEXTURE0)
    gl.BindTexture(gl.TEXTURE_2D, rp.fboRt.tex)
    gl.ActiveTexture(gl.TEXTURE1)
    gl.BindTexture(gl.TEXTURE_2D, rp.depthRt.tex)
    gl.ActiveTexture(gl.TEXTURE2)
    //gl.BindTexture(gl.TEXTURE_2D, rp.BgTex.tex)
    gl.BindTexture(gl.TEXTURE_2D, rp.bgTexRt.tex)
    gl.ActiveTexture(gl.TEXTURE3)
    //gl.BindTexture(gl.TEXTURE_2D, rp.BgDepthTex.tex)
    gl.BindTexture(gl.TEXTURE_2D, rp.bgDepthTexRt.tex)

    r.RenderFsQuad()

    rp.combShader.Unbind()
    gl.BindTexture(gl.TEXTURE_2D, 0)

    // end
  }
}
`
