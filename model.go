package rx

//"github.com/go-gl/glh"
//"rx/glh"

import (
	_path "path"
)

//  A multi-mesh entity.
type Model struct {
	name, path, dir string
	nodes           map[string]*Node
}

func NewModel() *Model {
	return &Model{
		nodes: make(map[string]*Node),
	}
}

func (m *Model) Nodes() []*Node {
	ret := make([]*Node, 0)
	for _, n := range m.nodes {
		ret = append(ret, n)
	}
	return ret
}

func (m *Model) Load(path string) {
	m.dir = _path.Dir(path)
	m.path = path

	sl := NewSceneLoader()
	nodes := sl.Load(path)
	/*
	  for i, n := range nodes {
	    p(i, n.Name(), n.MeshName())
	  }
	*/
	for _, n := range nodes {
		m.AddNode(n)
	}
}

func (m *Model) Spawn() []*Node {
	for _, n := range m.nodes {
		rxi.scene.Add(n)
	}
	return m.Nodes()
}

func (m *Model) Clone() *Model {
	dbg("Model.Clone")
	z := *m
	// clean nodes
	z.nodes = make(map[string]*Node)
	// clone nodes
	for s, n := range m.nodes {
		z.nodes[s] = n.Clone()
	}
	return &z
}

func (m *Model) GetNode(name string) *Node {
	if !m.HasNode(name) {
		m.PrintNodes()
		panic("no such node; name: " + name)
	}
	return m.nodes[name]
}

func (m *Model) AddNode(n *Node) {
	if m.HasNode(n.Mesh.MeshName()) {
		panic("already have node; name: " + n.Mesh.MeshName())
	}
	if n.Mesh.MeshName() == "" {
		panic("empty mesh name")
	}
	m.nodes[n.Mesh.MeshName()] = n
}

func (m *Model) HasNode(name string) bool {
	return m.nodes[name] != nil
}

func (m *Model) PrintNodes() {
	println("Model.PrintNodes begin")
	for k := range m.nodes {
		println("  " + k)
	}
	println("Model.PrintNodes end")
	println("path: " + m.path)
}

func (m *Model) Free() {
	dbg("Model.Free")
	//for _, n := range m.nodes {
	//	n.Free()
	//}
	println("Model.Free not implemented")
}
