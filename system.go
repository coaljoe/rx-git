package rx

import (
	"fmt"
	"reflect"
	//"gopkg.in/fatih/set.v0"
	//"github.com/emirpasic/gods/sets/hashset"
	"github.com/emirpasic/gods/lists/arraylist"
	//"github.com/emirpasic/gods/sets/treeset"
	//"github.com/emirpasic/gods/maps/treebidimap"
	//"container/list"
)

type hasName interface {
	Name() string
}

type System struct {
	name  string
	desc  string
	elems *arraylist.List
}

func NewSystem(name, desc string) *System {
	/*
		idComparator := func(a, b interface{}) {
			a.(hasId).Id() - b.(hasId).Id()
		}
	*/
	s := &System{
		name:  name,
		desc:  desc,
		elems: arraylist.New(),
	}
	return s
}

func (s *System) HasElem(el interface{}) bool {
	return s.elems.Contains(el)
}

func (s *System) idxElem(el interface{}) int {
	foundIndex, _ := s.elems.Find(
		func(index int, value interface{}) bool {
			return el == value
		})
	return foundIndex // -1 if not found
}

func (s *System) AddElem(el interface{}) {
	if s.HasElem(el) {
		Log.Err("already have element", el)
		panic("AddElem failed")
	}
	s.elems.Add(el)
	Log.Inf("added element '" + reflect.TypeOf(el).String() + "' to system '" + s.name + "'")
}

func (s *System) RemoveElem(el interface{}) {
	idx := s.idxElem(el)
	if idx == -1 {
		Log.Err("element not found", el)
		panic("RemoveElem failed")
	}
	s.elems.Remove(idx)
	Log.Inf("removed element '" + reflect.TypeOf(el).String() + "' from system '" + s.name + "'")
}

func (s *System) Elems() []interface{} {
	return s.elems.Values()
}

func (s *System) ListElems() {
	fmt.Println("list elements:")
	for n, el := range s.Elems() {
		name := "unknown"
		if xel, ok := el.(hasName); ok {
			name = xel.Name()
		}
		fmt.Printf(" -> %d: %s\n", n, name)
	}
	fmt.Println("end")
}
