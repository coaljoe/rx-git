package rx

import (
	. "bitbucket.org/coaljoe/rx/math"
	"math"
)

// Intersect info
type RayHitInfo struct {
	Hit      bool
	Distance float64
	Normal   Vec3
	Position Vec3
}

type Raycaster struct {
}

func NewRaycaster() *Raycaster {
	return &Raycaster{}
}

// Test if ray intersects scene nodes.
func (r *Raycaster) TestRayScene(ray Ray) []*Node {
	ret := make([]*Node, 0)
	_ = `
	sce := rxi.scene

	for _, node := range sce.GetVisibleNodes() {
		p(node, node.Type())
		switch node.Type() {
		case NodeType_Mesh:
			{
				mn := node.(*MeshNode)
				b := mn.GetAABB()
				if TestRayAABB(ray, b) {
					ret = append(ret, node)
				}
			}
		}
	}
	`
	sce := rxi.scene

	for _, node := range sce.GetVisibleNodes() {
		p(node, node.Type())
		switch node.Type() {
		case NodeType_Mesh:
			{
				b := node.Mesh.GetAABB()
				if TestRayAABB(ray, b) {
					ret = append(ret, node)
					p("hit", node.Name())
				} else {
					p("not_hit", node.Name())
				}
			}
		}
	}
	p("ray:", ray)
	p("ret:", ret)
	return ret
}

func (r *Raycaster) TestRayGround(ray Ray) RayHitInfo {
	hitInfo := TestRayGround(ray, 0)
	if hitInfo.Hit {
		p("hit ground at:", hitInfo.Position)
	} else {
		p("no hit ground")
		//pp(2)
	}
	return hitInfo
}

// Test if ray intersects AABB.
func TestRayAABB(r Ray, b_ AABB) bool {
	//return false

	_ = `
	double tmin = -INFINITY, tmax = INFINITY;
 
    if (ray.n.x != 0.0) {
        double tx1 = (b.min.x - r.x0.x)/r.n.x;
        double tx2 = (b.max.x - r.x0.x)/r.n.x;
 
        tmin = max(tmin, min(tx1, tx2));
        tmax = min(tmax, max(tx1, tx2));
    }
 
    if (ray.n.y != 0.0) {
        double ty1 = (b.min.y - r.x0.y)/r.n.y;
        double ty2 = (b.max.y - r.x0.y)/r.n.y;
 
        tmin = max(tmin, min(ty1, ty2));
        tmax = min(tmax, max(ty1, ty2));
    }
 
    return tmax >= tmin;
	`

	_ = `
	tmin := math.Inf(-1)
	tmax := math.Inf(+1)

	ro := r.Origin
	rd := r.Direction

	if rd.X() != 0.0 {
		tx1 := (b.min.X() - ro.X()) / rd.X()
		tx2 := (b.max.X() - ro.X()) / rd.X()

		tmin = math.Max(tmin, math.Min(tx1, tx2))
		tmax = math.Min(tmax, math.Max(tx1, tx2))
	}

	if rd.Y() != 0.0 {
		tx1 := (b.min.Y() - ro.Y()) / rd.Y()
		tx2 := (b.max.Y() - ro.Y()) / rd.Y()

		tmin = math.Max(tmin, math.Min(tx1, tx2))
		tmax = math.Min(tmax, math.Max(tx1, tx2))
	}

	return tmax >= tmin
	`

	_ = `
	float tmin = (min.x - r.orig.x) / r.dir.x;
	float tmax = (max.x - r.orig.x) / r.dir.x;

	if (tmin > tmax) swap(tmin, tmax);

	float tymin = (min.y - r.orig.y) / r.dir.y;
	float tymax = (max.y - r.orig.y) / r.dir.y;

	if (tymin > tymax) swap(tymin, tymax);

	if ((tmin > tymax) || (tymin > tmax))
	return false;

	if (tymin > tmin)
	tmin = tymin;

	if (tymax < tmax)
	tmax = tymax;

	float tzmin = (min.z - r.orig.z) / r.dir.z;
	float tzmax = (max.z - r.orig.z) / r.dir.z;

	if (tzmin > tzmax) swap(tzmin, tzmax);

	if ((tmin > tzmax) || (tzmin > tmax))
	return false;

	if (tzmin > tmin)
	tmin = tzmin;

	if (tzmax < tmax)
	tmax = tzmax;
	`
	_ = `
	// From:
	// https://www.scratchapixel.com/lessons/3d-basic-rendering/
	// minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
	ro := r.Origin
	rd := r.Direction

	min := b.min
	max := b.max

	tmin := (min.X() - ro.X()) / rd.X()
	tmax := (max.X() - ro.X()) / rd.X()

	if tmin > tmax {
		// swap
		tmin, tmax = tmax, tmin
	}

	tymin := (min.Y() - ro.Y()) / rd.Y()
	tymax := (max.Y() - ro.Y()) / rd.Y()

	if tymin > tymax {
		// swap
		tymin, tymax = tymax, tymin
	}

	if (tmin > tymax) || (tymin > tmax) {
		return false
	}

	if tymin > tmin {
		tmin = tymin
	}

	if tymax < tmax {
		tmax = tymax
	}

	tzmin := (min.Z() - ro.Z()) / rd.Z()
	tzmax := (max.Z() - ro.Z()) / rd.Z()

	if tzmin > tzmax {
		// swap
		tzmin, tzmax = tzmax, tzmin
	}

	if (tmin > tzmax) || (tzmin > tmax) {
		return false
	}

	if tzmin > tmin {
		tmin = tzmin
	}

	if tzmax < tmax {
		tmax = tzmax
	}

	return true
	`
	// From:
	// http://www.java-gaming.org/index.php?topic=37053.0

	origin := r.Origin
	dir := r.Direction

	a := b_.min
	b := b_.max

	tMinX := (a.X() - origin.X()) / dir.X()
	tMinY := (a.Y() - origin.Y()) / dir.Y()
	tMinZ := (a.Z() - origin.Z()) / dir.Z()
	tMaxX := (b.X() - origin.X()) / dir.X()
	tMaxY := (b.Y() - origin.Y()) / dir.Y()
	tMaxZ := (b.Z() - origin.Z()) / dir.Z()
	t1X := math.Min(tMinX, tMaxX)
	t1Y := math.Min(tMinY, tMaxY)
	t1Z := math.Min(tMinZ, tMaxZ)
	t2X := math.Max(tMinX, tMaxX)
	t2Y := math.Max(tMinY, tMaxY)
	t2Z := math.Max(tMinZ, tMaxZ)
	tNear := math.Max(math.Max(t1X, t1Y), t1Z)
	tFar := math.Min(math.Min(t2X, t2Y), t2Z)
	return tNear < tFar && tFar >= 0.0
}

func TestRayGround(ray Ray, groundHeight float64) RayHitInfo {
	// Based on raylib
	const EPSILON = 0.000001 // A small number

	var result RayHitInfo

	p("ray direction ->", ray.Direction)

	if math.Abs(ray.Direction.Z()) > EPSILON {
		distance := (ray.Origin.Z() - groundHeight) / -ray.Direction.Z()

		if distance >= 0.0 {
			result.Hit = true
			result.Distance = distance
			result.Normal = Vec3{0.0, 0.0, 1.0}
			result.Position = ray.Origin.Add(ray.Direction.Scale(distance))
		}
	}

	return result
}
