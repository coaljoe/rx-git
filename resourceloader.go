package rx

type ResourceLoader struct {
}

func NewResourceLoader() *ResourceLoader {
	return &ResourceLoader{}
}

/*
func (rl *ResourceLoader) LoadResource(path string) interface{} {
	return interface{}
}
*/
func (rl *ResourceLoader) LoadScene(path string) []*Node {
	sl := NewSceneLoader()
	nodes := sl.Load(path)
	return nodes
}
