package rx

import (
	. "bitbucket.org/coaljoe/rx/math"
	//js "github.com/bitly/go-simplejson"
)

type Material struct {
	Name     string
	Diffuse  Vec3
	Specular Vec3
	Ambient  Vec3
	Emission Vec3
	Hardness float64
	// full blending?
	Alpha bool
	// 1-bit alpha XXX fixme?
	AlphaTest bool
	texture   *Texture
	shader    *Shader
}

func (m *Material) Texture() *Texture { return m.texture }

func NewMaterial(path string) *Material {
	m := &Material{
		// Default material from Blender exported as Collada
		Diffuse:  Vec3{0.64, 0.64, 0.64},
		Specular: Vec3{0.5, 0.5, 0.5},
		Ambient:  Vec3Zero,
		Emission: Vec3Zero,
		Hardness: 50,
	}
	m.Load(path)
	return m
}

func (m *Material) Load(path string) {

	/*
		r, err := os.Open(path)
		if err != nil {
			println("cant read file: " + path)
			log.Fatal(err)
		}
		defer r.Close()
		d, err := js.NewFromReader(r)
		if err != nil {
			println("cant read json from file: " + path)
			log.Fatal(err)
		}
	*/
	d := getJsonFromFile(path)

	m.Name = d.Get("name").MustString()
	vs_path := d.Get("shaders").GetIndex(0).MustString()
	fs_path := d.Get("shaders").GetIndex(1).MustString()

	m.shader = NewShader(vs_path, fs_path)
	m.shader.compile()
}

func (m *Material) SetTexture(path string) {
	tex := NewTexture(path)
	m.texture = tex
	m.shader.vs_macro += "#define _TEXTURE_MAP 1\n"
	m.shader.fs_macro += "#define _TEXTURE_MAP 1\n"
	m.texture.Bind()
	m.shader.compile()
	m.texture.Unbind()
}

func (m *Material) SetTexture2(tex *Texture) {
	m.texture = tex
	m.shader.vs_macro += "#define _TEXTURE_MAP 1\n" // fixme
	m.shader.fs_macro += "#define _TEXTURE_MAP 1\n"
	m.texture.Bind()
	m.shader.compile()
	m.texture.Unbind()
}
