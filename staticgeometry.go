package rx

import (
	. "bitbucket.org/coaljoe/rx/math"
)

// StaticGeometry
type StaticGeometry struct {
	// Parent mesh
	mesh *Mesh
	//node     *Node
	//material *Material
	mb      *MeshBuffer
	created bool
	dirty   bool
}

func NewStaticGeometry(mesh *Mesh) *StaticGeometry {
	sg := &StaticGeometry{
		mesh: mesh,
		//node: node,
		/*
			aabb:    NewAABB(),
			hasUV:   false,
			isEmpty: false,
		*/
		mb:      NewMeshBuffer(),
		created: false,
	}
	return sg
}

func (sg *StaticGeometry) AddMesh(m *Mesh) {
	worldMat := m.node.WorldMat()
	sg.AddMeshAt(m, worldMat)
}

func (sg *StaticGeometry) AddMeshAt(m *Mesh, worldMat Mat4) {

	positionsW := make([]float32, 0)
	//normalsW := make([]float32, 0)
	normalsW := m.mb.normals // XXX in object/model space?
	texcoords := m.mb.texcoords
	indices := m.mb.indices

	//pp(len(m.mb.positions), len(m.mb.normals), len(m.mb.texcoords), len(m.mb.indices))

	//mat := m.node.WorldMat()
	mat := worldMat

	for _, p := range m.mb.GetVertices() {
		pWorld := mat.MulVec3(p)
		positionsW = append(positionsW, float32(pWorld.X()), float32(pWorld.Y()), float32(pWorld.Z()))
	}

	/*
		for _, n := range m.mb.GetNormals() {
			nWorld := mat.MulVec3(n)
			normalsW = append(normalsW, float32(nWorld.X()), float32(nWorld.Y()), float32(nWorld.Z()))
		}
	*/

	//pp(sg.mb)
	mbIdxOffset := len(sg.mb.positions)
	for i, idx := range indices {
		newIdx := uint32(mbIdxOffset) + idx
		indices[i] = newIdx
	}

	sg.mb.Append(positionsW, normalsW, texcoords, indices)

	sg.created = true
	sg.dirty = true
}

func (sg *StaticGeometry) Build() {
	sg.mb.ForceBuild()
	// Update AABB
	sg.mesh.aabb.calculateFromMeshBuffer(sg.mb)
	sg.dirty = false
}

func (sg *StaticGeometry) Render(r *Renderer) {
	if !sg.created {
		return
	}
	sg.mb.Render(r)
}
