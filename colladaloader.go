package rx

// Notes:
// 1. collada matrices are column major matrices
//    written in row major order (for better read by people)
//    https://www.khronos.org/bugzilla/show_bug.cgi?id=49#c1
//

import (
	"encoding/xml"
	"fmt"
	"os"
	"path"
	"strconv"
	"strings"
	//"github.com/go-gl/gl/v2.1/gl"
	//"github.com/go-gl/glh"
	//"rx/glh"
	cda "bitbucket.org/coaljoe/rx/collada"

	. "bitbucket.org/coaljoe/rx/math"
)

type ColladaLoader struct {
	c    *cda.Collada
	path string
}

func NewColladaLoader() *ColladaLoader {
	return &ColladaLoader{
		c: &cda.Collada{},
	}
}

func (cl *ColladaLoader) Reset() {
	Log.Inf("ColladaLoader.Reset")
	cl.c = &cda.Collada{}
}

func (cl *ColladaLoader) Load(path string) bool {
	Log.Inf("ColladaLoader.Load", path)
	cl.Reset()

	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	err = xml.NewDecoder(file).Decode(cl.c)
	if err != nil {
		panic(err)
	}

	Log.Dbg("Version:", cl.c.Version)
	cl.path = path
	return true
}

func (cl *ColladaLoader) MakeMeshNodes(nodeNameFilter string, noCache bool) []*Node {
	Log.Dbg("num nodes:", len(cl.GetNodeList()))
	var ret []*Node
	for i, n := range cl.GetNodeListSortedByParent() {
		Log.Dbg("node n:", i)
		Log.Dbg("-id", n.Id)
		Log.Dbg("-matrix", n.Matrix.CDATA)
		Log.Dbg("-instance_geometry", n.Instance_geometry.Url)

		// skip non-geometry nodes
		/*
		   if len(n.Instance_geometry.Url) < 1 {
		     Log.Dbg("skip non geometry node", n.Id)
		     continue
		   }
		*/
		// filter nodes by name
		if nodeNameFilter != "" {
			if n.Name != nodeNameFilter {
				Log.Dbg("skip ", n.Name)
				continue
			}
		}

		isEmpty := false
		if len(n.Instance_geometry.Url) < 1 {
			isEmpty = true
		}

		// transform
		matrix := parseMatrix(n.Matrix.CDATA)
		//matrix = matrix.Transpose()

		// convert xyz -> xzy
		// swap yz axes
		/*
		   //root_matrix := Mat4Identity
		   root_matrix := Matrix4(
		     1, 0, 0, 0,
		     0, 0, -1, 0,
		     0, 1, 0, 0,
		     0, 0, 0, 1) // Mat4ZToYUp
		   mat1 := matrix.Mul(root_matrix)
		   //_ = root_matrix
		*/
		//mat1 := matrix

		// decompose matrix
		//scale, shear, hpr := mat1.UpperMat3().Decompose(CoordSysYUpLeft)
		//scale, shear, hpr := matrix.Mat3().Decompose(CoordSysZUpRight)
		//scale, hpr := Extract3DScale(matrix)
		scale := matrix.ExtractScale()
		//rot = matrix.Rotation()
		//rot_deg := matrix.ExtractRotation().Degrees()
		rot_deg := matrix.ExtractRotation()
		//rot = Extract3DRotation(matrix)
		//_ = shear
		pos := matrix.ExtractTranslation()
		/*
		   // extract quat from mat1
		   q := QuatFromMat3(mat1.UpperMat3())
		   Log.Dbg(q.String())
		   Log.Dbg(q.Hpr(CoordSysYUpLeft).String())
		*/

		// create and setup new mesh node
		mn := NewMeshNode()
		mn.name = n.Name
		//mn.SetName(n.Name)
		//mn.SetMatrix(matrix)
		//mn.SetPos(matrix.Translation())
		//mn.SetPos(mat1.Translation())
		mn.SetPos(pos)
		if isEmpty {
			mn.Mesh.isEmpty = true
		}

		/*
		   //q1 := QuatIdentity
		   //mn.SetQuat(q1)
		   mn.SetRot(hpr.HprToXyz())
		   Log.Dbg("hpr: ", hpr.String())
		   Log.Dbg("hpr2xyz: ", hpr.HprToXyz().String())
		   Log.Dbg("rot: " + mn.Rot().String())
		*/

		//rot_deg := hpr.HprToXyz().Degrees()
		//rot_deg = Vec3{rot_deg.X, rot_deg.Z, rot_deg.Y}
		//tmp := rot_deg.MulScalar(1)
		//rot_deg = Vec3{tmp.X, tmp.Z, tmp.Y}
		//_ = rot_deg

		//mn.SetRot(Vec3{90, 0, 0})
		//mn.SetRot(rot_deg.Add(Vec3{90, 0, 0}))
		mn.SetRot(rot_deg)
		//mn.SetScale(Vec3{1, 1, -1})
		Log.Dbg("rot: " + mn.Rot().String())

		mn.SetScale(scale)

		// cache ids
		var cacheId, cachePath string
		meshName := strings.Replace(n.Instance_geometry.Url, "#", "", 1)
		if !isEmpty {
			cacheId = cl.path + "#" + meshName
			cachePath = cl.path
		} else {
			cacheId = "_empty_" + n.Name // warning: must be unique
			cachePath = ""
		}

		if m, ok := mn.Mesh.TryLoadFromCache(cacheId); ok && !noCache {
			mn.Mesh = m
		} else {
			// set mesh data
			var mb *MeshBuffer
			if !isEmpty {
				mb = cl.MakeMeshBufferForMesh(meshName)
			} else {
				mb = cl.MakeBoxMeshBuffer()
			}
			mn.Mesh.LoadFromMeshBuffer(mb, cacheId, cachePath)
		}

		// set material
		//mat := cl.MakeMaterialForMesh(strings.Replace(meshName, "-mesh", "", 1))
		var mat *Material
		if !isEmpty {
			mat = cl.MakeMaterialForNode(n.Name)
		} else {
			mat = cl.MakeEmptyMaterial()
		}
		mn.Mesh.SetMaterial(mat)

		// set mesh name
		mn.Mesh.SetMeshName(n.Name)

		// set parent
		if p, ok := cl.GetNodeParent(n); ok {
			Log.Dbg("set parent...")
			// find parent meshnode
			for _, x := range ret {
				if x.name == p.Name {
					Log.Dbg("p.Name", p.Name)
					Log.Dbg("n.Name", n.Name)
					//mn.SetParent(x)
					// fixme?
					//mn.Transform.Transform.SetParent(x.Transform.Transform)
					mn.Transform.SetParent(x.Transform)
					//panic(2)
				}
			}
		}

		ret = append(ret, mn)
	}
	/*
	   // set parents
	   for _, mn := range ret {
	     // set parent
	     if p := cl.GetNodeParent(n); p != nil {
	       mn.SetParent(p.Trans)
	     }
	   }
	*/
	return ret
}

func walk_node(n cda.Node, last bool, ret *[]cda.Node) {
	Log.Dbg("walk node")
	Log.Dbg("NAME", n.Name)
	*ret = append(*ret, n)
	if last {
		//*ret = append(*ret, n)
		Log.Dbg("LAST")
		return
	} else {
		Log.Dbg("hurr")
		//walk_node(n.Children[0], len(n.Children) > 0)
		for i := range n.Children {
			Log.Dbg("durr")
			walk_node(n.Children[i], len(n.Children) < 1, ret)
			//Log.Dbg("ret", ret)
		}
	}
}

func (cl *ColladaLoader) GetNodeList() []cda.Node {
	//Log.Dbg(cl.c.Library_Visual_Scenes)
	//Log.Dbg(cl.c.Library_Visual_Scenes.VisualScene)
	//Log.Dbg(cl.c.Library_Visual_Scenes.VisualScene.NodeList)
	//Log.Dbg(len(cl.c.Library_Visual_Scenes.VisualScene.NodeList))
	ret := make([]cda.Node, 0)
	for _, n := range cl.c.Library_Visual_Scenes.VisualScene.NodeList {
		//println("NAME", n.Name)
		//ret = append(ret, n)
		walk_node(n, len(n.Children) < 1, &ret)
	}
	//os.Exit(0)
	//return cl.c.Library_Visual_Scenes.VisualScene.NodeList
	//Log.Dbg(ret)
	Log.Dbg(len(ret))
	return ret
}

func (cl *ColladaLoader) GetNodeListSortedByParent() []cda.Node {
	ret := make([]cda.Node, 0)
	nodes := cl.GetNodeList()

	for _, n := range nodes {
		// if n is alread in ret, skip
		have := false
		for _, x := range ret {
			if x.Name == n.Name {
				have = true
			}
		}
		if have {
			continue
		}

		// make new extend slice
		sl := make([]cda.Node, 0)
		// lookup top parent
		var p0 cda.Node
		p0 = n
		for {
			if p, ok := cl.GetNodeParent(p0); ok {
				p0 = p
				// push-front parent
				//sl = append([]cda.Node{p}, sl...)

				have := false
				for _, x := range ret {
					if x.Name == p.Name {
						have = true
					}
				}
				if !have {
					ret = append([]cda.Node{p}, ret...)
				}
			} else {
				break
			}
		}
		// add the node itself
		sl = append(sl, n)
		for _, n := range sl {
			Log.Dbg(".", n.Name)
		}

		// concat slices
		ret = append(ret, sl...)
	}

	if len(ret) != len(nodes) {
		println("bad sort ", len(ret), len(nodes))
		for _, n := range ret {
			println(" -> " + n.Name)
		}
		panic(1)
	} else {
		Log.Dbg("good sort")
		for _, n := range ret {
			Log.Dbg(" -> " + n.Name)
		}
	}

	return ret
}

func (cl *ColladaLoader) GetNodeParent(n cda.Node) (cda.Node, bool) {
	for _, p := range cl.GetNodeList() {
		for _, ch := range p.Children {
			if ch.Name == n.Name {
				// if its a children, return parent node
				Log.Dbg("p -> c", p.Name, n.Name)
				//panic(2)
				return p, true
			}
		}
	}
	var ret cda.Node
	return ret, false
}

func (cl *ColladaLoader) MakeMaterialForNode(id string) *Material {
	// get binded material name
	var found bool
	var mat_name string
	found = false
	for _, n := range cl.GetNodeList() {
		Log.Dbg("->", n.Name)
		if n.Id == id {
			found = true
			mat_name = n.Instance_geometry.Bind_Material.Technique_Common.Instance_Material.Symbol
			//target := n.Instance_geometry.Bind_Material.Technique_Common.Instance_Material.Target

			Log.Dbg("->", mat_name)
			//Log.Dbg("->", target)
			break
		}
	}

	// fixme
	if !found {
		panic(fmt.Sprintf("Can't find binded material for mesh. id=%s", id))
	}

	//var material cda.Material
	found = false
	for _, m := range cl.c.Library_Materials.MaterialList {
		Log.Dbg("..", m.Id)
		if m.Id == mat_name {
			//material = m
			found = true
			break
		}
	}
	// fixme
	if !found {
		panic(fmt.Sprintf("Can't find material. name=%s", mat_name))
	}

	// get materials data
	var emission, ambient, diffuse, specular Vec3
	var shininess float64
	var diffuse_tex_path string

	found = false
	eff_name := strings.Replace(mat_name, "-material", "-effect", 1)
	Log.Dbg("eff_name:", eff_name)
	for _, e := range cl.c.Library_Effects.EffectList {
		if e.Id == eff_name {
			phong := e.Profile_COMMON.Technique.Phong
			//fmt.Printf("%#v\n", e)
			//dump(phong.Ambient)
			//p(phong.Ambient.Color.CDATA)
			//pdump(e.Profile_COMMON.Technique.Phong)
			//pdump(e)
			if phong.Ambient.Color.CDATA == "" {
				// not a phong material
				Log.Err("no material for: ", mat_name)
				panic("non phong materials are not supported")
			}
			//Log.Dbg("-->", phong.Shininess.Float.Sid)
			//Log.Dbg("-->", phong.Shininess.Float.CDATA)
			//Log.Dbg("-->", e.Profile_COMMON.Technique.Phong.Diffuse.Color.CDATA)
			emission = parseVec4(phong.Emission.Color.CDATA).ToVec3()
			ambient = parseVec4(phong.Ambient.Color.CDATA).ToVec3()

			if len(phong.Diffuse.Color.Sid) > 0 {
				diffuse = parseVec4(phong.Diffuse.Color.CDATA).ToVec3()
			} else {
				diffuse = Vec3One
				texName := strings.Replace(phong.Diffuse.Texture.Texture, "-sampler", "", 1)
				found1 := false
				for _, im := range cl.c.Library_Images.ImageList {
					if im.Id == texName {
						found1 = true
						diffuse_tex_path = path.Dir(cl.path) + "/" + im.Init_From.CDATA
						diffuse_tex_path = strings.Replace(diffuse_tex_path, "%20", " ", -1)
					}
				}
				if !found1 {
					panic(fmt.Sprintf("Can't find texture. name=%s", texName))
				}
			}

			specular = parseVec4(phong.Specular.Color.CDATA).ToVec3()
			var err error
			shininess, err = strconv.ParseFloat(strings.TrimSpace(phong.Shininess.Float.CDATA), 64)
			check(err)
			found = true
		}
	}
	// fixme
	if !found {
		panic(fmt.Sprintf("Can't find material's effect. name=%s", eff_name))
	}

	// create material
	var mat *Material
	//mat = NewMaterial(Conf.res_path + "/materials/static.json")
	mat = NewMaterial(Conf.res_path + Conf.DefaultMaterialPath)
	mat.Name = strings.Replace(mat_name, "-material", "", 1)
	mat.Emission = emission
	mat.Ambient = ambient
	mat.Diffuse = diffuse
	mat.Specular = specular
	mat.Hardness = shininess

	//Log.Dbg("HARDNESS", float32(shininess))

	if len(diffuse_tex_path) > 0 {
		mat.SetTexture(diffuse_tex_path)
	}

	return mat
}

func (cl *ColladaLoader) MakeEmptyMaterial() *Material {
	// create material
	var mat *Material
	//mat = NewMaterial(Conf.res_path + "/materials/static.json")
	mat = NewMaterial(Conf.res_path + Conf.DefaultMaterialPath)
	mat.Name = "_empty_material"
	mat.Emission = Vec3Zero
	mat.Ambient = Vec3ZUnit
	mat.Diffuse = Vec3{1.0, 0, 1.0}
	mat.Specular = Vec3{1.0, 0, 1.0}
	mat.Hardness = 50

	return mat
}

func (cl *ColladaLoader) MakeMeshBufferForMesh(id string) *MeshBuffer {
	var mesh cda.Mesh
	var pl cda.Polylist
	var tris cda.Triangles
	var g_id string
	found := false
	for _, g := range cl.c.Library_Geometries.GeometryList {
		Log.Dbg(g.Id)
		if g.Id == id {
			g_id = g.Id
			mesh = g.Mesh
			pl = g.Mesh.Polylist
			tris = g.Mesh.Triangles
			found = true
			break
		}
	}

	// fixme
	if !found {
		panic(fmt.Sprintf("Can't find mesh. id=%s", id))
	}

	// get mesh positions and normals
	var positions, in_positions []float32
	var normals, in_normals []float32
	var texcoords, in_texcoords []float32
	for _, s := range mesh.Source {

		if s.Id == g_id+"-positions" {
			in_positions = parseFloatArray(s.Float_array.CDATA)
		} else if s.Id == g_id+"-normals" {
			in_normals = parseFloatArray(s.Float_array.CDATA)
		} else if s.Id == g_id+"-map" || s.Id == g_id+"-map-0" {
			in_texcoords = parseFloatArray(s.Float_array.CDATA)
		} else {
			println("unknown mesh source: Id=", s.Id)
		}
	}

	if len(in_positions) < 1 {
		panic(fmt.Sprintf("Can't read mesh positions. id=%s", id))
	}
	if len(in_normals) < 1 {
		panic(fmt.Sprintf("Can't read mesh normals. id=%s", id))
	}

	/*
	   // read polylist
	   //var ret []float64
	   //stride := 3
	   //v_offset, n_offset := 0, 1
	   inVCount := strings.NewReader(pl.VCount)
	   inP := strings.NewReader(pl.P)
	   for i := 0; i < pl.Count; i++ {
	     var v_count int
	     fmt.Fscan(inVCount, "%d", &v_count)
	     for j := 0; j < v_count; j++ {
	       var p_v int
	       var p_n int
	       fmt.Fscan(inP, "%d", &p_v)
	       fmt.Fscan(inP, "%d", &p_n)
	       Log.Dbg("p_v p_n", p_v, p_n)
	     }
	   }
	*/

	var indices []uint32
	//p("-> indices:", pl.P)
	if pl.P != "" {
		indices = parseUIntArray(pl.P)
	} else {
		//pp("-> tris indices:", tris.P)
		indices = parseUIntArray(tris.P)
	}

	//indices = []uint32{0, 1, 3, 2, 0, 3}
	//Log.Dbg("num indices: ", len(indices))

	/*
	   unroll vertex data from indices
	*/
	nth := 2
	if len(in_texcoords) > 0 { // has_texcoords
		nth += 1
	}
	for i := range indices {
		idx := indices[i]
		//Log.Dbg(i, idx)
		if i%nth == 0 {
			//Log.Dbg("pos")
			positions = append(positions, in_positions[idx*3],
				in_positions[idx*3+1],
				in_positions[idx*3+2])
		}
		if i%nth == 1 {
			//Log.Dbg("nor")
			normals = append(normals, in_normals[idx*3],
				in_normals[idx*3+1],
				in_normals[idx*3+2])
		}
		if i%nth == 2 {
			//Log.Dbg("tc")
			/*
			   texcoords = append(texcoords, in_texcoords[idx*2],
			                                 1.0 - in_texcoords[idx*2+1])
			   // flip second coordinate: direct x -> opengl UV space
			*/
			texcoords = append(texcoords, in_texcoords[idx*2],
				1.0-in_texcoords[idx*2+1])
		}
	}

	// drop every nth value from indicies (remove normal/texcoord indicies)
	nth = 2
	if len(texcoords) > 0 { // has_texcoords
		nth += 1
	}
	var newIndices []uint32
	for i := range indices {
		if i%nth == 0 {
			a := indices
			newIndices = append(newIndices, a[i])
		}
	}
	indices = newIndices

	/*
	  Log.Dbg("indices", indices, len(indices))
	  Log.Dbg("positions", positions, len(positions))
	  Log.Dbg("normals", normals, len(normals))
	  Log.Dbg("texcoords", texcoords, len(texcoords))
	*/

	Log.Dbg("meshbuf building begin...")
	mb := NewMeshBuffer()
	Log.Dbg("end.")

	// Add the mesh to the buffer.
	//mb.Add(positions, normals, texcoords, indices)
	mb.Add(positions, normals, texcoords, []uint32{})

	/*
	  Log.Dbg("meshbuf building begin...")
	  mb := glh.NewMeshBuffer(
	    glh.RenderBuffered, // vbo
	    //glh.RenderClassic,
	    //glh.RenderArrays,
	    //glh.NewIndexAttr(1, gl.UNSIGNED_INT, gl.STATIC_DRAW),
	    glh.NewPositionAttr(3, gl.FLOAT, gl.STATIC_DRAW),
	    glh.NewNormalAttr(3, gl.FLOAT, gl.STATIC_DRAW),
	    glh.NewTexCoordAttr(2, gl.FLOAT, gl.STATIC_DRAW),
	  )
	  Log.Dbg("end.")

	  // Add the mesh to the buffer.
	  //mb.Add(indices, positions, normals)
	  //mb.Add(indices, positions)
	  //mb.Add(indices, positions, normals, texcoords)
	  mb.Add(positions, normals, texcoords)
	  //mb.Add(indices, positions, texcoords)
	  //mb.Add(positions, normals)
	  //_ = indices
	*/

	return mb
}

func (cl *ColladaLoader) MakeBoxMeshBuffer() *MeshBuffer {
	/*
	  cube := glh.NewMeshBuffer(
	    glh.RenderBuffered,
	    glh.NewPositionAttr(3, gl.FLOAT, gl.STATIC_DRAW),
	  )
	  cube.Add([]float32{
	    -1, -1, -1, -1, -1, 1,
	    -1, -1, -1, -1, 1, -1,
	    -1, -1, -1, 1, -1, -1,
	    -1, 1, 1, -1, -1, 1,
	    -1, 1, 1, -1, 1, -1,
	    -1, 1, 1, 1, 1, 1,
	    1, -1, 1, -1, -1, 1,
	    1, -1, 1, 1, -1, -1,
	    1, -1, 1, 1, 1, 1,
	    1, 1, -1, -1, 1, -1,
	    1, 1, -1, 1, -1, -1,
	    1, 1, -1, 1, 1, 1,
	  })
	  return cube
	*/
	positions := []float32{
		-1, -1, -1, -1, -1, 1,
		-1, -1, -1, -1, 1, -1,
		-1, -1, -1, 1, -1, -1,
		-1, 1, 1, -1, -1, 1,
		-1, 1, 1, -1, 1, -1,
		-1, 1, 1, 1, 1, 1,
		1, -1, 1, -1, -1, 1,
		1, -1, 1, 1, -1, -1,
		1, -1, 1, 1, 1, 1,
		1, 1, -1, -1, 1, -1,
		1, 1, -1, 1, -1, -1,
		1, 1, -1, 1, 1, 1,
	}

	mb := NewMeshBuffer()
	mb.Add(positions, []float32{}, []float32{}, []uint32{})
	return mb
}

func parseFloatArray(s string) []float32 {
	var a []float32
	for _, chunk := range strings.Split(s, " ") {
		v, err := strconv.ParseFloat(chunk, 32)
		if err != nil {
			panic(err)
		}
		a = append(a, float32(v))
	}
	return a
}

func parseUIntArray(s string) []uint32 {
	var a []uint32
	for _, chunk := range strings.Split(s, " ") {
		v, err := strconv.ParseUint(chunk, 0, 32)
		if err != nil {
			panic(err)
		}
		a = append(a, uint32(v))
	}
	return a
}

func parseMatrix(s string) Mat4 {
	/*
	   x, y = 0.5, colunm major

	   1 0 0 0.5 -- stored as row-major?
	   0 1 0 0.5
	   0 0 1 0
	   0 0 0 1

	   flat:
	   1 0 0 0.5 0 1 0 0.5 0 0 1 0 0 0 0 1
	*/

	ms := parseFloatArray(s)
	m := Mat4Identity

	//Log.Dbg(m.String())

	// fill matrix
	for i := 0; i < 4; i++ {
		for j := 0; j < 4; j++ {
			m[i][j] = float64(ms[i+j*4]) // row- -> col- major trans. not checked
			//m[j][i] = float64(ms[i+j*4]) // row- -> col- major trans. not checked
			//m.Set(i, j, float64(ms[i+j*4]))
		}
	}
	Log.Dbg("s:\n" + s)
	Log.Dbg("m:\n" + m.String())
	Log.Dbg("data:\n%v\n", m.Data())
	//pp("derp")

	return m
}

func parseVec4(s string) Vec4 {
	r := Vec4Zero
	for i, chunk := range strings.Split(s, " ") {
		v, err := strconv.ParseFloat(chunk, 64)
		if err != nil {
			Log.Err("can't parse chunk to Vec4; chunk:", chunk)
			panic(err)
		}
		switch i {
		case 0:
			r.SetX(v)
		case 1:
			r.SetY(v)
		case 2:
			r.SetZ(v)
		case 3:
			r.SetW(v)
		default:
			panic("unknown idx")
		}
	}
	return r
}
