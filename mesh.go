package rx

type MeshType int

const (
	MeshType_Mesh MeshType = iota
	MeshType_StaticGeometry
)

// MeshNode
type Mesh struct {
	node               *Node
	material           *Material
	mb                 *MeshBuffer
	aabb               AABB
	hasUV              bool
	cacheId, cachePath string
	meshName           string
	isEmpty            bool
	meshType           MeshType
	StaticGeometry     *StaticGeometry
}

func (m *Mesh) MeshName() string           { return m.meshName }
func (m *Mesh) SetMeshName(s string)       { m.meshName = s }
func (m *Mesh) Material() *Material        { return m.material }
func (m *Mesh) SetMaterial(mat *Material)  { m.material = mat }
func (m *Mesh) GetMeshBuffer() *MeshBuffer { return m.mb }

func NewMesh(node *Node) *Mesh {
	return &Mesh{
		node:     node,
		aabb:     NewAABB(),
		hasUV:    false,
		isEmpty:  false,
		meshType: MeshType_Mesh,
	}
}

func (m *Mesh) clone() *Mesh {
	z := *m
	//z.mb = m.mb
	//z.material = m.material
	return &z
}

func (m *Mesh) TryLoadFromCache(cacheId string) (*Mesh, bool) {
	// return from cache
	rs := rxi.ResourceSys
	if r := rs.Get(cacheId, ResourceTypeMesh); r != nil {
		println("reusing mesh, cacheId: " + cacheId)
		cm := r.data.(*Mesh)
		//m = cm
		return cm, true
	}
	return nil, false
}

func (m *Mesh) LoadFromMeshBuffer(mb *MeshBuffer, cacheId, cachePath string) {
	m.mb = mb
	//if len(mb.TexCoords()) > 0 {
	if mb.hasTexCoords {
		m.hasUV = true
	}

	// Update AABB
	m.aabb.calculateFromMeshBuffer(mb)

	m.cacheId = cacheId
	m.cachePath = cachePath

	// resource reusing
	r := NewResource()
	r.rtype = ResourceTypeMesh
	r.id = m.cacheId
	r.path = m.cachePath
	r.data = m
	rxi.ResourceSys.AddResource(r)
}

func (m *Mesh) FreeUnused() {
	// fixme check for other mb cache users
	dbg("fixme: no working mesh.freeunused")
}

func (m *Mesh) Free() {
	//dbg("Mesh.Free")
	m.mb.Release()
}

// From MeshNode

// Get AABB transformed to world space.
func (m *Mesh) GetAABB() AABB {
	if m.aabb.Empty() {
		return m.aabb
	}

	AABBlocal := m.aabb

	// Calculate world AABB
	Wmin := m.node.WorldMat().MulVec3(AABBlocal.min)
	Wmax := m.node.WorldMat().MulVec3(AABBlocal.max)
	AABBworld := AABB{Wmin, Wmax}

	/*
		Wmin := AABBlocal.min.Add(m.node.Pos())
		Wmax := AABBlocal.max.Add(m.node.Pos())
		AABBworld := AABB{Wmin, Wmax}
	*/

	//pp(AABBlocal, AABBworld)

	_ = AABBworld
	return AABBworld
	//return AABBlocal
}

/*
func (m *Mesh) Spawn() {
	pp("never used")
	rxi.scene.AddMeshNode(m.node)
}
*/

func (m *Mesh) Render(r *Renderer) {
	//p("Z XXX render mesh, name:", m.node.name)
	if m.meshType == MeshType_Mesh {
		m.mb.Render(r)
	} else if m.meshType == MeshType_StaticGeometry {
		m.StaticGeometry.Render(r)
	} else {
		pp("unknown meshType:", m.meshType)
	}
}
