package rx

import (
	//"encoding/xml"
	. "fmt"
	"path/filepath"
	"strings"
	//cda "bitbucket.org/coaljoe/rx/collada"
)

type SceneLoader struct {
	cl    *ColladaLoader
	gl    *GltfLoader
	nodes []*Node
}

func NewSceneLoader() *SceneLoader {
	return &SceneLoader{
		cl:    NewColladaLoader(),
		gl:    NewGltfLoader(),
		nodes: nil,
	}
}

func (sl *SceneLoader) Nodes() []*Node {
	return sl.nodes
}

func (sl *SceneLoader) load(path string, nodeNameFilter string, noCache bool) []*Node {
	println("SceneLoader.Load", path)
	pathlc := strings.ToLower(path)
	isGltf := false
	if filepath.Ext(pathlc) == ".gltf" || filepath.Ext(pathlc) == ".glb" {
		isGltf = true
	}

	var ok bool
	if !isGltf {
		ok = sl.cl.Load(path)
	} else {
		ok = sl.gl.Load(path)
	}
	if !ok {
		panic(Sprintf("can't load scene; path=%s", path))
	}

	if !isGltf {
		if len(sl.cl.GetNodeList()) < 1 {
			panic("can't load scene, node list is empty")
		}
	}

	if !isGltf {
		sl.nodes = sl.cl.MakeMeshNodes(nodeNameFilter, noCache)
	} else {
		sl.nodes = sl.gl.MakeMeshNodes(nodeNameFilter, noCache)
	}

	// fix dublicated names
	for _, node := range sl.nodes {
		node.SetName(node.name) // fixme?
	}
	/*
		// fix dublicated names
		for _, node := range sl.nodes {
			found := rxi.scene.HasNode(node.Name())
			if found {
				newName := node.Name() + "#next"
				println("renaming " + node.Name() + " to " + newName)
				//node.SetName(newName) // fixme
				node.name = newName
			}
		}
	*/

	return sl.nodes
}

func (sl *SceneLoader) Load(path string) []*Node {
	return sl.LoadFilter(path, "") // no filter
}

func (sl *SceneLoader) LoadFilter(path string, nodeNameFilter string) []*Node {
	return sl.load(path, nodeNameFilter, false)
}

func (sl *SceneLoader) LoadNoCache(path string) []*Node {
	return sl.load(path, "", true)
}

func (sl *SceneLoader) Spawn() {
	for _, mn := range sl.nodes {
		rxi.scene.Add(mn)
	}
}

func (sl *SceneLoader) Free() {
}
