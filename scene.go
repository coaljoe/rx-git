package rx

import (
	"fmt"
	//"sort"
)

type Scene struct {
	//nodes   []*Node
	//cameras []*Node
	//meshes  []*Node
	lights []*Node
	//empties []*EmptyNode
	renderables map[RenderableNodeI]bool
	light0      *Node
	// fixme: rename/move? XXX: use rx.Camera
	//cam0        *CameraNode
	// XXX random order (is bad for iteration?)
	nodesMap map[int]*Node
	// nodesMap's sorted keys
	//sortedKeys []int
	// nodeMap's ordered keys
	orderedKeys []int
}

func (s *Scene) Lights() []*Node         { return s.lights }
func (s *Scene) NodesMap() map[int]*Node { return s.nodesMap }

func NewScene() *Scene {
	return &Scene{
		//cameras: make(*[]CameraNode, 0),
		renderables: make(map[RenderableNodeI]bool),
		nodesMap:    make(map[int]*Node),
		orderedKeys: make([]int, 0),
	}
}

// Get node by id.
func (s *Scene) GetNode(id int) *Node {
	return s.GetNodeById(id)
}

func (s *Scene) GetNodes() []*Node {
	//return s.nodes
	r := make([]*Node, len(s.nodesMap))
	i := 0
	//for _, v := range s.nodesMap {
	for _, k := range s.orderedKeys {
		r[i] = s.nodesMap[k]
		i++
	}
	return r
}

func (s *Scene) GetVisibleNodes() []*Node {
	r := make([]*Node, 0)
	for _, n := range s.GetNodes() {
		if n.Visible() {
			r = append(r, n)
		}
	}
	return r
}

func (s *Scene) GetMeshNodes() []*Node {
	r := make([]*Node, 0)
	for _, n := range s.GetNodes() {
		if n.Mesh != nil {
			r = append(r, n)
		}
	}
	return r
}

func (s *Scene) GetNodeById(id int) *Node {
	if n, ok := s.nodesMap[id]; ok {
		return n
	} else {
		panic(fmt.Sprintf("node not found; id: %d", n.id))
	}
}

func (s *Scene) GetNodeByName(name string) *Node {
	if n := s.GetNodeByNameCheck(name); n != nil {
		return n
	} else {
		panic(fmt.Sprintf("node not found; name: %s", name))
	}
}

func (s *Scene) GetNodeByNameCheck(name string) *Node {
	for _, node := range s.GetNodes() {
		if node.name == name {
			return node
		}
	}
	return nil
}

func (s *Scene) AddRenderable(r RenderableNodeI) {
	s.renderables[r] = true
}

func (s *Scene) RemoveRenderable(r RenderableNodeI) {
	delete(s.renderables, r)
}

func (s *Scene) Add(n *Node) {
	// Not adding if we already have it
	if s.HasNode(n) {
		if Debug {
			if n != s.GetNode(n.id) {
				panic("node comparison filed")
			}
		}
		return
	}
	Log.Inff("[Scene] AddNode; name: %s, id: %d, addr: %p, clonedFromId: %d", n.name, n.id, n, n.clonedFromId)
	fmt.Printf("(1) %+v\n", n)
	fmt.Printf("(2) %#v\n", n)
	fmt.Printf("(3) %v\n", n)
	//dump(n)
	if s.HasNodeName(n.name) {
		p("already have this node name, try to rename")
		n.fixDuplicateNames()
		if s.HasNodeName(n.name) {
			p("ERROR: cannot fix the node's name")
			p(n)
			panic("failed to fix duplicates")
		}
	}

	switch n.nodetype {
	case NodeType_Mesh:
		{
		}
		/*
			if n == nil {
				panic("cannot add nil mesh node")
			}
			// Append
			s.meshes = append(s.meshes, n)
			// Insert
			//i := 0
			//s.meshes = append(s.meshes[:i], append([]*Node{n}, s.meshes[i:]...)...)
		*/

	case NodeType_Camera:
		{
		}
		/*
			s.cameras = append(s.cameras, n)
		*/

	case NodeType_Light:
		s.lights = append(s.lights, n)
		// set default light
		if s.light0 == nil {
			s.light0 = n
		}

	default:
		Log.Err("Don't know how to add node type; NodeType:", n.nodetype)
		pp("exit")
	}

	// For all nodes
	k := n.id
	s.nodesMap[k] = n

	/*
		// Update sorted keys
		var keys []int
		for k := range m {
			keys = append(keys, k)
		}
		sort.Ints(keys)
		s.sortedKeys = keys
	*/

	// Update ordered keys
	s.orderedKeys = append(s.orderedKeys, k)
}

func (s *Scene) CreateCameraNode(name string) *Node {
	cam := NewCameraNode()
	cam.name = name
	s.Add(cam)
	return cam
}

func (s *Scene) CreateLightNode(name string) *Node {
	li := NewLightNode()
	li.name = name
	s.Add(li)
	return li
}

// Returns l0
func (s *Scene) GetDefaultLight() *Node {
	return s.light0
}

/*
func (s *Scene) AddEmptyNode(en *EmptyNode) {
  if en == nil {
    panic("cannot add nil emptynode")
  }
  s.empties = append(s.empties, en)
  s.nodesMap[en.name] = en.Node
}
*/

// Return Node's content.
func (s *Scene) GetNodeContent(name string) interface{} {
	return s.GetNodeByName(name).Content()
}

// Check if scene contains the given node (by id).
func (s *Scene) HasNode(n *Node) bool {
	return s.nodesMap[n.id] != nil
}

// Check if scene contains the node (by name).
func (s *Scene) HasNodeName(name string) bool {
	return s.GetNodeByNameCheck(name) != nil
}

/*
func (s *Scene) RemoveNode(node interface{}) {
  switch node.(type) {
  case *MeshNode:
    s.meshes = append(s.meshes, mn)
    s.nodesMap[mn.name] = mn.Node
  }
}*/

// Deprecated
func (s *Scene) GetMeshNode(name string) *Node {
	return s.GetNodeByName(name)
	//println("meshnode not found; name:", name)
	//panic("key error")
}

// Deprecated
func (s *Scene) RemoveMeshNode(mn *Node) {
	/*
		for i, m := range s.meshes {
			if m == mn {
				s.meshes = append(s.meshes[:i], s.meshes[i+1:]...)
			}
		}
	*/
	if !s.HasNode(mn) {
		pp("cannot remove node: no such node")
	}
	delete(s.nodesMap, mn.id)

	// fix orderedKeys...
	for i, k := range s.orderedKeys {
		if s.nodesMap[k] == nil {
			//p("no key", i, k)
			s.orderedKeys = append(s.orderedKeys[:i],
				s.orderedKeys[i+1:]...)
		} else {
			//p("has key", i, k)
		}
	}
	//p(len(s.orderedKeys), len(s.nodesMap))
	if len(s.orderedKeys) != len(s.nodesMap) {
		pp("cant fix orderedKeys")
	}
	//pp(2)
	// free resources
	//mn.Free()
	mn.Mesh.FreeUnused()
}

func (s *Scene) ClearScene() {
	s.nodesMap = make(map[int]*Node)
	s.orderedKeys = make([]int, 0)
}

/*
func (s *Scene) GetActiveCamera() *CameraNode {
	return s.cam0
}
*/

/*
func (s *Scene) RemoveEmptyNode(en *EmptyNode) {
  for i, x := range s.empties {
    if x == en {
      s.empties = append(s.empties[:i], s.empties[i+1:]...)
    }
  }
  delete(s.nodesMap, en.name)
  // free resources
  en.Free()
}
*/

func (s *Scene) Update(dt float64) {
	//...
}
