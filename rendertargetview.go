package rx

// ***** RenderTarget Debug View System *****

type RenderTargetDebugViewSys struct {
	*System
}

func NewRenderTargetDebugViewSys() *RenderTargetDebugViewSys {
	s := &RenderTargetDebugViewSys{
		System: NewSystem("", ""),
	}
	return s
}

// ***** RenderTarget Debug View *****

type RenderTargetDebugView struct {
	rt      *RenderTarget
	c       *Card
	visible bool
}

func NewRenderTargetDebugView(rt *RenderTarget) *RenderTargetDebugView {
	v := &RenderTargetDebugView{
		rt: rt,
		c:  NewCard(),
	}
	v.c.w = 0.2
	v.c.h = 0.2
	v.c.x = 0.8
	v.c.y = 0.8
	v.c.tex = rt.fb.tex
	v.c.SetRenderLayer(RenderLayer_Debug)
	//rxi.RenderTargetDebugViewSys.addElem(v)
	v.c.Spawn()
	return v
}

func (v *RenderTargetDebugView) SetVisible(val bool) {
	v.visible = val
	v.c.SetVisible(v.visible)
}
