package rxgui

import (
	ps "bitbucket.org/coaljoe/lib/pubsub"
)

const (
	// FIXME: must be unique numbers
	Ev_gui_key_press     ps.EventType = -1
	Ev_gui_key_release                = -2
	Ev_gui_mouse_move                 = -3
	Ev_gui_mouse_press                = -4
	Ev_gui_mouse_release              = -5
	Ev_mouse_enter                    = -6
	Ev_mouse_out                      = -7
)

func Pub(eventType ps.EventType, data interface{}) {
	ps.Publish(eventType, data)
}

func Sub(eventType ps.EventType, fn ps.Callback) {
	ps.Subscribe(eventType, fn)
}
