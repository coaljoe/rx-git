package rxgui

import (
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"
	//"github.com/go-gl/gl/v2.1/gl"
)

type Image struct {
	*Widget
	tex  *rx.Texture
	card *rx.Card
}

func NewImage() *Image {
	i := &Image{
		Widget: NewWidget("image"),
	}
	return i
}

func (i *Image) Load(path string) {
	//i.tex.Load(path)
	i.tex = rx.NewTexture(path)
	i.card = rx.NewCard()
	i.card.SetTexture(i.tex)
}

func (i *Image) Render(r *rx.Renderer) {
	//pp(2)
	if i.tex == nil {
		return
	}
	rect := i.GetAbsoluteRect()
	//rect := Rect{0, 0, 0.5, 0.5}
	i.tex.Bind()
	rx.DrawSetColorV(Vec3{1, 0, 1})
	//rx.DrawQuadV(rect.ToVec4())
	//rxi.Renderer().RenderQuad(rect.X(), rect.Y(), rect.W(), rect.H(), 1)
	rxi.Renderer().RenderQuad(rect.X(), rect.Y(), rect.W(), rect.H(), 0.01*float64(i.Level()))
	rx.DrawResetColor()
	i.tex.Unbind()
}
