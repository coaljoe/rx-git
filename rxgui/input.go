package rxgui

import (
	ps "bitbucket.org/coaljoe/lib/pubsub"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"
	"fmt"
	"github.com/go-gl/glfw/v3.1/glfw"
)

type InputSys struct {
	Mouse    *rx.Mouse
	Keyboard *rx.Keyboard
	is       *rx.InputSys
}

func newInputSys() *InputSys {
	rxi := rx.Rxi()
	is := &InputSys{
		is:       rxi.InputSys,
		Mouse:    rxi.InputSys.Mouse,
		Keyboard: rxi.InputSys.Keyboard}
	is.init()
	return is
}

func (is *InputSys) init() {
	Sub(rx.Ev_mouse_move, is.OnMouseMove)
	Sub(rx.Ev_mouse_press, is.OnMousePress)
}

func (is *InputSys) OnMouseMove(ev *ps.Event) {
	m := ev.Data.(rx.EvMouseMoveData)
	//_log.Trc("cam.onmousemove", p.X, p.Y)
	//fmt.Println("rxgui.onmousemove", p.X, p.Y)
	fmt.Println("rxgui.onmousemove", m.X, m.Y, m.PrevX, m.PrevY)
	fmt.Println("rxgui.onmousemove", m.SX, m.SY, m.PrevSX, m.PrevSY)

	// Now active widgets
	activeWidgets := make([]WidgetI, 0)
	// Prev active widgets
	prevActiveWidgets := make([]WidgetI, 0)
	//c.mx = p.X
	//c.my = p.Y
	sheet := ctx.SheetSys.ActiveSheet()
	for _, w := range sheet.GetWidgets() {
		fmt.Println("w:", w.Name(), w)
		r := w.GetAbsoluteRect()
		if InRect(m.SX, m.SY, r.X(), r.Y(), r.W(), r.H()) {
			activeWidgets = append(activeWidgets, w)
			p("w:", w)
			//panic(2)
		}
		if InRect(m.PrevSX, m.PrevSY, r.X(), r.Y(), r.W(), r.H()) {
			prevActiveWidgets = append(prevActiveWidgets, w)
			//panic(3)
		}
	}
	fmt.Println("activeWidgets:", activeWidgets)
	fmt.Println("prevActiveWidgets:", prevActiveWidgets)

	var activeWidget WidgetI
	var prevActiveWidget WidgetI
	if len(activeWidgets) > 0 {
		activeWidget = activeWidgets[len(activeWidgets)-1]
	}
	if len(prevActiveWidgets) > 0 {
		prevActiveWidget = prevActiveWidgets[len(prevActiveWidgets)-1]
	}
	fmt.Println("activeWidget:", activeWidget)
	fmt.Println("prevActiveWidget:", prevActiveWidget)

	if activeWidget != prevActiveWidget {
		if activeWidget != nil {
			p("OnEnter")
			Pub(Ev_mouse_enter, activeWidget)
		}
		if prevActiveWidget != nil {
			p("OnOut")
			Pub(Ev_mouse_out, prevActiveWidget)
		}
	}
	//panic(2)
}

func (is *InputSys) OnMousePress(ev *ps.Event) {
	m := ev.Data.(glfw.MouseButton)
	fmt.Println("rxgui.onmousepress", m)
	//pp(2)
	sheet := ctx.SheetSys.ActiveSheet()
	for _, w := range sheet.GetWidgets() {
		fmt.Println("w:", w.Name(), w)
		if w.Hover() {
			p(w.Callback())
			cb := w.Callback()
			if cb != nil {
				cb()
			}
			//pp("hover")
		}
	}
}
