package rxgui

import (
	"bitbucket.org/coaljoe/rx"
	"fmt"
)

// Convert pixels to units
func Px(v int) float64 {
	r := rx.Rxi().Renderer()
	//nativeResX := r.Width()
	//nativeResY := r.Height()
	return float64(v) * r.GetPw() // Fixme: use GetPixelSize?
}

// Convert units to pixels
func UnitsToPx(v float64) int {
	r := rx.Rxi().Renderer()
	return int(v * float64(r.Width())) // XXX: fixme?
}

func p(args ...interface{}) {
	fmt.Println(args)
}

func pp(args ...interface{}) {
	fmt.Println(args)
	panic("pp")
}
