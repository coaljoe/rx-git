package rxgui

type Button struct {
	*Widget
	icon  *Image
	style *ButtonStyle
}

type ButtonStyle struct {
}

func NewButton(text string) *Button {
	b := &Button{
		Widget: NewWidget("button"),
		icon:   NewImage(),
	}
	b.AddChild(b.icon)
	return b
}

func (b *Button) SetIcon(path string) {
	b.icon.Load(path)
}
