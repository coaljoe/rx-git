package rxgui

import (
	"bitbucket.org/coaljoe/rx"

	"github.com/go-gl/gl/v2.1/gl"
)

// Globals
var rgi *RxGui
var ctx *Context
var rxi *rx.Rx // Fixme?

func Init() {
}

type RxGui struct {
	//Context *Context
	*Context
	// Enable throttling
	throttling bool
	// Render only Nth frame
	throttlingNth uint64
	enabled       bool
	hasMouseFocus bool
}

func (rg *RxGui) Enabled() bool             { return rg.enabled }
func (rg *RxGui) SetEnabled(v bool)         { rg.enabled = v }
func (rg *RxGui) Throttling() bool          { return rg.throttling }
func (rg *RxGui) SetThrottling(v bool)      { rg.throttling = v }
func (rg *RxGui) ThrottlingNth() uint64     { return rg.throttlingNth }
func (rg *RxGui) SetThrottlingNth(v uint64) { rg.throttlingNth = v }

func NewRxGui() *RxGui {
	Init() // fixme?
	rgi = &RxGui{
		enabled:       true,
		throttling:    false,
		throttlingNth: 4,
	}

	rgi.Context = newContext()

	// Set Globals
	ctx = rgi.Context

	// Add Subsystems
	ctx.SheetSys = newSheetSys()
	ctx.InputSys = newInputSys()

	// Add extra context
	//ctx.Style = NewStyle() // Default style
	ctx.Style = NewDarkStyle() // Default style
	ctx.TextRenderer = newTextRenderer()

	rxi = rx.Rxi()
	rxi.Renderer().AddGuiRP(rgi)

	return rgi
}

func (rg *RxGui) Name() string {
	return "RxGui"
}

func (rg *RxGui) HasMouseFocus() bool {
	//for x := range rg.SheetSys.activeSheet.root {
	//}
	//return true
	return rg.hasMouseFocus
}

// Render the gui.
// Note: already executed in 2D mode by the renderer.
func (rg *RxGui) Render(r *rx.Renderer) {
	if !rg.enabled {
		return
	}

	if rg.throttling {
		// Update gui every nth frame
		nth := rg.throttlingNth
		if rxi.App.Frame()%nth != 0 {
			return
		}
	}

	if !r.IsIn2dMode() {
		panic("not in 2d mode")
	}

	// XXX Fixme: first update gui?
	rg.Update(rxi.App.GetDt())

	println("rxgui: Render")

	//println("GUI RENDER")
	/*
		r.RenderQuad(0.5, 0.0, 0.2, 0.1, 0)
		r.Set2DMode()
		rx.DrawLine(0, 0, 0, .5, .5, 0)
		//rx.DrawLine(0, 0, 0, 1, 1, 0)
		//rx.DrawLine(0, 0, 0, 100, 100, 0)
		r.Unset2DMode()
	*/

	prevLineSmooth := false
	if gl.IsEnabled(gl.LINE_SMOOTH) {
		prevLineSmooth = true
	}

	// Enable antialiased lines
	gl.Enable(gl.LINE_SMOOTH)

	//gl.DepthFunc(gl.NEVER)
	//gl.Disable(gl.DEPTH_TEST)
	//gl.Disable(gl.CULL_FACE)
	//gl.DepthMask(true)

	sh := ctx.SheetSys.ActiveSheet()
	sh.Render(r)

	//gl.DepthMask(false)

	//gl.Enable(gl.DEPTH_TEST)
	//gl.Enable(gl.CULL_FACE)
	//panic(2)

	// Restore
	if prevLineSmooth == false {
		gl.Disable(gl.LINE_SMOOTH)
	}

	//gl.Enable(gl.DEPTH_TEST)
	//gl.DepthFunc(gl.LESS)
}

func (rg *RxGui) Update(dt float64) {
	if !rg.enabled {
		return
	}
	println("rxgui: Update")

	sh := ctx.SheetSys.ActiveSheet()
	sh.Update(dt)
	//panic(2)
}
