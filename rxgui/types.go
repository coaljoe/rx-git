package rxgui

import (
	. "bitbucket.org/coaljoe/rx/math"
)

var (
	ColorWhite       = Color{1, 1, 1, 1}
	ColorBlack       = Color{0, 0, 0, 1}
	ColorRed         = Color{1, 0, 0, 1}
	ColorGreen       = Color{0, 1, 0, 1}
	ColorBlue        = Color{0, 0, 1, 1}
	ColorTransparent = Color{0, 0, 0, 0}
)

////////
// Color

type Color Vec4

func (c Color) R() float64 { return c[0] }
func (c Color) G() float64 { return c[1] }
func (c Color) B() float64 { return c[2] }
func (c Color) A() float64 { return c[3] }

func (c *Color) FromRgb(r, g, b int) {
	c[0] = float64(r) / 255.
	c[1] = float64(r) / 255.
	c[2] = float64(r) / 255.
}

func (c *Color) FromRgba(r, g, b, a int) {
	c.FromRgb(r, g, b)
	c[3] = float64(r) / 255.
}

func (c Color) ToRgb() (int, int, int) {
	return int(c[0] * 255), int(c[1] * 255), int(c[2] * 255)
}

func (c Color) ToRgba() (int, int, int, int) {
	return int(c[0] * 255), int(c[1] * 255), int(c[2] * 255), int(c[3] * 255)
}

func (c Color) ToVec4() Vec4 {
	return Vec4(c)
}

///////
// Rect

type Rect Vec4 // x, y, w, h

func (r Rect) X() float64   { return r[0] }
func (r Rect) Y() float64   { return r[1] }
func (r Rect) W() float64   { return r[2] }
func (r Rect) H() float64   { return r[3] }
func (r Rect) X0() float64  { return r[0] }
func (r Rect) Y0() float64  { return r[1] }
func (r Rect) X1() float64  { return r[0] + r[2] }
func (r Rect) Y1() float64  { return r[1] + r[3] }
func (r Rect) ToVec4() Vec4 { return Vec4(r) }

//////
// Pos

type Pos Vec2
