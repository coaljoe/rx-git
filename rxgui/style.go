package rxgui

import (
	"bitbucket.org/coaljoe/rx/text"
)

// Style theme.
type Style struct {
	bgColor          Color `name:bgColor`
	textFont         *text.Font
	textColor        Color
	textBgColor      Color
	borderColor      Color
	borderWidth      float64
	hoverBorderColor Color
	hoverBorderWidth float64
	Label            LabelStyle
	Panel            PanelStyle
	Widget           WidgetStyle
}

func NewStyle() *Style {
	// Default style
	return &Style{
		bgColor: Color{.05, .05, .1, 1},
		//textFont:         newFont("res/gui/fonts/DejaVuSans.ttf", 16),
		//textFont:         newFont(rx.ConfGetResPath()+"/gui/fonts/DejaVuSans.ttf", 16),
		textFont:         text.DefaultFont,
		textColor:        Color{.4, .4, .2, 1}, //ColorRed, //ColorBlack,
		textBgColor:      ColorWhite,
		borderWidth:      Px(2),
		borderColor:      Color{.1, .4, .4, 1},
		hoverBorderColor: Color(ColorBlue.ToVec4().MulScalar(0.8)),
		hoverBorderWidth: Px(5),
	}
}

//////////////
// StyleOption

type StyleOption struct {
	name    string
	inherit bool
	_val    interface{}
}

func newStyleOption(val interface{}) StyleOption {
	return StyleOption{"", true, val}
}

func (so *StyleOption) set(val interface{}) {
	so.inherit = false
	so._val = val
}

func (so *StyleOption) unset() {
	so.inherit = true
	so._val = nil
}

func (so *StyleOption) value() interface{} {
	return so._val
}

func (so *StyleOption) isSet() bool {
	return so._val != nil
}
