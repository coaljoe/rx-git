package rxgui

import (
	"bitbucket.org/coaljoe/rx/text"
)

func NewDarkStyle() *Style {
	s := &Style{}

	s.Label = LabelStyle{}
	s.Label.TextFont = text.DefaultFont
	//s.Label.TextColor = ColorWhite
	//s.Label.TextColor = Color{.4, .4, .2, 1}
	s.Label.TextColor = Color{.4, .48, .5, 1}
	//pp(text.DefaultFont)

	s.Widget = WidgetStyle{}
	s.Widget.BgColor = Color{.05, .05, .1, 1}
	s.Widget.BorderWidth = Px(2)
	s.Widget.BorderColor = Color{.1, .4, .4, 1}
	s.Widget.HoverBorderColor = Color(ColorBlue.ToVec4().MulScalar(0.8))
	s.Widget.HoverBorderWidth = Px(5)

	s.Panel = PanelStyle{}
	s.Panel.BgColor = ColorWhite
	s.Panel.HoverBgColor = ColorRed

	return s
}
