package rxgui

// Gui Context.
type Context struct {
	SheetSys *SheetSys
	InputSys *InputSys
	Style    *Style
	//Font         *ft.Font
	TextRenderer *TextRenderer
}

func newContext() *Context {
	return &Context{}
}
