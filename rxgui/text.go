package rxgui

import (
	"bitbucket.org/coaljoe/rx"
	"bitbucket.org/coaljoe/rx/text"
)

type TextRenderer struct {
	font *text.Font // Fixme: add changeable fonts?
	//shader       *rx.Shader
	defaultColor Color
}

func newTextRenderer() *TextRenderer {
	tr := &TextRenderer{
		//defaultColor: ColorBlack,
		defaultColor: ColorRed,
	}
	tr.init()
	return tr
}

func (tr *TextRenderer) init() {
	//tr.font = newFont("res/gui/fonts/vera.ttf")
	//tr.font = newFont("res/gui/fonts/DejaVuSans.ttf")
	tr.font = ctx.Style.textFont
	//tr.font = newFont("res/gui/fonts/TerminusTTF.ttf")
	//tr.shader = rx.NewShader("gui/shaders/text.vert", "gui/shaders/text.frag")
}

// Render text with default color.
func (tr *TextRenderer) RenderText(text string, x, y float64) {
	tr.RenderTextColor(text, x, y, tr.defaultColor)
}

func (tr *TextRenderer) RenderTextColor(text string, x, y float64, color Color) {

	//sx := 2.0 / float64(rxi.Renderer().Width())
	//sy := 2.0 / float64(rxi.Renderer().Height())

	//x = -1.0 + (x * 2)
	//y = (y * 2) - 1.0

	rx.DrawText(text, x, y, color.ToVec4())

	rx.Xglcheck()
}

/*
func DrawText(s string, px, py float64, font *ft.Font) {

}
*/
