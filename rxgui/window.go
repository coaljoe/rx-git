package rxgui

import (
	"fmt"

	ps "bitbucket.org/coaljoe/lib/pubsub"
	"bitbucket.org/coaljoe/rx"
	"github.com/go-gl/glfw/v3.1/glfw"
)

type Window struct {
	*Panel
	title *WindowTitle
	// Client area
	client     *Panel
	resizeable bool
	drag       bool
	focus      bool
}

func NewWindow(rect Rect) *Window {
	w := &Window{
		Panel: NewPanel(rect),
	}
	w.Panel.style.BgColor = ColorRed
	//w.Panel.style.BgColor = ColorGreen
	crect := Rect{0, 0, 1, 1}
	bsize := Px(8)
	crect[0] += bsize
	crect[1] += bsize
	crect[2] -= bsize * 2
	crect[3] -= bsize * 2
	//pp(rect, crect, Px(2))
	w.client = NewPanel(crect)
	w.Panel.AddChild(w.client)
	//Sub(Ev_mouse_enter, w.EvMouseEnter)
	//Sub(Ev_mouse_out, w.EvMouseOut)
	Sub(rx.Ev_mouse_move, w.EvMouseMove)
	return w
}

func (w *Window) EvMouseMove(ev *ps.Event) {
	/*
		if ev.Data.(WidgetI).Id() != w.Id() {
			return
		}
	*/
	m := ev.Data.(rx.EvMouseMoveData)
	//_log.Trc("cam.onmousemove", p.X, p.Y)
	//fmt.Println("rxgui.onmousemove", p.X, p.Y)
	fmt.Println("x.onmousemove", m.X, m.Y, m.PrevX, m.PrevY)
	fmt.Println("x.onmousemove", m.SX, m.SY, m.PrevSX, m.PrevSY)
	//pp(2)
	w.drag = false
	if ctx.InputSys.Mouse.IsButtonPressed(glfw.MouseButton1) {

		if w.title == nil {
			if w.hover {
				w.drag = true
			}
		} else {
			if w.title.hover {
				w.drag = true
			}

			/*
				trect := w.title.GetAbsoluteRect()
				pp(m.SY)
				if m.SY > trect[1] {
					pp(2)
				}
			*/
		}
	}
	if w.drag {
		dx := m.SX - m.PrevSX
		dy := m.SY - m.PrevSY
		rect := w.Panel.rect
		rect[0] += dx
		rect[1] += dy
		w.Panel.rect = rect
	}

	// Update focus
	w.focus = false
	if w.title == nil {
		w.focus = true
	} else {
		if w.hover || w.title.hover {
			w.focus = true
		}
	}
}

func (w *Window) AddChild(widget WidgetI) {
	w.client.AddChild(widget)
}

func (w *Window) SetTitle(text string) {
	if w.title == nil {
		w.title = newWindowTitle(w, text)
		w.AddChild(w.title)
		p(w.Level())
		//pp(w.title.Level())
	} else {
		w.title.label.SetText(text)
	}
}

func (w *Window) Update(dt float64) {

}

type WindowTitle struct {
	*Panel
	label              *Label
	closeButton        *Button
	closeButtonVisible bool
}

func newWindowTitle(win *Window, text string) *WindowTitle {
	wt := &WindowTitle{
		Panel:              NewPanel(Rect{0, .85, 1, .15}),
		label:              NewLabel(Pos{.2, .2}, text),
		closeButton:        NewButton(""),
		closeButtonVisible: true,
	}
	wt.closeButton.SetIcon("res/gui/images/close_button_red.png")
	wt.closeButton.rect = Rect{.88, .1, .1, .7}
	//wt.closeButton.rect = Rect{.5, .1, Px(32), .7}
	wt.Panel.AddChild(wt.closeButton)
	wt.Panel.AddChild(wt.label)

	wt.Panel.style.BgColor = ColorGreen
	return wt
}
