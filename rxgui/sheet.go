package rxgui

import (
	"bitbucket.org/coaljoe/rx"
)

type SheetI interface {
	GetWidgets() []WidgetI
	Render(r *rx.Renderer)
	Update(dt float64)
}

// Base Sheet.
type Sheet struct {
	id      int
	name    string
	enabled bool
	root    *Widget
}

func (s *Sheet) Root() *Widget { return s.root }

func NewSheet(name string) *Sheet {
	s := &Sheet{
		name: name,
		root: NewWidget("root")}
	s.root.rect = Rect{0, 0, 1, 1}
	return s
}

// Get all sheets widgets.
func (s *Sheet) GetWidgets() []WidgetI {
	return s.root.GetAllChildren()
}

// A shortcut for Root().AddChild()
//func (s *Sheet) AddWidget(w []WidgetI) {
//s.Root().AddChild(w)
//}

func (s *Sheet) Render(r *rx.Renderer) {
	//s.root.Render(r)
	for _, c := range s.root.Children() {
		println("render child")
		c.Render(r)
	}
}

func (s *Sheet) Update(dt float64) {
	for _, c := range s.root.Children() {
		println("update child")
		c.Update(dt)
	}
}
