package rxgui

import (
	ps "bitbucket.org/coaljoe/lib/pubsub"
	"bitbucket.org/coaljoe/rx"
	"fmt"

	"github.com/go-gl/gl/v2.1/gl"

	. "bitbucket.org/coaljoe/rx/math"
)

// A Widget.
type Panel struct {
	*Widget
	// Properties
	text string
	// Components
	label *Label // XXX: make it public?
	style *PanelStyle
}

type PanelStyle struct {
	BgColor      Color
	HoverBgColor Color
}

func (p *Panel) BgColor() Color     { return p.style.BgColor }
func (p *Panel) SetBgColor(c Color) { p.style.BgColor = c }
func (p *Panel) Label() *Label      { return p.label }

func NewPanel(rect Rect) *Panel {
	p := &Panel{
		Widget: NewWidget("panel"),
		label:  NewLabel(Pos{0.5, 0.5}, "label"),
	}
	p.rect = rect
	p.label.SetText("PanelLabelText")
	p.label.border = false
	//w.label.bgColor.unset()
	//w.label.Widget.style.BgColor = Color{0, 1, 0}
	// Copy style
	style := ctx.Style.Panel
	p.style = &style
	Sub(Ev_mouse_enter, p.OnMouseEnterEv)
	Sub(Ev_mouse_out, p.OnMouseOutEv)
	return p
}

func (p *Panel) OnMouseEnterEv(ev *ps.Event) {
	if ev.Data.(WidgetI).Id() != p.Id() {
		return
	}
	/*
		colorObj := Color(p.style.BgColor.ToVec4().AddScalar(0.5))
		p.color = &colorObj
		p.color[3] = 1.0
		//pp(12)
	*/
}

func (p *Panel) OnMouseOutEv(ev *ps.Event) {
	if ev.Data.(WidgetI).Id() != p.Id() {
		return
	}
	/*
		colorObj := Color(p.style.BgColor.ToVec4().SubScalar(0.5))
		p.color = &colorObj
		p.color[3] = 1.0
		//pp(13)
	*/
}

func (p *Panel) Render(r *rx.Renderer) {
	println("Panel.Render")

	// Call Base
	p.Widget.Render(r)

	rect := p.GetAbsoluteRect()
	_ = rect
	//fmt.Println("p.color", p.color)

	//gl.PolygonOffset(1.0, 1.0)
	//gl.Enable(gl.POLYGON_OFFSET_FILL)

	gl.PushMatrix()
	//gl.Translatef(0.0, 0.0, 0.1)

	//r.RenderQuad(p.rect.X(), p.rect.Y(), p.rect.Width(), p.rect.Height(), 0)
	if true {
		rx.DrawBegin()
		//r.Set2DMode()
		gl.PushMatrix()
		//gl.Translatef(0, 0, .01) // fixme: use w.Depth() to calculate z degth?
		//gl.Translatef(0, 0, .1 * float32(p.Level())) // fixme: use w.Depth() to calculate z degth?

		color := p.style.BgColor
		if p.hover {
			color = p.style.HoverBgColor
		}
		rx.DrawSetColor(color.R(), color.G(), color.B())
		_ = color

		//rx.DrawSetColor(1.0, 0.0, 0.0)
		//rx.DrawRectV(Vec4(p.rect))
		//rx.DrawQuadV(Vec4(p.rect))
		rx.DrawQuadV(Vec4(rect))
		rx.DrawResetColor()
		//r.Unset2DMode()

		gl.PopMatrix()
		rx.DrawEnd()
	}

	p.label.Render(r)

	gl.PopMatrix()

	//p.label.Render(r)

	//gl.Disable(gl.POLYGON_OFFSET_FILL)

	println("Panel.Render done")
	//panic(2)
}

func (p *Panel) Update(dt float64) {
	p.Widget.Update(dt)
	//p.label.Update(dt)
	p.label.SetPos(Pos{p.rect[0] + (p.rect[2] / 2), p.rect[1] + (p.rect[3] / 2)})
	fmt.Println("p.rect:", p.rect)
	//panic(2)
}
