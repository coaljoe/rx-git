package rxgui

import (
	"fmt"

	ps "bitbucket.org/coaljoe/lib/pubsub"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"
	"github.com/go-gl/gl/v2.1/gl"
)

type WidgetI interface {
	Name() string
	Id() int
	HasParent() bool
	HasChildren() bool
	Children() []WidgetI
	SetParent(p WidgetI)
	GetAllChildren() []WidgetI
	Rect() Rect
	GetAbsoluteRect() Rect
	Hover() bool
	Callback() func()
	SetCallback(cb func())
	Render(r *rx.Renderer)
	Update(dt float64)
	//Widget() *Widget
}

// XXX rename to BaseWidget?
type Widget struct {
	*node
	rect     Rect // x, y, w, h
	border   bool // Outer border
	active   bool
	hover    bool // Mouse hover
	visible  bool
	style    *WidgetStyle
	parent   WidgetI
	children []WidgetI
	cb       func()
}

type WidgetStyle struct {
	BgColor          Color
	BorderColor      Color
	BorderWidth      float64
	HoverBorderColor Color
	HoverBorderWidth float64
}

func (w *Widget) SetName(name string)   { w.name = name }
func (w *Widget) Rect() Rect            { return w.rect }
func (w *Widget) Children() []WidgetI   { return w.children }
func (w *Widget) Visible() bool         { return w.visible }
func (w *Widget) SetVisible(v bool)     { w.visible = v }
func (w *Widget) Hover() bool           { return w.hover }
func (w *Widget) Callback() func()      { return w.cb }
func (w *Widget) SetCallback(cb func()) { w.cb = cb }

//func (w *Widget) Widget() *Widget     { return w }

func NewWidget(name string) *Widget {
	w := &Widget{
		node:     newNode(name),
		rect:     Rect{0, 0, 1, 1},
		border:   true,
		children: make([]WidgetI, 0),
		active:   false,
		hover:    false,
		visible:  true,
	}
	//w.style = StyleDefault().Widget
	// Copy style
	style := ctx.Style.Widget
	w.style = &style
	Sub(Ev_mouse_enter, w.EvMouseEnter)
	Sub(Ev_mouse_out, w.EvMouseOut)
	return w
}

func (w *Widget) EvMouseEnter(ev *ps.Event) {
	if ev.Data.(WidgetI).Id() != w.Id() {
		return
	}
	//pp("enter")
	w.hover = true
	rgi.hasMouseFocus = true
}

func (w *Widget) EvMouseOut(ev *ps.Event) {
	if ev.Data.(WidgetI).Id() != w.Id() {
		return
	}
	//pp("out")
	w.hover = false
	rgi.hasMouseFocus = false
}

func (w *Widget) HasChild(child WidgetI) bool {
	for _, ch := range w.children {
		if ch == child {
			return true
		}
	}
	return false
}

func (w *Widget) AddChild(child WidgetI) {
	if w.HasChild(w) {
		panic("already have child")
	}
	if w == nil {
		pp("can't add to nil")
	}
	child.SetParent(w)
	w.children = append(w.children, child)
}

func (w *Widget) HasParent() bool {
	return w.parent != nil
}

func (w *Widget) SetParent(p WidgetI) {
	w.parent = p
}

func (w *Widget) HasChildren() bool {
	return len(w.children) > 0
}

// Recursively get all widget's children (deep).
func (w *Widget) GetAllChildren() (ret []WidgetI) {
	// Node: Its possible to use non-recursive
	// version using parent back-links (child-parent links).

	fmt.Println("GetAllChildren", w.Name())
	fmt.Println("ret:", ret)
	if ret == nil {
		ret = make([]WidgetI, 0)
	}

	/*
		var find_rec func(cw WidgetI) []WidgetI
		find_rec = func(cw WidgetI) []WidgetI {
			if !cw.HasChildren() {
				fmt.Println("no children:", cw.Name(), cw)
				ret = append(ret, cw)
			} else {
				for _, c := range cw.Children() {
					//ret = append(ret, c.GetAllChildren()...)
					//d := find_rec()
					//cc := c.(*Widget)
					fmt.Println("process children:", c.Name(), c)
					ret = append(ret, c)
					d := find_rec(c)
					fmt.Println("d:", d)
					//ret = append(ret, d...)
					//ret = append(ret, find_rec(c)...)
				}
			}

			fmt.Println("returning ret:", ret)

			return ret
		}

		find_rec(w)
	*/
	var _find_rec func(cw WidgetI)
	_find_rec = func(cw WidgetI) {
		fmt.Println("_find_rec:", cw.Name(), cw)
		// Skip the top node
		if cw != w {
			ret = append(ret, cw)
		}
		// Traverse all children recursively
		for _, c := range cw.Children() {
			fmt.Println("process children:", c.Name(), c)
			_find_rec(c)
		}
	}

	_find_rec(w)

	fmt.Println("ret:", ret)

	// Reverse ret
	/*
		a := ret
		for i, j := 0, len(a)-1; i < j; i, j = i+1, j-1 {
			a[i], a[j] = a[j], a[i]
		}
		ret = a
	*/

	fmt.Println("ret (after reverse):", ret)

	return ret
}

// Depth level.
func (w *Widget) Level() int {
	lev := 0
	cw := w
	for {
		if cw.HasParent() {
			lev += 1
			cw = cw.parent.(*Widget)
		} else {
			break
		}
	}
	return lev
}

// Return rect in absolute position relative to its parent.
func (w *Widget) GetAbsoluteRect() Rect {
	if !w.HasParent() {
		return w.rect
	}
	//parentRect := w.parent.Rect()
	parentRect := w.parent.(*Widget).GetAbsoluteRect() // recursively
	r := Rect{
		parentRect.X() + (w.rect.X() * parentRect.W()),
		parentRect.Y() + (w.rect.Y() * parentRect.H()),
		w.rect.W() * parentRect.W(), w.rect.H() * parentRect.H()}
	//fmt.Println("rect:", w.rect, "parent rect:", parentRect, "absolute rect:", r)
	return r
}

func (w *Widget) String() string {
	return fmt.Sprintf("Widget<id=%d>", w.id)
}

func (w *Widget) Render(r *rx.Renderer) {
	if !w.visible {
		return
	}
	fmt.Printf("Widget.Render; name: %s\n", w.Name())

	//if w.name == "root":
	//	continue
	// Self render code
	//fmt.Println("widget render; name:", w.name, "level:", w.Level())

	rect := w.GetAbsoluteRect()

	// Border
	bcolor := w.style.BorderColor
	bwidth := w.style.BorderWidth

	if w.hover {
		bcolor = w.style.HoverBorderColor
		bwidth = w.style.HoverBorderWidth
	}

	brect := rect
	brect[0] -= bwidth / 2
	brect[1] -= bwidth / 2
	brect[2] += bwidth / 2
	brect[3] += bwidth / 2

	if false {

		//r.RenderQuad(p.rect.X(), p.rect.Y(), p.rect.Width(), p.rect.Height(), 0)
		rx.DrawBegin()

		if w.border {
			rx.DrawSetColor(bcolor.R(), bcolor.G(), bcolor.B())
			//rx.DrawSetLineWidth(bwidth)
			rx.DrawSetLineWidth(float64(UnitsToPx(bwidth)))
			rx.DrawRectV(Vec4(brect))
		}

		//if w.bgColor.isSet() {
		if false {
			// Bg
			bgColor := w.style.BgColor

			rx.DrawSetColor(bgColor.R(), bgColor.G(), bgColor.B())
			rx.DrawQuadV(w.rect.ToVec4())
			//pp(rect)
			//rx.DrawQuadV(rect.ToVec4())
			//rx.DrawQuadV(Vec4{0.5, 0.5, 0.25, 0.25})
		}

		rx.DrawResetColor()

		rx.DrawEnd()
	}

	/*
		fmt.Println(w.GetAbsoluteRect(), w.Rect())
		if w.HasParent() {
			fmt.Println("parent:", w.parent.(*Widget).name)
		}
	*/
	//step := float32(w.Level()) / 10.0
	//fmt.Println("step:", step)
	//gl.PushMatrix()
	//gl.Translatef(0, 0, float32(w.Level()))
	//gl.Translatef(0, 0, step)
	//gl.Translatef(0, 0, +.5)

	// Call children render code
	for _, ch := range w.children {
		gl.PushMatrix()
		gl.Translatef(0, 0, 0.01*float32(w.Level()))
		ch.Render(r)
		gl.PopMatrix()
	}

	//gl.PopMatrix()

	println("Widget.Render done")
}

func (w *Widget) Update(dt float64) {
	println("Widget.Update")
	//panic(2)
}
