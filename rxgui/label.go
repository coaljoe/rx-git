package rxgui

import (
	"bitbucket.org/coaljoe/rx"
	"bitbucket.org/coaljoe/rx/text"

	"github.com/go-gl/gl/v2.1/gl"
)

// A widget.
type Label struct {
	*Widget
	text  string
	style *LabelStyle
	//textColor Color
	//cb        func()
}

type LabelStyle struct {
	TextFont  *text.Font
	TextColor Color
}

func (la *Label) Text() string { return la.text }

//func (la *Label) SetCallback(cb func()) { la.cb = cb }

//func NewLabel(rect Rect) *Label {
func NewLabel(pos Pos, text string) *Label {
	la := &Label{
		Widget: NewWidget("label"),
	}
	la.rect[0] = pos[0]
	la.rect[1] = pos[1]
	// Copy style
	style := ctx.Style.Label
	la.style = &style
	// Update
	la.SetText(text)
	//Sub(Ev_mouse_enter, w.EvMouseEnter)
	//Sub(Ev_mouse_out, w.EvMouseOut)
	return la
}

func (la *Label) build() {
	ph := rxi.Renderer().GetPh()
	font := la.style.TextFont
	//la.rect[3] = float64(font.Height()) * ph
	metrics := font.Metrics()
	lineHeight := (metrics.Ascent + metrics.Descent).Ceil()
	la.rect[3] = float64(lineHeight) * ph

	//la.rect[3] = float64(32) * ph
}

func (la *Label) SetText(s string) {
	la.text = s
	la.build()
}

func (la *Label) SetPos(pos Pos) {
	la.rect[0] = pos[0]
	la.rect[1] = pos[1]
	la.build()
}

func (la *Label) Render(r *rx.Renderer) {
	//return
	la.Widget.Render(r)
	//sx := 2.0 / float64(r.Width())
	//sy := 2.0 / float64(r.Height())
	//px := la.rect.X()
	//py := la.rect.Y()
	//ctx.TextRenderer.RenderText(la.text, -1+8*sx, 1-50*sy, sx, sy)

	rect := la.GetAbsoluteRect()
	px := rect.X()
	py := rect.Y()

	/*
		prevDepthTest := false
		if gl.IsEnabled(gl.DEPTH_TEST) {
			prevDepthTest = true
			gl.Disable(gl.DEPTH_TEST)
		}
	*/

	ctx.TextRenderer.RenderTextColor(la.text, px, py,
		la.style.TextColor)
	//gl.Enable(gl.DEPTH_TEST)

	/*
		if prevDepthTest {
			gl.Enable(gl.DEPTH_TEST)
		}
	*/

	if false {
		//ctx.TextRenderer.RenderText("XXX", sx, sy, sx, sy)
		//ctx.TextRenderer.RenderText("YYY", 0, 0, sx, sy)
		//ctx.TextRenderer.RenderText("Последние новости", sx, sy)
		gl.Disable(gl.DEPTH_TEST)
		//ctx.TextRenderer.RenderText("Последние новости", 0.5, 0.5)
		ctx.TextRenderer.RenderText("Последние новости", .5, 0.5)
		ctx.TextRenderer.RenderTextColor("Последние новости 2", 0, 1, ColorGreen)
		gl.Enable(gl.DEPTH_TEST)
		gl.Disable(gl.DEPTH_TEST)
		//ctx.TextRenderer.RenderText("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", -1+1*sx, 1-100*sy)
		gl.Enable(gl.DEPTH_TEST)
	}
}

func (la *Label) Update(dt float64) {
	la.Widget.Update(dt)
}
