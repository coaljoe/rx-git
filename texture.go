package rx

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"

	//"image/draw"
	"log"
	"math"
	"os"
	_path "path"
	"strings"

	"github.com/disintegration/imaging"
	"github.com/go-gl/gl/v2.1/gl"

	//"image/draw"
	_ "image/jpeg"
	_ "image/png"
	//"github.com/pixiv/go-libjpeg/jpeg"
	//"github.com/kjk/golibjpegturbo"
)

const (
	TEXTURE_MAX_ANISOTROPY_EXT     = 0x84FE
	MAX_TEXTURE_MAX_ANISOTROPY_EXT = 0x84FF
)

type TextureFilter int
type TextureFormat int

const (
	TextureFilterLinear TextureFilter = iota
	TextureFilterNearest

	TextureFormatRGBA TextureFormat = iota
	TextureFormatFloat32
)

type ImageData struct {
	Data interface{} //[]uint8
	//DataFloat []float32 // for float images
	W, H int
}

type Texture struct {
	mipmaps    bool
	compress   bool
	anisotropy bool
	width      int
	height     int
	format     TextureFormat
	minFilter  TextureFilter
	magFilter  TextureFilter
	// gl
	tex        uint32
	min_filter int32
	mag_filter int32
}

func (t *Texture) Width() int                { return t.width }
func (t *Texture) Height() int               { return t.height }
func (t *Texture) Tex() uint32               { return t.tex }
func (t *Texture) Size() (int, int)          { return t.width, t.height }
func (t *Texture) Format() TextureFormat     { return t.format }
func (t *Texture) SetFormat(f TextureFormat) { t.format = f }
func (t *Texture) MinFilter() TextureFilter  { return t.minFilter }
func (t *Texture) MagFilter() TextureFilter  { return t.magFilter }

type TextureOptions struct {
	Nomipmaps bool
	Flip      bool
	MinFilter TextureFilter
	MagFilter TextureFilter
}

func NewTexture(path string) *Texture {
	//pp(2)
	return NewTextureOpt(path,
		TextureOptions{Flip: false})
}

func NewTextureOpt(path string, _opts ...TextureOptions) *Texture {
	t := &Texture{
		format:     TextureFormatRGBA,
		minFilter:  TextureFilterLinear, // default
		magFilter:  TextureFilterLinear, // default
		mipmaps:    !_opts[0].Nomipmaps,
		compress:   false,
		anisotropy: true,
	}
	t.SetOpts(_opts...)
	t.load(path, _opts[0].Flip)
	glcheck()
	return t
}

func NewEmptyTexture(width, height int, format TextureFormat) *Texture {
	t := &Texture{
		format:     format,
		minFilter:  TextureFilterLinear, // default
		magFilter:  TextureFilterLinear, // default
		compress:   false,
		anisotropy: true, // ?
	}

	// set default min mag filters (fixme)
	t.min_filter = gl.LINEAR
	t.mag_filter = gl.LINEAR

	t.genTexture(width, height, format)

	glcheck()
	return t
}

func (t *Texture) SetOpts(_opts ...TextureOptions) {
	opts := _opts[0]

	// disable mipmaps
	if opts.Nomipmaps {
		t.mipmaps = false
	}

	var min_filter int32
	switch opts.MinFilter {
	case TextureFilterLinear:
		t.minFilter = TextureFilterLinear
		min_filter = gl.LINEAR
	case TextureFilterNearest:
		t.minFilter = TextureFilterNearest
		min_filter = gl.NEAREST
	default:
		pp("unknown min filter", opts.MinFilter)
	}

	var mag_filter int32
	switch opts.MagFilter {
	case TextureFilterLinear:
		t.magFilter = TextureFilterLinear
		mag_filter = gl.LINEAR
	case TextureFilterNearest:
		t.magFilter = TextureFilterNearest
		mag_filter = gl.NEAREST
	default:
		pp("unknown mag filter", opts.MagFilter)
	}

	t.min_filter = min_filter // fixme: move to build?
	t.mag_filter = mag_filter

	t.build()
}

// XXX not well tested
func (t *Texture) SetMagFilter(v TextureFilter) {
	t.SetOpts(TextureOptions{
		MagFilter: v,
		MinFilter: t.minFilter})
}

// XXX not well tested
func (t *Texture) SetMinFilter(v TextureFilter) {
	t.SetOpts(TextureOptions{
		MinFilter: v,
		MagFilter: t.magFilter})
}

func (t *Texture) genTexture(width, height int, format TextureFormat) {
	// generate empty texture
	gl.GenTextures(1, &t.tex)
	gl.BindTexture(gl.TEXTURE_2D, t.tex)
	gl.PixelStorei(gl.UNPACK_ALIGNMENT, 1)

	if format == TextureFormatRGBA {
		gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA8,
			int32(width), int32(height), 0, gl.RGBA, gl.UNSIGNED_BYTE, nil)
	} else if format == TextureFormatFloat32 {
		gl.TexImage2D(gl.TEXTURE_2D, 0, gl.R32F,
			int32(width), int32(height), 0, gl.RED, gl.FLOAT, nil)
	} else {
		Log.Err("Unknown format;", format)
		panic("unknown format")
	}

	// update texture
	t.format = format

	t.build()
}

func (t *Texture) build() {
	t.Bind() // warning: dangerous, can rebind existing texture bind. (fixme?)

	/*
		https://www.opengl.org/wiki/Sampler_Object#Filtering

		GL_NEAREST - no filtering, no mipmaps
		GL_LINEAR - filtering, no mipmaps
		GL_NEAREST_MIPMAP_NEAREST - no filtering, sharp switching between mipmaps
		GL_NEAREST_MIPMAP_LINEAR - no filtering, smooth transition between mipmaps
		GL_LINEAR_MIPMAP_NEAREST - filtering, sharp switching between mipmaps
		GL_LINEAR_MIPMAP_LINEAR - filtering, smooth transition between mipmaps

		So:
		GL_LINEAR is bilinear
		GL_LINEAR_MIPMAP_NEAREST is bilinear with mipmaps
		GL_LINEAR_MIPMAP_LINEAR is trilinear
	*/
	if t.mipmaps {
		//	t.min_filter = gl.LINEAR_MIPMAP_LINEAR
		if t.minFilter == TextureFilterLinear && t.magFilter == TextureFilterLinear {
			t.min_filter = gl.LINEAR_MIPMAP_LINEAR
			//pp("this")
		} else if t.minFilter == TextureFilterLinear && t.magFilter == TextureFilterNearest {
			t.min_filter = gl.LINEAR_MIPMAP_NEAREST
		} else if t.minFilter == TextureFilterNearest && t.magFilter == TextureFilterLinear {
			t.min_filter = gl.NEAREST_MIPMAP_LINEAR
		} else if t.minFilter == TextureFilterNearest && t.magFilter == TextureFilterNearest {
			t.min_filter = gl.NEAREST_MIPMAP_NEAREST
		} else {
			pp("unknown mipmap combination; minFilter:", int(t.minFilter), "magFilter:", int(t.magFilter))
		}
	}

	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, t.min_filter)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, t.mag_filter)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT)
	glcheck()

	if t.anisotropy {
		if !checkGLExt("GL_EXT_texture_filter_anisotropic") {
			Log.Dbg("anisotropic textures not supported")
			return
		}
		var anisotropy float32
		gl.GetFloatv(MAX_TEXTURE_MAX_ANISOTROPY_EXT, &anisotropy)
		maxAniso := math.Min(float64(Conf.max_anisotropy), float64(anisotropy))
		gl.TexParameterf(gl.TEXTURE_2D, TEXTURE_MAX_ANISOTROPY_EXT, float32(maxAniso))
		//gl.TexParameterf(gl.TEXTURE_2D, TEXTURE_MAX_ANISOTROPY_EXT, float32(-100))
		//println("maxAniso:", maxAniso)
		glcheck()
	}

	// update width, height
	var w, h int32
	gl.GetTexLevelParameteriv(gl.TEXTURE_2D, 0, gl.TEXTURE_WIDTH, &w)
	gl.GetTexLevelParameteriv(gl.TEXTURE_2D, 0, gl.TEXTURE_HEIGHT, &h)
	t.width, t.height = int(w), int(h)

	t.Unbind() // warning: dangrous (can unbind existing GL_TEXTURE_2D)
}

func CreateTexture(width, height int, depth bool) *Texture {
	var tex uint32
	gl.GenTextures(1, &tex)
	//tex.Bind(gl.TEXTURE_2D)
	gl.BindTexture(gl.TEXTURE_2D, tex)

	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	//gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	//gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)

	if !depth {
		gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA8,
			int32(width), int32(height), 0, gl.RGBA, gl.UNSIGNED_BYTE, nil)
	} else {
		gl.TexImage2D(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT24,
			int32(width), int32(height), 0, gl.DEPTH_COMPONENT, gl.FLOAT, nil)

		//gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_COMPARE_FUNC, gl.LEQUAL)
		//gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_COMPARE_MODE, gl.COMPARE_R_TO_TEXTURE)
	}
	//tex.Unbind(gl.TEXTURE_2D)
	gl.BindTexture(gl.TEXTURE_2D, 0)

	// create texture record (fixme?)
	t := &Texture{
		width:      width,
		height:     height,
		format:     TextureFormatRGBA,
		minFilter:  TextureFilterLinear,
		magFilter:  TextureFilterLinear,
		mipmaps:    false,
		compress:   false,
		anisotropy: false,
		tex:        tex,
		min_filter: gl.LINEAR,
		mag_filter: gl.LINEAR,
	}
	return t
}

//func (t *Texture) loadDelayed(path string, flip bool) {
func (t *Texture) load(path string, flip bool) {
	//p("xxx -> load:", path)

	// TODO: DDS flipping code?
	isDDS := strings.HasSuffix(path, ".dds")

	if !isDDS {

		isFloat := false
		imData := t.loadImageData(path, flip)
		data_i, ix, iy := imData.Data, imData.W, imData.H

		switch data_i.(type) {
		case []float32:
			isFloat = true
		}
		//fmt.Printf("xxx -> %T\n", data_i)

		gl.GenTextures(1, &t.tex)
		gl.BindTexture(gl.TEXTURE_2D, t.tex)
		gl.PixelStorei(gl.UNPACK_ALIGNMENT, 1)

		// XXX duplicate: remove?
		if t.mipmaps {
			gl.TexParameteri(gl.TEXTURE_2D, gl.GENERATE_MIPMAP, gl.TRUE)
			//	t.min_filter = gl.LINEAR_MIPMAP_LINEAR
		}

		/*
			if !gray {
				gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA8, int32(ix), int32(iy), 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(image_data))
			} else {
				gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGB8, int32(ix), int32(iy), 0, gl.RGB, gl.UNSIGNED_BYTE, gl.Ptr(image_data))
			}
		*/
		if !isFloat {
			image_data := data_i.([]uint8)
			// use RGBA (32bit) from all textures
			gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA8, int32(ix), int32(iy), 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(image_data))
		} else {
			p("loading float image")
			//t.SetFormat(TextureFormatFloat32)
			//t.genTexture(ix, iy, TextureFormatFloat32)
			//t.SetTextureData(data_i)
			image_data := data_i.([]float32)
			//pp("ix:", ix, "iy:", iy, len(image_data))
			//gl.TexSubImage2D(gl.TEXTURE_2D, 0, gl.R32F, 0, int32(t.width), int32(t.height), gl.RED, gl.FLOAT, gl.Ptr(image_data))
			gl.TexImage2D(gl.TEXTURE_2D, 0, gl.R32F, int32(ix), int32(iy), 0, gl.RED, gl.FLOAT, gl.Ptr(image_data))
			// XXX fixme? need calling build() method for updates
			// XXX only single-channel float images supported
		}

	} else {
		// dds texture

		var err error
		t.tex, err = TextureFromDDS(path)
		if err != nil {
			panic(err)
		}
		glcheck()
	}
	/*
	   if t.mipmaps {
	       gl.TexParameteri(gl.TEXTURE_2D, gl.GENERATE_MIPMAP, gl.TRUE)
	       t.min_filter = gl.LINEAR_MIPMAP_LINEAR
	   }
	*/

	t.build()

	gl.BindTexture(gl.TEXTURE_2D, 0)

	//t.Save("_rx_tex.png")
	//pp(2)
}

//var _ = `
func (t *Texture) loadImageData(path string, flip bool) *ImageData {
	Log.Dbg("loadImageData: path:", path, "flip:", flip)
	//type im interface{}
	//im = image.NewNRGBA()

	///*
	// return from cache
	//rxi.ResourceSys.PrintResources()
	//println("PATH:", path)
	if r := rxi.ResourceSys.GetByPath(path, ResourceTypeImage); r != nil {
		println("reusing image, path: " + path)
		im := r.data.(*ImageData)
		//pp(2)
		return im
	}
	//*/

	im := loadImage(path)
	Log.Dbg("color model:", im.ColorModel(), fmt.Sprintf("%T", im))
	//p(im.Gray16At(0, 0))
	ix, iy := im.Bounds().Dx(), im.Bounds().Dy()

	// NOTE:
	//  instead of flipping colladaloader flips y-texcoord of models.
	//  [deprecated] same can be done in fullscreen shaders for gl_TexCoord[0] for quads. [/deprecated]
	//  currently not flipping textures by default here.
	//
	// see also:
	//  https://www.opengl.org/wiki/Common_Mistakes#y-axis
	//  https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_06

	if flip {
		gray16 := false
		if im.ColorModel() == color.Gray16Model {
			gray16 = true
		}

		if gray16 {
			// Flip manually test

			//im = color.Gray16Model.Convert(im)
			b := im.Bounds()
			//im2 := image.NewRGBA(image.Rect(0, 0, b.Dx(), b.Dy()))
			im2 := image.NewGray16(image.Rect(0, 0, b.Dx(), b.Dy()))
			for y := 0; y < b.Dy(); y++ {
				for x := 0; x < b.Dx(); x++ {
					c := im.(*image.Gray16).Gray16At(x, y)
					im2.SetGray16(x, b.Dy()-1-y, c)
				}
			}
			//draw.Draw(im2, im2.Bounds(), im, b.Min, draw.Src)

			f, err := os.Create("/tmp/x_16.png")
			if err != nil {
				panic(err)
			}
			defer f.Close()
			png.Encode(f, im2)
			//png.Encode(f, im)

			im = im2
		} else {
			// flip the image for proper opengl textures
			im = imaging.FlipV(im)
		}

		/*
			// Convert back to gray16 type
			if gray16 {
				im.ColorModel().Convert(color.White)
				//im = color.Gray16Model.Convert(im)
				b := im.Bounds()
				//im2 := image.NewRGBA(image.Rect(0, 0, b.Dx(), b.Dy()))
				im2 := image.NewGray16(image.Rect(0, 0, b.Dx(), b.Dy()))
				draw.Draw(im2, im2.Bounds(), im, b.Min, draw.Src)

				//im2.save("/tmp/1.png")
				f, err := os.Create("/tmp/1.png")
				if err != nil {
					panic(err)
				}
				defer f.Close()
				png.Encode(f, im2)

				im = im2
				// Check
				if im.ColorModel() != color.Gray16Model {
					pp("bad color conversion:", fmt.Sprintf("%T %#v", im, im.ColorModel()))
				}
			}
		*/
	}

	//image_data := im.Pix
	//var image_data []uint8
	var data interface{}

	//panic(ix*iy*3)
	//panic(len(im.Pix))

	//image_data = make([]uint8, ix*iy*4)

	// need flipping on one axis? (flipV)
	//idx := 0
	//var _ = idx
	//idx2 := len(im.Pix)-1
	/*
	   for idx2 >= 0 {
	     //println(idx2)
	     image_data[idx] = im.Pix[idx2-3]
	     image_data[idx+1] = im.Pix[idx2-2]
	     image_data[idx+2] = im.Pix[idx2-1]
	     idx += 3
	     idx2 -= 4
	   }
	*/

	/*
	   // slow flip
	   for y := iy-1; y >= 0; y-- {
	     for x := 0; x < ix; x++ {
	       //println(x, y)
	       r, g, b, _ := im.At(x, y).RGBA()
	       image_data[idx] = uint8(r)
	       image_data[idx+1] = uint8(g)
	       image_data[idx+2] = uint8(b)
	       idx += 3
	     }
	   }
	*/

	/*
		Log.Dbg(im.ColorModel(), fmt.Sprintf("%T", im))

		im.ColorModel().Convert(color.White)
		_ = color.White
		Log.Dbg(im.ColorModel(), fmt.Sprintf("%T", im))
		//im.ColorModel().Convert(color.Gray16Model)
	*/

	//fmt.Printf("xxx im.type -> %T\n", im);

	gray := false   // indexed 8 bit
	gray16 := false // 16 bit gray (single channel)
	//switch im.(type) {
	switch im.ColorModel() {
	//case image.NRGBA:
	case color.NRGBAModel:
		data = im.(*image.NRGBA).Pix
		//gray = true
		//pp("derp")
	//case image.RGBA:
	case color.RGBAModel:
		data = im.(*image.RGBA).Pix
	//case image.Gray:
	case color.GrayModel:
		gray = true
	//case image.Gray16:
	case color.Gray16Model:
		gray16 = true
		//pp("derp")
	case color.YCbCrModel:
		// XXX FIXME?
		im2 := image.NewNRGBA(im.Bounds())
		draw.Draw(im2, im.Bounds(), im, image.Pt(0, 0), draw.Src)
		data = im2.Pix
	/*
		//case image.Paletted:
		case color.Paletted:
			gray = true // fixme?
	*/
	default:
		Log.Err("image type not supported:", im.ColorModel())
		panic(im)
	}

	// Convert gray image to rgba
	if gray {
		//pp(path)
		// rgb
		image_data := make([]uint8, ix*iy*4)

		// invert y-coord
		//for y := iy - 1; y >= 0; y-- {
		//	for x := 0; x < ix; x++ {

		idx := 0
		for y := 0; y < iy; y++ {
			for x := 0; x < ix; x++ {
				//println(x, y)
				r, g, b, a := im.At(x, y).RGBA()
				image_data[idx] = uint8(r)
				image_data[idx+1] = uint8(g)
				image_data[idx+2] = uint8(b)
				image_data[idx+3] = uint8(a) //uint8(255) //uint8(a)
				idx += 4
				_ = a
			}
		}

		data = image_data
	}

	if gray16 {
		//pp(2)
		image_data := make([]float32, ix*iy)

		idx := 0
		for y := 0; y < iy; y++ {
			for x := 0; x < ix; x++ {
				//println(x, y)
				y := im.(*image.Gray16).Gray16At(x, y).Y
				//p("y:", y, float32(y) / 0xffff)
				image_data[idx] = float32(y) / 0xffff
				idx += 1
			}
		}

		data = image_data
	}

	imData := &ImageData{
		Data: data,
		W:    ix, H: iy,
	}

	// store resource
	r := NewResource()
	r.rtype = ResourceTypeImage
	r.id = fmt.Sprintf("image#%d", t.tex)
	r.path = path
	r.data = imData
	rxi.ResourceSys.AddResource(r)

	//return image_data, ix, iy
	return imData
}

//`

var _ = `
func (t *Texture) load(path string, flip bool) {
	rs := rxi.ResourceSys
	r := rs.AddResourceFromFile(path) // XXX fixme: remove

	cc := glfw.GetCurrentContext()
	fmt.Println("context", cc)

	// TODO: DDS flipping code?
	isDDS := strings.HasSuffix(path, ".dds")

	if !isDDS {

		im := r.data.(image.Image)
		ix, iy := im.Bounds().Dx(), im.Bounds().Dy()

		// NOTE:
		//  instead of flipping colladaloader flips y-texcoord of models.
		//  [deprecated] same can be done in fullscreen shaders for gl_TexCoord[0] for quads. [/deprecated]
		//  currently not flipping textures by default here.
		//
		// see also:
		//  https://www.opengl.org/wiki/Common_Mistakes#y-axis
		//  https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_06

		if flip {
			// flip the image for proper opengl textures
			im = imaging.FlipV(im)
		}

		//image_data := im.Pix
		var image_data []uint8

		//panic(ix*iy*3)
		//panic(len(im.Pix))

		image_data = make([]uint8, ix*iy*4)

		// need flipping on one axis? (flipV)
		idx := 0
		var _ = idx
		//idx2 := len(im.Pix)-1
		/*
		   for idx2 >= 0 {
		     //println(idx2)
		     image_data[idx] = im.Pix[idx2-3]
		     image_data[idx+1] = im.Pix[idx2-2]
		     image_data[idx+2] = im.Pix[idx2-1]
		     idx += 3
		     idx2 -= 4
		   }
		*/

		/*
		   // slow flip
		   for y := iy-1; y >= 0; y-- {
		     for x := 0; x < ix; x++ {
		       //println(x, y)
		       r, g, b, _ := im.At(x, y).RGBA()
		       image_data[idx] = uint8(r)
		       image_data[idx+1] = uint8(g)
		       image_data[idx+2] = uint8(b)
		       idx += 3
		     }
		   }
		*/

		gray := false // indexed 8 bit
		switch im.(type) {
		case *image.NRGBA:
			//image_data = im.(*image.NRGBA).Pix
			gray = true
		case *image.RGBA:
			image_data = im.(*image.RGBA).Pix
		case *image.Gray:
			gray = true
		case *image.Paletted:
			gray = true // fixme?
		default:
			Log.Err("image type not supported:", im.ColorModel())
			panic(im)
		}

		// Convert gray image to rgba
		if gray {
			//pp(path)
			// rgb
			image_data = make([]uint8, ix*iy*4)

			// invert y-coord
			//for y := iy - 1; y >= 0; y-- {
			//	for x := 0; x < ix; x++ {

			for y := 0; y < iy; y++ {
				for x := 0; x < ix; x++ {
					//println(x, y)
					r, g, b, a := im.At(x, y).RGBA()
					image_data[idx] = uint8(r)
					image_data[idx+1] = uint8(g)
					image_data[idx+2] = uint8(b)
					image_data[idx+3] = uint8(a)
					idx += 4
				}
			}
		}

		gl.GenTextures(1, &t.tex)
		gl.BindTexture(gl.TEXTURE_2D, t.tex)
		gl.PixelStorei(gl.UNPACK_ALIGNMENT, 1)

		// XXX duplicate: remove?
		if t.mipmaps {
			gl.TexParameteri(gl.TEXTURE_2D, gl.GENERATE_MIPMAP, gl.TRUE)
			t.min_filter = gl.LINEAR_MIPMAP_LINEAR
		}

		/*
			if !gray {
				gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA8, int32(ix), int32(iy), 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(image_data))
			} else {
				gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGB8, int32(ix), int32(iy), 0, gl.RGB, gl.UNSIGNED_BYTE, gl.Ptr(image_data))
			}
		*/
		// use RGBA (32bit) from all textures
		gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA8, int32(ix), int32(iy), 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(image_data))

	} else {
		// dds texture

		var err error
		t.tex, err = TextureFromDDS(path)
		if err != nil {
			panic(err)
		}
		glcheck()
	}
	/*
	   if t.mipmaps {
	       gl.TexParameteri(gl.TEXTURE_2D, gl.GENERATE_MIPMAP, gl.TRUE)
	       t.min_filter = gl.LINEAR_MIPMAP_LINEAR
	   }
	*/
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, t.min_filter)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, t.mag_filter)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT)

	if t.anisotropy {
		/*
			if !checkGLExt("GL_EXT_texture_filter_anisotropic") {
				Log.Dbg("anisotropic textures not supported")
				return
			}
		*/
		var anisotropy float32
		gl.GetFloatv(MAX_TEXTURE_MAX_ANISOTROPY_EXT, &anisotropy)
		maxAniso := math.Min(float64(conf.max_anisotropy), float64(anisotropy))
		gl.TexParameterf(gl.TEXTURE_2D, TEXTURE_MAX_ANISOTROPY_EXT, float32(maxAniso))
		//gl.TexParameterf(gl.TEXTURE_2D, TEXTURE_MAX_ANISOTROPY_EXT, float32(-100))
		//println("maxAniso:", maxAniso)
		glcheck()
	}

	// update width, height
	var w, h int32
	gl.GetTexLevelParameteriv(gl.TEXTURE_2D, 0, gl.TEXTURE_WIDTH, &w)
	gl.GetTexLevelParameteriv(gl.TEXTURE_2D, 0, gl.TEXTURE_HEIGHT, &h)
	t.width, t.height = int(w), int(h)

	gl.BindTexture(gl.TEXTURE_2D, 0)

	// resource reusing
	r = NewResource()
	r.rtype = ResourceTypeTexture
	r.id = fmt.Sprintf("texture#%d", t.tex)
	r.path = path
	r.data = t
	rs.AddResource(r)

	gl.Flush()
}
`

func (t *Texture) SetTextureDataFloat32(data []float64) {

}

func (t *Texture) SetTextureData(data interface{}) {
	t.Bind()
	switch t.format {
	case TextureFormatRGBA:
		d := data.([]uint8)
		gl.TexSubImage2D(gl.TEXTURE_2D, 0, 0, 0, int32(t.width), int32(t.height), gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(&d[0]))
		glcheck()
	case TextureFormatFloat32:
		df := data.([]float32)
		d := df
		/*
			d := make([]byte, len(df)*4)
			j := 0
			for _, f := range df {
				bits := math.Float32bits(f)
				bytes := make([]byte, 4)
				binary.LittleEndian.PutUint32(bytes, bits)
				d[j] = bytes[0]
				d[j+1] = bytes[1]
				d[j+2] = bytes[2]
				d[j+3] = bytes[3]
				j += 4
			}
		*/
		glcheck()
		p(t.width, t.height, len(d))

		//gl.TexSubImage2D(gl.TEXTURE_2D, 0, 0, 0, int32(t.width), int32(t.height), gl.RED, gl.FLOAT, gl.Ptr(&d[0]))
		gl.TexSubImage2D(gl.TEXTURE_2D, 0, gl.R32F, 0, int32(t.width), int32(t.height), gl.RED, gl.FLOAT, gl.Ptr(&d[0]))
		glcheck()
	default:
		panic("unknown format")
	}
	t.Unbind()
	glcheck()
}

func (t *Texture) GetImageData() *ImageData {
	iw, ih := t.Size()
	var raw_img []uint8 = make([]uint8, iw*ih*4)

	//gl.Disable(gl.BLEND)
	t.Bind()
	gl.GetTexImage(gl.TEXTURE_2D, 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(raw_img))
	t.Unbind()
	//gl.Enable(gl.BLEND)

	imData := &ImageData{
		Data: raw_img,
		W:    iw, H: ih,
	}

	return imData
}

func (t *Texture) Save(path string) {
	// fixme: doesn't work with depth textures
	Log.Inf("Saving texture to path: ", path)
	iw, ih := t.Size()
	var raw_img []uint8 = make([]uint8, iw*ih*4)

	//gl.Disable(gl.BLEND)
	t.Bind()
	gl.GetTexImage(gl.TEXTURE_2D, 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(raw_img))
	t.Unbind()
	//gl.Enable(gl.BLEND)
	glcheck()

	im := image.NewNRGBA(image.Rect(0, 0, iw, ih))
	im.Pix = raw_img
	// flipY
	im = imaging.FlipV(im)

	of, err := os.Create(path)
	defer of.Close()
	if err != nil {
		Log.Err("can't save image to path; path: ", path)
		panic(err)
	}
	// disable compression
	enc := &png.Encoder{CompressionLevel: -1}
	enc.Encode(of, im)
}

func (t *Texture) Bind() {
	gl.ActiveTexture(gl.TEXTURE0) // fixme: remove?
	gl.BindTexture(gl.TEXTURE_2D, t.tex)
	//t.tex_obj.Bind(gl.TEXTURE_2D)
}

func (t *Texture) Unbind() {
	gl.BindTexture(gl.TEXTURE_2D, 0)
	//t.tex_obj.Unbind(gl.TEXTURE_2D)
}

func (t *Texture) Free() {
	dbg("Texture.Free")
	//t.tex_obj.Delete()
	gl.DeleteTextures(1, &t.tex)
}

func loadImage(path string) image.Image {
	var im image.Image
	var err error
	rr, err := os.Open(path)
	if err != nil {
		println("cant read file: " + path)
		log.Fatal(err)
	}
	defer rr.Close()

	ext := _path.Ext(path)
	if ext == ".png" {
		im, _, err = image.Decode(rr)
		if err != nil {
			panic(err)
		}
	} else {
		//pp(jpeg.SupportRGBA())

		im, _, err = image.Decode(rr)
		if err != nil {
			panic(err)
		}

		/*
			im, err = jpeg.Decode(rr, &jpeg.DecoderOptions{
				DCTMethod:              jpeg.DCTFloat,
				DisableFancyUpsampling: true})
			if err != nil {
				panic(err)
			}
		*/

		/*
			im, err = golibjpegturbo.Decode(rr)
			if err != nil {
				panic(err)
			}
		*/
	}
	/*
	  // fixme: slow, remove
	  im2 := image.NewNRGBA(im.Bounds())
	  draw.Draw(im2, im.Bounds(), im, image.Pt(0, 0), draw.Src)
	  //image_data := im2.Pix
	  return im2
	*/
	return im
}
