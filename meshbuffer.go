package rx

import (
	. "bitbucket.org/coaljoe/rx/math"

	"github.com/go-gl/gl/v2.1/gl"
)

type MeshBufferType int

const (
	MeshBufferType_Static MeshBufferType = iota
	MeshBufferType_Dynamic
	//MeshBufferType_Streaming
)

type MeshBuffer struct {
	positions []float32
	normals   []float32
	texcoords []float32
	indices   []uint32
	hasPositions,
	hasNormals,
	hasTexCoords,
	hasIndices bool
	mbType MeshBufferType
	// gl
	vBuf,
	nBuf,
	tBuf,
	iBuf uint32
}

func (mb *MeshBuffer) TexCoords() []float32 { return mb.texcoords }

func NewMeshBuffer() (t *MeshBuffer) {
	t = &MeshBuffer{
		positions: make([]float32, 0),
		normals:   make([]float32, 0),
		texcoords: make([]float32, 0),
		indices:   make([]uint32, 0),
		mbType:    MeshBufferType_Static,
	}
	return
}

func (mb *MeshBuffer) SetMeshBufferType(mbType MeshBufferType) {
	mb.mbType = mbType
}

func (mb *MeshBuffer) Add(positions, normals, texcoords []float32, indices []uint32) {
	mb.positions = positions
	mb.normals = normals
	mb.texcoords = texcoords
	mb.indices = indices
	mb.build()
}

func (mb *MeshBuffer) Append(positions, normals, texcoords []float32, indices []uint32) {
	mb.positions = append(mb.positions, positions...)
	mb.normals = append(mb.normals, normals...)
	mb.texcoords = append(mb.texcoords, texcoords...)
	mb.indices = append(mb.indices, indices...)
	//mb.build()
}

func (mb *MeshBuffer) ForceBuild() {
	mb.build()
}

func (mb *MeshBuffer) build() {
	//pp(2)

	// check before for previous errrors
	glcheck()

	sizeGLfloat := 4
	sizeGLint := 4
	if len(mb.positions) > 0 {

		// seems doesn't work properly
		var _ = `
		// check max number of vertices per vbo
		var maxVertices int32
		gl.GetIntegerv(gl.MAX_ELEMENTS_VERTICES, &maxVertices)

		/*
		println(maxVertices)
		panic(2)
		*/

		if len(mb.positions) > int(maxVertices) {
			Log.Dbg("WARNING?: more vertices than VBO can handle: ", len(mb.positions), 
				  "; max vertices: ", maxVertices)
			//panic("exit")
			if maxVertices == 0 {
				panic("fixme: exit")
			}
		}
		`

		mb.hasPositions = true
		gl.GenBuffers(1, &mb.vBuf)
		glcheck()
		gl.BindBuffer(gl.ARRAY_BUFFER, mb.vBuf)
		glcheck()
		gl.BufferData(gl.ARRAY_BUFFER, len(mb.positions)*sizeGLfloat, gl.Ptr(mb.positions), gl.STATIC_DRAW)
		glcheck()
	}
	if len(mb.normals) > 0 {
		mb.hasNormals = true
		gl.GenBuffers(1, &mb.nBuf)
		gl.BindBuffer(gl.ARRAY_BUFFER, mb.nBuf)
		gl.BufferData(gl.ARRAY_BUFFER, len(mb.normals)*sizeGLfloat, gl.Ptr(mb.normals), gl.STATIC_DRAW)
	}
	if len(mb.texcoords) > 0 {
		mb.hasTexCoords = true
		gl.GenBuffers(1, &mb.tBuf)
		gl.BindBuffer(gl.ARRAY_BUFFER, mb.tBuf)
		gl.BufferData(gl.ARRAY_BUFFER, len(mb.texcoords)*sizeGLfloat, gl.Ptr(mb.texcoords), gl.STATIC_DRAW)
	}
	if len(mb.indices) > 0 {
		mb.hasIndices = true
		gl.GenBuffers(1, &mb.iBuf)
		gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, mb.iBuf)
		gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(mb.indices)*sizeGLint, gl.Ptr(mb.indices), gl.STATIC_DRAW)
	}

	glcheck()

	// unbind buffers
	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, 0)
}

func (mb *MeshBuffer) Render(r *Renderer) {
	if !mb.hasPositions {
		panic("no positions")
	}

	mat := r.curMaterial
	if mat == nil {
		panic("current material cannot be nil")
	}

	glcheck()

	sh := mat.shader
	attrVPosition := sh.GetAttribLocation("v_position")
	attrVNormal := sh.GetAttribLocation("v_normal")
	var attrVTexCoord int32
	if mb.hasTexCoords {
		attrVTexCoord = sh.GetAttribLocation("v_texCoord")
	}

	gl.EnableVertexAttribArray(uint32(attrVPosition))
	gl.EnableVertexAttribArray(uint32(attrVNormal))
	if mb.hasTexCoords {
		gl.EnableVertexAttribArray(uint32(attrVTexCoord))
	}
	//sh.saveShaderSource()
	//pp(attrVPosition)
	glcheck()

	gl.BindBuffer(gl.ARRAY_BUFFER, mb.vBuf)
	gl.VertexAttribPointer(
		uint32(attrVPosition),
		3,        // Number of elements per vertex
		gl.FLOAT, // Element type
		false,
		0,
		nil)

	gl.BindBuffer(gl.ARRAY_BUFFER, mb.nBuf)
	gl.VertexAttribPointer(
		uint32(attrVNormal),
		3,        // Number of elements per vertex
		gl.FLOAT, // Element type
		false,
		0,
		nil)

	/*
		if mb.hasNormals {
			gl.BindBuffer(gl.ARRAY_BUFFER, mb.nBuf)
			gl.NormalPointer(gl.FLOAT, 0, nil)
		}
	*/
	if mb.hasTexCoords {
		gl.BindBuffer(gl.ARRAY_BUFFER, mb.tBuf)
		gl.VertexAttribPointer(
			uint32(attrVTexCoord),
			2,        // Number of elements per vertex
			gl.FLOAT, // Element type
			false,
			0,
			nil)
	}

	if mb.hasIndices {
		gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, mb.iBuf)
		//gl.DrawElements(gl.TRIANGLES, int32(len(mb.indices)*3), gl.UNSIGNED_INT, nil)
		gl.DrawElements(gl.TRIANGLES, int32(len(mb.indices)), gl.UNSIGNED_INT, gl.PtrOffset(0))
		//gl.DrawElements(gl.TRIANGLES, int32(len(mb.indices)), gl.UNSIGNED_INT, nil)
		// can be UNSIGNED_SHORT
	} else {
		gl.DrawArrays(gl.TRIANGLES, int32(0), int32(len(mb.positions)/3))
		//gl.DrawArrays(gl.TRIANGLES, int32(0), 1)
		//gl.DrawArrays(gl.TRIANGLE_STRIP, int32(0), int32(len(mb.positions)/4))
	}

	/*
		if mb.hasNormals {
			gl.DisableClientState(gl.NORMAL_ARRAY)
		}
	*/
	if mb.hasTexCoords {
		gl.DisableVertexAttribArray(uint32(attrVTexCoord))
	}
	gl.DisableVertexAttribArray(uint32(attrVNormal))
	gl.DisableVertexAttribArray(uint32(attrVPosition))

	glcheck()

	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, 0)
}

func (mb *MeshBuffer) GetVertices() []Vec3 {
	size := len(mb.positions) / 3
	r := make([]Vec3, size)
	j := 0
	for i := 0; i < size; i++ {
		r[i] = Vec3{float64(mb.positions[j]), float64(mb.positions[j+1]), float64(mb.positions[j+2])}
		j += 3
		/*
			if j > 10 {
				break
			}
		*/
	}
	/*
		p(mb.positions)
		p(len(mb.positions), len(r), size)
		p("----")
		pp(r)
	*/
	return r
}

func (mb *MeshBuffer) GetNormals() []Vec3 {
	size := len(mb.normals) / 3
	r := make([]Vec3, size)
	j := 0
	for i := 0; i < size; i++ {
		r[i] = Vec3{float64(mb.normals[j]), float64(mb.normals[j+1]), float64(mb.normals[j+2])}
		j += 3
	}
	return r
}

func (mb *MeshBuffer) GetTexCoords() []Vec2 {
	size := len(mb.texcoords) / 2
	r := make([]Vec2, size)
	j := 0
	for i := 0; i < size; i++ {
		r[i] = Vec2{float64(mb.texcoords[j]), float64(mb.texcoords[j+1])}
		j += 2
	}
	return r
}

func (mb *MeshBuffer) Release() {
	//dbg("mb.Release")
	mb.positions = nil
	mb.normals = nil
	mb.texcoords = nil
	mb.indices = nil
	bufs := [4]uint32{mb.vBuf, mb.nBuf, mb.tBuf, mb.iBuf}
	gl.DeleteBuffers(4, &bufs[0])
}
