package rx

type RenderableI interface {
	Render(r *Renderer)
}

type RenderableNodeI interface {
	RenderableI
	NodeI
}

/*
type Renderable struct {

}
*/
