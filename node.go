package rx

import (
	"fmt"
	"strconv"
	"strings"
  "bitbucket.org/coaljoe/rx/transform"
)

type NodeI interface {
	//Name() string
	//SetName(s string)
	GetNode() *Node
	//Type() NodeType
}

// XXX: rename to BaseNode?
// Base of any element of the scene.
type Node struct {
	*transform.Transform
	id   int
	name string
	// NodeType marking
	nodetype    NodeType
	renderlayer RenderLayer
	visible     bool
	Mesh        *Mesh
	Light       *Light
	Camera      *Camera
	// XXX overshadowing Transform's parent?
	parent       *Node
	children     []*Node
	culling      bool
	clonedFromId int
	// A custom content
	content interface{}
	tags    map[string]string
}

func (n *Node) GetNode() *Node             { return n }
func (n *Node) Name() string               { return n.name }
func (n *Node) Id() int                    { return n.id }
func (n *Node) Content() interface{}       { return n.content }
func (n *Node) SetContent(ctx interface{}) { n.content = ctx }
func (n *Node) Visible() bool              { return n.visible }
func (n *Node) SetVisible(v bool)          { n.visible = v }
func (n *Node) ToggleVisible()             { n.visible = !n.visible }
func (n *Node) SetParent(p *Node)          { n.parent = p }
func (n *Node) Parent() *Node              { return n.parent }
func (n *Node) Children() []*Node          { return n.children }
func (n *Node) Culling() bool              { return n.culling }
func (n *Node) SetCulling(v bool)          { n.culling = v }

/*
func NewNode(nodetype NodeType) *Node {
	return &Node{
		Transform:   transform.NewTransform(),
		id:          _ider.GetNextId("maxNodeId"),
		name:        "unnamed",
		nodetype:    nodetype,
		renderlayer: RenderLayer_Default,
		visible:     true,
		culling:     true,
		clonedFromId: -1,
		tags:        make(map[string]string, 0),
	}
}
*/
func NewNode(nodetype NodeType) *Node {
	n := &Node{}
	n.setDefaults()
	n.nodetype = nodetype
	n.id = _ider.GetNextId("maxNodeId")
	Log.Inff("[Node] new node was created: name: %s,  id: %d, addr: %p", n.name, n.id, n)
	return n
}

func (n *Node) setDefaults() {
	n.Transform = transform.NewTransform()
	n.id = -1
	n.name = "unnamed"
	//n.nodetype = NodeType_Unknown
	n.renderlayer = RenderLayer_Default
	n.visible = true
	n.culling = true
	n.clonedFromId = -1
	n.tags = make(map[string]string, 0)
	//n.SetName(n.name)
}

func (n *Node) OnDeserialize() {

}

func (n *Node) clone() *Node {
	z := *n
	z.Transform = n.Transform.Clone()
	// Fix name
	//z.name = z.name + "#"
	z.fixDuplicateNames()
	// Fix id
	z.clonedFromId = z.id
	z.id = _ider.GetNextId("maxNodeId")
	return &(z)
}

func (n *Node) Clone() *Node {
	var ret *Node
	switch n.Type() {
	case NodeType_Mesh:
		ret = cloneMeshNode(n)
	default:
		pp("don't know how to clone node type:", n.Type())
	}
	return ret
}

func (n *Node) SetRenderLayer(rl RenderLayer) {
	n.renderlayer = rl
}

/*
func (n *Node) Transform() *Transform {
  return n.Transform
}
*/

func (n *Node) SetName(name string) {
	Log.Inf("[Node] SetName; name:", name)
	//return
	n.name = name

	//p(rxi.scene.HasNodeName(name))
	//p(rxi.scene.GetNodes())
	//pp(2)

	// Try fix duplicate names
	n.fixDuplicateNames()
}

func (n *Node) fixDuplicateNames() {
	s := n.name
	name := n.name
	// fix dublicated names
	for {
		//p("1")
		found := rxi.scene.HasNodeName(name)
		//p("2")
		if !found {
			if name != s {
				Log.Inf("renaming node " + s + " to " + name)
			}
			break
		} else {
			//postfix := ""
			/*
				idx, err := strconv.Atoi()
				if err != nil {
					panic(err)
				}
				if idx != -1 {
					continue
				}
			*/
			renamed := false
			a := strings.Split(name, "#")
			if len(a) > 1 {
				postfix := a[len(a)-1]
				num, err := strconv.Atoi(postfix)
				if err != nil {
					p("postfix:", postfix)
					p("a:", a)
					panic(err)
				}
				name = a[0] + "#" + strconv.Itoa(num+1)
				//p(name)
				//pp(2)
			} else {
				name = name + "#2"
				//p(name)
				//pp(2)
				//renamed = true
			}
			if renamed {
				Log.Inf("renaming node " + s + " to " + name)
				break
			}
		}
	}
	n.name = name
}

func (n *Node) HasTag(name string) bool {
	return n.tags[name] != ""
}

func (n *Node) GetTag(name string) string {
	if !n.HasTag(name) {
		panic("no such tag; name: " + name)
	}
	return n.tags[name]
}

func (n *Node) AddTag(name, value string) {
	if n.HasTag(name) {
		panic("already have tag; name: " + name)
	}
	n.tags[name] = value
}

func (n *Node) RemoveTag(name string) {
	if !n.HasTag(name) {
		panic("no such tag; name: " + name)
	}
	delete(n.tags, name)
}

func (n *Node) Type() NodeType {
	return n.nodetype
}

func (n *Node) String() string {
	return fmt.Sprintf("Node<\"%s\" id=%d>", n.name, n.id)
}

/**** MeshNode ****/

func NewMeshNode() *Node {
	n := NewNode(NodeType_Mesh)
	n.Mesh = NewMesh(n)
	//mn.SetContent(mn)
	return n
}

func cloneMeshNode(n *Node) *Node {
	/*
		n := mn.Node.clone()
		m := mn.Mesh.clone()
		return &MeshNode{
			Node: n,
			Mesh: m,
		}
	*/
	nm := n.Mesh.clone()
	nn := n.clone()
	nn.Mesh = nm
	return nn
}

/**** CameraNode ****/

func NewCameraNode() *Node {
	if Log != nil {
		Log.Inf("new camera node")
	}
	n := NewNode(NodeType_Camera)
	n.Camera = NewCamera(n)
	return n
}

/**** LightNode ****/

func NewLightNode() *Node {
	if Log != nil {
		Log.Inf("new light node") // fixme
	}
	n := NewNode(NodeType_Light)
	n.Light = NewLight(n)
	return n
}

/*
type EmptyNode struct {
  *Node
}

func NewEmptyNode() *EmptyNode {
  return &EmptyNode{
    Node: NewNode(),
  }
}

func (en *EmptyNode) Free() {
  return
}
*/

/**** StaticGeometryNode ****/

func NewStaticGeometryNode() *Node {
	//n := NewNode(NodeType_Custom)
	//n := NewNode(NodeType_StaticGeometry)
	n := NewNode(NodeType_Mesh)
	n.Mesh = NewMesh(n)
	n.Mesh.meshType = MeshType_StaticGeometry
	n.Mesh.StaticGeometry = NewStaticGeometry(n.Mesh)
	fmt.Printf("n -> %p\n", n)
	fmt.Printf("n.Mesh -> %p\n", n.Mesh)
	fmt.Printf("n.Mesh.StaticGeometry -> %p\n", n.Mesh.StaticGeometry)
	//mn.SetContent(mn)
	return n
}

var _ = `
func cloneStaticGeometryNode(n *Node) *Node {
	/*
		n := mn.Node.clone()
		m := mn.Mesh.clone()
		return &MeshNode{
			Node: n,
			Mesh: m,
		}
	*/
	nsg := n.StaticGeometry.clone()
	nn := n.clone()
	nn.StaticGeometry = nsg
	return nn
}
`
