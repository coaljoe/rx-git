package rx

import . "bitbucket.org/coaljoe/rx/math"

// LightNode
type Light struct {
	node                     *Node
	castShadows, onlyShadows bool
	diffuse, specular        Vec3
	attenuation              float64
	intensity                float64
}

func NewLight(node *Node) *Light {
	return &Light{
		node:        node,
		castShadows: false,
		onlyShadows: false,
		diffuse:     Vec3One,
		specular:    Vec3One,
		attenuation: 0.0,
		intensity:   1.0,
	}
}

func (l *Light) SetIntensity(v float64) {
	l.intensity = v
}
