package rx

import (
	"fmt"
	//"gopkg.in/fatih/set.v0"
	//"github.com/emirpasic/gods/sets/hashset"
	"github.com/emirpasic/gods/lists/arraylist"
	//"container/list"
)

type hasName interface {
	Name() string
}

type System struct {
	name  string
	elems *arraylist.List
}

func newSystem(name string) *System {
	s := &System{
		name:  name,
		elems: arraylist.New(),
	}
	return s
}

func (s *System) hasElem(e interface{}) bool {
	return s.elems.Contains(e)
}

func (s *System) addElem(e interface{}) {
	if s.hasElem(e) {
		pp("already have element", e)
	}
	//*s.elems.PushBack(e)
	//s.elems.PushBack(e)
	//s.elems[e] = true
	s.elems.Put(e)
}

func (s *System) removeElem(e interface{}) {
	if !s.hasElem(e) {
		dbg("no such element", e)
	}
	//s.elems.Remove(e)
}

func (s *System) Elems() []interface{} {
	/*
		keys := make([]interface{}, len(s.elems))
		n := 0
		for el, _ := range s.elems {
			//keys = append(keys, (interface{}(el)))
			//keys[n] = (interface{}(*el))
			//*keys = append(*keys, el)
			keys = append(keys, el)
			fmt.Println(el)
			n++
		}
		fmt.Println(keys)
		return keys
	*/
	return *s.elems
}

/*
func (s *System) Elems() []interface{} {
	keys := make([]interface{}, s.elems.Len())
	x := s.elems
	for el := x.Front(); el != nil; el = el.Next() {
		fmt.Println(el.Value.(int))
		v := el.Value
		keys = append(keys, v)
	}
	fmt.Println(keys)
	return keys
}
*/

func (s *System) ListElems() {
	fmt.Println("list elements:")
	n := 0
	for _, el := range s.Elems() {
		//for el := range s.elems.List() {
		//for el, _ := range s.elems {
		name := "unknown"
		if el.(hasName) != nil {
			name = el.(hasName).Name()
		}
		fmt.Printf(" -> %d: %s\n", n, name)
		n++
	}
	fmt.Println("end")
}
