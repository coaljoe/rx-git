package main

import (
	"fmt"
	"reflect"
)

type Container []interface{}

func (c *Container) Put(elem interface{}) {
	*c = append(*c, elem)
}

// return first
func (c *Container) Get() interface{} {
	elem := (*c)[0]
	*c = (*c)[1:]
	return elem
}

type hasName interface {
	Name() string
}

type Z struct {
	v int
	v1 string
}

func (z *Z) Name() string {
	return z.v1
}

func main() {
	intContainer := &Container{}
	//intContainer.Put("string")
	intContainer.Put(7)
	intContainer.Put(42)
	//intContainer.Put("string")

	
	elem, ok := intContainer.Get().(int) // assert that the actual type is int
	if !ok {
		fmt.Println("Unable to read an int from intContainer")
	}
	fmt.Printf("assertExample: %d (%T)\n", elem, elem)
	
	// pop
	_ = intContainer.Get()
	
	z := &Z{v: 11, v1: "TEST"}
	fmt.Println(z.v)
	fmt.Println(reflect.TypeOf(z))
	//var zi interface{}
	//zi = z
	//zv := (*z)
	//fmt.Println(reflect.TypeOf(zv))
	//intContainer.Put(zv)
	//intContainer.Put(zi)
	intContainer.Put(z)
	

	//i := (*Z)(intContainer.Get().(*Z))
	//i := intContainer.Get().(*Z)
	
	ii := intContainer.Get()
	fmt.Println(ii.(hasName).Name())
	i := ii.(*Z)
	
	fmt.Println(reflect.TypeOf(i))
	fmt.Println(i.v)
        i.v = 12
	fmt.Println(i.v)
	fmt.Println(z.v)

/*
	// ref version		
	z := &Z{v: 11, v1: "test"}
	fmt.Println(z.v)
	fmt.Println(reflect.TypeOf(z))
	//var zi interface{}
	//zi = z
	//zv := (*z)
	//fmt.Println(reflect.TypeOf(zv))
	//intContainer.Put(zv)
	//intContainer.Put(zi)
	intContainer.Put(z)

	i := (*Z)(intContainer.Get().(*Z))
	fmt.Println(reflect.TypeOf(i))
	fmt.Println(i.v)
        i.v = 12
	fmt.Println(i.v)
	fmt.Println(z.v)
*/	
	
/*	
	// value version
	z := &Z{v: 11, v1: "test"}
	fmt.Println(z.v)
	fmt.Println(reflect.TypeOf(z))
	//var zi interface{}
	//zi = z
	zv := (*z)
	fmt.Println(reflect.TypeOf(zv))
	intContainer.Put(zv)
	//intContainer.Put(zi)
	//intContainer.Put(z)

	i := (Z)(intContainer.Get().(Z))
	fmt.Println(reflect.TypeOf(i))
	fmt.Println(i.v)
        i.v = 12
	fmt.Println(i.v)
	fmt.Println(z.v)
*/	
	/*
	//elem1, ok1 := intContainer.Get().((*Z)) // assert that the actual type is in
	if !ok1 {
		fmt.Println("Unable to read an int from intContainer")
		panic(2)
	}
	fmt.Printf("assertExample: %d (%T)\n", elem1, elem1)
	fmt.Println(elem1.v)
	//fmt.Println((*elem1).v)
	*/
}

