@echo off
setlocal
set DEBUG=1
set LOGLEVEL=DEBUG

@rem set mingw env
rem set PATH=C:\users\k\k\progs\mingw-w64\x86_64-6.1.0-posix-seh-rt_v5-rev0\mingw64\bin;%PATH%
if exist C:\Program Files\mingw-w64\x86_64-5.3.0-posix-seh-rt_v4-rev0\mingw64\bin (
  set PATH=C:\Program Files\mingw-w64\x86_64-5.3.0-posix-seh-rt_v4-rev0\mingw64\bin;%PATH%
) else if exist C:\users\k\k\progs\mingw-w64\x86_64-6.2.0-posix-seh-rt_v5-rev1\mingw64\binz (
  set PATH=C:\users\k\k\progs\mingw-w64\x86_64-6.2.0-posix-seh-rt_v5-rev1\mingw64\bin;%PATH%
)

@rem set apitrace path
set PATH=C:\Users\k\k\progs\apitrace\bin;%PATH%

go clean -i rx

rem go run -x -v test/main_rx.go

if "%1" == "-trace" (
  goto trace
) else (
  goto run
)

:trace
shift
rem fixme, shift doesn't work with %*
rem echo %1 %2 %3 %4 %5
apitrace trace -o trace.trace go run -v %1 %2 %3 %4 %5
goto end

:run
go run -v %*

:end
endlocal