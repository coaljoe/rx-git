package collada

import "encoding/xml"

type Collada struct {
	XMLName               xml.Name `xml:"COLLADA"`
	Version               string   `xml:"version,attr"`
	Library_Geometries    LibraryGeometries
	Library_Visual_Scenes LibraryVisualScenes
	Library_Materials     LibraryMaterials
	Library_Effects       LibraryEffects
	Library_Images        LibraryImages
}

type LibraryGeometries struct {
	XMLName      xml.Name   `xml:"library_geometries"`
	GeometryList []Geometry `xml:"geometry"`
}

type Geometry struct {
	XMLName xml.Name `xml:"geometry"`
	Id      string   `xml:"id,attr"`
	Name    string   `xml:"name,attr"`
	Mesh    Mesh     `xml:"mesh"`
}

type Mesh struct {
	XMLName  xml.Name `xml:"mesh"`
	Source   []Source `xml:"source"`
	Polylist Polylist `xml:polylist`
	Triangles Triangles `xml:triangles`
}

type Source struct {
	XMLName     xml.Name   `xml:"source"`
	Id          string     `xml:"id,attr"`
	Float_array FloatArray `xml:"float_array"`
}

type FloatArray struct {
	XMLName xml.Name `xml:"float_array"`
	Id      string   `xml:"id,attr"`
	Count   string   `xml:"count,attr"`
	CDATA   string   `xml:",chardata"`
}

type Polylist struct {
	XMLName xml.Name `xml:"polylist"`
	Id      string   `xml:"id,attr"`
	Count   int      `xml:"count,attr"`

	// List of integers, each specifying the number of vertices for one polygon
	VCount string `xml:"vcount"`

	// list of integers that specify the vertex attributes
	P string `xml:"p"`
}

type Triangles struct {
	XMLName xml.Name `xml:"triangles"`
	Id      string   `xml:"id,attr"`
	Count   int      `xml:"count,attr"`

	P string `xml:"p"`
}

type LibraryVisualScenes struct {
	XMLName     xml.Name `xml:"library_visual_scenes"`
	VisualScene VisualScene
}

type VisualScene struct {
	XMLName  xml.Name `xml:"visual_scene"`
	Name     string   `xml:"name,attr"`
	NodeList []Node   `xml:"node"`
}

type Node struct {
	XMLName           xml.Name `xml:"node"`
	Id                string   `xml:"id,attr"`
	Name              string   `xml:"name,attr"`
	Matrix            Matrix
	Instance_geometry InstanceGeometry `xml:,any` // remove ,any?
	Children          []Node           `xml:"node"`
}

type Matrix struct {
	XMLName xml.Name `xml:"matrix"`
	Sid     string   `xml:"sid,attr"`
	CDATA   string   `xml:",chardata"`
}

type InstanceGeometry struct {
	XMLName       xml.Name      `xml:"instance_geometry"`
	Url           string        `xml:"url,attr"`
	Bind_Material Bind_Material `xml:"bind_material"`
}

type Bind_Material struct {
	XMLName          xml.Name         `xml:"bind_material"`
	Technique_Common Technique_Common `xml:"technique_common"`
}

type Technique_Common struct {
	XMLName           xml.Name          `xml:"technique_common"`
	Instance_Material Instance_Material `xml:"instance_material"`
}

type Instance_Material struct {
	XMLName xml.Name `xml:"instance_material"`
	Symbol  string   `xml:"symbol,attr"`
	Target  string   `xml:"target,attr"`
}

// materials

type LibraryMaterials struct {
	XMLName      xml.Name   `xml:"library_materials"`
	MaterialList []Material `xml:"material"`
}

type LibraryEffects struct {
	XMLName    xml.Name `xml:"library_effects"`
	EffectList []Effect `xml:"effect"`
}

type Material struct {
	XMLName         xml.Name `xml:"material"`
	Name            string   `xml:"name,attr"`
	Id              string   `xml:"id,attr"`
	Instance_Effect InstanceEffect
}

type InstanceEffect struct {
	XMLName xml.Name `xml:"instance_effect"`
	Url     string   `xml:"url,attr"`
}

type Effect struct {
	XMLName        xml.Name       `xml:"effect"`
	Id             string         `xml:"id,attr"`
	Profile_COMMON Profile_COMMON `xml:"profile_COMMON"`
}

type Profile_COMMON struct {
	XMLName   xml.Name  `xml:"profile_COMMON"`
	Technique Technique `xml:"technique"`
}

type Technique struct {
	XMLName xml.Name `xml:"technique"`
	Sid     string   `xml:"sid,attr"`
	Phong   Phong    `xml:"phong"`
}

type Phong struct {
	XMLName   xml.Name  `xml:"phong"`
	Emission  Emission  `xml:"emission"`
	Ambient   Ambient   `xml:"ambient"`
	Diffuse   Diffuse   `xml:"diffuse"`
	Specular  Specular  `xml:"specular"`
	Shininess Shininess `xml:"shininess"`
}

type Emission struct {
	XMLName xml.Name `xml:"emission"`
	Color   Color    `xml:"color"`
}

type Ambient struct {
	XMLName xml.Name `xml:"ambient"`
	Color   Color    `xml:"color"`
}

type Diffuse struct {
	XMLName xml.Name `xml:"diffuse"`
	Color   Color    `xml:"color"`
	Texture Texture  `xml:"texture"`
}

type Specular struct {
	XMLName xml.Name `xml:"specular"`
	Color   Color    `xml:"color"`
}

type Color struct {
	XMLName xml.Name `xml:"color"`
	Sid     string   `xml:"sid,attr"`
	CDATA   string   `xml:",chardata"`
}

type Texture struct {
	XMLName  xml.Name `xml:"texture"`
	Texture  string   `xml:"texture,attr"`
	Texcoord string   `xml:"texcoord,attr"`
}

type Shininess struct {
	XMLName xml.Name `xml:"shininess"`
	Sid     string   `xml:"sid,attr"`
	Float   Float    `xml:"float"`
}

type Float struct {
	XMLName xml.Name `xml:"float"`
	Sid     string   `xml:"sid,attr"`
	CDATA   string   `xml:",chardata"`
}

// textures

type LibraryImages struct {
	XMLName   xml.Name `xml:"library_images"`
	ImageList []Image  `xml:"image"`
}

type Image struct {
	XMLName   xml.Name `xml:"image"`
	Id        string   `xml:"id,attr"`
	Name      string   `xml:"name,attr"`
	Init_From Init_From
}

type Init_From struct {
	XMLName xml.Name `xml:"init_from"`
	CDATA   string   `xml:",chardata"`
}
