package transform

import (
	//"math"
	. "bitbucket.org/coaljoe/rx/math"
	"fmt"
	"testing"
)

func TestTransformInitial(tt *testing.T) {
	t := NewTransform()
	m := Mat4{
		{1., 0., 0., 0.},
		{0., 1., 0., 0.},
		{0., 0., 1., 0.},
		{0., 0., 0., 1.}}

	if !t.Mat().Equal(m) {
		tt.Fail()
	}

	// test pos
	if !(t.Pos() == Vec3Zero) {
		tt.Fail()
	}

	// test rot
	if !(t.Rot() == Vec3Zero) {
		tt.Fail()
	}

	// test scale
	if !(t.Scale() == Vec3One) {
		tt.Fail()
	}
}

func TestMoveByVec2(tt *testing.T) {
	t := NewTransform()

	t.SetRot(Vec3{2, 3, 4})
	//t.Translate(Vec3{1, 0, 0})
	t.MoveByVec(Vec3{0, 1, 0})
	fmt.Println("pos:", t.Pos())

	if !t.Pos().ApproxEqual(Vec3{-0.06789, 0.99708, 0.03485}, 0.0001) {
		tt.Fatal("failed")
	}
}

func TestMoveByVec(tt *testing.T) {
	t := NewTransform()

	// rotate by z: 45 degrees and move locally by x: 1

	t.SetRot(Vec3{0, 0, 45})
	//t.Translate(Vec3{1, 0, 0})
	t.MoveByVec(Vec3{1, 0, 0})
	fmt.Println("pos:", t.Pos())

	if !t.Pos().ApproxEqual(Vec3{0.7071, 0.7071, 0}, 0.0001) {
		tt.Error("derp1")
		//tt.FailNow()
		tt.Fatal("failed 1")
	}

	// move back by x: -2

	t.MoveByVec(Vec3{-2, 0, 0})
	fmt.Println("pos:", t.Pos())

	if !t.Pos().ApproxEqual(Vec3{-0.70711, -0.70711, 0}, 0.00001) {
		tt.Error("derp2")
		tt.FailNow()
	}

	// rotate by x: 45 degrees and move locally by z: 1

	rot := t.Rot()
	rot[0] = 45
	t.SetRot(rot)
	fmt.Println("rot:", t.Rot())
	fmt.Println("pos:", t.Pos())
	//t.Rot().SetX(45)
	//t.AdjRot(Vec3{45, 0, 0})
	t.MoveByVec(Vec3{0, 0, 1})
	fmt.Println("rot:", t.Rot())
	fmt.Println("pos:", t.Pos())

	if !t.Pos().ApproxEqual(Vec3{-0.20711, -1.20711, 0.70711}, 0.00001) {
		tt.Fatal("failed 3")
	}

	/*
		if !t.Pos().Equal(Vec3{0.707107, 0.707107, 0}) {
			tt.Fail()
		}
	*/
}

func TestIso(tt *testing.T) {
	// tests with iso.blend

	t := NewTransform()
	// set iso rot
	t.SetRot(Vec3{35.264, 0, 45})
	//t.Translate(Vec3{1, 0, 0})

	// move from center
	t.SetPos(Vec3Zero)
	t.MoveByVec(Vec3{0, 0, 10})
	fmt.Println("pos:", t.Pos())

	if !t.Pos().ApproxEqual(Vec3{4.0824, -4.0824, 8.1650}, 0.0001) {
		tt.Fatal("failed 1")
	}

	// long move
	t.SetPos(Vec3{10, -10, 20})
	t.MoveByVec(Vec3{0, 0, 50})
	fmt.Println("pos:", t.Pos())

	if !t.Pos().ApproxEqual(Vec3{30.4122, -30.4122, 60.8250}, 0.0001) {
		tt.Fatal("failed 2")
	}

	// move local by x: 10
	t.SetPos(Vec3{10, -10, 20})
	t.MoveByVec(Vec3{10, 0, 0})
	fmt.Println("pos:", t.Pos())

	if !t.Pos().ApproxEqual(Vec3{17.0710, -2.9289, 20.0000}, 0.0001) {
		tt.Fatal("failed 3")
	}

	// move local by y: 10
	t.SetPos(Vec3{10, -10, 20})
	t.MoveByVec(Vec3{0, 10, 0})
	fmt.Println("pos:", t.Pos())

	//if !t.Pos().ApproxEqual(Vec3{4.2264, -4.2264, 25.7734}, 0.0001) {
	if !t.Pos().ApproxEqual(Vec3{4.22647, -4.22647, 25.77345}, 0.00001) {
		tt.Fatal("failed 4")
	}

	// move local by z: 10
	t.SetPos(Vec3{10, -10, 20})
	t.MoveByVec(Vec3{0, 0, 10})
	fmt.Println("pos:", t.Pos())

	if !t.Pos().ApproxEqual(Vec3{14.08244, -14.08244, 28.16500}, 0.00001) {
		tt.Fatal("failed 5")
	}

	// move local by x: -10
	t.SetPos(Vec3{10, -10, 20})
	t.MoveByVec(Vec3{-10, 0, 0})
	fmt.Println("pos:", t.Pos())

	if !t.Pos().ApproxEqual(Vec3{2.92893, -17.07107, 20.00000}, 0.00001) {
		tt.Fatal("failed 6")
	}

	// move local by y: -10
	t.SetPos(Vec3{10, -10, 20})
	t.MoveByVec(Vec3{0, -10, 0})
	fmt.Println("pos:", t.Pos())

	if !t.Pos().ApproxEqual(Vec3{15.77353, -15.77353, 14.22655}, 0.00001) {
		tt.Fatal("failed 7")
	}

	// move local by z: -10
	t.SetPos(Vec3{10, -10, 20})
	t.MoveByVec(Vec3{0, 0, -10})
	fmt.Println("pos:", t.Pos())

	if !t.Pos().ApproxEqual(Vec3{5.91756, -5.91756, 11.83499}, 0.00001) {
		tt.Fatal("failed 8")
	}

	//fmt.Println(t)
}

func TestLookAt2d(tt *testing.T) {
	t := NewTransform()

	t.SetRot(Vec3{0, 0, 0})
	t.Translate(Vec3{1, 1, 0})
	t.LookAt2d(Vec3{0, 0, 0})

	// or Vec3{0, 0, 225} // CCW
	if !t.Rot().ApproxEqual(Vec3{0, 0, -135}, 0.0001) {
		fmt.Println("Rot:", t.Rot())
		//tt.Error("")
		tt.FailNow()
		//tt.Fatal("failed 1")
	}

	t.SetPos(Vec3{-1, -1, 0})
	t.SetRot(Vec3{0, 0, 0})
	t.LookAt2d(Vec3{0, 0, 0})

	if !t.Rot().ApproxEqual(Vec3{0, 0, 45}, 0.0001) {
		tt.FailNow()
	}

}

func TestTranslate(tt *testing.T) {
	t := NewTransform()
	if !t.Pos().Equal(Vec3Zero) {
		tt.Fatal()
	}

	// set translation
	t.SetPos(Vec3One)
	if !t.Pos().Equal(Vec3One) {
		tt.Fatal()
	}

	// translate by x
	t.SetPos(Vec3Zero)
	t.Translate(Vec3{1, 0, 0})
	t.Translate(Vec3{1, 0, 0})
	if !t.Pos().Equal(Vec3{2, 0, 0}) {
		tt.Fatal()
	}

	// translate by all axes
	t.SetPos(Vec3Zero)
	t.Translate(Vec3{1, 0, 0})
	t.Translate(Vec3{0, 1, 0})
	t.Translate(Vec3{0, 0, 1})
	t.Translate(Vec3{1, 1, 1})
	if !t.Pos().Equal(Vec3{2, 2, 2}) {
		tt.Fatal()
	}
}

func TestExtra(tt *testing.T) {
	t := NewTransform()
	t.SetPos(Vec3{-12.93624, -46.53374, 12.93307})
	//t.SetPos(Vec3Zero)
	t.SetRot(Vec3{75, -2, -15})
	//t.SetRot(Vec3{75, 0, 0})
	//t.SetRot(Vec3{0, -2, 0})
	//t.SetRot(Vec3{0, 0, -15})
	//t.SetPos(Vec3{-12.936243057250977, -46.53374099731445, 12.933069229125977})
	//t.SetRot(Vec3{74.99997818140304, -2.0000023323847937, -14.999998709865736})

	fmt.Println(t.Mat())
	fmt.Println(t.Mat().Transpose())
	fmt.Println("pos:", t.Pos())

	t.MoveByVec(Vec3{0, 0, -30.6510})
	fmt.Println("pos:", t.Pos())
	if !t.Pos().ApproxEqual(Vec3{-5.00, -18.00, 5.00}, 0.01) {
		tt.Fatal()
	}

	// Test camera transform
	c := NewCameraNode()
	c.SetPos(Vec3Zero)
	c.SetRot(Vec3{75, -2, -15})

	c.MoveByVec(Vec3{0, 0, 100})
	fmt.Println("c pos:", c.Pos())
	if !c.Pos().ApproxEqual(Vec3{-25.87, -93.06, 25.86}, 0.01) {
		tt.Fatal()
	}

	c.MoveByVec(Vec3{0, 0, -100})
	fmt.Println("c pos:", c.Pos())
	if !c.Pos().ApproxEqual(Vec3Zero, 0.01) {
		tt.Fatal()
	}

	// Test increment transform
	c.SetPos(Vec3Zero)
	for i := 0; i < 100; i += 10 {
		c.MoveByVec(Vec3{0, 0, 10})
	}

	fmt.Println("c pos:", c.Pos())
	if !c.Pos().ApproxEqual(Vec3{-25.87, -93.06, 25.86}, 0.01) {
		tt.Fatal()
	}
}

// Test based on glm-verified value.
func TestGlm(tt *testing.T) {
	t := NewTransform()
	t.SetRot(Vec3{45, 45, 45})
	t.SetPos(Vec3{2, 3, 4})

	/*
		XYZ: wrong.
		glm output:
			mat4x4((0.500000, 0.853553, 0.146447, 0.000000),
				   (-0.500000, 0.146447, 0.853553, 0.000000),
				   (0.707107, -0.500000, 0.500000, 0.000000),
		           (2.000000, 3.000000, 4.000000, 1.000000))
		ZYX: correct.
			mat4x4((0.500000, 0.500000, -0.707107, 0.000000),
				   (-0.146447, 0.853553, 0.500000, 0.000000),
				   (0.853553, -0.146447, 0.500000, 0.000000),
				   (2.000000, 3.000000, 4.000000, 1.000000))

	*/

	a := t.Mat()

	b := Mat4{
		{0.500000, 0.500000, -0.707107, 0.000000},
		{-0.146447, 0.853553, 0.500000, 0.000000},
		{0.853553, -0.146447, 0.500000, 0.000000},
		{2, 3, 4, 1}}

	fmt.Println(a)
	fmt.Println(a.Transpose())
	fmt.Println(b)
	fmt.Println(a.Equal(b))
	fmt.Println(a.ApproxEqual(b, 0.01))
	fmt.Println(a.ApproxEqual(b, 1.0E-4))
	fmt.Println(a.Equal(b))

	//if !a.ApproxEqual(b, 1.0E-4) {
	if !a.ApproxEqual(b, 0.01) {
		tt.Fatal()
	}
}

func TestForward(tt *testing.T) {
	t := NewTransform()
	t.SetRot(Vec3{0, 0, 45})
	f := t.Forward()
	fmt.Println("f:", f)

	t = NewTransform()
	t.SetPos(Vec3{12.999, 10, 0})
	f = t.Forward()
	fmt.Println("f:", f, t.Rot())

	t = NewTransform()
	t.SetPos(Vec3{12.999, 10, 0})
	t.MoveByVec(Vec3{1, 0, 0})
	fmt.Println("f:", t.Pos(), t.Rot())
}

func TestTRS(tt *testing.T) {
	t := NewTransform()
	t.SetRot(Vec3{45, 45, 45})
	t.SetPos(Vec3{2, 3, 4})
	p("t")
	t.SetScale(Vec3{1, 2, 3})
	fmt.Println("res:", t.Mat())
}
