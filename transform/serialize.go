package transform

import (
  "bitbucket.org/coaljoe/lib/sr"
)

func (t *Transform) GobEncode() ([]byte, error) {
	//return sr.SerializeValue(t), nil
	//return sr.SerializeByFields(t), nil

	return sr.SerializeByFields(
		t.mat, t.pos, t.rot, t.scale,
		t.parent, t.children, t.worldMat,
		t.dirty), nil

	/*
		return sr.SerializeByFields(
			t.mat, t.worldMat, t.pos,
			t.dirty), nil
	*/

	/*
		return sr.SerializeByFields(
			t.mat, t.pos, t.rot, t.scale,
			t.children, t.worldMat,
			t.dirty), nil
	*/

}

func (t *Transform) GobDecode(b []byte) error {
	//t = sr.DeserializeValue(b)
	//sr.DeserializeValue(b, &t)
	/*
		sr.DeserializeByFields(b,
			&t.mat, &t.pos, &t.rot, &t.scale,
			&t.children, &t.worldMat,
			&t.dirty)
	*/

	sr.DeserializeByFields(b,
		&t.mat, &t.pos, &t.rot, &t.scale,
		&t.parent, &t.children, &t.worldMat,
		&t.dirty)

	/*
		sr.DeserializeByFields(b,
			&t.mat, &t.worldMat, &t.pos,
			&t.dirty)
	*/
	return nil
}