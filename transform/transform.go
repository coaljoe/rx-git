package transform

import (
	. "bitbucket.org/coaljoe/rx/math"
	"fmt"
	"math"
  "log"
	//mgl "github.com/go-gl/mathgl/mgl64"
)

var Debug bool = true

/*
type TransformableI interface {
	Transform() *Transform
}
*/

type Transform struct {
	//mat *Mat4 // transform matrix
	mat             Mat4 // local? transform matrix
	pos, rot, scale Vec3 // local, relative to parent. fixme?: pos -> translate
	//parent          TransformableI
	parent   *Transform
	children []*Transform
	worldMat Mat4 // local-to-world matrix?
	dirty    bool
	//derpq           bool
}

func (t *Transform) Pos() Vec3   { return t.pos }
func (t *Transform) Rot() Vec3   { return t.rot }
func (t *Transform) Scale() Vec3 { return t.scale }
func (t *Transform) Mat() Mat4   { return t.mat }

func NewTransform() *Transform {
	t := &Transform{
		mat: Mat4Identity,
		//mat: Ident4(),
		//mat:   mgl.Ident4(),
		scale:    Vec3One,
		parent:   nil,
		children: make([]*Transform, 0),
		worldMat: Mat4Identity,
		dirty:    true,
	}
	t.build()
	return t
}

func (t *Transform) OnDeserialize() {
	// Reinit something
	t.children = make([]*Transform, 0)
	//t.build()
}

func (t *Transform) Clone() *Transform {
	//z := &Transform{t.Copy()}
	z := *t
	return &z

}

func (t *Transform) setDirty() {
	t.dirty = true
	t.build() // fixme?

	// update children
	for _, child := range t.children {
		//child.setDirty() // recursive? not tested.
		child.dirty = true
		child.build()
	}
}

func (t *Transform) build() {
	if !t.dirty {
		return
	}

	//p("derp")
	//p(t.pos, t.rot, t.scale)
	//p(t.mat)
	/*
		var m Mat4
		if t.Parent() == nil {
			m = Mat4Identity
		} else {
			pt := t.Parent().Transform()
			pt.build()
			m = pt.Mat()
			//println(m.String())
			//fmt.Println(m.Data())
			//pp("derp")
		}
		//m := Ident4()
	*/
	var m Mat4 = Mat4Identity
	var worldM Mat4 = Mat4Identity

	// translate
	//tm := Translate3D(t.pos)
	//m = m.Translate(t.pos.X(), t.pos.Y(), t.pos.Z())
	tm := m.Translate(t.pos.X(), t.pos.Y(), t.pos.Z())
	//m = m.Mul(tm)

	// rotate
	//rm := Rotate3D(t.rot)
	//m = m.Mul4(rm)

	rot_rad := t.rot.Radians()
	//rm := m.RotateZ(rot_rad.Z())
	//rm := m.Rotate(1, 1, 1, t.rot.Z())
	//rm := m.Rotate(0, 0, 1, t.rot.Z())

	// должно быть XYZ врашение как в блендере, но работает неправильно (transform_test)
	// нужно проверить почему ZYX работает, возможно проблема в коде вращения матриц, row/col major matrices
	//rm := m.RotateX(rot_rad.X()).RotateY(rot_rad.Z()).RotateZ(rot_rad.Y())
	//rm := m.RotateX(rot_rad.X()).RotateY(rot_rad.Y()).RotateZ(rot_rad.Z())
	//m = m.RotateZ(rot_rad.Z()).RotateY(rot_rad.Y()).RotateX(rot_rad.X())
	rm := m.RotateZ(rot_rad.Z()).RotateY(rot_rad.Y()).RotateX(rot_rad.X()) // yaw, pitch, roll; 'XYZ' in blender
	//m = m.Mul(rm)
	//m = m.Transpose()

	//rm := m.RotateX(rot_rad.X()).RotateY(rot_rad.Y()).RotateZ(rot_rad.Z())
	//rm := m.RotateZ(rot_rad.Z()).RotateY(rot_rad.Y()).RotateX(rot_rad.X())
	//m = m.Mul(rm)
	//rm := Mat4Identity.NewEulerRotation(rot_rad.Z(), rot_rad.Y(), rot_rad.X())
	//rm := Mat4Identity.NewEulerRotation(rot_rad.X(), rot_rad.Y(), rot_rad.Z())
	//m = m.Mul(rm)

	//sm := Mat4Identity
	sm := Mat4{
		{t.Scale().X(), 0, 0, 0},
		{0, t.Scale().Y(), 0, 0},
		{0, 0, t.Scale().Z(), 0},
		{0, 0, 0, 1}}

	/*
		p("--")
		p("tm:", tm)
		p("rm:", rm)
		p("sm:", sm)
	*/

	// modelM = T * R * S (works like tm * rm * sm in glm with ZYX rm order)
	modelM := tm.Mul(rm).Mul(sm)
	// XXX T * R * S reversed (emulated) (doesn't work)
	//modelM := sm.Mul(rm).Mul(tm)

	// local matrix
	m = modelM
	worldM = m

	if t.parent != nil {
		pt := t.Parent().Transform()
		pt.build()
		parentM := pt.Mat()
		//println(m.String())
		//fmt.Println(m.Data())
		//println(parentM.String())
		//fmt.Println(parentM.Data())

		m = m.Mul(parentM)
		//m = parentM.Mul(m)

		//worldM = worldM.Mul(pt.WorldMat())
		//worldM = worldM.Mul(pt.Mat())

		//println(m.String())
		//pp("derp")
	}

	// world matrix
	//worldM = m.Invert()
	//worldM = m

	if t.parent != nil {
		pt := t.Parent().Transform()
		pWorldM := pt.WorldMat()
		//worldM = worldM.Mul(pt.WorldMat())
		worldM = pWorldM.Mul(worldM)
	}

	//m = m.Transpose()

	// update
	t.mat = m
	t.worldMat = worldM
	t.dirty = false

	//p(t.mat)
}

func (t *Transform) SetPos(v Vec3) {
	if Debug && v.IsNaN() {
		log.Panicf("cannot be NaN.", v)
	}

	t.pos = v
	//t.mat = Translate3D(v)
	t.setDirty()
}

func (t *Transform) SetRot(v Vec3) {
	if Debug && v.IsNaN() {
		log.Panicf("cannot be NaN.", v)
	}

	t.rot = v
	//t.mat =
	t.setDirty()
}

func (t *Transform) SetScale(v Vec3) {
	if Debug && v.IsNaN() {
		log.Panicf("cannot be NaN.", v)
	}

	t.scale = v
	t.setDirty()
}

func (t *Transform) SetMatrix(m Mat4) {
	t.mat = m
}

/*
func (t *Transform) Transform() *Transform {
  return t
}
*/

// Return local rotation.
func (t *Transform) LocalRot() Vec3 {
	//return t.LocalTransform().Rotation()
	return t.Mat().ExtractRotation()
}

// Return local position.
func (t *Transform) LocalPos() Vec3 {
	return t.Mat().Translation()
}

// Return final world transform.
func (t *Transform) FinalTransform() Mat4 {
	//return t.Convert(gfx.LocalToWorld)
	//println("derp")
	//z := Mat4Identity
	//z := t.mat
	//z := t.worldMat
	//return z
	return t.WorldMat()
}

// Return world transform matrix.
func (t *Transform) WorldMat() Mat4 {
	return t.worldMat
}

// Return local transform matrix, without parents etc.
func (t *Transform) LocalMat() Mat4 {
	return t.Mat()
}

// Absolute transform in the world.
func (t *Transform) WorldPos() Vec3 {
	return t.WorldMat().Translation()
}

/*
// World-space rotation.
func (t *Transform) WorldRot() Vec3 {
	// absolute transform in the world
	return t.WorldMat().Rotation()
}
*/

func (t *Transform) LookAt2d(p_ Vec3) {
	// Should work according to this:
	// http://wikicode.wikidot.com/get-angle-of-line-between-two-points

	//panic("xyzfix")
	//ox, oz := n.Pos.X, n.Pos.Z
	//px, pz := p.X, p.Z
	//dir := t.WorldPos().Sub(p_)
	dir := p_.Sub(t.WorldPos())
	//rot := Degrees(math.Atan2(dir.X, dir.Y))
	rot := Degrees(math.Atan2(dir.Y(), dir.X())) // Y,X order like in geom.go
	//t.SetRot(Vec3{0, 90+rot, 0})

	/*
		p("p_:", p)
		p("t.wpos", t.WorldPos())
		p("t.rot", t.Rot())
		p("dir:", dir)
		pp("rot: ", rot)
	*/
	// note: this works with the current x-oriented meshes.
	//t.SetRot(Vec3{0, 0, -90 + -rot})
	t.SetRot(Vec3{0, 0, rot})
	_ = rot
}

func (t *Transform) LookAt(p Vec3) {
	t.mat = t.mat.LookAt(p)
	// update transform props: pos, rot, scale
	t.pos = t.mat.ExtractTranslation()
	t.rot = t.mat.ExtractRotation()
	t.scale = t.mat.ExtractScale()
}

func (t *Transform) MoveByVec(v Vec3) {
	mat := t.WorldMat().Upper3() // world(?) rotation matrix
	//mat.SetTranslation(0, 0, 0) // clear translation
	// FIXME: should use 3x3 rotation matrix for multiplication,
	// but works this way too

	trans_local := v

	trans_world := mat.MulVec3(trans_local)

	new_pos := t.Pos().Add(trans_world)

	t.SetPos(new_pos)
}

// Move locally by vec.
// fixme: not precise? matrices are not synced with blender?
func (t *Transform) MoveByVec0(v Vec3) {
	//mat := t.Convert(gfx.LocalToWorld)
	mat := t.WorldMat()
	/*
	  _, shr, hpr := mat.UpperMat3().Decompose(CoordSysZUpRight)
	  _ = shr
	  p(hpr.HprToXyz())
	  p(hpr.HprToXyz().Degrees())
	  p(mat.Transposed())
	  m2 := Mat3Compose(Vec3One, Vec3Zero, hpr, CoordSysZUpRight)
	  p(m2.Transposed())
	  hprconv := Vec3{hpr.Y, hpr.X, hpr.Z}
	  //hprconv := Vec3{hpr.X, hpr.Y, hpr.Z}
	  //m3 := Mat3Compose(Vec3One, shr, hprconv, CoordSysZUpRight)
	  m3 := Mat3Compose(Vec3One, Vec3Zero, hprconv, CoordSysZUpRight)
	  //v1 := Vec3{75, -2, -15}
	  //v1 = v.XyzToHpr()
	  //m3 := Mat3Compose(Vec3One, Vec3Zero, v1, CoordSysZUpRight)
	  p(m3.Transposed())
	  //p(mat)
	*/

	//mat = mat.Transposed()
	//p(mat)

	/*
		invMat, ok := mat.Inverse()
		if !ok {
			panic("bad matrix")
		}
	*/
	// fixme: return old checks?
	invMat := mat.Invert()

	//p(mat)
	//p(invMat)

	m := invMat
	//m, _ := t.Convert(gfx.LocalToWorld).UpperMat3().InverseTransposed()
	//p(m)
	/*
	   vec_rot := Vec3{
	     v.X * m.Col(0).X + v.Y * m.Col(0).Y + v.Z * m.Col(0).Z,
	     v.X * m.Col(1).X + v.Y * m.Col(1).Y + v.Z * m.Col(1).Z,
	     v.X * m.Col(2).X + v.Y * m.Col(2).Y + v.Z * m.Col(2).Z}
	*/

	// mult v * m

	/*
		vec_rot := Vec3{
			v.X()*m[0][0] + v.Y()*m[0][1] + v.Z()*m[0][2],
			v.X()*m[1][0] + v.Y()*m[1][1] + v.Z()*m[1][2],
			v.X()*m[2][0] + v.Y()*m[2][1] + v.Z()*m[2][2]}
	*/
	vec_rot := v.ToVec4().MulMat4(m).ToVec3()

	//vec_rot := m.MulVec4(v.ToVec4()).ToVec3()

	//p(vec_rot)

	new_pos := t.Pos().Add(vec_rot)
	//p(new_pos)

	t.SetPos(new_pos)

	/*
	   cs := CoordSysZUpRight

	   m := invMat
	   q := QuatFromHpr(v.XyzToHpr().Radians(), cs)
	   m = q.ExtractToMat4().Mul(m)
	   q = QuatFromMat3(m.UpperMat3())
	   vec_rot := q.Hpr(cs).HprToXyz().Degrees()

	   p(v, vec_rot)
	   new_pos := t.Pos().Add(vec_rot)

	   t.SetPos(new_pos)
	*/
}

// Adjust position.
func (t *Transform) AdjPos(v Vec3) {
	t.SetPos(t.Pos().Add(v))
}

// Adjust rotation.
func (t *Transform) AdjRot(v Vec3) {
	t.SetRot(t.Rot().Add(v))
}

// Move locally.
/*
func (t *Transform) Translate(x, y, z float64) {
	t.MoveByVec(Vec3{x, y, z})
}
*/

func (t *Transform) Copy() *Transform {
	return t.Clone()
}

// Get forward direction vector, same as in Unity but for X axis.
func (t *Transform) Forward() Vec3 {
	v := Vec3{1, 0, 0}

	// Copied from MoveByVec
	mat := t.WorldMat().Upper3() // world(?) rotation matrix
	//mat.SetTranslation(0, 0, 0) // clear translation
	// FIXME: should use 3x3 rotation matrix for multiplication,
	// but works this way too
	//p("mat:", mat)

	trans_local := v

	trans_world := mat.MulVec3(trans_local)

	//p("trans_world:", trans_world)

	trans_world = trans_world.Norm()

	var _ = `
	new_pos := t.Pos().Add(trans_world)
	
	p("new_pos 1:", new_pos)

	// Normalize
	new_pos = new_pos.Norm()
	
	p("new_pos 2:", new_pos)
	
	return new_pos
	`

	return trans_world
}

func (t *Transform) Back() Vec3 {
	return t.Forward().MulScalar(-1)
}

func (t *Transform) Translate(v Vec3) {
	/*
		//tm := Mat4Identity.Translate(v.X(), v.Y(), v.Z())
		//t.mat = t.mat.Mul(tm)
		t.mat = t.mat.Translate(v.X(), v.Y(), v.Z())
		//t.mat = t.mat.Mul(tm)

		// update pos?
		t.pos = t.mat.Translation()
	*/

	// adj pos
	t.SetPos(t.Pos().Add(v))
}

//func (t *Transform) Parent() TransformableI {
func (t *Transform) Parent() *Transform {
	return t.parent
}

//func (t *Transform) SetParent(p TransformableI) {
func (t *Transform) SetParent(p *Transform) {
	if p == t {
		log.Panicf("cannot selfparent (cyclic dependency)")
		return
	}

	if t.parent == nil {
		t.parent = p
		t.setDirty()

		// add children
		p.Transform().children = append(p.Transform().children, t)
	} else {
		log.Panicf("transform already has parent")
		return
	}
}

// Return Transform (self)
func (t *Transform) Transform() *Transform {
	return t
}

func (t *Transform) String() string {
	return fmt.Sprintf("Transform<pos: %f %f %f, rot: %f %f %f, scale: %f %f %f>",
		t.pos.X(), t.pos.Y(), t.pos.Z(), t.rot.X(), t.rot.Y(), t.rot.Z(),
		t.scale.X(), t.scale.Y(), t.scale.Z())
}

func (t *Transform) SetRotZ(v float64) {
	rot := t.Rot()
	t.SetRot(Vec3{rot.X(), rot.Y(), v})
}

func (t *Transform) SetRotY(v float64) {
	rot := t.Rot()
	t.SetRot(Vec3{rot.X(), v, rot.Z()})
}

func (t *Transform) SetRotX(v float64) {
	rot := t.Rot()
	t.SetRot(Vec3{v, rot.Y(), rot.Z()})
}

func (t *Transform) SetPosZ(v float64) {
	p := t.Pos()
	p.SetZ(v)
	t.SetPos(p)
}

func (t *Transform) SetPosY(v float64) {
	p := t.Pos()
	p.SetY(v)
	t.SetPos(p)
}

func (t *Transform) SetPosX(v float64) {
	p := t.Pos()
	p.SetX(v)
	t.SetPos(p)
}

// Shortcuts
func (t *Transform) PosX() float64 { return t.pos.X() }
func (t *Transform) PosY() float64 { return t.pos.Y() }
func (t *Transform) PosZ() float64 { return t.pos.Z() }
func (t *Transform) RotX() float64 { return t.rot.X() }
func (t *Transform) RotY() float64 { return t.rot.Y() }
func (t *Transform) RotZ() float64 { return t.rot.Z() }
