#define SMAA_GLSL_3 1
#define SMAA_PRESET_LOW 1
#define SMAA_INCLUDE_PS 1
#define SMAA_INCLUDE_VS 0
#define SMAA_RT_METRICS float4(1.0 / 800.0, 1.0 / 600.0, 800.0, 600.0)

#include "smaa.h"

uniform sampler2D fboTex;
uniform vec2 ScreenPos;
uniform int effect_number = 0;
uniform bool use_fxaa = true;

void main()
{
    vec3 color;
    //color = vec3(0.0,0.0,1.0);

    // test
    //vec3 c2 = texture2D(fboTex, ScreenPos).rgb;
    //vec3 c2 = texture2D(fboTex, gl_TexCoord[0].st).rgb;
    //color = (color + c2) / 2;
    //color = vec3(color.r, max(color.g,  color.b), color.b);

    if (use_fxaa) {
        //vec2 buffersize = vec2(800,600);
        vec2 buffersize = textureSize(fboTex, 0).xy;

        vec2 rcpFrame; 
        rcpFrame.x = 1.0 / buffersize.x;
        rcpFrame.y = 1.0 / buffersize.y;
 
        vec2 pos = gl_FragCoord.xy / buffersize.xy;

        // Only used on FXAA Quality.
        // Choose the amount of sub-pixel aliasing removal.
        // This can effect sharpness.
        //   1.00 - upper limit (softer)
        //   0.75 - default amount of filtering
        //   0.50 - lower limit (sharper, less sub-pixel aliasing removal)
        //   0.25 - almost off
        //   0.00 - completely off
        //float QualitySubpix = 0.75;
        float QualitySubpix = 0.5;
        
        // The minimum amount of local contrast required to apply algorithm.
        //   0.333 - too little (faster)
        //   0.250 - low quality
        //   0.166 - default
        //   0.125 - high quality 
        //   0.033 - very high quality (slower)
        //float QualityEdgeThreshold = 0.033;
        float QualityEdgeThreshold = 0.125;

        // skips dark areas from processing (note green luma)
        float QualityEdgeThresholdMin = 0.2; //0.02;//0.0625; // ?
  
        float dummy1 = 0;
        vec4 dummy4 = vec4(0);
        color = vec3(FxaaPixelShader(pos, dummy4, fboTex, fboTex, fboTex,
                                     rcpFrame, dummy4, dummy4, dummy4,
                                     QualitySubpix, QualityEdgeThreshold, QualityEdgeThresholdMin,
                                     dummy1, dummy1, dummy1, dummy4));
    }
    else {
        color = texture2D(fboTex, gl_TexCoord[0].st).rgb;
    }


    /*** post processing ***/

    // night vision
    if(effect_number == 1)
    {
        color *= 0.3;
        //color *= 0.05;

        const float brightness = 4;
        float lum = dot(vec3(0.30, 0.59, 0.11), color);
        color = vec3(0.5, 0.6, 0.5) * max(.1, 1.0 - (lum*brightness));
    }

    gl_FragColor.rgb = color;
}
