// Blend fragment shader

uniform sampler2D edge_tex; 
uniform sampler2D area_tex; 
uniform sampler2D search_tex; 
vec2 texcoord;
vec2 pixcoord; 
vec4 offset[3];
vec4 dummy2;

void main()
{
  gl_FragColor = SMAABlendingWeightCalculationPS(texcoord, pixcoord, offset, edge_tex, area_tex, search_tex, ivec4(0));
  //gl_FragColor = texture2D(search_tex, texcoord);//gl_TexCoord[0].xy);
  //gl_FragColor.a = 1.0;
}