// Edge vertex shader
vec2 texcoord;
vec4 offset[3];
vec4 dummy2;

void main()
{
    texcoord = gl_MultiTexCoord0.xy;
    vec4 dummy1 = vec4(0);
    SMAAEdgeDetectionVS(dummy1, dummy2, texcoord, offset);
    gl_Position = ftransform();
}

