#include "uniforms.vert"

void main(void)
{
    // vertex position
	f_position = vec3(m * vec4(v_position, 1.0));

    f_texCoord = v_texCoord;

	//mat3 normalMatrix = transpose(inverse(mat3(m)));
	/*
	mat3 normalMatrix = mat3(m);
    f_normal = normalize(normalMatrix*v_normal);
	*/
	//mat3 normalMatrix = mat3(m);
    //f_normal = normalize(normalMatrix*v_normal);
	f_normal = normalize(mat3(m_3x3_inv_transp) * v_normal);

#ifdef _SHADOWS
	/*
    // calculate shadow
    shadowCoord = shadowMatrix * gl_ModelViewMatrix * gl_Vertex;
	*/
#endif

#ifdef _NORMAL_MAP
	/*
    vec3 n = normalize (gl_NormalMatrix * gl_Normal);
    vec3 t = normalize (gl_NormalMatrix * gl_MultiTexCoord7.xyz);
    //vec3 t = normalize (gl_NormalMatrix * tangent);
    //vec3 t = normalize (gl_NormalMatrix * (gl_Color.rgb - 0.5));
    vec3 b = cross (n,t) * gl_MultiTexCoord7.w;

    mat3 tbn = mat3(t,b,n);
    
    ///*
    L = normalize(L) * tbn;
    L = normalize(L);

    H = normalize(L + normalize(V));
    //*/
    L = L * tbn;
	*/
#endif

    //gl_TexCoord[0] = gl_MultiTexCoord0; // texture
    //gl_TexCoord[1] = gl_MultiTexCoord1; // lightmap
    //gl_TexCoord[2] = gl_MultiTexCoord2; // normalmap

    gl_Position = mvp * vec4(v_position, 1.0);
    //gl_Position = mvp * vec4(v_position + V, 1.0);
}
