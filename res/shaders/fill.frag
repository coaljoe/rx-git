#include "uniforms.frag"
uniform vec4 fill_color;


void main() {
    vec4 color = fill_color;

    gl_FragColor = color;
}
