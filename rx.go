package rx

import (
	//. "fmt"
	//. "scene"
	"bitbucket.org/coaljoe/lib/xlog"
	"bitbucket.org/coaljoe/rx/text"
	"fmt"
	"os"
)

// Init on package import.
func init() {
	p("-> rx init")

	p("-> done rx init")
}

// globals
var (
	Log              *xlog.Logger
	_InputSys        *InputSys
	_RenderTargetSys *RenderTargetSys
)
var rxi *Rx = nil

type Rx struct {
	App                      *App
	InputSys                 *InputSys
	renderer                 *Renderer
	scene                    *Scene
	Camera                   *Node // Active Camera; XXX rename to CameraNode?
	ResourceSys              *ResourceSys
	ResourceLoader           *ResourceLoader
	RenderTargetSys          *RenderTargetSys
	RenderTargetDebugViewSys *RenderTargetDebugViewSys
}

func (rx *Rx) Scene() *Scene       { return rx.scene }
func (rx *Rx) Renderer() *Renderer { return rx.renderer }

func Rxi() *Rx { return rxi }

// Shortcuts
func Init(width, height int) *Rx {
	return NewRx().Init(width, height)
}
func InitFullscreen(width, height int) *Rx {
	return NewRx().InitFullscreen(width, height)
}

func NewRx() *Rx {
	if rxi == nil {
		println("rx.new")
		//SetDefaultConf()

		// Set res_path
		//?
		// create logger
		Log = xlog.NewLogger("rx", Conf.log_level)
		if loglev := os.Getenv("LOGLEVEL"); loglev != "" {
			Log.SetLogLevelStr(loglev)
			Conf.log_level = Log.LogLevel() // fixme
		}
		Log.SetOutputFile("rx.log")

		rxi = &Rx{
			App:                      NewApp(),
			scene:                    NewScene(),
			renderer:                 NewRenderer(),
			ResourceSys:              NewResourceSys(),
			ResourceLoader:           NewResourceLoader(),
			RenderTargetDebugViewSys: NewRenderTargetDebugViewSys(),
		}
		// fixme
		rxi.InputSys = NewInputSys(rxi.App)
		_InputSys = rxi.InputSys // update global
		rxi.RenderTargetSys = NewRenderTargetSys()

		// update globals
		//_InputSys = rxi.InputSys
		_RenderTargetSys = rxi.RenderTargetSys

	}
	return rxi
}

func (rx *Rx) SetResPath(path string) {
	Log.Inf("setting res_path to ", path)
	Conf.res_path = path
}

func (rx *Rx) init(width, height int, fullscreen bool) *Rx {
	println("rx.init")
	if width < 1 || height < 1 {
		panic(fmt.Sprintf("bad width and height: (%d, %d)", width, height))
	}

	// Fixme?
	if !fullscreen {
		rx.App.Init(width, height)
	} else {
		rx.App.InitFullscreen(width, height)
	}

	// XXX fixme move to app/scene?
	// move to setDefaultScene
	cam_ := rx.scene.CreateCameraNode("cam")
	//cam.SetupView(10, float64(width)/float64(height), 0.1, 1000, true)
	cam_.Camera.SetupView(10, float64(width)/float64(height), 1, 100, true)
	//cam.SetupView(90, float64(width)/float64(height), 0.1, 1000, false)
	rx.Camera = cam_
	rx.renderer.Init(rx, width, height)

	// Init default font
	if true {
		p("rx: trying to load default font...")
		// Create default font
		const fontName = "res/fonts/FreeSans.ttf"
		if font, err := text.NewFont(fontName); err == nil {
			text.DefaultFont = font
		} else {
			panic(err)
		}
		fa := text.FontAttributes{}
		//fa.PointSize = 60
		fa.PointSize = 16
		fa.DPI = 72
		//fa.Hinting = text.HintingNone
		fa.Hinting = text.HintingFull
		fa.LineSpacing = 1.0
		text.DefaultFontAttributes = fa
	}

	return rxi
}

func (rx *Rx) Init(width, height int) *Rx {
	return rx.init(width, height, false)
}

func (rx *Rx) InitFullscreen(width, height int) *Rx {
	return rx.init(width, height, true)
}

func (rx *Rx) Render() {
	//println("rx.render")
	rx.renderer.Render(rx.Camera)
}

func (rx *Rx) Update(dt float64) {
	rx.scene.Update(dt)
}
