package rx

import (
	. "fmt"
	"sort"
	//_ "math"

	"github.com/emirpasic/gods/lists/arraylist"
	"github.com/go-gl/gl/v2.1/gl"
	//mgl "github.com/go-gl/mathgl/mgl32"
	//"github.com/go-gl/glu"
	//"github.com/go-gl/glh"
	//"rx/glh"
	. "bitbucket.org/coaljoe/rx/math"
)

var _ = Println
var _ = Vec3Zero

type Renderer struct {
	rxi           *Rx
	width, height int
	frame         int
	lastShader    *Shader
	//lastTexture       *Texture
	reused            bool // fixme
	renderPasses      *arraylist.List
	guiRP             RenderPassI
	defaultRenderPass bool
	curCamRef         **Node // Ref
	activeNode        *Node
	curMaterial       *Material
	defaultMaterial   *Material
	// Fixme: use type fsquad (uint32, uint32, uint32) (?)
	matView          Mat4
	matProjection    Mat4
	matMVPIdent      Mat4 // MVP matrix with identity model matrix
	fsQuad           uint32
	in2dmode         bool
	inWireframemode  bool
	push2dprev2dmode bool
	debugRenderer    *DebugRenderer
	postRenderCB     func(r *Renderer)
}

func (r *Renderer) CurCam() *Node                 { return *r.curCamRef }
func (r *Renderer) Width() int                    { return r.width }
func (r *Renderer) Height() int                   { return r.height }
func (r *Renderer) Size() (int, int)              { return r.width, r.height }
func (r *Renderer) IsIn2dMode() bool              { return r.in2dmode }
func (r *Renderer) DebugRenderer() *DebugRenderer { return r.debugRenderer }
func (r *Renderer) ActiveNode() *Node             { return r.activeNode }
func (r *Renderer) SetActiveNode(n *Node)         { r.activeNode = n }

func NewRenderer() *Renderer {
	r := &Renderer{
		lastShader:        nil,
		renderPasses:      arraylist.New(),
		guiRP:             nil,
		defaultRenderPass: true,
	}
	r.debugRenderer = newDebugRenderer(r)
	return r
}

func (r *Renderer) Init(rxi *Rx, w, h int) {
	r.rxi = rxi
	r.width = w
	r.height = h
	//r.SetViewport(0, 0, r.width, r.height)

	init_draw()
	drawer.init()

	r.fsQuad, _, _ = BuildFsQuad()

	//defaultMaterial:   NewMaterial(Conf.res_path + "/materials/static.json"),
	r.defaultMaterial = NewMaterial(Conf.res_path + Conf.DefaultMaterialPath)

	// gl-related

	//gl.ClearColor(0.1,0.1,0.1,1.)
	//gl.ClearDepth(1.0)
	gl.Enable(gl.DEPTH_TEST)
	//gl.DepthFunc(gl.LEQUAL)
	gl.DepthFunc(gl.LESS)
	//glDepthFunc(GL_LESS)

	//gl.Enable(gl.COLOR_MATERIAL)
	// enable textures by default
	gl.Enable(gl.TEXTURE_2D)

	// enable blending
	//gl.Enable(gl.ALPHA_TEST)
	//gl.Enable(gl.BLEND)
	gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	//gl.BlendFunc(gl.ONE, gl.SRC_COLOR)
	//gl.BlendEquation(gl.ADD)
	//gl.DepthMask(false)

	//gl.BlendFunc(gl.ONE, gl.ONE)
	//gl.Disable(gl.DEPTH_TEST)

	gl.Enable(gl.CULL_FACE)
	gl.CullFace(gl.BACK)
	//gl.CullFace(gl.FRONT)
	//gl.FrontFace(gl.CW)

	// Set viewport
	r.SetViewport(0, 0, r.width, r.height)

	r.debugRenderer.Init(w, h)
}

// Return pixel width.
func (r *Renderer) GetPw() float64 {
	return 1.0 / float64(r.width)
}

// Return pixel height.
func (r *Renderer) GetPh() float64 {
	return 1.0 / float64(r.height)
}

func (r *Renderer) SetViewport(x, y, w, h int) {
	// warning: x, y origin is lower left
	if Debug {
		if w < 0 || h < 0 {
			pp("w or h cannot be negative.", w, h)
		}
	}
	gl.Viewport(int32(x), int32(y), int32(w), int32(h))
	glcheck()
}

func (r *Renderer) SetCamera(cam_ *Node) {
	//gl.Viewport(0,0,int32(r.width),int32(r.height))
	//gl.MatrixMode(gl.PROJECTION)
	//gl.LoadIdentity()

	var projM Mat4

	cam := cam_.Camera

	viewM := cam.GetViewMatrix()
	r.matView = viewM

	if cam.ortho {
		/*
			gl.Ortho(cam.orthoPlanes[0], cam.orthoPlanes[1],
				cam.orthoPlanes[2], cam.orthoPlanes[3], cam.znear, cam.zfar)
		*/
		projM = cam.GetProjectionMatrix()
		//d := projM.Data()
		//gl.MultMatrixd(&d[0])
		r.matProjection = projM
	} else {
		panic("no glu.Perspective yet")
		//glu.Perspective(cam.fov, cam.aspect, cam.znear, cam.zfar)
	}

	matMVPIdent := projM.Mul(viewM)
	r.matMVPIdent = matMVPIdent

	//glu.Perspective(20, 1024.0/576.0, cam.znear, cam.zfar)

	// set camera position
	//gl.Translated(cam.Pos.x, cam.Pos.y, cam.Pos.z)

	/*
	  glu.LookAt(cam.Pos().X, cam.Pos().Y, cam.Pos().Z,
	             cam.target.X, cam.target.Y, cam.target.Z, 0, 1, 0)
	*/

	//var proj_matrix [16]float32
	//var modelview_matrix [16]float32

	//gl.GetFloatv(gl.PROJECTION_MATRIX, &proj_matrix[0])
	//glcheck()
	//Println(proj_matrix)

	/*
	   m := rm
	   mf := []float64{m[0][0], m[0][1], m[0][2], m[0][3],
	                       m[1][0], m[1][1], m[1][2], m[1][3],
	                       m[2][0], m[2][1], m[2][2], m[2][3],
	                       m[3][0], m[3][1], m[3][2], m[3][3]}
	*/

	//camMatrix := mgl.Ident4()

	/*
	   // no support for targeted camera atm
	   rm := mgl.LookAt(float32(cam.Pos().X), float32(cam.Pos().Y), float32(cam.Pos().Z),
	                    float32(cam.target.X), float32(cam.target.Y), float32(cam.target.Z),
	                    0, 0, 1)
	   var _ = rm

	   gl.MultMatrixf(&rm[0])
	*/

	//gl.GetFloatv(gl.PROJECTION_MATRIX, &proj_matrix[0])
	//glcheck()
	//Println(proj_matrix)
	//panic(2)
	/*
	   Println(rm)
	   panic(2)

	   proj_matrix = gl.GetFloatv(gl.PROJECTION_MATRIX)


	   Println(rm)
	   panic(2)
	*/

	//gl.Rotated(90, 0, 0, 1)
	//gl.Scaled(1, 1, -1)
	/*
	  gl.Rotated(-45, 1, 0, 0) // roll
	  gl.Rotated(0, 0, 1, 0) // pitch
	  gl.Rotated(45, 0, 0, 1) // heading
	  gl.Translated(0,0, -10)
	*/
	//rot := Vec3{45, 0, -45}
	//pos := Vec3{-10, -10, 10}
	var _ = `
	//rot := Vec3{75, -2, -15}
	//pos := Vec3{3, -8, 8}
	rot := cam.Rot()
	pos := cam.Pos()
	// xyz rot order
	gl.Rotatef(float32(-rot.X()), 1, 0, 0) // roll
	gl.Rotatef(float32(-rot.Y()), 0, 1, 0) // pitch
	gl.Rotatef(float32(-rot.Z()), 0, 0, 1) // heading
	/*
		// yxz rot order
		gl.Rotatef(float32(-rot.Y()), 0, 1, 0) // pitch
		gl.Rotatef(float32(-rot.X()), 1, 0, 0) // roll
		gl.Rotatef(float32(-rot.Z()), 0, 0, 1) // heading
	*/
	gl.Translatef(float32(-pos.X()), float32(-pos.Y()), float32(-pos.Z()))
	`

	//var _ = `
	//viewM := cam.GetViewMatrix()
	//vm_d := viewM.Data()
	//gl.MultMatrixd(&vm_d[0]) // multiply by view matrix
	//`

	/*
	   // set camera position
	   // transform order: SRT
	   gl.Rotated(cam.Rot.x, 1, 0, 0) // pitch
	   gl.Rotated(cam.Rot.y, 0, 1, 0) // heading
	   gl.Rotated(cam.Rot.z, 0, 0, 1) // roll
	   gl.Translated(cam.Pos.x, cam.Pos.y, cam.Pos.z)
	   //glTranslatef(1, 1, -10)
	   //glRotatef(90, 1, 0, 0)
	*/

	/*
	   // set camera position
	   // transform order: SRT
	   gl.Rotated(cam.Rot.z, 0, 0, 1) // roll
	   gl.Rotated(cam.Rot.y, 0, 1, 0) // heading
	   gl.Rotated(cam.Rot.x, 1, 0, 0) // pitch
	   gl.Translated(-cam.Pos.x, -cam.Pos.y, -cam.Pos.z)
	   //glTranslatef(1, 1, -10)
	   //glRotatef(90, 1, 0, 0)

	*/

	//gl.MatrixMode(gl.MODELVIEW)
	//gl.LoadIdentity()

	/*
	  # save camera matrix
	  cam._proj_matrix = glGetDoublev(GL_PROJECTION_MATRIX)
	  cam._mv_matrix = glGetDoublev(GL_MODELVIEW_MATRIX)
	  cam._vp = glGetIntegerv(GL_VIEWPORT)
	*/

	//gl.MultMatrixf(&rm[0]) // test
	//gl.GetFloatv(gl.MODELVIEW_MATRIX, &modelview_matrix[0])
	//glcheck()
	//Println(modelview_matrix)
	//panic(2)

	r.curCamRef = &cam_
}

/*
// Get cam's view matrix. fixme: move to Camera/CameraNode
func (r *Renderer) GetViewMatrix(cam *CameraNode) Mat4 {

	//viewM := Mat4Identity
	//viewM.
	//t := newTransform()
	//t.Rotate()

	// works buggy (with isometric cameras)
	var _ = `
	// Create cam's view matrix
	// http://www.3dgep.com/understanding-the-view-matrix/
	rot := cam.Rot()
	pos := cam.Pos()
	//t := cam.Transform.clone()
	t := NewTransform()
	t.SetRot(Vec3{-rot.X(), -rot.Y(), -rot.Z()})
	//t.SetRot(Vec3{rot.X(), rot.Y(), rot.Z()})
	t.SetPos(Vec3{-pos.X(), -pos.Y(), -pos.Z()})
	//t.SetRot(rot.MulScalar(-1))
	//t.SetPos(pos.MulScalar(-1))
	//viewM := t.WorldTransform()
	viewM := t.Mat()
	`

	// create inverse matrix (view matrix) (works fine)
	// TODO: add cache?
	viewM := cam.Mat().Invert()
	return viewM
}
*/

func (r *Renderer) setMaterial(mat *Material) {
	r.curMaterial = mat
	shader := mat.shader
	shader.Bind()

	// Camera
	//matView := r.CurCam().Camera.GetViewMatrix()
	//matProjection := r.CurCam().Camera.GetProjectionMatrix()
	matView := r.matView
	matProjection := r.matProjection

	shader.PassUniformMatrix4f("v", matView)
	shader.PassUniformMatrix4f("p", matProjection)

	/*
	  // same shader reusing
	  if r.lastShader == nil || shader.shaderHash != r.lastShader.shaderHash {
	    shader.Bind()
	    //if r.lastShader != nil {
	    //  Println(shader.Hash(), r.lastShader.Hash(), shader == r.lastShader)
	    //}
	    r.lastShader = shader
	    r.reused = false
	  } else {
	    shader = r.lastShader
	    r.reused = true
	    //panic("reusing shader")
	  }
	*/
	shader.PassUniform3f_("mat.diffuse", mat.Diffuse)
	shader.PassUniform3f_("mat.specular", mat.Specular)
	shader.PassUniform1f("mat.hardness", mat.Hardness)

	if len(rxi.Scene().Lights()) > 1 {
		panic("error: rendering multiple lights are not supported")
	}

	if rxi.Scene().light0 != nil {
		shader.PassUniform3f_("lightPosition", rxi.Scene().light0.Pos())
		//pp(rxi.Scene().light0.Pos())
		/*
			if shader.HasUniform("mat.hardness") {
				pp(2)
			}
		*/
		shader.PassUniform1f("light.intensity", rxi.Scene().light0.Light.intensity)
	} else {
		panic("error: no light in the scene")
	}
	//
	// texture
	if mat.texture != nil {
		gl.ActiveTexture(gl.TEXTURE0)
		mat.texture.Bind()
		shader.PassUniform1i("texMap", 0)
		gl.ActiveTexture(gl.TEXTURE0)
	}

	if mat.Alpha {
		// enable blending
		gl.Enable(gl.ALPHA_TEST)
		gl.Enable(gl.BLEND)
		//gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
		//gl.BlendFunc(gl.ONE, gl.SRC_COLOR)
		//gl.BlendEquation(gl.ADD)
		gl.DepthMask(false)
	}

	// 1-bit alpha
	if mat.AlphaTest {
		gl.Enable(gl.ALPHA_TEST)
		// FIXME? need to be unset to default value?
		gl.AlphaFunc(gl.GREATER, 0.5)
	}
}

func (r *Renderer) unsetMaterial(mat *Material) {
	/*
	  if r.reused {
	    mat.shader.Unbind()
	  }
	*/
	mat.shader.Unbind()
	if mat.texture != nil {
		mat.texture.Unbind()
	}

	if mat.Alpha {
		// disable blending
		gl.Disable(gl.ALPHA_TEST)
		gl.Disable(gl.BLEND)
		gl.DepthMask(true)
	}

	if mat.AlphaTest {
		gl.Disable(gl.ALPHA_TEST)
		gl.AlphaFunc(gl.ALWAYS, 0)
	}

	r.curMaterial = nil
}

// The default way to clear renderer's context.
func (r *Renderer) Clear() {
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
}

// Clear with specific color.
func (r *Renderer) ClearColor(_r, g, b float64) {
	gl.PushAttrib(gl.CURRENT_BIT)
	gl.ClearColor(float32(_r), float32(g), float32(b), 1.0)
	r.Clear()
	gl.PopAttrib()
}

func (r *Renderer) renderMesh(m *Mesh) {
	if !m.isEmpty {
		//gl.Enable(gl.COLOR_MATERIAL)
		//gl.Enable(gl.POLYGON_OFFSET_FILL)
		//gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
		//m.mb.Render()
		m.Render(r)
	} else {
		if Conf.debug_draw {
			//m.mb.Render()
			m.Render(r)
		}
	}
}

// Enter 2D mode
func (r *Renderer) Set2DMode() {
	if r.in2dmode {
		return
	}

	// Prepare
	gl.PushAttrib(gl.CURRENT_BIT | gl.TEXTURE_BIT | gl.DEPTH_TEST)
	//gl.Disable(gl.DEPTH_TEST)
	gl.Color3f(1, 1, 1) // important

	gl.MatrixMode(gl.PROJECTION)
	gl.PushMatrix()
	gl.LoadIdentity()
	gl.Ortho(0, 1, 0, 1, -1, 1) // normal coords
	//gl.Ortho(0, 1, 1, 0, -1, 1) // Y-flipped
	gl.MatrixMode(gl.MODELVIEW)
	gl.PushMatrix()
	gl.LoadIdentity()

	r.in2dmode = true
}

// Exit 2D mode
func (r *Renderer) Unset2DMode() {
	if !r.in2dmode {
		return
	}
	//r.SetCamera(r.curCamRef)

	// Restore
	gl.MatrixMode(gl.PROJECTION)
	gl.PopMatrix()
	gl.MatrixMode(gl.MODELVIEW)
	gl.PopMatrix()

	gl.PopAttrib()

	r.in2dmode = false
}

func (r *Renderer) Push2DMode() {
	r.push2dprev2dmode = r.in2dmode
	r.Set2DMode()
}

func (r *Renderer) Pop2DMode() {
	// Unset 2d mode if previously not in 2d mode
	if !r.push2dprev2dmode {
		r.Unset2DMode()
	}
}

func (r *Renderer) SetWireframeMode(v bool) {
	// XXX remove?
	if v == false && !r.inWireframemode {
		return
	}

	switch v {
	case true:
		gl.PolygonMode(gl.FRONT_AND_BACK, gl.LINE)
		r.inWireframemode = true
	case false:
		gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
		r.inWireframemode = false
	}
}

func (r *Renderer) RenderFsQuad() {
	// deprecated block
	/*
		// render fullscreen quad
		//r.renderQuad(0, 0, 1, 1, 0)
	*/
	// block end

	// from https://www.opengl.org/wiki/Viewing_and_Transformations#How_do_I_draw_a_full-screen_quad.3F

	// Prepare
	gl.PushAttrib(gl.DEPTH_TEST)
	gl.Disable(gl.DEPTH_TEST)
	gl.Color3f(1.0, 1.0, 1.0) // important

	var z float32 = -1.0

	gl.MatrixMode(gl.MODELVIEW)
	gl.PushMatrix()
	gl.LoadIdentity()
	gl.MatrixMode(gl.PROJECTION)
	gl.PushMatrix()
	gl.LoadIdentity()
	gl.Begin(gl.QUADS)
	gl.TexCoord2i(0, 0)
	gl.Vertex3f(-1, -1, z)
	gl.TexCoord2i(1, 0)
	gl.Vertex3f(1, -1, z)
	gl.TexCoord2i(1, 1)
	gl.Vertex3f(1, 1, z)
	gl.TexCoord2i(0, 1)
	gl.Vertex3f(-1, 1, z)
	gl.End()
	gl.PopMatrix()
	gl.MatrixMode(gl.MODELVIEW)
	gl.PopMatrix()

	gl.PopAttrib()
}

func (r *Renderer) renderQuad(x, y, w, h, z float64, flipY bool) {
	if Debug {
		if w > 1.0 || h > 1.0 {
			panic("w or h is too big (must be <= 1.0)")
		}
	}

	var _ = `
	// Prepare
	gl.PushAttrib(gl.CURRENT_BIT | gl.TEXTURE_BIT | gl.DEPTH_TEST)
	//gl.Disable(gl.DEPTH_TEST)
	gl.DepthFunc(gl.LEQUAL)
	gl.Color3f(1, 1, 1) // important

	gl.MatrixMode(gl.PROJECTION)
	gl.PushMatrix()
	gl.LoadIdentity()
	if !flipY {
		gl.Ortho(0, 1, 0, 1, -1, 1) // normal coords
	} else {
		// XXX doesn't work; flipping coords in vertices instead
		//gl.Disable(gl.CULL_FACE)
		gl.Ortho(0, 1, 0, 1, -1, 1) // normal coords
		//gl.Viewport(0, 0, 1, 1)
		//gl.Scalef( 1, -1, 1 )
		//gl.Ortho(0, 1, 1, 0, -1, 1) // Y-flipped
	}
	gl.MatrixMode(gl.MODELVIEW)
	gl.PushMatrix()
	gl.LoadIdentity()

	//gl.Translatef(0, 0, float32(z)) // ok
	//gl.Translatef(0, float32(z), 0) // xyz upd: check [bad]

	gl.Begin(gl.QUADS)
	/*
	  gl.TexCoord2f(0, 0); gl.Vertex2f(0, 0)
	  gl.TexCoord2f(0, 1); gl.Vertex2f(0, 1)
	  gl.TexCoord2f(1, 1); gl.Vertex2f(1, 1)
	  gl.TexCoord2f(1, 0); gl.Vertex2f(1, 0)
	*/
	/*
	  gl.TexCoord2f(0, 0); gl.Vertex2f(0, 0)
	  gl.TexCoord2f(1, 0); gl.Vertex2f(1, 0)
	  gl.TexCoord2f(1, 1); gl.Vertex2f(1, 1)
	  gl.TexCoord2f(0, 1); gl.Vertex2f(0, 1)
	*/
	/*
		// good
		gl.TexCoord2f(0, 0)
		gl.Vertex2f(float32(x), float32(y))
		gl.TexCoord2f(1, 0)
		gl.Vertex2f(float32(x+w), float32(y))
		gl.TexCoord2f(1, 1)
		gl.Vertex2f(float32(x+w), float32(y+h))
		gl.TexCoord2f(0, 1)
		gl.Vertex2f(float32(x), float32(y+h))
	*/
	// good + z
	if !flipY {
		gl.TexCoord2f(0, 0)
	} else {
		gl.TexCoord2f(0, 1)
	}
	gl.Vertex3f(float32(x), float32(y), float32(z))
	if !flipY {
		gl.TexCoord2f(1, 0)
	} else {
		gl.TexCoord2f(1, 1)
	}
	gl.Vertex3f(float32(x+w), float32(y), float32(z))
	if !flipY {
		gl.TexCoord2f(1, 1)
	} else {
		gl.TexCoord2f(1, 0)
	}
	gl.Vertex3f(float32(x+w), float32(y+h), float32(z))
	if !flipY {
		gl.TexCoord2f(0, 1)
	} else {
		gl.TexCoord2f(0, 0)
	}
	gl.Vertex3f(float32(x), float32(y+h), float32(z))
	/*
	  gl.TexCoord2d(0, 0); gl.Vertex2d(x, y)
	  gl.TexCoord2d(1, 0); gl.Vertex2d(x, x+w)
	  gl.TexCoord2d(1, 1); gl.Vertex2d(h, y+h)
	  gl.TexCoord2d(0, 1); gl.Vertex2d(h, x+h)
	*/
	gl.End()
	`
	//DrawFsQuad(r.fsQuad) // need more fixes/tests

	/*
		// Restore
		gl.MatrixMode(gl.PROJECTION)
		gl.PopMatrix()
		gl.MatrixMode(gl.MODELVIEW)
		gl.PopMatrix()

		gl.PopAttrib()
	*/
}

// fixme
func (r *Renderer) RenderQuad(x, y, w, h, z float64) {
	r.renderQuad(x, y, w, h, z, false)
}

func (r *Renderer) RenderQuadPx(x, y, w, h int, z float64) {
	pw := r.GetPw()
	ph := r.GetPh()
	r.renderQuad(float64(x)*pw, float64(y)*ph, float64(w)*pw, float64(h)*ph, z, false)
}

func (r *Renderer) RenderQuadFlipY(x, y, w, h, z float64) {
	r.renderQuad(x, y, w, h, z, true)
}

func (r *Renderer) RenderScene() {

	/*
	   root_matrix := Matrix4(
	       1, 0, 0, 0,
	       0, 0, -1, 0,
	       0, 1, 0, 0,
	       0, 0, 0, 1) // Mat4ZToYUp
	   _ = root_matrix
	*/

	/*
	  mesh_matrix := Mat4FromAxisAngle(Vec3XUnit, Radians(90), CoordSysYUpLeft)
	  m := mesh_matrix
	  mf1 := [16]float64{m[0][0], m[0][1], m[0][2], m[0][3],
	                      m[1][0], m[1][1], m[1][2], m[1][3],
	                      m[2][0], m[2][1], m[2][2], m[2][3],
	                      m[3][0], m[3][1], m[3][2], m[3][3]}
	  _ = mf1
	*/

	// fixme: optimize?
	var meshes []*Node
	if false {
		//meshes = rxi.scene.meshes
	} else {
		/*
			meshes = make([]*Node, 0)
			for _, n := range rxi.scene.GetNodes() {
				if n.Mesh != nil {
					meshes = append(meshes, n)
				}
			}
		*/
		if r.activeNode == nil {
			meshes = rxi.scene.GetMeshNodes()
		} else {
			meshes = make([]*Node, 0)
			meshes = append(meshes, r.activeNode)
		}
	}

	sorted_meshes := make([]*Node, len(meshes))
	sorted_meshes = meshes
	/*
		slice.Sort(sorted_meshes, func(i, j int) bool {
			m1 := sorted_meshes[i].Mesh.Material()
			m2 := sorted_meshes[j].Mesh.Material()
			return btoi(m1.Alpha) < btoi(m2.Alpha)

		})
	*/
	sort.Slice(sorted_meshes, func(i, j int) bool {
		m1 := sorted_meshes[i].Mesh.Material()
		m2 := sorted_meshes[j].Mesh.Material()
		return btoi(m1.Alpha) < btoi(m2.Alpha)

	})
	if false {
		i := len(sorted_meshes) - 1
		sorted_meshes[i], sorted_meshes[i-1] = sorted_meshes[i-1], sorted_meshes[i]
	}
	//pp(sorted_meshes)

	var prevMat *Material
	_ = prevMat

	enableCulling := Conf.EnableCulling
	var cullAABB AABB
	if enableCulling {
		curCam := *r.curCamRef
		cullAABB = curCam.Camera.GetCullAABB()
	}

	// render meshes
	//for i, mn := range meshes {
	for i, mn := range sorted_meshes {
		_ = i

		// Skip invisible nodes
		if !mn.Visible() {
			continue
		}

		if enableCulling && mn.culling {
			aabb := mn.Mesh.GetAABB()
			//if !r.curCam.Camera.cullAABB.ContainsBox(mn.Mesh.GetAABB()) {
			//if !r.curCam.Camera.cullAABB.ContainsBox(mn.Mesh.aabb) {
			//if !r.curCam.Camera.cullAABB.ContainsPoint(mn.Mesh.aabb.max) {
			//aabb := mn.Mesh.GetAABB()
			//if !cullAABB.ContainsPoint(aabb.Center()) {
			if !cullAABB.ContainsBox(aabb) {
				continue
			}
		}

		var _ = `
		gl.PushMatrix()

		//m := mn.Mat4()
		m := mn.FinalTransform()

		//m = Mat4Identity.Translate(10, 10, 0).Mul(m)
		//tm := Mat4Identity.Translate(10, 10, 0)
		//m = m.Mul(tm)

		/*
		   m = m.Mul(mesh_matrix) // additional vertex transform for xyz models
		                          // (apply model matrix)
		*/

		/*
			mf := []float32{
				float32(m[0][0]), float32(m[0][1]), float32(m[0][2]), float32(m[0][3]),
				float32(m[1][0]), float32(m[1][1]), float32(m[1][2]), float32(m[1][3]),
				float32(m[2][0]), float32(m[2][1]), float32(m[2][2]), float32(m[2][3]),
				float32(m[3][0]), float32(m[3][1]), float32(m[3][2]), float32(m[3][3])}
		*/

		/*
			// row-major(?)
			mf := [16]float32{
				float32(m[0][0]), float32(m[0][1]), float32(m[0][2]), float32(m[0][3]),
				float32(m[1][0]), float32(m[1][1]), float32(m[1][2]), float32(m[1][3]),
				float32(m[2][0]), float32(m[2][1]), float32(m[2][2]), float32(m[2][3]),
				float32(m[3][0]), float32(m[3][1]), float32(m[3][2]), float32(m[3][3])}

		*/

		d := m.Data()
		// convert to float32
		/*
			var mf [16]float32
			for i := 0; i < 16; i++ {
				mf[i] = float32(d[i])
			}
		*/

		/*
			mf := [16]float32{
				float32(m[0][0]), float32(m[0][0]), float32(m[2][0]), float32(m[3][0]),
				float32(m[1][0]), float32(m[1][1]), float32(m[1][1]), float32(m[1][1]),
				float32(m[2][0]), float32(m[2][1]), float32(m[2][2]), float32(m[2][3]),
				float32(m[3][0]), float32(m[3][1]), float32(m[3][2]), float32(m[3][3])}
		*/

		/*
			// XXX Fixme: Not Tested
			mf := []float32{float32(m[0]), float32(m[1]), float32(m[2]), float32(m[3]),
				float32(m[4]), float32(m[5]), float32(m[6]), float32(m[7]),
				float32(m[8]), float32(m[9]), float32(m[10]), float32(m[11]),
				float32(m[12]), float32(m[13]), float32(m[14]), float32(m[15])}
			p(mn.Mat())
		*/
		//p(m)
		//p("mf:", mf, len(mf))
		//p(m.String())
		//pp(mf)

		/*
		   mf := [16]float64{1, 0, 0, 0.5,
		                     0, 1, 0, 0.5,
		                     0, 0, 1, 0,
		                     0, 0, 0, 1}
		   mf = [16]float64{1, 0, 0, 0,
		                    0, 1, 0, 0,
		                    0, 0, 1, 0,
		                    0.5, 0.5, 0, 1}
		*/_ = m

		//gl.MultMatrixd(mn.Matrix)
		//gl.MultMatrixd(&mf1)

		//p := gl.Ptr(mf)
		//gl.MultMatrixd(p)
		//gl.MultMatrixd(&mf)
		//gl.MultMatrixf(&mf[0])
		gl.MultMatrixd(&d[0]) // float64
		//gl.MultTransposeMatrixf(&mf[0]) // ?? for testing only
		//_ = mf

		// fix xyz -> xzy for models
		//gl.MultMatrixd(&mf1)
		//gl.Rotated(90, 1, 0, 0)
		//gl.Scaled(1, 1, -1)

		// ?? fixme?
		// Blender Collada / ColladaLoader's ZY -> YZ compensation?
		// ---
		// another solution: rotate -90X and apply rotation in blender, then this can be commented out.
		//gl.Rotatef(270, 1, 0, 0)
		//gl.Rotatef(-90, 1, 0, 0)
		// fixme: can swap z y coords in collada loader instead
		`

		// Set material
		mat := mn.Mesh.material
		skipMat := mat == prevMat
		skipMat = false
		/*
			if i < 500 {
				skipMat = true
			}
		*/
		if skipMat {
			//pp("skipMat", mat, prevMat, i)
		}
		if mat != nil && !skipMat {
			r.setMaterial(mat)
		}

		glcheck()

		// Calculate shader values
		matModel := mn.FinalTransform()
		matMVP := r.matProjection.Mul(r.matView).Mul(matModel)
		//matMVP := matModel.Mul(r.matView).Mul(r.matProjection)
		// XXX not using proper mat3 for simplicity (mat4 instead)
		mat3x3InvTransp := matModel.Upper3().Invert().Transpose()

		//pp(matMVP)

		mat.shader.PassUniformMatrix4f("m", matModel)
		//glcheck()
		mat.shader.PassUniformMatrix4f("mvp", matMVP)
		mat.shader.PassUniformMatrix4f("m_3x3_inv_transp", mat3x3InvTransp)

		// Render mesh data
		{
			//gl.PushMatrix()

			//m := mn.FinalTransform()
			//d := m.Data()
			//gl.MultMatrixd(&d[0]) // float64

			r.renderMesh(mn.Mesh)

			//gl.PopMatrix()
		}

		// Unset material
		if mat != nil && !skipMat {
			r.unsetMaterial(mat)
		}
		//prevMat = mat
	}
	/*
	  // clear last shader [fixme]
	  if r.lastShader != nil {
	    r.lastShader.Unbind()
	    r.lastShader = nil
	  }
	*/
}

// for capability
func (r *Renderer) RenderRenderables() {
	r.renderRenderables(RenderLayer_Default)
}

func (r *Renderer) renderRenderables(rl RenderLayer) {
	for rn := range rxi.Scene().renderables {
		n := rn.GetNode()
		// render rl's renderables
		if n.renderlayer == rl {
			rn.Render(r)
		}
	}
}

func (r *Renderer) SetPostRenderCB(cb func(r *Renderer)) {
	r.postRenderCB = cb
}

func (r *Renderer) AddGuiRP(rp RenderPassI) {
	r.guiRP = rp
}

func (r *Renderer) RenderGuiRP() {
	if r.guiRP == nil {
		return
	}
	// Reset camera matrix
	/*
		gl.MatrixMode(gl.PROJECTION)
		gl.LoadIdentity()
		gl.MatrixMode(gl.MODELVIEW)
		gl.LoadIdentity()
	*/

	r.Set2DMode()
	r.guiRP.Render(r)
	r.Unset2DMode()
}

func (r *Renderer) PreRender() {
}

func (r *Renderer) RenderAll() {
	r.RenderScene()
	//r.RenderRenderables() // RLDefault
	r.RenderRenderables() // RLDefault
	r.debugRenderer.Render(r)
	r.RenderGuiRP() // fixme?
}

func (r *Renderer) PostRender() {
	//r.RenderDebug() // doesn't work properly here at the moment, fixme: test with renderpasses
	//r.RenderOverlays() // Gui, Hud etc(?)

	// render debug render layer
	r.renderRenderables(RenderLayer_Debug)

	// do debugrender stuff
	r.debugRenderer.PostRender(r)

	if r.postRenderCB != nil {
		r.postRenderCB(r)
	}
}

func (r *Renderer) AddRenderPass(rp RenderPassI) {
	if r.renderPasses.Contains(rp) {
		pp("renderpass already added")
	}
	r.renderPasses.Add(rp)
}

func (r *Renderer) RemoveRenderPass(rp RenderPassI) {
	pp("not implemented")
	//delete(r.renderPasses, rp)
}

func (r *Renderer) SetDefaultRenderPass(v bool) {
	r.defaultRenderPass = v
}

// This function must be called after glfw windows' flip (update) call.
// Used for possible debug drawing in update code.
func (r *Renderer) PostWindowFlip() {
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
}

func (r *Renderer) CalculateMVP(m, v, p Mat4) Mat4 {
	mvp := p.Mul(v).Mul(m)
	return mvp
}

// Calculate mvp matrix for model matrix.
func (r *Renderer) CalculateMVPModel(m Mat4) Mat4 {
	mvp := r.matProjection.Mul(r.matView).Mul(m)
	return mvp
}

func (r *Renderer) Render(cam_ *Node) {

	cam := cam_.Camera

	// Unset 2D mode, if forgotten
	r.Unset2DMode()

	// bind framebuffer
	if cam.HasRT() {
		cam.rt.Bind()
	}

	// XXX cleaning in PostWindowFlip instead
	//gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	//gl.BindTexture(gl.TEXTURE_2D, 0)
	//gl.ActiveTexture(gl.TEXTURE0)
	/*
		wireframe := false
		//twosided = true
		switch wireframe {
		case true:
			gl.PolygonMode(gl.FRONT_AND_BACK, gl.LINE)
		case false:
			gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
		}
	*/
	//glDisable(GL_CULL_FACE) if twosided else glEnable(GL_CULL_FACE)

	/*
	  gl.Enable(gl.CULL_FACE)
	  gl.CullFace(gl.BACK)
	*/

	// Reset matrix
	gl.MatrixMode(gl.MODELVIEW)
	gl.LoadIdentity()
	gl.MatrixMode(gl.PROJECTION)
	gl.LoadIdentity()

	r.SetCamera(cam_)

	// !!
	// make it left handed
	// https://www.opengl.org/wiki/Viewing_and_Transformations
	// #Can_I_make_OpenGL_use_a_left-handed_coordinate_space.3F
	//gl.Scalef(1., 1., -1.) // left-handed

	// extra?
	//gl.MatrixMode(gl.MODELVIEW)
	//gl.LoadIdentity()

	/*
	  gl.MatrixMode(gl.PROJECTION)
	  gl.LoadIdentity()

	  glu.Perspective(cam.fov * 180/math.Pi, cam.aspect, cam.znear, cam.zfar)
	  Println(cam.aspect)
	  //gl.Translated(0.1, 0.5, 0.7)

	  gl.MatrixMode(gl.MODELVIEW)
	  gl.LoadIdentity()
	*/

	// perform default render pass
	if r.defaultRenderPass {
		// reset color
		gl.Color3f(1.0, 1.0, 1.0)
		r.RenderAll()
	}

	//gl.Disable(gl.CULL_FACE)

	// render render passes
	it := r.renderPasses.Iterator()
	for it.Next() {
		rp := it.Value().(RenderPassI)
		//p(rp.Name())
		//pp(r.renderPasses)
		rp.Render(r)
	}
	//pp(2)

	// unbind framebuffer
	if cam.HasRT() {
		cam.rt.Unbind()
	}

	// check
	glcheck()

	// unbind
	gl.BindFramebuffer(gl.FRAMEBUFFER, 0)
	gl.BindTexture(gl.TEXTURE_2D, 0)

	// do post-render job
	r.PostRender()
	r.frame += 1
}
