// A Debug renderer for main renderer.
//
package rx

import "github.com/go-gl/gl/v2.1/gl"
import . "bitbucket.org/coaljoe/rx/math"

type DebugRenderer struct {
	r                 *Renderer
	ShowBoundingBoxes bool
	ShowLinks         bool
	ShowLights        bool
	ShowAxes          bool
	ShowGrid          bool
	ShowElems         bool
	ShowDebugDraw     bool
	GridSize          float64
	GridCellSize      float64
	elems             []*Node
	debugDrawRt       *RenderTarget // An Rt for debug.go calls
	enabled           bool
}

func newDebugRenderer(r *Renderer) *DebugRenderer {
	dr := &DebugRenderer{r: r,
		ShowBoundingBoxes: true,
		ShowLinks:         true,
		ShowLights:        true,
		ShowAxes:          true,
		ShowGrid:          true,
		ShowElems:         true,
		ShowDebugDraw:     true,
		GridSize:          50,
		GridCellSize:      1,
		enabled:           true}
	return dr
}

func (dr *DebugRenderer) Init(w, h int) {
	dr.debugDrawRt = NewRenderTarget("rx_DebugDrawRt", w, h, false)
}

func (dr *DebugRenderer) Render(r *Renderer) {
	if !Conf.debug_draw {
		return
	}

	if !dr.enabled {
		return
	}

	if dr.ShowGrid {
		draw_grid(dr.GridSize, dr.GridCellSize)
	}

	if dr.ShowAxes {
		draw_axes()

		// test x
		gl.PushMatrix()
		gl.Translatef(2, 0, 0)
		gl.Scalef(0.5, 0.5, 0.5)
		draw_axes()
		gl.PopMatrix()

		// test z
		gl.PushMatrix()
		gl.Translatef(0, 0, 2)
		gl.Scalef(0.5, 0.5, 0.5)
		draw_axes()
		gl.PopMatrix()

		// test y
		gl.PushMatrix()
		gl.Translatef(0, 2, 0)
		gl.Scalef(0.5, 0.5, 0.5)
		draw_axes()
		gl.PopMatrix()
	}

	if dr.ShowLinks {
		// render links
		color := Vec3{1, 0.5, 1}
		//color := Vec3{0, 0, 1}
		gl.Disable(gl.DEPTH_TEST)
		for _, mn := range rxi.scene.GetMeshNodes() {
			if p := mn.Transform.Parent(); p != nil {
				//p1 := p.Transform().Convert(gfx.LocalToWorld).Translation()
				//p1 := p.Transform().WorldMat().Translation()
				p1 := p.Transform().WorldPos()
				p2 := mn.WorldPos()
				draw_line_color(p1.X(), p1.Y(), p1.Z(), p2.X(), p2.Y(), p2.Z(),
					color.X(), color.Y(), color.Z())
				//Println(p1, p2)
				//panic(3)
			}
		}
		//draw_line(2, 0, 2, 2, 10, 2)
		gl.Enable(gl.DEPTH_TEST)
	}

	if dr.ShowLights {
		// render lights
		for _, ln := range rxi.scene.lights {
			//pp("derp")
			pos := ln.Pos()
			//gl.PushMatrix()
			//gl.Translatef(float32(pos.X()), float32(pos.Y()), float32(pos.Z()))
			drawer.SetAt(pos.X(), pos.Y(), pos.Z())
			//draw_axes()
			//gl.PushAttrib(gl.CURRENT_BIT)
			//gl.Color3f(0.8, 0.8, 0)
			//gl.Color3f(0.9, 0.9, 0.5)
			//gl.Color3f(1.0, 1.0, 1.0)
			drawer.SetColor(1.0, 1.0, 1.0)
			//gl.Color3f(0.8, 0.8, 0.8)
			/*
				r.SetWireframeMode(true)
				gl.Rotatef(float32(45), float32(1), float32(0), float32(0))
				gl.Rotatef(float32(45), float32(0), float32(0), float32(1))
			*/
			//DrawCube(0.5)
			//DrawCube(0.8)

			// TODO: add zoom-independant rendering
			drawer.DrawCube(1)
			//drawer.DrawCube(10)
			//drawer.DrawCube(100)

			//r.SetWireframeMode(false)
			//DrawBox(0.5, 0.5, 0.5)
			//gl.PopAttrib()
			//gl.PopMatrix()
		}
	}

	if dr.ShowBoundingBoxes {

		r.SetWireframeMode(true)
		gl.LineWidth(1.2) // fixme: save previous value

		// Render Bounding Boxes
		for _, mn := range rxi.scene.GetMeshNodes() {
			//pp("derp")
			bbox := mn.Mesh.GetAABB()
			if bbox.Empty() {
				continue
			}
			//pos := bbox.min
			//size := bbox.max.Sub(bbox.min).MulScalar(0.5)
			size := bbox.max.Sub(bbox.min)
			pos := bbox.min.Add(size.MulScalar(0.5))
			margin := 0.05
			_ = size
			if pos.IsNaN() {
				p("pos is NaN")
				maabb := mn.Mesh.aabb
				pp("name:", mn.name, "bbox:", bbox, "size:", size, "pos:", pos, "maabb:", maabb)
			}
			//gl.PushMatrix()
			//gl.Translatef(float32(pos.X()), float32(pos.Y()), float32(pos.Z()))
			drawer.SetAt(pos.X(), pos.Y(), pos.Z())
			//draw_axes()
			//gl.PushAttrib(gl.CURRENT_BIT)
			//gl.Color3f(0.6, 0.6, 0.2)
			drawer.SetColor(0.6, 0.6, 0.2)
			//DrawCube(0.5)
			drawer.DrawBox(size.X()+margin, size.Y()+margin, size.Z()+margin)
			//DrawBox(1, 1, 1)
			//gl.PopAttrib()
			//gl.PopMatrix()
		}
		dr.debugDrawRt.Bind()
		DebugDrawCube(Vec3Zero, 5, Vec3{1, 0, 0})
		dr.debugDrawRt.Unbind()

		r.SetWireframeMode(false)
		gl.LineWidth(1.0)
	}
}

func (dr *DebugRenderer) PostRender(r *Renderer) {
	//DebugDrawCube(Vec3Zero, 5, Vec3{1, 0, 0})
	/*
		dr.debugDrawRt.Bind()
		DebugDrawCube(Vec3Zero, 5, Vec3{1, 0, 0})
		dr.debugDrawRt.Unbind()
	*/

	//dr.debugDrawRt.fb.tex.Bind()
	//r.RenderFsQuad()
	//dr.debugDrawRt.fb.tex.Unbind()
}
