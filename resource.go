package rx

type Resource struct {
	id, path string
	data     interface{}
	rtype    ResourceType
	//loaded bool
}

func NewResource() *Resource {
	t := &Resource{
		rtype: ResourceTypeNone,
	}
	return t
}

/*
func (r *Resource) LoadFromFile(path string) {
	rr, err := os.Open(path)
	if err != nil {
		println("cant read file: " + path)
		log.Fatal(err)
	}
	defer rr.Close()

	r.path = path
	r.id = path

	ext := Ext(path)
	switch ext {
	case ".jpeg", ".jpg", ".png":
		r.rtype = ResourceTypeImage
		r.data = r.loadImage(rr, ext)
	case ".dds":
		//pass
	default:
		panic("unknown resource ext: " + ext)
	}
	// warn no auto adding to resourcesys
}
*/

func (r *Resource) Free() {
	if x, ok := r.data.(FreeableI); ok {
		Log.Dbg("freeing resource,",
			"\n id:", r.id, "\n path:", r.path, "\n type:", r.rtype.String())
		x.Free()
	}
}

/***** ResourceType *****/

type ResourceType int

const (
	ResourceTypeNone ResourceType = iota
	ResourceTypeImage
	ResourceTypeTexture
	ResourceTypeMesh
	ResourceTypeScene
	ResourceTypeShader
)

func (rt ResourceType) String() string {
	switch rt {
	case ResourceTypeNone:
		return "None"
	case ResourceTypeImage:
		return "Image"
	case ResourceTypeTexture:
		return "Texture"
	case ResourceTypeMesh:
		return "Mesh"
	case ResourceTypeScene:
		return "Scene"
	case ResourceTypeShader:
		return "Shader"
	default:
		panic("unknown resourcetype")
	}
}
